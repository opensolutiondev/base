using System;
using WestPA.Standalone;

namespace POS.Example
{
    class SessionContext
    {
        /// <summary>
        /// The current SessionContext. The context changes on every RunPreSession.
        /// </summary>
        public static SessionContext Current { get; private set; }

        /// <summary>
        /// The previous SessionContext.
        /// </summary>
        public static SessionContext Last { get; private set; }

        static SessionContext()
        {
            // Initialise Current and Last with dummy values.
            Current = Last = new SessionContext();
        }

        /// <summary>
        /// Create a new SessionContext. The current context becomes the last context.
        /// </summary>
        public static void New()
        {
            Last = Current;
            Current = new SessionContext
                      {
                          IsValid = true,
                      };
        }

        /// <summary>
        /// True if this is a valid context. All contexts except the initial ones
        /// created in the static constructor are valid.
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// This terminal's TSP ID
        /// </summary>
        public string TerminalId = String.Empty;

        /// <summary>
        /// This flag tells us whether this session has done something useful or not. Right now
        /// "something useful" is defined as one of these things:
        ///   - Opened the management menu
        ///   - Executed OnPreTransaction
        /// This is used to detect situations where there is a problem that causes OnPreSession
        /// to be called repeatedly without any way for the user to interact with the terminal.
        /// </summary>
        public bool HasDoneSomething = false;

        /// <summary>
        /// Contains the terminal status, full of useful and interesting information
        /// </summary>
        public TerminalStatus Terminal = null;
    }
}