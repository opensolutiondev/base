using WestPA.Standalone;

namespace OSHipPay
{
    /// <summary>
    /// The transaction context contains information regarding a single transaction.
    /// A new context is created on every PreTransaction()
    /// </summary>
    public class TransactionContext
    {
        static TransactionContext()
        {
            // Initialise fields with dummy values so that we avoid null pointer problems.
            Current = new TransactionContext();
            Last = new TransactionContext();
        }

        public static TransactionContext Current { get; private set; }

        public static TransactionContext Last { get; private set; }

        public TransactionContext()
        {
            Cashier = "";
            CashRegisterId = "";
            CashRegisterPaymentId = "";
        }
        /// <summary>
        /// Create a new transaction context. The context that was current becomes the last context.
        /// </summary>
        public static void New()
        {
            Last = Current;
            Current = new TransactionContext();
        }

        /// <summary>
        /// The TransactionData for the next transaction to execute.
        /// </summary>
        public TransactionData NextTransaction;

        /// <summary>
        /// The reference number of the transaction. Only valid if ReceiptData has been set.
        /// </summary>
        public string ReferenceNumber => ReceiptData?.ReferenceNumber;

        /// <summary>
        /// Holder for close batch data supplied by the payment application.
        /// This is only used when the transaction is a close batch transaction.
        /// If the batch was empty then this property will be null.
        /// </summary>
       // public BatchData BatchData { get; set; }

        /// <summary>
        /// Indicates if the merchant receipt has been printed or not.
        /// </summary>
        /// <remarks>
        /// When using signature verification the merchant and customer receipts are printed separately
        /// and we need to remember if the merchant receipt has been printed.
        /// </remarks>
        public bool IsMerchantReceiptPrinted;

        public string Cashier { get; set; }
        public string CashRegisterId { get; set; }
        public string CashRegisterPaymentId { get; set; }

        /// <summary>
        /// Holder for receipt data supplied by the payment application
        /// </summary>
        public ReceiptData ReceiptData;
    }
}