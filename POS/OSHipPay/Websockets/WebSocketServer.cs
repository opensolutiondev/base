﻿using OSHipPay.Configuration;
using System;
using System.Collections;
using System.Net;
using System.Threading;
using WebSockets;

namespace OSHipPay.WebSockets
{

    public class WebSocketServerBase
    {

        //private static byte[] _buffer = new byte[2048];
        //private static IDictionary<string, Socket> clients = new Dictionary<string, Socket>();

        //private static readonly IList socket_list = new ArrayList();
        private static WebSocketServer server ;

        public delegate void CallBackDelegate(WebSocket s, string str);
        private static CallBackDelegate _callBackFunction;
       //private static readonly string _guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

        private WebSocketServerBase()
        {

        }
        /*
        public bool ReplyMessage(String msg) {
            
            return false;
        }
        */

        public WebSocketServerBase(int portToBindTo, CallBackDelegate Callback)
        {
            License.GetIPAddress(out string ip);
            Logging.Log.Debug("Opening WebServerSocket " + ip + ":" + portToBindTo, new WebSocketServerBase());
            try
            {
                _callBackFunction = Callback;
                server = new WebSocketServer();
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ip), portToBindTo);
                server.Bind(endpoint);
                server.Connected += Server_Connected;
                server.StartAccept();
//                new Thread(() => server.AcceptAsync()).Start();
            } catch (Exception e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Server Socket)", e, new WebSocketServerBase());
                throw e;
            }
//            Logging.Log.Debug("Finished for WebServerSocket " + ip + ":" + portToBindTo, new WebSocketServerBase());

            /*
            try
            {
                License.GetIPAddress(out string ip);
               // ip = "192.168.11.161";
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ip), portToBindTo);
                Logging.Log.Debug("Opening WebServerSocket "+ip+":"+portToBindTo, new WebSocketServerBase());
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Bind(endpoint);
                _callBackFunction = Callback;
                //server.BeginAccept(null, 0, OnAccept, null);
                Listen_new(server);
                clients.Add("server_socket", server);
            }
            catch (Exception e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Server Socket)", e, new WebSocketServerBase());
                throw e;
            }
            */
        }

        private void Server_Connected(object sender, WebSocket e)
        {
            while (e.State == System.Net.WebSockets.WebSocketState.Open) { 
                WebSocketTextReceiveResult r = e.ReceiveTextAsync(CancellationToken.None).Result;
                _callBackFunction(e, r.Content);
            }
        }

        public void Close()
        {
            try
            {
                if (server != null)
                    server.Dispose();
                server = null;
            } catch (Exception e)
            {
                Logging.Log.Error("WebServerSocket Close Error Occured (Server Socket)", e, new WebSocketServerBase());
            }
        }

/*
        private static void OnAccept(IAsyncResult result)
        {
            byte[] buffer = new byte[1024];
            try
            {
                Socket client = null;
                string headerResponse = "";
                if (server != null && server.IsBound)
                {
                    client = server.EndAccept(result);
                    var i = client.Receive(buffer);
                    headerResponse = (System.Text.Encoding.UTF8.GetString(buffer)).Substring(0, i);
                    // write received data to the console
                    Console.WriteLine(headerResponse);

                }
                if (client != null)
                {
                    // Handshaking and managing ClientSocket 

                    var key = headerResponse.Replace("ey:", "`")
                              .Split('`')[1]                     // dGhlIHNhbXBsZSBub25jZQ== \r\n .......
                              .Replace("\r", "").Split('\n')[0]  // dGhlIHNhbXBsZSBub25jZQ==
                              .Trim();

                    // key should now equal dGhlIHNhbXBsZSBub25jZQ==
                    var test1 = AcceptKey(ref key);

                    var newLine = "\r\n";

                    var response = "HTTP/1.1 101 Switching Protocols" + newLine
                         + "Upgrade: websocket" + newLine
                         + "Connection: Upgrade" + newLine
                         + "Sec-WebSocket-Accept: " + test1 + newLine + newLine
                         //+ "Sec-WebSocket-Protocol: chat, superchat" + newLine
                         //+ "Sec-WebSocket-Version: 13" + newLine
                         ;

                    // which one should I use? none of them fires the onopen method
                    client.Send(System.Text.Encoding.UTF8.GetBytes(response));

                    var i = client.Receive(buffer); // wait for client to send a message

                    // once the message is received decode it in different formats
                    Console.WriteLine(Convert.ToBase64String(buffer).Substring(0, i));

                    //Console.WriteLine("\n\nPress enter to send data to client");
                    //Console.Read();

                    var subA = SubArray<byte>(buffer, 0, i);
                    client.Send(subA);
                    Thread.Sleep(10000);//wait for message to be send


                }
            }
            catch (Exception exception)
            {
                Logging.Log.Error("WebServerSocket accept Error Occured (Server Socket)", exception, new WebSocketServer());
                throw exception;
            }
            finally
            {
                if (server != null && server.IsBound)
                {
                    server.BeginAccept(null, 0, OnAccept, null);
                }
            }
        }

        public static T[] SubArray<T>(T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        private static string AcceptKey(ref string key)
        {
            string longKey = key + _guid;
            byte[] hashBytes = ComputeHash(longKey);
            return Convert.ToBase64String(hashBytes);
        }

        static SHA1 sha1 = SHA1CryptoServiceProvider.Create();
        private static byte[] ComputeHash(string str)
        {
            return sha1.ComputeHash(System.Text.Encoding.ASCII.GetBytes(str));
        }
*/
  /*      private static void Listen_new(Socket sock)
        {

            try
            {
                Logging.Log.Debug("WebServerSocket listening incoming connnection...", new WebSocketServerBase());
                sock.Listen(5);
                sock.BeginAccept(new AsyncCallback(Start_accept_new), sock);
            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Listen New)", e, new WebSocketServerBase());
            }
        }
        private static void Start_accept_new(IAsyncResult AR)
        {
            try
            {
                Logging.Log.Debug("WebServerSocket Accepting New Connection....", new WebSocketServerBase());
                Socket sock = ((Socket)AR.AsyncState).EndAccept(AR);
                Listen_new((Socket)AR.AsyncState);
                sock.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(Begin_handshake), sock);
            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Start Accept New)", e, new WebSocketServerBase());
            }

        }
        private static void Begin_handshake(IAsyncResult AR)
        {
            Logging.Log.Debug("WebServerSocket Shaking Hands....", new WebSocketServerBase());
            Socket sock = ((Socket)AR.AsyncState);
            int Data = sock.EndReceive(AR);
            byte[] databyte = new byte[Data];
            Array.Copy(_buffer, databyte, Data);

            String text = Encoding.ASCII.GetString(databyte);
            List<string> headers = Retriveheaders(text);
            Acceptuser(headers, sock);

        }
        private static void Acceptuser(List<string> headers, Socket sock)
        {

            ICollection<string> keys = (clients.Keys);
            IList user_list = new ArrayList();
            user_list = keys.ToList();

            if (user_list.Contains(headers[0]))
            {

                Logging.Log.Debug("WebServerSocket User Already Connected....", new WebSocketServerBase());
                bool check = clients.TryGetValue(headers[0], out Socket currentsock);


                Close_frame(currentsock);
                clients.Remove(headers[0]);


                string handshake;
                string Key = headers[4].Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
                SHA1 sha = new SHA1CryptoServiceProvider();
                clients.Add(headers[0], sock);
                byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(Key));
                handshake = Convert.ToBase64String(hash);
                string AcceptKey = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Protocol: " + headers[5].Trim() + "\r\nSec-WebSocket-Accept: " + handshake.Trim() + "\r\n\r\n";
                Send_handshake(AcceptKey, sock);
                Message_listener(sock);

            }
            else
            {

                string handshake;
                string Key = headers[4].Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
                SHA1 sha = new SHA1CryptoServiceProvider();
                clients.Add(headers[0], sock);
                byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(Key));
                handshake = Convert.ToBase64String(hash);
                string AcceptKey = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Protocol: " + headers[5].Trim() + "\r\nSec-WebSocket-Accept: " + handshake.Trim() + "\r\n\r\n";
                Send_handshake(AcceptKey, sock);
                Message_listener(sock);
            }

        }

        private static void Close_frame(Socket sock)
        {

            Byte[] frame = new Byte[2];
            frame[0] = (Byte)136;
            frame[1] = 0;
            try
            {
                sock.Send(frame);

            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Close Frame)", e, new WebSocketServerBase());
            }


        }
        private static void Message_listener(Socket insock)
        {
            start(insock);

            void start(Socket sock)
            {
                Logging.Log.Debug("WebServerSocket Looping Handle: "+sock.Handle, new WebSocketServerBase());
                byte[] m_buffer = new byte[5000];
                try
                {
                    sock.BeginReceive(m_buffer, 0, m_buffer.Length, SocketFlags.None, ar => { int dat = sock.EndReceive(ar); processdata(m_buffer, dat, ar); }, sock);

                }
                catch (Exception e)
                {
                    Logging.Log.Error("WebServerSocket Error Occured (Begin Receive)", e, new WebSocketServerBase());
                }


            }

            void processdata(byte[] r_buffer, int size, IAsyncResult AR)
            {

                Socket sockp = ((Socket)AR.AsyncState);
                Logging.Log.Debug("WebServerSocket Data Received: " + size, new WebSocketServerBase());
                Logging.Log.Debug("WebServerSocket IAsync Result: " + AR.AsyncState, new WebSocketServerBase());
                Logging.Log.Debug("WebServerSocket Opcode" + (r_buffer[0] & 81), new WebSocketServerBase());
                switch (r_buffer[0] & 81)
                {
                    case 1:
                        string s = Datadecode(r_buffer);
                        _callBack(sockp, s);
                        start(sockp);
                        break;
                    case 0:
                        if (size == 6)
                        {
                            string myKey = clients.FirstOrDefault(x => x.Value == sockp).Key;
                            if (myKey != null)
                            {
                                Close_frame(sockp);
                                sockp.Shutdown(SocketShutdown.Both);
                                sockp.Close();
                                sockp.Dispose();
                                clients.Remove(myKey);
                            }
                            else
                            {

                                sockp.Shutdown(SocketShutdown.Both);
                                sockp.Close();
                                sockp.Dispose();

                            }

                        }
                        else
                        {
                            sockp.Shutdown(SocketShutdown.Both);
                            sockp.Close();
                            sockp.Dispose();
                            string myKey = clients.FirstOrDefault(x => x.Value == sockp).Key;
                            clients.Remove(myKey);
                        }
                        break;
                    default:
                        Console.WriteLine("Default Switch");
                        break;

                }

            }

        }

        private static void _callBack(WebSocketServer sockp, string s)
        {
            _callBackFunction(sockp, s);
        }

        private static void Send_data(String data, Socket socket)
        {
            byte[] r_data = EncodeMessageToSend(data);
            try
            {
                Logging.Log.Debug("WebServerSocket Sending Handshake", new WebSocketServerBase());
                socket.BeginSend(r_data, 0, r_data.Length, SocketFlags.None, new AsyncCallback(Sent), socket);
            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Sending Data)", e, new WebSocketServerBase());
            }
        }


        private static void Send_handshake(String data, Socket socket)
        {
            byte[] send_key = Encoding.UTF8.GetBytes(data);
            try
            {
                Logging.Log.Debug("WebServerSocket Sending Handshake", new WebSocketServerBase());
                socket.BeginSend(send_key, 0, send_key.Length, SocketFlags.None, new AsyncCallback(Sent), socket);
            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Sending Handshake)", e, new WebSocketServerBase());
            }
        }

        private static void Sent(IAsyncResult AR)
        {
            Socket sock = (Socket)AR.AsyncState;
            try
            {
                sock.EndSend(AR);

            }
            catch (SocketException e)
            {
                Logging.Log.Error("WebServerSocket Error Occured (Set)", e, new WebSocketServerBase());
            }
        }


        private static List<string> Retriveheaders(String Data)
        {
            Logging.Log.Debug("WebServerSocket retrieving headers", new WebSocketServerBase());

            List<string> headers = new List<string>
            {
                Regex.Match(Data, "(?<=GET /)(.*)(?= HTTP)").ToString(),
                Regex.Match(Data, "(?<=Host: )(.*)[?=\r\n*]").ToString(),
                Regex.Match(Data, "(?<=Upgrade: )(.*)[?=\r\n]").ToString(),
                Regex.Match(Data, "(?<=Connection: )(.*)[?=\r\n]").ToString(),
                Regex.Match(Data, "(?<=Sec-WebSocket-Key: )(.*)[?=\r\n]").ToString(),
                Regex.Match(Data, "(?<=Sec-WebSocket-Protocol: )(.*)[?=\r\n]").ToString(),
                Regex.Match(Data, "(?<=Sec-WebSocket-Version: )(.*)[?=\r\n]").ToString(),
                Regex.Match(Data, "(?<=Origin: )(.*)\r\n").ToString(),
                Regex.Match(Data, "\r\n(.*)$").ToString()
            };

            return headers;

        }
        private static Byte[] EncodeMessageToSend(String message)
        {
            Byte[] response;
            Byte[] bytesRaw = Encoding.UTF8.GetBytes(message);
            Byte[] frame = new Byte[10];

            Int32 indexStartRawData = -1;
            Int32 length = bytesRaw.Length;

            frame[0] = (Byte)129;
            if (length <= 125)
            {
                frame[1] = (Byte)length;
                indexStartRawData = 2;
            }
            else if (length >= 126 && length <= 65535)
            {
                frame[1] = (Byte)126;
                frame[2] = (Byte)((length >> 8) & 255);
                frame[3] = (Byte)(length & 255);
                indexStartRawData = 4;
            }
            else
            {
                frame[1] = (Byte)127;
                frame[2] = (Byte)((length >> 56) & 255);
                frame[3] = (Byte)((length >> 48) & 255);
                frame[4] = (Byte)((length >> 40) & 255);
                frame[5] = (Byte)((length >> 32) & 255);
                frame[6] = (Byte)((length >> 24) & 255);
                frame[7] = (Byte)((length >> 16) & 255);
                frame[8] = (Byte)((length >> 8) & 255);
                frame[9] = (Byte)(length & 255);

                indexStartRawData = 10;
            }

            response = new Byte[indexStartRawData + length];

            Int32 i, reponseIdx = 0;

            //Add the frame bytes to the reponse
            for (i = 0; i < indexStartRawData; i++)
            {
                response[reponseIdx] = frame[i];
                reponseIdx++;
            }

            //Add the data bytes to the response
            for (i = 0; i < length; i++)
            {
                response[reponseIdx] = bytesRaw[i];
                reponseIdx++;
            }

            return response;
        }

        private static string Datadecode(byte[] rawdata)
        {
            Logging.Log.Debug("WebServerSocket message unmasking", new WebSocketServerBase());

            Thread.Sleep(10000);

            var fin = rawdata[0] & 0x81;

            bool res = fin != 129;
            Logging.Log.Debug("WebServerSocket Opcode: "+res, new WebSocketServerBase());
            var Lenght = rawdata[1] & 127;
            byte b = rawdata[1];
            int totalLength = 0;
            int keyIndex = 0;
            int dataLength = 0;


            if (Lenght <= 125)
            {

                keyIndex = 2;
                totalLength = Lenght + 6;
                dataLength = Lenght;

            }

            if (Lenght == 126)
            {

                dataLength = (int)BitConverter.ToUInt16(new byte[] { rawdata[3], rawdata[2] }, 0);
                keyIndex = 4;
                totalLength = dataLength + 8;

            }
            if (Lenght == 127)
            {

                dataLength = (int)BitConverter.ToInt64(new byte[] { rawdata[9], rawdata[8], rawdata[7], rawdata[6], rawdata[5], rawdata[4], rawdata[3], rawdata[2] }, 0);
                keyIndex = 10;
                totalLength = dataLength + 14;

            }

            byte[] key = new byte[] { rawdata[keyIndex], rawdata[keyIndex + 1], rawdata[keyIndex + 2], rawdata[keyIndex + 3] };

            int dataIndex = keyIndex + 4;
            int count = 0;


            for (int i = dataIndex; i < totalLength; i++)
            {
                rawdata[i] = (byte)(rawdata[i] ^ key[count % 4]);
                count++;
            }

            string message = Encoding.ASCII.GetString(rawdata, dataIndex, dataLength);
            Logging.Log.Debug("WebServerSocket message received "+message, new WebSocketServerBase());
            return message;
        }
     */   
    }
}