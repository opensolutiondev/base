﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OSHipPay.Logging.PersistentQueue
{
    public interface IPersistentQueue<T>
    {
        int Count { get; }
        void Enqueue(PersistentQueueItem<T> item);
        PersistentQueueItem<T> Dequeue(bool force);
        void Delete(PersistentQueueItem<T> item);
    }
}
