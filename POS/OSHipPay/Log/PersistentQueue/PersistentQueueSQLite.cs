﻿using System;
using System.IO;
using System.Xml.Serialization;
using OSHipPay.Core;

namespace OSHipPay.Logging.PersistentQueue
{
    public class PersistentQueueSQLite: IPersistentQueue<string>
    {
        private readonly string _fileName;
        private readonly string _connectionString;
        private readonly TimeSpan _defaultInvisibleTimeSpan = new TimeSpan(0, 0, 60);
        private readonly int _maxSize = 30;
        private readonly Connection _sqliteConnection;
        private int _currentQueueCount = int.MinValue;

        public PersistentQueueSQLite(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException("fileName");

            _fileName = fileName;
            _connectionString = "Data Source=\"" + fileName + "\";Version=3;";
            _sqliteConnection = new Connection(_connectionString, Initialise);
            _sqliteConnection.Open();
        }
        /*
        ~PersistentQueueSQLite()
        {
            _sqliteConnection.Close();
        }
        */
        public PersistentQueueSQLite(string fileName, int maxSize)
            : this(fileName)
        {
            _maxSize = maxSize;
        }

        public PersistentQueueSQLite(string fileName, int maxSize, TimeSpan invisibleTimeSpan)
            :this(fileName, maxSize)
        {
            _defaultInvisibleTimeSpan = invisibleTimeSpan;
        }

        public void Initialise()
        {
            CheckAndCreateSchema(false);
        }
        public void Initialise(bool recreateTable)
        {
            CheckAndCreateSchema(recreateTable);
        }

        #region IPersistentQueue members

        public int Count
        {
            get
            {
                if (_currentQueueCount != int.MinValue)
                    return _currentQueueCount;

                if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                    _sqliteConnection.Open();

                using (var command = new Command(CountStatement, _sqliteConnection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var tmp = reader.GetInt32(0);
                            _currentQueueCount = tmp;
                            return tmp;
                        }
                    }
                }

                return -1;
            }
        }
        public void Enqueue( PersistentQueueItem<string> item )
        {
            if (item == null)
                return;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            var count = Count;
            if (count == -1 || count >= _maxSize)
            {
                if (count > _maxSize)
                {
                    var numberToRemove = count - _maxSize;
                    using (var command = new Command(DeleteByNumberStatement, _sqliteConnection))
                    {
                        command.Parameters.AddWithValue("@number", numberToRemove);
                        command.ExecuteNonQuery();
                        _currentQueueCount -= count - _maxSize;
                    }
                }

                // TODO: how can we indicate this issue??
                return;
            }

            using (var command = new Command(EnqueueStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@id", item.Id.ToString());
                command.Parameters.AddWithValue("@invisibleUntil", item.InvisibleUntil);
                command.Parameters.AddWithValue("@dequeueCount", item.DequeueCount);
                command.Parameters.AddWithValue("@data", item.Data);
                command.ExecuteNonQuery();
                _currentQueueCount++;

            }
        }

        public PersistentQueueItem<string> Dequeue(bool force)
        {
          
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            PersistentQueueItem<string> result = null;

            using (var command = new Command(force ? DequeueForceStatement : DequeueStatement, _sqliteConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var id = (string)reader["id"];
                        var invisibleUntil = (DateTime)reader["invisibleUntil"];
                        var dequeueCount = (int)reader["dequeueCount"];
                        var data = (string)reader["data"];

                        result = new PersistentQueueItem<string>(data)
                        {
                            Id = new Guid(id),
                            InvisibleUntil = invisibleUntil,
                            DequeueCount = dequeueCount
                        };
                    }
                }
            }

            if (result != null)
            {
                result.InvisibleUntil = result.InvisibleUntil.Add(_defaultInvisibleTimeSpan);
                result.DequeueCount++;
                using (var command = new Command(UpdateItemStatement, _sqliteConnection))
                {
                    command.Parameters.AddWithValue("@id", result.Id.ToString());
                    command.Parameters.AddWithValue("@invisibleUntil", result.InvisibleUntil);
                    command.Parameters.AddWithValue("@dequeueCount", result.DequeueCount);
                    command.ExecuteNonQuery();
                }
            }

            //}

            return result;
    
        }

        public void Delete(PersistentQueueItem<string> item)
        {
            if (item == null)
                return;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(DeleteItemStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@id", item.Id.ToString());
                command.ExecuteNonQuery();
                _currentQueueCount--;
            }
        }

        #endregion

        // Important! Only for migrate from <=1.9.2 to >=1.9.3! This must be removed when  there are no terminals with OSPay < 1.9.3
        public void MigrateFrom(string oldStorage)
        {
            if (String.IsNullOrEmpty(oldStorage))
                return;

            if (!File.Exists(oldStorage))
                return;

            if (File.Exists(_fileName))
            {
                try
                {
                    File.Delete(oldStorage);
                }
                catch (Exception e)
                {
                    Log.Warning(
                        "Have both " + oldStorage + " and " + _fileName + ", but cant delete " + oldStorage +
                        " due to error: " + e.ToString(), this);
                }

                return;
            }

            try
            {
                PersistentQueueItem<string>[] oldItems;
                using (var stream = new StreamReader(oldStorage))
                {
                    oldItems = (PersistentQueueItem<string>[])(new XmlSerializer(typeof(PersistentQueueItem<string>[]))).Deserialize(stream);
                }

                if (oldItems != null)
                {
                    foreach (var batchData in oldItems)
                    {
                        Enqueue(batchData);
                    }
                }

                Log.Notice(oldItems.Length + " queue items was migrated from " + oldStorage + ". The file will be deleted.", this);
                
                File.Delete(oldStorage);
            }
            catch (Exception e)
            {
                Log.Error("Cant migrate queue items from " + oldStorage + ": ", e, this);
            }
        }

        private void CheckAndCreateSchema( bool recreateTable )
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            if (recreateTable)
            {
                using (var command = new Command(DeleteTableStatement, _sqliteConnection))
                {
                    command.ExecuteNonQuery();
                }
            }

            using (var command = new Command(CreateTableStatement, _sqliteConnection))
            {
                command.ExecuteNonQuery();
            }

            using (var command = new Command(CreateUpdateDateTimeIndexStatement, _sqliteConnection))
            {
                command.ExecuteNonQuery();
            }
        }

        #region Command statements

        private const string CreateTableStatement =
            "CREATE TABLE if not exists [queue] (" +
            "   [id] string primary key ASC," +
            "   [invisibleUntil] datetime," +
            "   [dequeueCount] int, " +
            "   [data] text );";

        private const string CreateUpdateDateTimeIndexStatement =
            "CREATE INDEX if not exists [invisibleUntil] " +
            "   on [queue] ([invisibleUntil]);";

        private const string DeleteTableStatement =
            "DROP TABLE if exists [queue];";

        private const string EnqueueStatement =
            "INSERT OR REPLACE into [queue] " +
            "   ([id], [invisibleUntil], [dequeueCount], [data])" +
            "   values (@id, @invisibleUntil, @dequeueCount, @data);";

        private const string DequeueStatement =
            "SELECT [id], [invisibleUntil], [dequeueCount], [data]" +
            "  from [queue]" +
            "  where [invisibleUntil] <= datetime()" +            
            "  order by [invisibleUntil] asc" +
            "  limit 1;";
        private const string DequeueForceStatement =
           "SELECT [id], [invisibleUntil], [dequeueCount], [data]" +
           "  from [queue]" +
           "  order by [invisibleUntil] asc" +
           "  limit 1;";

        private const string UpdateItemStatement =
            "UPDATE [queue]" +
            "   set [invisibleUntil]=@invisibleUntil, [dequeueCount]=@dequeueCount" +
            "   where [id]=@id;";

        private const string DeleteItemStatement =
            "DELETE from [queue]" +
            "   where [id]=@id;";

        private const string DeleteByNumberStatement =
            "DELETE from [queue]" +
            "  where [id] in" +
            "    (select [id] from [queue] order by [invisibleUntil] asc limit @number);";

        private const string CountStatement = 
            "SELECT count(*) as [count] from [queue]";

        #endregion
    }
}
