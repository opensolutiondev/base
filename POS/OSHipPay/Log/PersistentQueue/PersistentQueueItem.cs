﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace OSHipPay.Logging.PersistentQueue
{
    public class PersistentQueueItem<T>
    {
        public Guid Id { get; set; }
        public DateTime InvisibleUntil { get; set; }
        public int DequeueCount { get; set; }

        public T Data { get; set; }

        public PersistentQueueItem()
        {
            Id = Guid.NewGuid();
            InvisibleUntil = DateTime.Now;
            DequeueCount = 0;
        }

        public PersistentQueueItem(T data)
            : this()
        {
            Data = data;
        }
    }
}
