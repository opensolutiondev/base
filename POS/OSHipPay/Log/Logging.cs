﻿using Newtonsoft.Json;
using OSHipPay.Logging.PersistentQueue;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using WestPA.Standalone;

namespace OSHipPay.Logging
{
    public enum Severity
    {
        Emergency = 0,
        Alert = 1,
        Critical = 2,
        Error = 3,
        Warning = 4,
        Notice = 5,
        Informational = 6,
        Debug = 7,
        Spam = 8,
        Insane = 9,
        Impractical = 10,
        Brainfuck = 11
    };

    public enum LogResult { Sent, NotSent, LogLevelTooLow, NotWaiting };


    public class LogEntry
    {
        public Severity Severity;
        public string LogMessage;
    }


    public class Log
    {
        private static string _publicIpAddress;
        static readonly object Lock = new object();

        static Guid myAgentGuid = Guid.Parse("001b2908-fb28-465c-a98e-724b1f4130b8");
        static Serilog.Core.Logger SeriLogger = null;
        public static int LastLogEntries = 100;
        private static List<LogEntry> entries = new List<LogEntry>();
        private static string LogEntriesFileName;
        private static string LogPath;

        public static Uri DEFAULT_LOG_URI = new Uri("http://94.246.97.170/license/?method=TerminalLog");
        public static string DEFAULT_LICENSE_HOST = "http://94.246.97.170/license/";
        public static Uri LOG_URI = DEFAULT_LOG_URI;
        public static string LICENSE_HOST = DEFAULT_LICENSE_HOST;
        public static string License = String.Empty;
        public static string Version = String.Empty;
        public static string TerminalId = String.Empty;
        public static string IncludeHostsInLog = String.Empty;
        public static bool IncludePublicIPInLog = false;
        public static int WSTimeout = 30000;

        public static IPersistentQueue<string> Queue;

        public static Severity LogLevel = Severity.Warning;

        public static ILogService Sender;
        static Settings Settings;

        public static void VSlog(string text, object category)
        {
            if (System.Diagnostics.Debugger.IsAttached &&
                !string.IsNullOrEmpty(text) &&
                category != null)
            {
                System.Diagnostics.Trace.WriteLine(text, category.GetType().ToString());
            }
        }
        public static string GetAllThreadsForLog() {
            StringBuilder stb = new StringBuilder();
            /*
            var result = new Dictionary<int, string[]>();

            var pid = Process.GetCurrentProcess().Id;
            using (var dataTarget = DataTarget.AttachToProcess(pid, 5000, AttachFlag.Passive))
            {
                string dacLocation = dataTarget.ClrVersions[0].TryGetDacLocation();
                var runtime = dataTarget.CreateRuntime(dacLocation);

                foreach (var t in runtime.Threads)
                {
                    result.Add(
                        t.ManagedThreadId,
                        t.StackTrace.Select(f =>
                        {
                            if (f.Method != null)
                            {
                                return f.Method.Type.Name + "." + f.Method.Name;
                            }

                            return null;
                        }).ToArray()
                    );
                }
            }
            foreach (ProcessThread thread in currentThreads)    
            {

                stb.Append("   " + thread.ToString() +"\r");
                var trace = new System.Diagnostics.StackTrace(thread, true);
                stb.Append(trace + "\r");
                // Do whatever you need
            }*/
            return stb.ToString();
        }
        #region levels
        public static bool Error(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Android.Util.Log.Error("OSHipPay", text);
            Send(Severity.Error, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Error(string text, Exception error, object category)
        {
            return Error(text + ". " + error.ToString()+" "+error.StackTrace, category);
        }

        public static bool Warning(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Android.Util.Log.Warn("OSHipPay", text);
            Send(Severity.Warning, category.GetType().ToString() + ": " + text);
            return false;
        }

        internal static void CloseAndFlush()
        {
            if (SeriLogger != null)
                SeriLogger.Dispose();
        }

        public static bool Alert(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Send(Severity.Alert, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Informational(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Android.Util.Log.Info("OSHipPay", text);
            Send(Severity.Informational, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Debug(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Android.Util.Log.Debug("OSHipPay", text);
            Send(Severity.Debug, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Notice(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Send(Severity.Notice, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Emergency(string text, object category)
        {
            if (!text.StartsWith("<"))
                VSlog(text, category);
            Send(Severity.Emergency, category.GetType().ToString() + ": " + text);
            return false;
        }

        public static bool Emergency(string text, Exception error, object category)
        {
            return Emergency(text + ". " + error.ToString(), category);
        }
        public static bool Spam(int text, object category)
        {
            return Spam(text.ToString(), category);
        }
        public static bool Spam(double text, object category)
        {
            return Spam(text.ToString(), category);
        }

        public static bool Spam(string text, object category)
        {
            if (LogResult.LogLevelTooLow != Send(Severity.Spam, category.GetType().ToString() + ": " + text, false))
                if (!text.StartsWith("<"))
                    VSlog(text, category);
            return false;
        }

        public static bool Insane(string text, object category)
        {
            if (LogResult.LogLevelTooLow != Send(Severity.Insane, category.GetType().ToString() + ": " + text, false))
                if (!text.StartsWith("<"))
                    VSlog(text, category);
            return false;
        }

        public static bool Impractical(string text, object category)
        {
            if (LogResult.LogLevelTooLow != Send(Severity.Impractical, category.GetType().ToString() + ": " + text, false))
                if (!text.StartsWith("<"))
                    VSlog(text, category);
            return false;
        }

        #endregion

        public static void Send(Severity severity, string message)
        {
            Send(severity, message, false);
        }

        [Serializable()]
        [XmlType(TypeName = "TerminalLog")]
        public class TerminalLog
        {
            private Severity _severity;
            private String _message;

            public TerminalLog() { }

            public TerminalLog(Severity Severity, String Message)
            {
                this.Severity = Severity;
                this.Message = Message;
            }

            public DateTime DateTime { get => DateTime.UtcNow; set => DateTime = value; }
            public string License { get => Log.License; set => License = value; }
            public string Version { get => Log.Version; set => Version = value; }
            public string Terminal { get => Log.TerminalId; set => Terminal = value; }
            public Severity Severity { get => _severity; set => _severity = value; }
            public string Message { get => (new System.Xml.XmlDocument()).CreateCDataSection(_message).OuterXml; set => _message = value; }
        }

        static void CreateLogger(Settings set)
        {
            try
            {
                if (set.UseLumberJack)
                    SeriLogger = new LoggerConfiguration()
                        .MinimumLevel.Verbose()
                        .Enrich.WithThreadId()
                        .Enrich.WithMachineName()
                        .Enrich.WithProperty(Constants.SourceContextPropertyName, "OSHipPay") 
                        .WriteTo.AndroidLog()
                        .WriteTo.Trace()
                        .WriteTo.EmbeddedAgent(myAgentGuid, OSHipPay.Configuration.License.GetAppName(), OSHipPay.Configuration.License.GetVersion(), TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(30), LogPath)
                        //.WriteTo.Console()
                        .CreateLogger();
            }
            catch (Exception e)
            {
                SeriLogger = null;
                Log.Error("Failed to initialize SeriLog", e, null);
            }

        }

        public static void Init(Settings set, SystemInfo sysinfo)
        {
            LogEntriesFileName = Path.Combine(sysinfo.PersistentDirectory, "LogEntries.json");
            LogPath = Path.Combine(sysinfo.PersistentDirectory, "OS.Agent.Embedded");
            if (!set.UseLumberJack)
                Sender = new OSLogService(set.LogTimeout);
            Settings = set;
            TerminalId = set.TerminalId;
            License = "e9978810ffd25fe8c86d73ea9191d465";
            if (set.LogLevel > 0)
            {
                LogLevel = (Severity)set.LogLevel;
            }
            if (set.License != null)
                License = set.License;
            UpdatePublicIpAddress();
            if (set.Guid.IsNotNullOrEmpty())
            {
                myAgentGuid = Guid.Parse(set.Guid);
            }
            CreateLogger(set);
        }

        public static void Init2(IPaymentApp app)
        {
            Version = OSHipPay.Configuration.License.GetAppName()+" "+ OSHipPay.Configuration.License.GetVersion()+" ("+app.GetType().Assembly.GetName().Version.ToString()+")";
            LoadEntriesFromFile(entries);
        }

        public static LogResult Send(Severity severity, string message, bool waitForTheResult)
        {
            if (String.IsNullOrEmpty(TerminalId))
                return LogResult.NotSent;
            if (severity > LogLevel)
                return LogResult.LogLevelTooLow;
            lock (Lock)
            {
                entries.Add(new LogEntry() { Severity = severity, LogMessage = message });
                while (entries.Count > LastLogEntries)
                {
                    entries.RemoveAt(0);
                }
                SaveEntriesToFile(entries);
            }
            try
            {
                StringBuilder messageToSend = new StringBuilder(message);
                if (!String.IsNullOrEmpty(_publicIpAddress) && IncludePublicIPInLog)
                    messageToSend.Append(" - " + _publicIpAddress);
                if (!String.IsNullOrEmpty(IncludeHostsInLog))
                    messageToSend.Append(" - " + IncludeHostsInLog);
                if (SeriLogger!=null)
                {
                    switch (severity)
                    {
                        case Severity.Insane:
                        case Severity.Spam:
                        case Severity.Brainfuck:
                        case Severity.Impractical:
                            Android.Util.Log.Verbose("OSHipPay", message);
                            SeriLogger.Verbose(message);
                            break;
                        case Severity.Debug:
                            Android.Util.Log.Debug("OSHipPay", message);
                            SeriLogger.Debug(message);
                            break;
                        case Severity.Warning:
                            Android.Util.Log.Warn("OSHipPay", message);
                            SeriLogger.Warning(message);
                            break;
                        case Severity.Critical:
                            Android.Util.Log.Wtf("OSHipPay", message);
                            SeriLogger.Fatal(message);
                            break;
                        case Severity.Notice:
                        case Severity.Informational:
                            Android.Util.Log.Info("OSHipPay", message);
                            SeriLogger.Information(message);
                            break;
                        default:
                            Android.Util.Log.Error("OSHipPay", message);
                            SeriLogger.Error(message);
                            break;
                    }
                    return LogResult.Sent;
                }
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TerminalLog));

                StringBuilder xmlSb = new StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Encoding = System.Text.Encoding.UTF8  //Doesn't seem to work....
                };
                XmlWriter write = XmlWriter.Create(xmlSb, settings);
                serializer.Serialize(write, new TerminalLog(severity, messageToSend.ToString()));
                if (waitForTheResult)
                {
                    if (PostXML(xmlSb, LOG_URI))
                        return LogResult.Sent;
                }
                else
                {
                    ThreadPool.QueueUserWorkItem((x) => PostXML(xmlSb, LOG_URI));
                }
            } catch (Exception e)
            {
                VSlog("Failed to send log " + e.Message, new Log());
            }
            return LogResult.NotWaiting;
        }

        private static void SaveEntriesToFile(List<LogEntry> entries)
        {
            try
            {
                StreamWriter outputFile = new StreamWriter(LogEntriesFileName);
                string s = JsonConvert.SerializeObject(entries);
                outputFile.WriteLine(s);
                outputFile.Close();
            } catch (Exception e)
            {
                VSlog("Failed to save logentries " + e.Message + " " + e.StackTrace, Severity.Error);
            }
        }

        private static void LoadEntriesFromFile(List<LogEntry> entries)
        {
            try
            {
                if (File.Exists(LogEntriesFileName))
                using (StreamReader r = new StreamReader(LogEntriesFileName))
                {
                    string json = r.ReadToEnd();
                    List<LogEntry> items = JsonConvert.DeserializeObject<List<LogEntry>>(json);
                    entries = items;
                }
            }
            catch (Exception e)
            {
                VSlog("Failed to load logentries " + e.Message + " " + e.StackTrace, Severity.Error);
                entries = new List<LogEntry>();
            }
        }


        /// <returns>True if sent online, False if enqueued to the queue.</returns>
        public static bool PostXML(StringBuilder xelement, Uri uri)
        {
            String sMsg = xelement.ToString().Replace("utf-16", "utf-8").Replace("&lt;", "<").Replace("&gt;", ">");
            if (Sender!=null)
                try
                {
                    VSlog("Sending: " + sMsg, new Log());
                    Sender.PostMessage(sMsg, uri);
                    return true;
                }
                catch (Exception e)
                {
                    VSlog("PostXml failed " + e.Message, new Log());
                }
            if (Queue != null)
                Queue.Enqueue(new PersistentQueueItem<string>(sMsg));
            xelement = null;
            return false;
        }
        /*

        [Serializable()]
        [XmlType(TypeName = "UnhandledException")]
        public class UnhandledException
        {
            public DateTime DateTime = DateTime.UtcNow;
            public String Licensekey = Log.License;
            public String Version = Log.Version;
            public String CashId = String.Empty;
            public String Type = String.Empty;
            public LogException Exception ;

            public UnhandledException() { }

            public UnhandledException(string Type, Exception e)
            {
                this.Exception = new LogException(Type, e, 0);
                this.Type = Type;
                this.CashId = "0";
            }

            [XmlType(TypeName = "Exception")]
            public class LogException
            {
                public String Source = String.Empty;
                public String Method = String.Empty;
                public String Type = String.Empty;
                public String Message = String.Empty;
                public String StackTrace = String.Empty;
                public LogException InnerException;
                public LogException() { }

                public LogException(String Type, Exception e, int level)
                {
                    this.Type = Type;
                    this.Message = e.Message;
                    this.StackTrace = e.StackTrace;
                    if (e.InnerException!=null)
                    this.InnerException = new LogException(Type, e.InnerException, ++level);
                }

            }
            public string ToSeriLogMessage()
            {
                return OSHipPay.WebSockets.JSONProcessor.ConvertToJson(this);
            }
    
        }
    
        */

        /*
        public static void ReportUnhandledException(Exception exception)
        {
            if (SeriLogger!=null)
            {
                try
                {
                    UnhandledException uhe = new UnhandledException(exception.GetType().Name, exception);
                    string message = uhe.ToSeriLogMessage();
                    Android.Util.Log.Wtf("OSHipPay", message);
                    SeriLogger.Fatal(message);
                    return;
                }
                catch { }
            }
            else
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TerminalLog));
                StringBuilder xmlSb = new StringBuilder();
                XmlWriter write = XmlWriter.Create(xmlSb);
                serializer.Serialize(write, new UnhandledException(exception.GetType().Name, exception));
                PostXML(xmlSb, new Uri(LICENSE_HOST));
            }
            catch { }
        }
        */
        public static void UpdatePublicIpAddress()
        {
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create("http://176.58.123.25");
                req.Timeout = 3000;
                using (System.Net.WebResponse resp = req.GetResponse())
                {
                    using (var reader = resp.GetResponseStream())
                    {
                        var buffer = new byte[256];
                        var read = reader.Read(buffer, 0, buffer.Length);
                        var str = Encoding.UTF8.GetString(buffer, 0, read).Trim();

                        // check if it is correct IP address
                        IPAddress.Parse(str);
                        _publicIpAddress = str;
                    }
                }
            }
            catch { _publicIpAddress = String.Empty; }
        }

        internal static void SetLogLevel(int logLevel)
        {
            if (LogLevel > 0)
                LogLevel = (Severity)logLevel;
        }

        internal static void SetGUID(string gUID)
        {
            myAgentGuid = Guid.Parse(gUID);
            if (SeriLogger != null)
            {
                SeriLogger.Dispose();
                CreateLogger(Settings);
            }
        }

        internal static string GetLastEntries()
        {
            StringBuilder stb = new StringBuilder();
            lock (Lock)
            {
                foreach (LogEntry entry in entries)
                {
                    stb.Append(entry.Severity).Append(": ");
                    stb.Append(entry.LogMessage);
                    stb.Append('\n');
                }
            }
            return stb.ToString();
        }

        internal static string GetLogCat()
        {
            System.Text.StringBuilder log = new StringBuilder();
            //IPaymentApp.ExtractLogFile....
            /*
            Java.Lang.Process process = Java.Lang.Runtime.GetRuntime().Exec("logcat -d "); // |grep -e 'OSHipPay' -e 'WestPA' didn't work for some reason...
            Java.IO.BufferedReader bufferedReader = new Java.IO.BufferedReader(
              new Java.IO.InputStreamReader(process.InputStream));

            string line = "";
            while ((line = bufferedReader.ReadLine()) != null)
            {
                String s = line.ToLower();
                if (!s.Contains("oshippay") && !s.Contains("westpa")) continue;
                
                log.Append(line).Append('\n');
            }
            */
            return log.ToString();
        }

        internal static void MailAsAttachment(string sendTo, StringBuilder stb)
        {
            
        }
    }

}
