﻿using System;

namespace OSHipPay.Logging
{
    public interface ILogService
    {
        void PostMessage(string message, Uri url);
    }
}
