﻿using System;
using System.Net;
using System.Text;

namespace OSHipPay.Logging
{
    public class OSLogService : ILogService
    {
        private readonly int _timeout;

        public OSLogService(int timeout)
        {
            _timeout = timeout;
        }

        public void PostMessage(string message, Uri uri)
        {
            if (String.IsNullOrEmpty(message))
                return;

            WebRequest webRequest = WebRequest.Create(uri);
            webRequest.Proxy = null;
            webRequest.Timeout = _timeout;
            webRequest.ContentType = "text/xml";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.GetEncoding("UTF-8").GetBytes(message);
            webRequest.ContentLength = bytes.Length;

            using (var requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
            }

            WebResponse webResponse = null;
            try
            {
                webResponse = webRequest.GetResponse();
            }
            finally
            {
                if (webResponse != null)
                    webResponse.Close();
            }
        }
    }
}