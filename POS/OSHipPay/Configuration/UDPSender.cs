﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace OSHipPay.Configuration
{
    public class UDP
    {
        static int port = 3334;

        public static void SetPort(int p)
        {
            port = p;
        }

        public static void Send(IPEndPoint ep, string message)
        {
            try
            {
                UdpClient client = new UdpClient();
                byte[] bytes = Encoding.UTF8.GetBytes(message);
                client.Send(bytes, bytes.Length, ep);
                client.Close();
            }
            catch (Exception e)
            {
                Logging.Log.Error("UDP send failed", e, new UDP());
            }
        }


        public static void Send(string message)
        {
            try
            {
                IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, port);
                Send(ip, message);
            } catch (Exception e)
            {
                Logging.Log.Error("UDP Broadcast failed", e, new UDP());
            }
        }

    }
}