﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using WestPA.Standalone;

namespace OSHipPay.Configuration
{
    public static class MerchantInfo
    {
        public static string Name { get; set; }
        public static string Address { get; set; }
        public static string ZipCode { get; set; }
        public static string City { get; set; }
        public static string PhoneNumber { get; set; }
        public static string OrganisationNumber { get; set; }
        public static string CustomerNumber { get; set; }
        public static string SecretKey { get; set; }
        public static string ApiKey { get; set; }
        public static string ControlUnit { get; set; }
        static public void Init(Settings set)
        {
            Name = "";
            Address = "";
            ZipCode = "";
            City = "";
            PhoneNumber = "";
            OrganisationNumber = "";
            CustomerNumber = "";
            SecretKey = "";
            ApiKey = "";
            ControlUnit = "";
            
        }

    }
    public class PA_AppSettings
    {
        private static string terminalId;
        private static string version = "0";

        //private static string currentCashier = "1";

        public static string SerialNumber { get; set; }

        private static int transactionDaysLimit = 2;
        public static int TransactionDaysLimit { get { return transactionDaysLimit; } set { transactionDaysLimit = value; } }
        private static int transactionNumbersLimit = 1024;
        public static int TransactionNumbersLimit { get { return transactionNumbersLimit; } set { transactionNumbersLimit = value; } }

        public static string TerminalId { get { return terminalId; } set { Logging.Log.TerminalId = value; terminalId = value; } }
        public static IPEndPoint PPLHost { get; set; }
        public static IPEndPoint PPLHost2 { get; set; }
        public static IPEndPoint SPDHHost { get; set; }
        public static IPEndPoint SPDHHost2 { get; set; }
        public static IPEndPoint DCAPPHost { get; set; }

        public static IPEndPoint LicenseServer { get; set; }

        public static string Logo  { get; set; }
        public static String APN { get; internal set; }
        public static bool WifiOff { get; internal set; }
        public static IPEndPoint LogFileAddress { get; internal set; }

        public static string GetVersion()
        {
            return version;
        }

        static public void Init(Settings set, SystemInfo sysinfo)
        {
            try
            {
                terminalId = set.TerminalId;
            } catch (Exception e2)
            {
                Logging.Log.Error("Exception in terminalId " + terminalId, e2, new PayApp());
            }
            try
            {
                PPLHost = new IPEndPoint(IPAddress.Parse(set.PPLAddress), set.PPLPort);
            }
            catch (Exception e2)
            {
                Logging.Log.Error("Exception in PPLHost " + PPLHost, e2, new PayApp());
            }
            try
            {
                SPDHHost = new IPEndPoint(IPAddress.Parse(set.SPDHAddress), set.SPDHPort);
            }
            catch (Exception e2)
            {
                Logging.Log.Error("Exception in SPDHHost " + SPDHHost, e2, new PayApp());
            }
            SerialNumber = sysinfo.SerialNumber;
            version = sysinfo.PaymentAppVersion;
            if (set.Logo != null)
                Logo = set.Logo;
            else Logo = "hippo";
            if (set.TransactionDaysLimit > 0)
            {
                transactionDaysLimit = set.TransactionDaysLimit;
            }
            if (set.TransactionNumbersLimit > 0)
            {
                transactionDaysLimit = set.TransactionNumbersLimit;
            }

            APN = set.APN;
            WifiOff = set.WifiSSID == null || set.WifiPassword == null;
            if (set.LogFileEndPoint!=null)
               LogFileAddress =  new IPEndPoint(IPAddress.Parse(set.LogFileEndPoint), 21);

    }

}
    /*
    public class AppSettings
    {
        public static event EventHandler<AddressUpdatedEventArgs> AddressUpdated = delegate { };
        private static string terminalId;
        private static string currentCashier = "1";
        public static string CurrentCashier { get { return currentCashier; } set { currentCashier = value; } }
        public static int? Port { get; set; }
        private static int paymentTimeout = DefaultAppSettings.PaymentTimeout;
        public static int PaymentTimeout { get { return paymentTimeout; } set { paymentTimeout = value; } }
        public static CurrencyCode DefaultCurrency { get; set; }
        public static int PresessionRetries { get; set; }
        public static string Terminal { get { return terminalId; } set { Logging.Log.Terminal = value; terminalId = value; } }
        public static IPEndPoint PPLHost { get; set; }
        public static IPEndPoint PPLHost2 { get; set; }
        public static IPEndPoint SPDHHost { get; set; }
        public static IPEndPoint SPDHHost2 { get; set; }
        public static IPEndPoint DCAPPHost { get; set; }
        public static List<ApplicationModes> ApplicationMode { get; set; }
        public static int Baudrate { get; set; }
        public static string APN { get; set; }
        public static string WifiSSID { get; set; }
        public static string WifiPassword { get; set; }
        public static string RevertApplication { get; set; }
        public static string Logo { get; set; }
        public static string SerialNumber { get; set; }
        public static DEBUG DEBUG { get; set; }
        public static Dictionary<string, string> ExtraHTTPHeaders { get; set; }
        private static string licenseAddress = DefaultAppSettings.LicenseAddress;
        private static string fileAddress = DefaultAppSettings.FileAddress;
        private static string launcherAddress = DefaultAppSettings.LauncherAddress;
        private static string logAddress = DefaultAppSettings.LogAddress;
        private static Uri mobileApiAddress = DefaultAppSettings.MobileApiAddress;
        private static Uri apiAddress = DefaultAppSettings.ApiAddress;
        private static IPEndPoint logFileAddress = DefaultAppSettings.LogFileEndPoint;
        public static string LicenseAddress { get { return licenseAddress; } set { licenseAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("LicenseAddress", value)); } }
        public static string FileAddress { get { return fileAddress; } set { fileAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("FileAddress", value)); } }
        public static string LauncherAddress { get { return launcherAddress; } set { launcherAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("LauncherAddress", value)); } }
        public static string LogAddress { get { return logAddress; } set { logAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("LogAddress", value)); } }
        public static Uri MobileApiAddress { get { return mobileApiAddress; } set { mobileApiAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("MobileApiAddress", value.ToString())); } }
        public static Uri ApiAddress { get { return apiAddress; } set { apiAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("ApiAddress", value.ToString())); } }
        public static bool SendLogFile { get; set; }
        public static IPEndPoint LogFileAddress { get { return logFileAddress; } set { logFileAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("LogFileAddress", value.ToString())); } }
        public static WestPA.Standalone.TerminalStatus.LoggingLevels? PALogLevel { get; set; }
        public static bool IncludeHostsInLog { set { if (value) Logging.Log.IncludeHostsInLog = "S:" + AppSettings.SPDHHost + " P:" + AppSettings.PPLHost; else Logging.Log.IncludeHostsInLog = String.Empty; } }
        public static bool IncludePublicIPInLog { set { if (value) Logging.Log.IncludePublicIPInLog = true; else Logging.Log.IncludePublicIPInLog = false; } }
        public static ushort OfflineLimit { get; set; }
        private static int wsTimeout = DefaultAppSettings.WSTimeout;
        public static int WSTimeout { get { return wsTimeout; } set { wsTimeout = value; } }
        public static bool WifiOff { get; set; }
        public static bool ApnOff { get; set; }
        public static int OutcomeDisplayTime { get; set; }
        public static int SignatureButtonActivationTime { get; set; }
        private static int batchDataListSize = DefaultAppSettings.BatchDataListSize;
        public static int BatchDataListSize { get { return batchDataListSize; } set { batchDataListSize = value; } }
        public static bool AskCodeOnIncorrectEndOfDay { get; set; }
        public static string TransactionReportsAddress { get; set; }
        public static bool SwipeAhead { get; set; }
        private static int transactionDaysLimit = DefaultAppSettings.TransactionDaysLimit;
        public static int TransactionDaysLimit { get { return transactionDaysLimit; } set { transactionDaysLimit = value; } }
        private static int transactionNumbersLimit = DefaultAppSettings.TransactionNumbersLimit;
        public static int TransactionNumbersLimit { get { return transactionNumbersLimit; } set { transactionNumbersLimit = value; } }
        public static bool EnableManualEndOfDay { get; set; }
        private static int serialReadTimeout = DefaultAppSettings.SerialReadTimeout;
        public static int SerialReadTimeout { get { return serialReadTimeout; } set { serialReadTimeout = value; } }
        public static bool UseStartPages { get { return ApplicationMode.Contains(ApplicationModes.StartPages); } }
        private static string ocfAddress = DefaultAppSettings.OcfAddress;
        public static string OcfAddress
        {
            get { return ocfAddress; }
            set { ocfAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("OcfAddress", value)); }
        }
        public static bool BonuscardsEnabled { get; set; }
        public static bool Retain24Enabled { get; set; }
        private static int logQueueMaxSize = DefaultAppSettings.LogQueueMaxSize;
        public static int LogQueueMaxSize { get { return logQueueMaxSize; } set { logQueueMaxSize = value; } }
        private static int transactionReportQueueMaxSize = DefaultAppSettings.TransactionReportQueueMaxSize;
        public static int TransactionReportQueueMaxSize { get { return transactionReportQueueMaxSize; } set { transactionReportQueueMaxSize = value; } }
        private static int endOfDayDelay = DefaultAppSettings.EndOfDayDelay;
        public static int EndOfDayDelay { get { return endOfDayDelay; } set { endOfDayDelay = value; } }
        private static int licenseTimeout = DefaultAppSettings.LicenseTimeout;
        public static int LicenseTimeout { get { return licenseTimeout; } set { licenseTimeout = value; } }
        private static int licenseRetries = DefaultAppSettings.LicenseRetries;
        public static int LicenseRetries { get { return licenseRetries; } set { licenseRetries = value; } }
        private static int wsRetries = DefaultAppSettings.WSRetries;
        public static int WSRetries { get { return wsRetries; } set { wsRetries = value; } }
        public static bool SendStoreAndForwardImmediately { get; set; }
        private static int authorisationRetries = DefaultAppSettings.AuthorisationRetries;
        public static int AuthorisationRetries { get { return authorisationRetries; } set { authorisationRetries = value; } }
        private static int enqTimeoutMilliseconds = DefaultAppSettings.ENQTimeoutMilliseconds;
        public static int ENQTimeoutMilliseconds { get { return enqTimeoutMilliseconds; } set { enqTimeoutMilliseconds = value; } }
        private static int socketConnectTimeoutSeconds = DefaultAppSettings.SocketConnectTimeoutSeconds;
        public static int SocketConnectTimeoutSeconds { get { return socketConnectTimeoutSeconds; } set { socketConnectTimeoutSeconds = value; Utilities.Utils.SocketConnectTimeoutSeconds = value; } }
        private static bool cancelTransactionBeforeCloseBatch = DefaultAppSettings.CancelTransactionBeforeCloseBatch;
        public static bool CancelTransactionBeforeCloseBatch { get { return cancelTransactionBeforeCloseBatch; } set { cancelTransactionBeforeCloseBatch = value; } }
        public static OrgNrMatchingOptions OrgNrMatching { get; set; }
        private static ushort letoOfflineLimit = DefaultAppSettings.LetoOfflineLimit;
        public static ushort LetoOfflineLimit { get { return letoOfflineLimit; } set { letoOfflineLimit = value; } }
        private static UInt32? currentOfflineOrderId;
        public static UInt32? CurrentOfflineOrderId
        {
            get
            {
                if (currentOfflineOrderId == null)
                {
                    object x = Storage.Registry.Load("CurrentOfflineOrderId");
                    if (x != null)
                    {
                        currentOfflineOrderId = Convert.ToUInt32(x);
                        return Convert.ToUInt32(x);
                    }
                    return null;
                }
                else
                    return currentOfflineOrderId;
            }
            set { currentOfflineOrderId = value; }
        }
        public static void SetCurrentOfflineOrderId(UInt32? value)
        {
            currentOfflineOrderId = value;
            ThreadPool.QueueUserWorkItem((arg) =>
            {
                Storage.Registry.Save("CurrentOfflineOrderId", value, Microsoft.Win32.RegistryValueKind.DWord);
            });

        }
        private static string letoOfflineAddress = DefaultAppSettings.LetoOfflineAddress;
        public static string LetoOfflineAddress { get { return letoOfflineAddress; } set { letoOfflineAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("LetoOfflineAddress", value)); } }
        public static string CurrentProject { get; set; }
        private static Uri swishApiAddress = DefaultAppSettings.SwishApiAddress;
        public static Uri SwishApiAddress { get { return swishApiAddress; } set { swishApiAddress = value; AddressUpdated(null, new AddressUpdatedEventArgs("SwishApiAddress", value.ToString())); } }
    }

    public static class UserSettings
    {
        public static bool TippingEnabled { get; set; }
        public static List<TimeInterval> TippingTimes { get; set; }
        public static bool ChipXpressEnabled { get; set; }
        public static List<TimeInterval> ChipXpressTimes { get; set; }
        private static bool integerAmounts = DefaultAppSettings.IntegerAmounts;
        public static bool IntegerAmounts { get { return integerAmounts; } set { integerAmounts = value; } }
        public static string VoiceReferralCode { get; set; }
        private static DateTime endOfDay_at = DefaultAppSettings.EndOfDay_at;
        public static DateTime EndOfDay_at { get { return endOfDay_at; } set { endOfDay_at = value; } }
        public static List<string> GreetingText { get; set; }
        public static Languages Language { get; set; }
        private static string refundPassword = DefaultAppSettings.RefundPassword;
        public static string RefundPassword { get { return refundPassword; } set { refundPassword = value; } }
        public static ushort MerchantReceiptCopies { get; set; }
        public static bool PrintReceipts { get; set; }
        public static bool ReloadSettingsAfterMgmtMenu { get; set; }
        public static bool KeyBeep { get; set; }
        public static bool CashierLogin { get; set; }
        public static bool PrintCardholderName { get; set; }
        public static bool PrintSignatureReceiptOnTerminal { get; set; }
        public static bool LoginByCode { get; set; }
        public static string DefaultWaiterId { get; set; }
        public static bool AllowDuplicateOrderId { get; set; }
        public static bool PrintTransactionReportAtEndOfDay { get; set; }
        private static long tipWarningThresholdPercentage = DefaultAppSettings.TipWarningThresholdPercentage;
        public static long TipWarningThresholdPercentage { get { return tipWarningThresholdPercentage; } set { tipWarningThresholdPercentage = value; } }
        private static long tipMaximumPercentage = DefaultAppSettings.TipMaximumPercentage;
        public static long TipMaximumPercentage { get { return tipMaximumPercentage; } set { tipMaximumPercentage = value; } }
        private static long maximumTip = DefaultAppSettings.MaximumTip;
        public static long MaximumTip { get { return maximumTip; } set { maximumTip = value; } }
        public static string EthernetAddress { get; set; }
        public static string WifiAddress { get; set; }
        public static bool UseStartPageBorders { get; set; }
        public static uint CartArticleRows { get; set; }
        public static int? DefaultTableNumber { get; set; }
        private static TipCustomisation.Prompts tipPromt = DefaultAppSettings.TipPrompt;
        public static TipCustomisation.Prompts TipPrompt { get { return tipPromt; } set { tipPromt = value; } }
        private static bool tipAllowSkipOnOk = DefaultAppSettings.TipAllowSkipOnOk;
        public static bool TipAllowSkipOnOk { get { return tipAllowSkipOnOk; } set { tipAllowSkipOnOk = value; } }
        public static bool ConfirmCartReset { get; set; }
        private static int articleAddedDelay = DefaultAppSettings.ArticleAddedDelay;
        public static int ArticleAddedDelay { get { return articleAddedDelay; } set { articleAddedDelay = value; } }
        private static bool closeCardBatchOnZReport = DefaultAppSettings.CloseCardBatchOnZReport;
        public static bool CloseCardBatchOnZReport { get { return closeCardBatchOnZReport; } set { closeCardBatchOnZReport = value; } }
        private static bool showCart = DefaultAppSettings.ShowCart;
        public static bool ShowCart { get { return showCart; } set { showCart = value; } }
        private static bool showPaymentScreen = DefaultAppSettings.ShowPaymentScreen;
        public static bool ShowPaymentScreen { get { return showPaymentScreen; } set { showPaymentScreen = value; } }
        public static bool AllowTip { get; set; }
        private static bool allowZeroAmount = DefaultAppSettings.AllowZeroAmount;
        public static bool AllowZeroAmount { get { return allowZeroAmount; } set { allowZeroAmount = value; } }
        public static bool UseLogout { get; set; }
        public static bool PrintEmployeeReportAtLogout { get; set; }
        public static bool CreateZReportAtLogout { get; set; }
        private static bool forceOnline = DefaultAppSettings.ForceOnline;
        public static bool ForceOnline { get { return forceOnline; } set { forceOnline = value; } }
        public static uint DefaultStartPageId { get; set; }
        private static bool enableConnectionCheck = DefaultAppSettings.EnableConnectionCheck;
        public static bool EnableConnectionCheck { get { return enableConnectionCheck; } set { enableConnectionCheck = value; } }
        private static bool contactlessEnabled = DefaultAppSettings.ContactlessEnabled;
        public static bool ContactlessEnabled { get { return contactlessEnabled; } set { contactlessEnabled = value; } }
        public static List<Utilities.WifiSSIDEntry> WifiSSIDList { get; internal set; }
        public static bool UseProjects { get; set; }
        public static int DelayBetweenReceiptsSeconds { get; set; }
        public static bool DisableReversalPassword { get; set; }
        public static string SwishMerchant { get; set; }
        private static bool signatureEnabled = DefaultAppSettings.SignatureEnabled;
        public static bool SignatureEnabled { get { return signatureEnabled; } set { signatureEnabled = value; } }
        private static decimal rounding = DefaultAppSettings.Rounding;
        public static decimal Rounding { get { return rounding; } set { rounding = value; } }
    }
*/
    public class AddressUpdatedEventArgs : EventArgs
    {
        public string Name { get; private set; }
        public string Value { get; private set; }
        public AddressUpdatedEventArgs(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
