﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using OSHipPay.WebSockets;

namespace OSHipPay.Configuration
{
    public class UDPListener
    {
        static int port = 3334;
        static bool Running = true;
        UdpClient listener;
        public static void SetPort(int p)
        {
            port = p;
        }

        private UDPListener(bool value)
        {

        }

        private void StartPolling()
        {
            listener = new UdpClient(port);
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, port);
            try
            {
                Logging.Log.Debug("Waiting for broadcast "+groupEP.ToString(), new UDPListener(true));
                while (Running)
                {
                    byte[] bytes = listener.Receive(ref groupEP);
                    String sMsg = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
                    if (sMsg.Contains("\"CashRegisterBroadcast\""))
                    {
                        Logging.Log.Debug($"Received broadcast from {groupEP} :", new UDPListener(true));
                        Logging.Log.Debug($" {sMsg}", new UDPListener(true));
                        WebSocketProcessor.ProcessECRBroadCast(groupEP);
                    }
                    else Logging.Log.Spam("<Received broadcast " + sMsg, new UDPListener(false));
                }
            }
            catch (Exception e)
            {
                if (Running)
                    Logging.Log.Error("UDPListener failed", e, new UDPListener(false));
            }
            finally
            {
                if (listener != null)
                    listener.Close();
                listener = null;
            }
        }

        public UDPListener() {
            new Thread(() => StartPolling()).Start();
        }

        internal void Close()
        {
            Running = false;
            if (listener != null)
                listener.Close();
            listener = null;
        }
    }
}