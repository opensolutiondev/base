﻿using Android.App;
using Android.Content;
using Android.Net.Wifi;
using OSHipPay.Utilities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;

namespace OSHipPay.Configuration
{
    class License
    {

        private static string AppName { get => "OSHipPay"; }
        private static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(4); }

        public static string GetAppName()
        {
            return AppName;
        }

        public static string GetVersion()
        {
            return Version;
        }

        public static string GetSubnetMask(String address)
        {
            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
                {
                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        if (address.Equals(unicastIPAddressInformation.Address.ToString()))
                        {
                            return unicastIPAddressInformation.IPv4Mask.ToString();
                        }
                    }
                }
            }
            return null;
        }

        public static bool GetIPAddress()
        {
            return GetIPAddress(out string x);
        }
        public static bool GetIPAddress(out string addresses)
        {
            System.Net.IPAddress[] ipAddressArray = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList;

            List<string> ipAddresses = new List<string>();
            foreach (var ip in ipAddressArray)
            {
                if (ip.ToString().StartsWith("127.0.0"))
                    continue;
                ipAddresses.Add(ip.ToString());
            }
            if (ipAddresses.Count == 0)
            {
                addresses = "?.?.?.?";
                return false;
            }
            else
            {
                addresses = String.Join(" / ", ipAddresses.ToArray());
                return true;
            }
        }

        public static long PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = null;
            long i = -1;
            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
                if (pingable)
                    i = reply.RoundtripTime;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return i;
        }

        //TODO - Fix....
        public static long GetAndPingDefaultGateway(out System.Net.IPAddress address)
        {
            try
            {
                WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
                String addr = Utils.FromIntToIPString(wifiManager.DhcpInfo.Gateway);
                address = IPAddress.Parse(addr);
                return PingHost(addr);
            }
            catch { }
            address = System.Net.IPAddress.Parse("0.0.0.0");
            return -1;
        }

        public static String GetDefaultGateway()
        {
            try
            {
                WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
                String addr = Utils.FromIntToIPString(wifiManager.DhcpInfo.Gateway);
                return addr;
            }
            catch { }
            return "0.0.0.0";
        }
    }
}