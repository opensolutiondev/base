﻿using OSHipPay.Core;
using System;
using System.IO;
using WestPA.Standalone;

namespace OSHipPay.Configuration
{
    class Config
    {
    //    public static readonly string assemblyPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\Configuration.dll";
        public static readonly WestPA.Standalone.ImageData imageData = new WestPA.Standalone.ImageData();
        private readonly static long CurrentVersion = DateTime.Parse("01.23.2019 19:07").Ticks;

        internal static void Init(Settings settings, SystemInfo sysinfo)
        {
            Logging.Log.Debug("Config current version before init " + settings.CurrentVersion, new Config());
            //settings.CurrentVersion = 0;
            if (settings.CurrentVersion < CurrentVersion)
            {
                long version1 = DateTime.Parse("10.12.2018 19:07").Ticks;
                if (settings.CurrentVersion < version1)
                {
                    //Delete old tables....
                    Logging.Log.Debug("Old version - dropping tables", new Config());
                    new TransactionRepositorySQLite(Path.Combine(sysinfo.PersistentDirectory, "transactions.sqlite")).Initialise(true);
                    //new TransactionIdentifiersRepositorySQLite(Path.Combine(sysinfo.PersistentDirectory, "transactionIdentifiers.sqlite")).Initialise(true);
                    Logging.Log.Debug("Old version - dropped tables", new Config());
                    settings.CurrentVersion = version1;
                }
                long version2 = DateTime.Parse("12.18.2018 19:07").Ticks;
                if (settings.CurrentVersion < version2)
                {
                    settings.LogLevel = (int)Logging.Severity.Warning;
                    Logging.Log.Debug("Changed LogLevel to Warning", new Config());
                    settings.WebSocketPort = 3333;
                    settings.UDPPort = 3334;
                    Logging.Log.Debug("Changed UDP/WebSocket Port to defaults", new Config());
                    settings.NoPrinter = true;
                    settings.CurrentVersion = version2;
                }
                long version3 = DateTime.Parse("12.28.2018 19:07").Ticks;
                if (settings.CurrentVersion < version3)
                {
                    //Delete old tables....
                    Logging.Log.Debug("Old version - dropping tables", new Config());
                    new TransactionRepositorySQLite(Path.Combine(sysinfo.PersistentDirectory, "transactions.sqlite")).Initialise(true);
                    //new TransactionIdentifiersRepositorySQLite(Path.Combine(sysinfo.PersistentDirectory, "transactionIdentifiers.sqlite")).Initialise(true);
                    Logging.Log.Debug("Old version - dropped tables", new Config());
                    settings.CurrentVersion = version3;
                }
                long version4 = DateTime.Parse("01.14.2019 19:07").Ticks;
                if (settings.CurrentVersion < version4)
                {
                    settings.CurrentVersion = version4;
                    settings.TransactionDaysLimit = 2;
                    settings.TransactionNumbersLimit = 2048;
                }
                long version5 = DateTime.Parse("01.23.2019 19:07").Ticks;
                if (settings.CurrentVersion < version5)
                {
                    settings.CurrentVersion = version5;
                    settings.APN = "ospay.ts.m2m";
                    settings.LogFileEndPoint = "212.16.186.132";
                }

    }
    Logging.Log.Debug("Config current version after init "+settings.CurrentVersion, new Config());

        }
    }
}