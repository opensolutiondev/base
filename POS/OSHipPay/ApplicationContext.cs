using System;
using System.Collections.Generic;
using OSHipPay.UI;
using WestPA.Standalone;

namespace OSHipPay
{
    class ApplicationContext
    {
        static ApplicationContext _Current;

        public static ApplicationContext Current
        {
            get
            {
                if (_Current == null)
                    throw new InvalidOperationException("Application context is not initialised.");

                return _Current;
            }
        }

        public static void Initialise( UserInterface ui,
                                       Settings settings,
                                       StringDefinitions strings,
                                       SystemInfo sysInfo)
        {
            if (_Current != null)
            {
                throw new InvalidOperationException("Application context is already initialised.");
            }

            _Current = new ApplicationContext
            {
                UI                 = ui,
                Settings           = settings,
                Strings            = strings,
                InstalledLanguages = new List<string>(),
                SysInfo            = sysInfo
            };
        }

        public static void InitializeForTesting(ApplicationContext context)
        {
            _Current = context;
        }

        protected ApplicationContext() { }

        /// <summary>
        /// Set to True if the next RunPreSession() should open the management functions.
        /// </summary>
        public bool OpenManagmentFunctionsOnNextSession { get; set; }

        /// <summary>
        /// Set to True if the next RunPreSession() should open the TerminalSettings functions.
        /// </summary>
        public bool OpenTerminalSettingsScreenOnNextSession { get; set; }

        //Not needed anymore - Patric from West 2.11.2018
        //public bool CloseBatchOnNextSession { get; set; }

        public bool HandleTransactionListOnNextSession { get; set; }

        public bool DiagnosticsOnNextSession { get; set; }

        public bool ReconnectedNetwork { get; set; }
        

        public virtual List<string> InstalledLanguages { get; private set; }

        public virtual Settings Settings { get; private set; }

        public virtual StringDefinitions Strings { get; set; }

        public virtual UserInterface UI { get; private set; }

        public virtual SystemInfo SysInfo { get; private set; }

        /// <summary>
        /// Record in the application context what languages are installed.
        /// </summary>
        /// <param name="languagesInstalled"></param>
        public void CheckInstalledLanguages(IList<string> languagesInstalled)
        {
            if (Current.InstalledLanguages.Count == 0)
            {
                Current.InstalledLanguages.AddRange(languagesInstalled);
            }
        }
    }
}