﻿using System.Collections.Generic;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI
{
    /// <summary>
    /// Lists of user inputs.
    /// </summary>
    static class UserInputsLists
    {
        public static readonly UserInputs[] Digits =
        {
            UserInputs.Zero,
            UserInputs.One,
            UserInputs.Two,
            UserInputs.Three,
            UserInputs.Four,
            UserInputs.Five,
            UserInputs.Six,
            UserInputs.Seven,
            UserInputs.Eight,
            UserInputs.Nine
        };

        /// <summary>
        /// The inputs used when editing a string.
        /// </summary>
        public static readonly List<UserInputs> EditString;

        /// <summary>
        /// No inputs.
        /// </summary>
        public static readonly List<UserInputs> Empty = new List<UserInputs>();

        static UserInputsLists()
        {
            EditString = Digits
                .Append(UserInputs.Cancel,
                        UserInputs.Enter,
                        UserInputs.Backspace,
                        UserInputs.QuestionMark,
                        UserInputs.Button0,
                        UserInputs.Button1,
                        UserInputs.Symbol,
                        UserInputs.LetterKey)
                .ToList();
        }
    }
}