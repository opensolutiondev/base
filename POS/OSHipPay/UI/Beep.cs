using WestPA.Standalone;

namespace OSHipPay.UI
{
    public delegate void BeepHandler(BeepType type);

    static class Beep
    {
        public static BeepHandler Handler;

        public static void Angry()
        {
            HandleBeep(BeepType.CardErrorOrDeclined);
        }

        public static void Normal()
        {
            HandleBeep(BeepType.Default);
        }

        static void HandleBeep(BeepType type)
        {
            try
            {
                Handler?.Invoke(type);
            }
            catch
            {
                // ignore
            }
        }
    }
}