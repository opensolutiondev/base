﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using OSHipPay.UI.Screens;
using WestPA.Standalone;

namespace OSHipPay.UI
{
    public class UserInterface
    {
        public static StringDefinitions Strings;
        readonly IPaymentApp _app;
        readonly StringDefinitions _strings;
        readonly Settings _set;
        readonly InputHandler _newInputHandler;
        private bool Continue = false;
        private bool Abort = false;

        /// <summary>
        /// Create a new TextLine.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static DisplayData.TextLine TextLine(string text)
        {
            return new DisplayData.TextLine(text ?? String.Empty, DisplayData.Alignment.Left, DisplayData.Size.Regular);
        }

        UserInterface() { }

        public UserInterface(IPaymentApp app, StringDefinitions strings, Settings set)
        {
            _app = app;
            _strings = strings;
            Strings = _strings;
            _newInputHandler = new InputHandler();
            TextBlock.Updated += OnTextBlockUpdate;
            Beep.Handler = OnBeep;
            _set = set;
        }

        void OnTextBlockUpdate(object sender, TextBlock.UpdateEventArgs args)
        {
            _app.UpdateLine(args.Line, args.Value);
        }

        protected static List<DisplayData.TextLine> GetLines(List<string> strings, DisplayData.Alignment alignment, DisplayData.Size size)
        {
            List<DisplayData.TextLine> list = new List<DisplayData.TextLine>();
            foreach (string s in strings)
                list.Add(new DisplayData.TextLine(s, alignment, size));
            return list;
        }

        protected static List<DisplayData.TextLine> GetLines(List<string> strings)
        {
            return GetLines(strings, DisplayData.Alignment.Centre, DisplayData.Size.Regular);
        }


        public void FourLinesAndOK(string line1, string line2, string line3, string line4)
        {
            DisplayData d = new DisplayData(DisplayData.DisplayTemplate.FourLinesWithButtons,
                GetLines(new List<string> { line1, line2, line3, line4 }));
            d.SetButton(DisplayData.ButtonId.RightButton,
                new DisplayData.ButtonData(_strings.okButton, Color.Green, Color.Black));
            _app.Display(d);
            _app.EnableKeypadInputs(new List<UserInputs> { UserInputs.Enter, UserInputs.Cancel});
        }

        void OnBeep(WestPA.Standalone.BeepType type)
        {

            _app.Beep(type);
        }

        public IPaymentApp GetApp()
        {
            return _app;
        }

        /// <summary>
        /// Receives user input from the payment application.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="keyCode"></param>
        public virtual void HandleUserInput(UserInputs item, char keyCode)
        {
            Logging.Log.Debug("HandleUserInput "+item+" "+keyCode, new UserInterface());
            _newInputHandler.HandleUserInput(item, keyCode);
        }

        public virtual Boolean ShowFlexDisplay(FlexibleDisplay fd, List<UserInputs> inputs)
        {
            _app.Display(fd);
            char ch = InputKey(inputs);
            return ch==InputCharacter.Enter;
        }

        /// <summary>
        /// Display a message on the screen and an OK button.
        /// </summary>
        /// <param name="lines">Lines of text to display</param>
        public virtual void Alert(params string[] lines)
        {
            var dd = new DisplayData(DisplayData.DisplayTemplate.ThreeLinesWithButtons);
            foreach (var line in lines.Take(3))
            {
                dd.AddLine(new DisplayData.TextLine(line, DisplayData.Alignment.Centre, DisplayData.Size.Regular));
            }
            dd.SetButton(DisplayData.ButtonId.RightButton, new DisplayData.ButtonData(_strings.okButton, Color.DarkGreen, null));
            _app.Display(dd);
            // We will accept Enter or the left button
            InputKey(new List<UserInputs> {UserInputs.Enter, UserInputs.Cancel, UserInputs.Button1});
        }

        internal void SetAbort(bool Value)
        {
            Abort = Value;
        }

        internal bool GetAbort()
        {
            return Abort;
        }

        public bool UpdateFlexibleItem(int elementId, string text, Color? foreground, Color? background)
        {
            return _app.UpdateFlexibleItem(elementId, text, foreground, background);
        }

        public virtual IEnumerable<char> GetInputs(InputsSpecification inputs)
        {
            _app.EnableKeypadInputs(inputs.Inputs ?? UserInputsLists.Empty, _set.KeyBeepDefault);
            TimeSpan t = new TimeSpan(0, 0, 0, 0, 100);
            inputs.Timeout = t;
            for (; ; )
            {
                if (Continue)
                {
                    Continue = false;
                    yield return InputCharacter.Continue;
                }
                yield return _newInputHandler.GetNextInput(new TimeSpan(0));
            }
        }

        public virtual IEnumerable<char> GetInputsWithTimeOut(InputsSpecification inputs)
        {
            _app.EnableKeypadInputs(inputs.Inputs ?? UserInputsLists.Empty, _set.KeyBeepDefault);
            TimeSpan t = new TimeSpan(0,0,0, 1, 100);
            inputs.Timeout  = t;
            for (; ; )
            {
                if (Continue) {
                    Continue = false;
                    yield return InputCharacter.Continue;
                }
                yield return _newInputHandler.GetNextInput(inputs.Timeout);
            }
        }

        public virtual void SetContinue()
        {
            Continue = true;
        }

        public virtual void Display(DisplayData dd) { _app.Display(dd); }

        public virtual void Display(FlexibleDisplay dd) { _app.Display(dd); }

        /// <summary>
        /// Display some text on the screen. User input is disabled.
        /// </summary>
        /// <param name="lines"></param>
        public virtual void Display(params string[] lines)
        {
            _app.EnableKeypadInputs(UserInputsLists.Empty);
            var screen = new FiveLinesScreen();
            screen.SetScreenLines(lines);
            Display(screen);
        }

        /// <summary>
        /// Displays the message "Please wait" on the screen. User input is disabled.
        /// </summary>
        /// <param name="message">An additional message that will be displayed too.</param>
        public virtual void PleaseWait(string message)
        {
            _app.EnableKeypadInputs(UserInputsLists.Empty, _set.KeyBeepDefault);
            var dd = new DisplayData(DisplayData.DisplayTemplate.FiveLines);
            dd.AddLine(new DisplayData.TextLine(_strings.pleaseWait, DisplayData.Alignment.Centre, DisplayData.Size.Regular));
            dd.AddLine(new DisplayData.TextLine(String.Empty, DisplayData.Alignment.Centre, DisplayData.Size.Regular));
            dd.AddLine(new DisplayData.TextLine(message ?? String.Empty, DisplayData.Alignment.Centre, DisplayData.Size.Regular));
            _app.Display(dd);
        }

        /// <summary>
        /// Request a monetary amount from the user with Menu button
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="subtitle">The subutitle of the screen.</param>
        /// <returns>The amount entered or null if the user cancelled.</returns>
        public virtual uint? RequestAmountWithMenu(string title, string subtitle)
        {
            Logging.Log.Debug("RequestAmountWithMenu title: " + title+ " subtitle: " +subtitle, new UserInterface());
            var screen = new AmountEditorScreen(title, subtitle, 0, true, true)
            {
                DisableDecimals = ApplicationContext.Current.Settings.DisableDecimals
            };
            Display(screen);
            return screen.IsCancelled ? (uint?)null : screen.Amount;
        }

        /// <summary>
        /// Request a monetary amount from the user.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="subtitle">The subutitle of the screen.</param>
        /// <returns>The amount entered or null if the user cancelled.</returns>
        public virtual uint? RequestAmount(string title, string subtitle)
        {
            var screen = new AmountEditorScreen(title, subtitle, 0, true)
            {
                DisableDecimals = ApplicationContext.Current.Settings.DisableDecimals
            };
            Display(screen);
            return screen.IsCancelled ? (uint?) null : screen.Amount;
        }

        /// <summary>
        /// Display a question for the user and the buttons Yes and No.
        /// </summary>
        /// <param name="lines">One or more lines of text to display</param>
        /// <returns>True if the user pressed Yes or Enter, else False</returns>
        public virtual bool YesNo(params string[] lines)
        {
            return OkCancel(lines, _strings.yes, _strings.no);
        }

        /// <summary>
        /// Display a question for the user and the buttons OK and Cancel.
        /// </summary>
        /// <param name="lines">One or more lines of text to display</param>
        /// <returns>True if the user pressed OK or Enter, else False</returns>
        public virtual bool OkCancel(params string[] lines)
        {
            return OkCancel(lines, _strings.okButton, _strings.cancel);
        }

        /// <summary>
        /// Display a question for the user and the buttons OK and Cancel.
        /// </summary>
        /// <param name="lines">One or more lines of text to display</param>
        /// <param name="okText">The label on the OK button</param>
        /// <param name="cancelText">The label on the Cancel button</param>
        /// <returns>True if the user pressed OK or Enter, else False</returns>
        bool OkCancel(string[] lines, string okText, string cancelText)
        {
            var screen = new OkCancelScreen(lines, okText, cancelText);
            Display(screen);
            return !screen.IsCancelled;
        }

        public virtual char InputKeyWithTimeOut(List<UserInputs> keys)
        {
            return GetInputsWithTimeOut(keys).First();
        }
        public virtual char InputKey(List<UserInputs> keys)
        {
            return GetInputs(keys).First();
        }

        public virtual void Display(IScreen screen)
        {
            Logging.Log.Debug("UserInterface.Display initializing  "+screen.ToString(), new UserInterface());
            if (screen == null) throw new ArgumentNullException(nameof(screen));
            _app.EnableKeypadInputs(UserInputsLists.Empty, _set.KeyBeepDefault);
            if (screen is CompositeScreenBase compositeScreen)
            {
                foreach (var vm in compositeScreen.GetScreens())
                {
                    Display(vm);
                }
            }
            else if (screen is SimpleScreen simpleScreen)
            {
                simpleScreen.Displayed(this);
            }
            else
            {
                DisplayScreen(screen);
                if (screen is IHandleInput inputScreen)
                {
                    var inputStream = GetInputs(inputScreen.AcceptedInputs);
                    inputScreen.HandleInput(inputStream);
                }
            }
            Logging.Log.Debug("UserInterface.Display finished  " + screen.ToString(), new UserInterface());
        }

        void DisplayScreen(IScreen screen)
        {
            if (screen == null) throw new ArgumentNullException(nameof(screen));
            var template = GetDisplayTemplate(screen);
            var dd = new DisplayData(template);
            foreach (var textBlock in screen.AllTextBlocks)
            {
                var tl = TextLine(textBlock.Value);
                tl.Alignment = textBlock.Alignment;
                tl.Size = textBlock.Size;
                dd.AddLine(tl);
            }
            if (screen is IViewWithButtons buttonsModel)
            {
                var btn = buttonsModel.Left;
                if (btn.Text.IsNotNullOrEmpty())
                {
                    dd.SetButton(DisplayData.ButtonId.LeftButton,
                                 new DisplayData.ButtonData(btn.Text, btn.Background, btn.Colour));
                }
                btn = buttonsModel.Right;
                if (btn.Text.IsNotNullOrEmpty())
                {
                    dd.SetButton(DisplayData.ButtonId.RightButton,
                                 new DisplayData.ButtonData(btn.Text, btn.Background, btn.Colour));
                }
            }
            _app.Display(dd);
        }

        static DisplayData.DisplayTemplate GetDisplayTemplate(IScreen screen)
        {
            if (screen is IViewFiveLines)
            {
                return DisplayData.DisplayTemplate.FiveLines;
            }
            if (screen is IViewFourLines)
            {
                if (screen is IViewWithButtons)
                {
                    return DisplayData.DisplayTemplate.FourLinesWithButtons;
                }
                else
                {
                    return DisplayData.DisplayTemplate.FourLines;
                }
            }
            if (screen is IViewThreeLines)
            {
                return DisplayData.DisplayTemplate.ThreeLinesWithButtons;
            }
            throw new InvalidOperationException("Cannot get display template for screen");
        }

        public bool Display(ImageData image)
        {
            return _app.Display(image);
        }

        /// <summary>
        /// Ask the user for the merchant password.
        /// </summary>
        /// <param name="caption">The text that will be shown at the top of the screen. It should describe
        /// what function the user is trying to access.</param>
        /// <returns>True if the user entered the correct password.</returns>
        public bool CheckMerchantPassword(string caption)
        {
            var pwScreen = new PasswordCheckScreen(caption, input => _app.VerifyMerchantPassword(input));
            Display(pwScreen);
            return pwScreen.IsValid;
        }

        public bool CheckDailyPassword(string caption)
        {
            var pwScreen = new PasswordCheckScreen(caption, input => _app.VerifyDailyPassword(input));
            Display(pwScreen);
            return pwScreen.IsValid;
        }

        public bool CheckDailyPassword2(string caption)
        {
            var pwScreen = new PasswordCheckScreen(caption, input => IsDailyPassword(input));
            Display(pwScreen);
            return pwScreen.IsValid;
        }

        private bool IsDailyPassword(String password)
        {
            return password.Equals(DateTime.Now.ToString("MMdd"));
        }

        /// <summary>
        /// Disable input and sleep for a while.
        /// </summary>
        /// <param name="duration">The length of time to sleep</param>
        public void Pause(TimeSpan duration)
        {
            _app.EnableKeypadInputs(UserInputsLists.Empty, _set.KeyBeepDefault);
            Thread.Sleep((int) duration.TotalMilliseconds);
        }

    }
}
