﻿
using WestPA.Standalone;

namespace OSHipPay.UI
{
    /// <summary>
    /// Contains character mappings for special inputs and conversion methods between chars and UserInputs.
    /// </summary>
    static class InputCharacter
    {
        public static readonly char[] AllDigits = {'0','1','2','3','4','5','6','7','8','9'};
        public const char Enter = '\r';
        public const char Backspace = '\b';
        public const char Tab = '\t';
        public const char Cancel = '\x1E';
        public const char Timeout = '\x1F';
        public const char Continue = '\x20';
        public const char Button0 = '\x81';
        public const char Button1 = '\x82';
        public const char Unknown = '\\';
        public const char Empty = '\x0';

        /// <summary>
        /// Get the char corresponding to the given UserInputs and keyCode
        /// </summary>
        /// <param name="input"></param>
        /// <param name="keyCode"></param>
        /// <returns></returns>
        public static char Get(UserInputs input, char keyCode)
        {
            switch (input)
            {
                case UserInputs.Zero: return '0';
                case UserInputs.One: return '1';
                case UserInputs.Two: return '2';
                case UserInputs.Three: return '3';
                case UserInputs.Four: return '4';
                case UserInputs.Five: return '5';
                case UserInputs.Six: return '6';
                case UserInputs.Seven: return '7';
                case UserInputs.Eight: return '8';
                case UserInputs.Nine: return '9';
                case UserInputs.Enter: return Enter;
                case UserInputs.Backspace: return Backspace;
                case UserInputs.Cancel: return Cancel;
                case UserInputs.Tab: return Tab;
                case UserInputs.QuestionMark: return '?';
                case UserInputs.Button0: return Button0;
                case UserInputs.Button1: return Button1;
                case UserInputs.LetterKey: return keyCode;
                default:
                    return Unknown;
            }
        }

        /// <summary>
        /// Get the UserInputs corresponding to the given char.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static UserInputs Reverse(char c)
        {
            switch (c)
            {
                case '0': return UserInputs.Zero;
                case '1': return UserInputs.One;
                case '2': return UserInputs.Two;
                case '3': return UserInputs.Three;
                case '4': return UserInputs.Four;
                case '5': return UserInputs.Five;
                case '6': return UserInputs.Six;
                case '7': return UserInputs.Seven;
                case '8': return UserInputs.Eight;
                case '9': return UserInputs.Nine;
                case Enter: return UserInputs.Enter;
                case Backspace: return UserInputs.Backspace;
                case Cancel: return UserInputs.Cancel;
                case Tab: return UserInputs.Tab;
                case '?': return UserInputs.QuestionMark;
                case Button0: return UserInputs.Button0;
                case Button1: return UserInputs.Button1;
                default: return default(UserInputs);
            }
        }
    }
}