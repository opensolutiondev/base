﻿using System.Globalization;

namespace OSHipPay.UI
{
    public partial class StringDefinitions
    {
        /// <summary>
        /// Index of current language. Defaults to 0 (should be en-US).
        /// </summary>
        int _index;

        /// <summary>
        /// Change the language used. Returns true if successful and false if the code is unknown.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool SetLanguage(string code)
        {
            var i = _Languages.IndexOf(code);
            if (i < 0)
                return false;
            _index = i;
            return true;
        }

        /// <summary>
        /// Get the CultureInfo for the currently used language.
        /// </summary>
        public CultureInfo GetCultureInfo()
        {
            return CultureInfo.GetCultureInfo(culture);
        }

        /// <summary>
        /// Get a string in the assigned language, or if that string does not exist, in the default language, or an empty string.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        string Get(string[] item)
        {
            return item[_index] ?? item[0] ?? "";
        }

        public string GetIso639_2()
        {
            return _Languages_iso_639_2[_index];
        }
    }
}
