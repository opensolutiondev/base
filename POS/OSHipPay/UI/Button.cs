using System.Drawing;

namespace OSHipPay.UI
{
    public class Button
    {
        public Color? Background { get; set; }
        public Color? Colour { get; set; }
        public string Text { get; set; }

        public void Show(string text, Color background, Color? colour)
        {
            Text = text;
            Background = background;
            Colour = colour;
        }

        public void Show(string text)
        {
            Text = text;
        }
    }
}