﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WestPA.Standalone;

namespace OSHipPay.UI
{

    class OSInputters
    {

    }

    class InputLists
    {
        static public List<UserInputs> empty = new List<UserInputs>();
    }


    #region SINGLE_KEY_INPUT

    public class SingleKeyInputter
    {
        /// <summary>
        /// Records the string that was entered by the generic string input handler
        /// </summary>
        private UserInputs mTriggerEvent;
        public static readonly int _prevPageControlId = -2;
        public static readonly int _nextPageControlId = -1;


        public UserInputs LastInput { get { return mTriggerEvent; } }

        /// <summary>
        /// Used to signal that input has finished
        /// </summary>
        private AutoResetEvent mNotification;

        /// <summary>
        /// Local reference to the wider input handler lock object
        /// </summary>
        private readonly object mHandlerLock;

        /// <summary>
        /// Local reference to the payment application
        /// </summary>
        private IPaymentApp mApp;

        private readonly UserInputHandler mPreviousHandler;

        /// <summary>
        /// set to the id of the button pressed if available
        /// </summary>
        private int mButtonId = int.MinValue;

        public int ButtonId { get { return mButtonId; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="app">Payment application</param>
        /// <param name="handlerRef">User input handler</param>
        /// <param name="handlerLock">User input handler lock object</param>
        /// <param name="inputList">List of possible inputs</param>
        public SingleKeyInputter(IPaymentApp app, ref UserInputHandler handlerRef, object handlerLock, List<UserInputs> inputList)
        {
            mNotification = new AutoResetEvent(false);
            mApp = app;
            mHandlerLock = handlerLock;

            lock (mHandlerLock)
            {
                mPreviousHandler = handlerRef;

                handlerRef = SingleButtonInputHandler;

                if (inputList != null)
                    mApp.EnableKeypadInputs(inputList);
            }
        }

        /// <summary>
        /// Waits for input to finish then returns the triggering input
        /// </summary>
        /// <param name="handlerRef">Reference to the input handler in the POS component</param>
        /// <returns>The user input that was received</returns>
        public UserInputs WaitForFinish(ref UserInputHandler handlerRef)
        {
            mNotification.WaitOne();
            return Finish(ref handlerRef);
        }

        public UserInputs WaitForFinishUntilTimeout(ref UserInputHandler handlerRef, int milisecondsTimeout)
        {
            mNotification.WaitOne(milisecondsTimeout, false);
            return Finish(ref handlerRef);
        }


        /// <summary>
        /// Returns the string that was input
        /// </summary>
        public UserInputs Finish(ref UserInputHandler handlerRef)
        {
            mApp.EnableKeypadInputs(InputLists.empty);
            lock (mHandlerLock)
            {
                //handlerRef = null;
                handlerRef = mPreviousHandler;
            }
            return mTriggerEvent;
        }

        /// <summary>
        /// Input event handler that finishes after a single input
        /// </summary>
        /// <param name="input">Input event</param>
        private void SingleButtonInputHandler(UserInputs input, char character, int id)
        {
            // Handles user input events during amount entry
            mTriggerEvent = input;
            mButtonId = id;
            mNotification.Set();
            mApp.EnableKeypadInputs(InputLists.empty);
        }
    }

    #endregion

    #region STRING_INPUT

    public class StringInputter
    {
        /// <summary>
        /// Records the string that was entered by either input handler
        /// </summary>
        private string mStringInput = string.Empty;

        /// <summary>
        /// Records the supplied/original string 
        /// </summary>
        private readonly string mOriginalString = string.Empty;

        /// <summary>
        /// Used to signal that input has finished
        /// </summary>
        private AutoResetEvent mNotification;

        /// <summary>
        /// Defines maximum string length
        /// </summary>
        private readonly int mMaxLength;

        /// <summary>
        /// Local reference to the wider input handler lock object
        /// </summary>
        private readonly object mHandlerLock;

        /// <summary>
        /// Local reference to the payment application
        /// </summary>
        private IPaymentApp mApp;

        /// <summary>
        /// Defines where to display input string
        /// </summary>
        private readonly int mUpdateLine;

        /// <summary>
        /// set true if 'cancel' was pressed
        /// </summary>
        private bool mCancelPressed = false;

        /// <summary>
        /// set true if button0 was pressed
        /// </summary>
        private bool mButton0Pressed = false;

        /// <summary>
        /// set true if button1 was pressed
        /// </summary>
        private bool mButton1Pressed = false;

        /// <summary>
        /// set true if 'Tab' was pressed
        /// </summary>
        private bool mTabPressed = false;

        private readonly UserInputHandler mPreviousHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="app">Payment application instance</param>
        /// <param name="handlerRef">User input handler</param>
        /// <param name="handlerLock">Lock object for the user input handler</param>
        /// <param name="maxLength">Maximum acceptable length of string input</param>
        /// <param name="updateLine">Indicates which line of the display to update on changes</param>
        /// <param name="defaultString">Default value</param>
        /// <param name="ipAddress">Indicates if the string being entered is an IP address</param>
        public StringInputter(IPaymentApp app, ref UserInputHandler handlerRef, object handlerLock,
                              int maxLength, int updateLine, string defaultString, bool ipAddress)
        {
            mNotification = new AutoResetEvent(false);
            mApp = app;
            mMaxLength = maxLength;
            mUpdateLine = updateLine;
            mHandlerLock = handlerLock;
            mStringInput = defaultString;
            mOriginalString = defaultString;

            List<UserInputs> inputs = new List<UserInputs>();
            for (UserInputs key = UserInputs.Zero; key <= UserInputs.Nine; ++key)
            {
                inputs.Add(key);
            }
            inputs.Add(UserInputs.Enter);
            inputs.Add(UserInputs.Cancel);
            inputs.Add(UserInputs.Backspace);
            inputs.Add(UserInputs.QuestionMark);
            inputs.Add(UserInputs.Button0);
            inputs.Add(UserInputs.Button1);
            inputs.Add(UserInputs.Tab);
            inputs.Add(UserInputs.LetterKey);

            lock (mHandlerLock)
            {
                mPreviousHandler = handlerRef;

                if (ipAddress)
                {
                    mMaxLength = 15; // over-ride supplied value 
                    handlerRef = IpAddressInputHandler;
                }
                else
                {
                    handlerRef = StringInputHandler;
                }

                mApp.UpdateLine(mUpdateLine, mStringInput.ToString());
                mApp.EnableKeypadInputs(inputs);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="app">Payment application instance</param>
        /// <param name="handlerRef">User input handler</param>
        /// <param name="handlerLock">Lock object for the user input handler</param>
        /// <param name="maxLength">Maximum acceptable length of string input</param>
        /// <param name="updateLine">Indicates which line of the display to update on changes</param>
        public StringInputter(IPaymentApp app, ref UserInputHandler handlerRef, object handlerLock,
                              int maxLength, int updateLine)
            : this(app, ref handlerRef, handlerLock, maxLength, updateLine, string.Empty, false)
        {
        }

        /// <summary>
        /// Waits for input to finish then returns the completed input string
        /// </summary>
        /// <param name="handlerRef">Reference to the input handler in the POS component</param>
        /// <returns>The entered string</returns>
        public string WaitForFinish(ref UserInputHandler handlerRef)
        {
            mNotification.WaitOne();
            return Finish(ref handlerRef);
        }

        /// <summary>
        /// Returns the string that was input
        /// </summary>
        private string Finish(ref UserInputHandler handlerRef)
        {
            mApp.EnableKeypadInputs(InputLists.empty);
            lock (mHandlerLock)
            {
                //handlerRef = null;
                handlerRef = mPreviousHandler;
            }
            return mStringInput;
        }

        /// <summary>
        /// Returns true if 'cancel' selected/pressed
        /// </summary>
        public bool Cancelled
        {
            get
            {
                return mCancelPressed;
            }
        }

        public bool Button0
        {
            get { return mButton0Pressed; }
        }

        public bool Button1
        {
            get { return mButton1Pressed; }
        }

        public bool Tab
        {
            get { return mTabPressed; }
        }


        /// <summary>
        /// This is an input event handler that will build up a string from the inputs
        /// and will update the text onto line 2 of the display.
        /// </summary>
        /// <param name="input">Input event</param>
        private void StringInputHandler(UserInputs input, char character, int id)
        {
            // Handles user input events during amount entry
            if (input == UserInputs.Button1)
            {
                mButton1Pressed = true;
                mNotification.Set();
            }
            else if (input == UserInputs.Enter)
            {
                mNotification.Set();
            }
            else if (input == UserInputs.Cancel)
            {
                mCancelPressed = true;
                mNotification.Set();
            }
            else if (input == UserInputs.Button0)
            {
                mButton0Pressed = true;
                mNotification.Set();
            }
            else if (input == UserInputs.Tab)
            {
                mTabPressed = true;
                mNotification.Set();
            }
            else
            {
                if (input == UserInputs.Backspace)
                {
                    if (mStringInput.Length > 0)
                    {
                        mStringInput = mStringInput.Substring(0, mStringInput.Length - 1);
                    }
                }
                else
                {
                    if (mStringInput.Length < mMaxLength)
                        mStringInput += character;
                    else
                        mApp.Beep(WestPA.Standalone.BeepType.Default);
                }
                mApp.UpdateLine(mUpdateLine, mStringInput);
                mApp.UpdateLine(mUpdateLine + 1, string.Empty);
                mCancelPressed = false;
            }
        }

        /// <summary>
        /// This is an input event handler that will build up an IP date string from the inputs
        /// </summary>
        /// <param name="input">Input event</param>
        private void IpAddressInputHandler(UserInputs input, char character, int id)
        {
            if ((input == UserInputs.Enter) || (input == UserInputs.Button1))
            {
                // test for valid ip address - between ???.???.???.? and ???.???.???.???
                if ((mStringInput.Length >= mMaxLength - 2) &&
                    (mStringInput.Length <= mMaxLength))
                {
                    mNotification.Set();
                }
                else
                {
                    mApp.Beep(WestPA.Standalone.BeepType.Default);
                }
            }
            else if ((input == UserInputs.Cancel) || (input == UserInputs.Button0))
            {
                mCancelPressed = true;
                mNotification.Set();
            }
            else
            {
                // Update the display after these.
                if ((input >= UserInputs.Zero) && (input <= UserInputs.Nine))
                {
                    int number = (input - UserInputs.Zero);
                    if ((mStringInput.Length == 3) || (mStringInput.Length == 7) || (mStringInput.Length == 11))
                    {
                        mStringInput += ".";
                        mStringInput += number.ToString();
                    }
                    else if (mStringInput.Length < mMaxLength)
                    {
                        mStringInput += number.ToString();

                        // check if address valid
                        if ((mStringInput.Length == 3) || (mStringInput.Length == 7) ||
                            (mStringInput.Length == 11) || (mStringInput.Length == mMaxLength))
                        {
                            if (Convert.ToInt32((mStringInput.Substring(mStringInput.Length - 3))) > 255)
                            {
                                // remove all three digits
                                mStringInput = mStringInput.Remove(mStringInput.Length - 3, 3);
                                mApp.Beep(WestPA.Standalone.BeepType.Default);
                            }
                        }

                        if ((mStringInput.Length == 3) || (mStringInput.Length == 7) || (mStringInput.Length == 11))
                        {
                            mStringInput += ".";
                        }
                    }
                    else
                    {
                        mApp.Beep(WestPA.Standalone.BeepType.Default);
                    }
                }
                else if (input == UserInputs.Backspace)
                {
                    mStringInput = mStringInput.Remove(mStringInput.Length - 1, 1);
                }

                mApp.UpdateLine(mUpdateLine, mStringInput);
                mApp.UpdateLine(mUpdateLine + 1, String.Empty);
            }
        }
    }

    #endregion

    #region NUMERIC_INPUT

    public class NumericInputter
    {
        /// <summary>
        /// Records the value that was entered by either input handler
        /// </summary>
        private uint mNumericInput = 0;

        /// <summary>
        /// Used to signal that input has finished
        /// </summary>
        private AutoResetEvent mNotification;

        /// <summary>
        /// Local reference to the wider input handler lock object
        /// </summary>
        private readonly object mHandlerLock;

        /// <summary>
        /// Local reference to the payment application
        /// </summary>
        private IPaymentApp mApp;

        /// <summary>
        /// Defines where to display input value
        /// </summary>
        private readonly int mUpdateLine;

        /// <summary>
        /// set true if 'cancel' was pressed (twice if  mCancelAllowed == false)
        /// </summary>
        private bool mCancelPressed = false;

        /// <summary>
        /// set true if 'Tab' was pressed
        /// </summary>
        private bool mTabPressed = false;

        /// <summary>
        /// set true if amount input required
        /// </summary>
        private readonly bool mAmountRequired = false;

        private readonly ushort? mMaxLength = null;

        private readonly UserInputHandler mPreviousHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="app">Payment application instance</param>
        /// <param name="handlerRef">User input handler</param>
        /// <param name="handlerLock">Lock object for the user input handler</param>
        /// <param name="updateLine">Indicates which line of the display to update on changes</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="amountRequired">Indicates if an amount is being input</param>
        public NumericInputter(IPaymentApp app, ref UserInputHandler handlerRef, object handlerLock,
                               int updateLine, uint defaultValue, bool amountRequired) :
            this(app, ref handlerRef, handlerLock, updateLine, defaultValue, amountRequired, null)
        { }

        public NumericInputter(IPaymentApp app, ref UserInputHandler handlerRef, object handlerLock,
                               int updateLine, uint defaultValue, bool amountRequired, ushort? maxLength)
        {
            mNumericInput = defaultValue;
            mNotification = new AutoResetEvent(false);
            mApp = app;
            mUpdateLine = updateLine;
            mHandlerLock = handlerLock;
            mAmountRequired = amountRequired;
            mMaxLength = maxLength;

            List<UserInputs> inputs = new List<UserInputs>();
            for (UserInputs key = UserInputs.Zero; key <= UserInputs.Nine; ++key)
            {
                inputs.Add(key);
            }
            inputs.Add(UserInputs.Enter);
            inputs.Add(UserInputs.Cancel);
            inputs.Add(UserInputs.Backspace);
            inputs.Add(UserInputs.QuestionMark);
            inputs.Add(UserInputs.Button0);
            inputs.Add(UserInputs.Button1);
            inputs.Add(UserInputs.Tab);

            lock (mHandlerLock)
            {
                mPreviousHandler = handlerRef;

                if (mAmountRequired)
                {
                    handlerRef = AmountInputHandler;
                }
                else
                {
                    handlerRef = NumericInputHandler;
                }

                mApp.EnableKeypadInputs(inputs);
                if (mAmountRequired)
                    mApp.UpdateLine(mUpdateLine, (((double)mNumericInput) / 100).ToString("F2"));
                else
                    mApp.UpdateLine(mUpdateLine, mNumericInput.ToString());
            }
        }

        /// <summary>
        /// Waits for input to finish then returns the completed numeric string
        /// </summary>
        /// <param name="handlerRef">Reference to the input handler in the POS component</param>
        /// <returns>The entered number</returns>
        public uint WaitForFinish(ref UserInputHandler handlerRef)
        {
            Logging.Log.Spam("WaitForFinish ", new OSInputters());
            mNotification.WaitOne();
            return Finish(ref handlerRef);
        }

        /// <summary>
        /// Returns the value that was input
        /// </summary>
        private uint Finish(ref UserInputHandler handlerRef)
        {
            Logging.Log.Spam("Finish ", new OSInputters());
            mApp.EnableKeypadInputs(InputLists.empty);
            lock (mHandlerLock)
            {
                handlerRef = mPreviousHandler;
            }
            return mNumericInput;
        }

        /// <summary>
        /// Returns true if 'cancel' selected/pressed
        /// </summary>
        public bool Cancelled { get { return mCancelPressed; } }

        /// <summary>
        /// Returns true if 'Tab' selected/pressed
        /// </summary>
        public bool Tab { get { return mTabPressed; } }

        /// <summary>
        /// This is an input event handler that build an Amount value from the inputs
        /// </summary>
        /// <param name="input">Input event</param>
        private void AmountInputHandler(UserInputs input, char character, int id)
        {
            Logging.Log.Spam("AmountInputHandler ", new OSInputters());
            if ((input == UserInputs.Enter) || (input == UserInputs.Button1))
            {
                mNotification.Set();
            }
            else if (input == UserInputs.Cancel)
            {
                mCancelPressed = true;
                mNotification.Set();
            }
            else if (input == UserInputs.Tab || (input == UserInputs.Button0))
            {
                mTabPressed = true;
                mNotification.Set();
            }
            else
            {
                // Update the display after these.
                if ((input >= UserInputs.Zero) && (input <= UserInputs.Nine))
                {
                    int increment = (input - UserInputs.Zero);
                    if (((long)mNumericInput * 10) <= (uint.MaxValue - increment) &&
                        (mMaxLength != null && mNumericInput < Math.Pow(10, (double)mMaxLength)))
                    {
                        mNumericInput = (uint)((mNumericInput * 10) + increment);
                    }
                    else
                    {
                        mApp.Beep(WestPA.Standalone.BeepType.Default);
                    }
                }
                else if (input == UserInputs.Backspace)
                {
                    mNumericInput /= 10;
                }

                mApp.UpdateLine(mUpdateLine, (mNumericInput / 100.0).ToString("N2"));
                mApp.UpdateLine(mUpdateLine + 1, string.Empty);
            }
        }

        /// <summary>
        /// This is an input event handler that will build up a numeric value (uint) from the inputs
        /// </summary>
        /// <param name="input">Input event</param>
        private void NumericInputHandler(UserInputs input, char character, int id)
        {
            Logging.Log.Spam("NumericInputHandler ", new OSInputters());
            // Handles user input events during numeric entry
            if (input == UserInputs.Enter || input == UserInputs.Button1)
            {
                mNotification.Set();
            }
            else if (input == UserInputs.Cancel)
            {
                mCancelPressed = true;
                mNotification.Set();
            }
            else if (input == UserInputs.Tab || input == UserInputs.Button0)
            {
                mTabPressed = true;
                mNotification.Set();
            }
            else
            {
                // Update the display after these.
                if ((input >= UserInputs.Zero) && (input <= UserInputs.Nine))
                {
                    int increment = (input - UserInputs.Zero);
                    if (((long)mNumericInput * 10) <= (uint.MaxValue / 100 - increment) &&
                        (mMaxLength == null || (mMaxLength != null && mNumericInput < Math.Pow(10, (double)mMaxLength))))
                    {
                        mNumericInput = (uint)((mNumericInput * 10) + increment);
                    }
                    else
                    {
                        Beep.Normal();
                    }
                }
                else if (input == UserInputs.Backspace)
                {
                    mNumericInput /= 10;
                }

                mApp.UpdateLine(mUpdateLine, mNumericInput.ToString());
                mApp.UpdateLine(mUpdateLine + 1, string.Empty);
            }
        }
    }

    #endregion



}