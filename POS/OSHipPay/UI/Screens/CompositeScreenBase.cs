using System.Collections.Generic;
using System.Linq;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This class is a base class for screens that are a composition of other screens.
    /// The main function of the composite screen is the GetScreens() function.
    /// </summary>
    abstract class CompositeScreenBase : IScreen
    {
        /// <summary>
        /// Gets or sets value.
        /// </summary>
        public bool IsCancelled { get; protected set; }

        /// <summary>
        /// Gets an enumerator of all text blocks on a screen.
        /// </summary>
        public IEnumerable<TextBlock> AllTextBlocks => Enumerable.Empty<TextBlock>();

        /// <summary>
        /// Deliver one or more screens to the user interface. Each screen is displayed
        /// and then given the input stream. When the screen is completed then the next
        /// next screen is handled until there are no more screens.
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<IScreen> GetScreens();

        /// <summary>
        /// Get strings from the string library.
        /// </summary>
        protected static StringDefinitions Strings => ApplicationContext.Current.Strings;
    }
}