using System;
using System.Text;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// The Time editor is used to enter time values, ranging from 00:00 to 23:59.
    /// </summary>
    class TimeEditorScreen : StringEditorScreen
    {
        /// <summary>
        /// We don't allow more than 4 digits in a time value (HHmm).
        /// </summary>
        const int MaxBufferSize = 4;

        /// <summary>
        /// This is used to build the display value from the buffer.
        /// </summary>
        readonly StringBuilder displayBuffer = new StringBuilder(5);

        public TimeEditorScreen(string title, TimeSpan? value, bool isOptional)
            : base(title,
                   ((value != null) ? value.Value.ToString("HH:mm") : ""),
                   isOptional)
        {
        }

        /// <summary>
        /// The only visual change is that the displayed
        /// string contains a colon between the hours and the minutes.
        /// <see cref="StringEditorScreen.FormatBufferForDisplay"/>
        /// </summary>
        protected override string FormatBufferForDisplay(string buffer)
        {
            if (buffer.Length < 2)
            {
                return buffer;
            }

            displayBuffer.Length = 0;
            displayBuffer.Append(buffer);
            displayBuffer.Insert(2, ":");
            return displayBuffer.ToString();
        }

        /// <summary>
        /// Initialise the internal string buffer from the initial value.
        /// <see cref="StringEditorScreen.GetBuffer"/>
        /// </summary>
        /// <param name="initialValue">The initial value passed to the constructor.</param>
        /// <returns>The value without the colon.</returns>
        protected override string GetBuffer(string initialValue)
        {
            return initialValue.Replace(":", "");
        }

        /// <summary>
        /// Get the final result. The result is padded with zeroes if the user didn't
        /// enter all the 4 digits. The result also includes the colon between the hours
        /// and the minutes.
        /// <see cref="StringEditorScreen.FormatBufferForResult"/>        
        /// </summary>
        protected override string FormatBufferForResult(string buffer)
        {
            return FormatBufferForDisplay(buffer.PadRight(MaxBufferSize, '0'));
        }

        /// <summary>
        /// Check if the buffer is a valid time or not.
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <returns>True if the buffer is a valid time, otherwise false.
        /// There is no undetermined case for time values.</returns>
        protected override bool? IsValidBuffer(string buffer)
        {
            return buffer.Length <= MaxBufferSize
                   && FormatBufferForResult(buffer).ToTimeSpan("HH:mm") != null;
        }
    }
}