﻿using System.Collections.Generic;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// All screens must implement this interface.
    /// </summary>
    public interface IScreen
    {
        bool IsCancelled { get; }
        IEnumerable<TextBlock> AllTextBlocks { get; }
    }

    public interface IViewThreeLines
    {
        TextBlock Line1 { get; }
        TextBlock Line2 { get; }
        TextBlock Line3 { get; }
    }

    public interface IViewFourLines : IViewThreeLines
    {
        TextBlock Line4 { get; }
    }

    public interface IViewFiveLines : IViewFourLines
    {
        TextBlock Line5 { get; }
    }

    public interface IViewWithButtons
    {
        Button Left { get; }
        Button Right { get; }
    }

    /// <summary>
    /// Screens that handle input must implement this interface.
    /// </summary>
    public interface IHandleInput
    {
        /// <summary>
        /// The list of user inputs this screen should receive.
        /// </summary>
        InputsSpecification AcceptedInputs { get; }

        /// <summary>
        /// After the screen is displayed then this function is called with the current input stream.
        /// When this function returns the screen is considered completed.
        /// </summary>
        /// <param name="input"></param>
        void HandleInput(IEnumerable<char> input);
    }

}