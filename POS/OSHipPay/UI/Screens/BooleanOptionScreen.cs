using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enable and disable a boolean property
    /// </summary>
    class BooleanOptionScreen : OptionScreen
    {
        /// <summary>
        /// Source property get function
        /// </summary>
        readonly Func<bool> mGetter = null;

        /// <summary>
        /// Source property set function
        /// </summary>
        readonly Action<bool> mSetter = null;

        /// <summary>
        /// BooleanOptionScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="disableOption">Disable option text</param>
        /// <param name="enableOption">Enable option text</param>
        /// <param name="getProp">Source property getter</param>
        /// <param name="setProp">Source property setter</param>
        public BooleanOptionScreen(string title, string disableOption, string enableOption, Func<bool> getProp, Action<bool> setProp)
        {
            mGetter = getProp;
            mSetter = setProp;
            Initialise(title, disableOption, enableOption);
        }

        /// <summary>
        /// Gets and sets the current value of the settings with reference to the getter and setter
        /// functions provided in the constructor.
        /// <see cref="OptionScreen.CurrentValue"/>
        /// </summary>
        protected override bool CurrentValue
        {
            get
            {
                bool result = false;

                if (mGetter != null)
                {
                    result = mGetter();
                }
                return result;
            }

            set => mSetter?.Invoke(value);
        }
    }
}
