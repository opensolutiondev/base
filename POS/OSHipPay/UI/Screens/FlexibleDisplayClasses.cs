﻿using System.Drawing;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    class FlexibleDisplayClasses
    {
    }


    public class DefaultFlexibleDisplayElement : FlexibleDisplayElement
    {
        public DefaultFlexibleDisplayElement(string text, uint row, float width, int id, Color backColor)
            : base(text, SizeLarge, (int)row, width, true, id)
        {
            ShowBorder = false;
            BackgroundColour = backColor;
            TextColour = Color.Black;
            VisibleClick = false;
            IsClickable = true;
            HorizontalAlignment = StringAlignment.Center;
            VerticalAlignment = StringAlignment.Center;
        }

        public DefaultFlexibleDisplayElement(string text, uint row, float width, int id)
            : this(text, row, width, id, Color.White) { }
    }

    public class EmptyFlexibleDisplayElement : DefaultFlexibleDisplayElement
    {
        public EmptyFlexibleDisplayElement(uint row, float widthPercentage)
            : base(string.Empty, row, widthPercentage, 0)
        {
            IsClickable = false;
        }
    }

    public class DefaultFlexibleDisplay : FlexibleDisplay
    {
        public DefaultFlexibleDisplay()
            : base()
        {
            ContainerMargins = new Size(0, 0);
            ElementMargins = new Size(0, 0);
        }

        public DefaultFlexibleDisplay(uint minimumRows)
            : base((int)minimumRows)
        {
            ContainerMargins = new Size(0, 0);
            ElementMargins = new Size(0, 0);
        }
    }

}