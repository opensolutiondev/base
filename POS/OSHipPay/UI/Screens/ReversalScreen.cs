using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to perform a reversal transaction.
    /// </summary>
    class ReversalScreen : SimpleScreen
    {
        /// <summary>
        /// <see cref="SimpleScreen.Displayed"/>
        /// </summary>
        /// <param name="ui"></param>
        public override void Displayed(UserInterface ui)
        {
            // Reversal depends on having a previous transaction that was completed.
            string reference = TransactionContext.Last.ReferenceNumber;

            if (reference.IsNullOrEmpty())
            {
                ui.Alert(Strings.msg_nothingToReverse);
                IsCancelled = true;
            }
            else if (ui.CheckMerchantPassword(Strings.reversal) && ui.YesNo(Strings.confirm_reverse))
            {
                // run a reversal transaction
                TransactionContext.Current.NextTransaction = new TransactionData
                {
                    Type = TransactionType.Reversal,
                    RetrievalReference = reference,
                };

                // Display message while reversing
                string message = "{0}: {1}".Fmt(Strings.reference, reference);
                ui.Display(Strings.reversingTransaction, message);
            }
            else
            {
                IsCancelled = true;
            }
        }
    }
}