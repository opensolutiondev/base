

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// A simple string editor that displays asterisks for the input.
    /// </summary>
    class PasswordEditorScreen : StringEditorScreen
    {
        public PasswordEditorScreen(string title, string value, bool isOptional) : base(title, value, isOptional)
        {
            Line2.SetValue(Strings.enter_password).AlignCentre();
            Line3.AlignCentre();
        }

        /// <summary>
        /// <see cref="StringEditorScreen.FormatBufferForDisplay"/>
        /// </summary>
        protected override string FormatBufferForDisplay(string buffer)
        {
            // Display asterisks instead of the edited string.
            return new string('*', buffer.Length);
        }

        /// <summary>
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        protected override bool? IsValidBuffer(string buffer)
        {
            // Don't accepts empty passwords and long passwords (> 100 chars).
            return buffer.IsNotNullOrEmpty() && buffer.Length < 100;
        }
    }
}