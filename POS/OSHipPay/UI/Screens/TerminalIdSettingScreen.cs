﻿using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen confirms and edits the terminal id.
    /// </summary>
    class TerminalIdSettingScreen : EditSettingScreenBase
    {
        public string TerminalId { get; set; }

        /// <summary>
        /// TerminalIdSettingScreen constructor.
        /// </summary>
        /// <param name="terminalId">Terminal id</param>
        /// <param name="canCancel">true/false</param>
        public TerminalIdSettingScreen(string terminalId, bool canCancel) : base(canCancel)
        {
            TerminalId = terminalId ?? String.Empty;
        }

        /// <summary>
        /// <see cref="EditSettingScreenBase.Value"/>
        /// </summary>
        public override string Value => TerminalId;

        /// <summary>
        /// <see cref="EditSettingScreenBase.ConfirmTitle"/>
        /// </summary>
        public override string ConfirmTitle => Strings.mgmt_setTerminalID;

        /// <summary>
        /// <see cref="EditSettingScreenBase.GetEditor"/>
        /// </summary>
        /// <returns></returns>
        protected override StringEditorScreen GetEditor()
        {
            var editor = new TerminalIdEditorScreen(Strings.mgmt_setTerminalID, TerminalId, false);
            editor.Completed += (sender, args) => TerminalId = args.Value;
            return editor;
        }
    }
}