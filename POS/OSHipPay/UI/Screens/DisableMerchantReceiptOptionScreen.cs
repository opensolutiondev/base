﻿namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enable and disable printout of the merchant's receipt.
    /// </summary>
    class DisableMerchantReceiptOptionScreen : OptionScreen
    {
        /// <summary>
        /// DisableMerchantReceiptOptionScreen constructor.
        /// </summary>
        public DisableMerchantReceiptOptionScreen()
            : base(Strings.opt_merchantReceiptEnabled, Strings.opt_disableMerchantReceipt, Strings.opt_enableMerchantReceipt)
        {
        }

        /// <summary>
        /// Gets or sets the value of DisableMerchantReceipt.
        /// <see cref="OptionScreen.CurrentValue"/>
        /// </summary>
        protected override bool CurrentValue
        {
            get => !ApplicationContext.Current.Settings.DisableMerchantReceipt;
            set => ApplicationContext.Current.Settings.DisableMerchantReceipt = !value;
        }
    }
}
