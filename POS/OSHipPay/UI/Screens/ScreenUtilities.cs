﻿using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Miscellaneous utility functions for screens.
    /// </summary>
    static class ScreenUtilities
    {
        /// <summary>
        /// Sets one or more lines of text on the screen.
        /// </summary>
        /// <param name="screen">A screen with four lines of text.</param>
        /// <param name="lines">An array of strings.</param>
        public static void SetScreenLines(this IViewFourLines screen, params string[] lines)
        {
            SetScreenLines(screen, DisplayData.Alignment.Left, lines);

        }
        /// <summary>
        /// Sets one or more lines of text on the screen.
        /// </summary>
        /// <param name="screen">A screen with four lines of text.</param>
        /// <param name="alignment">Text alignment</param>
        /// <param name="lines">An array of strings.</param>
        public static void SetScreenLines(this IViewFourLines screen, DisplayData.Alignment alignment, params string[] lines)
        {
            switch (alignment)
            {
                case DisplayData.Alignment.Centre:
                    screen.Line1.SetValue(lines.GetValueOrDefault(0)).AlignCentre();
                    screen.Line2.SetValue(lines.GetValueOrDefault(1)).AlignCentre();
                    screen.Line3.SetValue(lines.GetValueOrDefault(2)).AlignCentre();
                    screen.Line4.SetValue(lines.GetValueOrDefault(3)).AlignCentre();
                    break;
                case DisplayData.Alignment.Right:
                    screen.Line1.SetValue(lines.GetValueOrDefault(0)).AlignRight();
                    screen.Line2.SetValue(lines.GetValueOrDefault(1)).AlignRight();
                    screen.Line3.SetValue(lines.GetValueOrDefault(2)).AlignRight();
                    screen.Line4.SetValue(lines.GetValueOrDefault(3)).AlignRight();
                    break;
                default:
                    screen.Line1.SetValue(lines.GetValueOrDefault(0));
                    screen.Line2.SetValue(lines.GetValueOrDefault(1));
                    screen.Line3.SetValue(lines.GetValueOrDefault(2));
                    screen.Line4.SetValue(lines.GetValueOrDefault(3));
                    break;
            }     
        }

        /// <summary>
        /// Sets one or more lines of text on the screen.
        /// </summary>
        /// <param name="screen">A screen with five lines of text.</param>
        /// <param name="lines">An array of strings.</param>
        public static void SetScreenLines(this IViewFiveLines screen, params string[] lines)
        {
            SetScreenLines(screen, DisplayData.Alignment.Left, lines);

        }

        /// <summary>
        /// Sets one or more lines of text on the screen.
        /// </summary>
        /// <param name="screen">A screen with five lines of text.</param>
        /// <param name="lines">An array of strings.</param>
        public static void SetScreenLines(this IViewFiveLines screen, DisplayData.Alignment alignment, params string[] lines)
        {
            ((IViewFourLines)screen).SetScreenLines(alignment, lines);

            switch (alignment)
            {
                case DisplayData.Alignment.Centre:
                    screen.Line5.SetValue(lines.GetValueOrDefault(4)).AlignCentre();
                    break;
                case DisplayData.Alignment.Right:
                    screen.Line5.SetValue(lines.GetValueOrDefault(4)).AlignRight();
                    break;
                default:
                    screen.Line5.SetValue(lines.GetValueOrDefault(4));
                    break;
            }
        }
    }
}