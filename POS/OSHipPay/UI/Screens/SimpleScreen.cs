using System.Collections.Generic;
using System.Linq;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Base for simple screens.
    /// </summary>
    abstract class SimpleScreen : IScreen
    {
        public bool IsCancelled { get; protected set; }
        public IEnumerable<TextBlock> AllTextBlocks => Enumerable.Empty<TextBlock>();

        protected static StringDefinitions Strings => ApplicationContext.Current.Strings;

        /// <summary>
        /// Display message on screen.
        /// </summary>
        /// <param name="ui"></param>
        public abstract void Displayed(UserInterface ui);
    }
}