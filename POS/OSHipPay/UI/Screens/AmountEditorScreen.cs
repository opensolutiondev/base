using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enter an amount.
    /// </summary>
    class AmountEditorScreen : StringEditorScreen
    {
        /// <summary>
        /// True if the screen should not allow entering of decimals.
        /// </summary>
        public bool DisableDecimals;


        /// <summary>
        /// Gets integer value considering if decimals is allowed or not. 
        /// </summary>
        public virtual uint Amount
        {
            get
            {
                var value = Value.ToUInt32(0);
                if (DisableDecimals)
                {
                    value *= 100;
                }
                return value;
            }
        }
        public AmountEditorScreen()
        {

        }

        /// <summary>
        /// AmountEditorScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="line2">Text on line 2</param>
        /// <param name="initialValue">Initial value for the amount</param>
        /// <param name="isOptional">Indicates if the value is optional or not</param>
        public AmountEditorScreen(string title, string line2, uint initialValue, bool isOptional, Boolean showMenu)
            : base(title, initialValue > 0 ? initialValue.ToString() : String.Empty, isOptional, showMenu)
        {
            Logging.Log.Debug("AmountEditorScreen initializing title: "+title+" initValue "+initialValue, new AmountEditorScreen());
            if (line2.IsNotNullOrEmpty())
            {
                Line2.SetValue(line2).AlignCentre().SizeSmall();
            }

            Line3.AlignCentre();
        }

        /// <summary>
        /// AmountEditorScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="line2">Text on line 2</param>
        /// <param name="initialValue">Initial value for the amount</param>
        /// <param name="isOptional">Indicates if the value is optional or not</param>
        public AmountEditorScreen(string title, string line2, uint initialValue, bool isOptional)
            : base(title, initialValue > 0 ? initialValue.ToString() : String.Empty, isOptional)
        {
            if (line2.IsNotNullOrEmpty())
            {
                Line2.SetValue(line2).AlignCentre().SizeSmall(); 
            }

            Line3.AlignCentre();
        }

        /// <summary>
        /// <see cref="StringEditorScreen.FormatBufferForDisplay"/>
        /// </summary>
        protected override string FormatBufferForDisplay(string buffer)
        {
            decimal value = buffer.ToUInt32(0);
            if (!DisableDecimals)
            {
                value = value/100m;
            }

            //Don't show currency until we have decided how to handle it in the pos.
            return value.ToString("N2", Strings.GetCultureInfo());
        }

        /// <summary>
        /// Validate amount against max allowed value. 
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="editValue">Value to validate</param>
        /// <returns>true/false</returns>
        protected override bool? IsValidBuffer(string editValue)
        {
            return editValue.ToInt64(0) < uint.MaxValue;
        }
    }
}