using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Screen for confirming and editing settings that are IPv4 addresses.
    /// </summary>
    class IpAddressSettingScreen : EditSettingScreenBase
    {
        readonly string _title;

        /// <summary>
        /// The address that should be edited.
        /// </summary>
        public string Address;

        /// <summary>
        /// Indicates if an empty/null value is allowed for the address.
        /// </summary>
        public bool IsOptional;

        /// <summary>
        /// IpAddressSettingScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="address">IP address</param>
        /// <param name="canCancel">true/false</param>
        public IpAddressSettingScreen(string title, string address, bool canCancel) : base(canCancel)
        {
            _title = title;
            Address = address ?? String.Empty;
        }

        /// <summary>
        /// <see cref="EditSettingScreenBase.Value"/>
        /// </summary>
        public override string Value => Address;

        /// <summary>
        /// <see cref="EditSettingScreenBase.ConfirmTitle"/>
        /// </summary>
        public override string ConfirmTitle => _title;

        /// <summary>
        /// <see cref="EditSettingScreenBase.GetEditor"/>
        /// </summary>
        /// <returns></returns>
        protected override StringEditorScreen GetEditor()
        {
            var editor = new IPAddressEditorScreen(_title, Address, IsOptional);
            editor.Completed += (sender, args) => Address = args.Value;
            return editor;
        }
    }
}