

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to edit an IP port value.
    /// </summary>
    class IpPortEditorScreen : StringEditorScreen
    {
        /// <summary>
        /// IpPortEditorScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="value">Initial value</param>
        /// <param name="isOptional">true/false</param>
        public IpPortEditorScreen(string title, int value, bool isOptional)
            : base(title, value.ToString(), isOptional)
        {
        }

        /// <summary>
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="value">IP Port value to validate</param>
        /// <returns></returns>
        protected override bool? IsValidBuffer(string value)
        {
            if (value.IsNullOrEmpty())
                return IsOptional ? true : (bool?)null;
            int v = value.ToInt32(-1);
            return v > 0 && v < 0x10000;
        }
    }
}