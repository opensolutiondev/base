﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSHipPay.UI.Screens
{
    class OSStringEditorScreen: EditSettingScreenBase
    {
        readonly string _title;

        /// <summary>
        /// The address that should be edited.
        /// </summary>
        public string GivenValue;

        /// <summary>
        /// Indicates if an empty/null value is allowed for the address.
        /// </summary>
        public bool IsOptional;

        /// <summary>
        /// IpAddressSettingScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="address">IP address</param>
        /// <param name="canCancel">true/false</param>
        public OSStringEditorScreen(string title, string value, bool canCancel) : base(canCancel)
        {
            _title = title;
            GivenValue = value ?? String.Empty;
        }

        /// <summary>
        /// <see cref="EditSettingScreenBase.Value"/>
        /// </summary>
        public override string Value => GivenValue;

        /// <summary>
        /// <see cref="EditSettingScreenBase.ConfirmTitle"/>
        /// </summary>
        public override string ConfirmTitle => _title;

        /// <summary>
        /// <see cref="EditSettingScreenBase.GetEditor"/>
        /// </summary>
        /// <returns></returns>
        protected override StringEditorScreen GetEditor()
        {
            var editor = new StringEditorScreen(_title, GivenValue, IsOptional);
            editor.Completed += (sender, args) => GivenValue = args.Value;
            return editor;
        }
    }


}