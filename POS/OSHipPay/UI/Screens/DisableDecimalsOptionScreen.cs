namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enable and disable decimal handling in the amounts screens.
    /// </summary>
    class DisableDecimalsOptionScreen : OptionScreen
    {
        /// <summary>
        /// Consructor
        /// </summary>
        public DisableDecimalsOptionScreen()
            : base(Strings.opt_decimalsEnabled, Strings.opt_disableDecimals, Strings.opt_enableDecimals)
        {
        }

        /// <summary>
        /// Gets or sets the value of DisableDecimals. Note that in the user interface it's called
        /// "enable decimals" but in the registry it's "disable decimals" (because the default value must
        /// be decimals enabled, i.e. false), to we invert the value here.
        /// <see cref="OptionScreen.CurrentValue"/>
        /// </summary>
        protected override bool CurrentValue
        {
            get => !ApplicationContext.Current.Settings.DisableDecimals;
            set => ApplicationContext.Current.Settings.DisableDecimals = !value;
        }
        
    }
}