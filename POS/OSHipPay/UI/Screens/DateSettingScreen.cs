using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Screen for confirming and editing settings that are IPv4 addresses.
    /// </summary>
    class DateSettingScreen : EditSettingScreenBase
    {
        readonly string _title;

        public string date;

        public DateSettingScreen():base(true) { }

        public DateSettingScreen(string title, string dt, bool canCancel) : base(canCancel)
        {
            Logging.Log.Debug("DateSettingScreen initializing", new DateSettingScreen());
            _title = title;
            date = dt ?? String.Empty;
        }

        public override string Value => date;

        public override string ConfirmTitle => _title;

        protected override StringEditorScreen GetEditor()
        {
            var editor = new DateEditorScreen(_title, date, false);
            editor.Completed += (sender, args) => date = args.Value;
            return editor;
        }
    }
}