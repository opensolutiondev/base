using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// The OkCancelScreen displays a message and one or two buttons. The buttons are usually named "Ok",
    /// and "Cancel" but the texts can be anything. If the cancel button's text is set to null or the empty
    /// string then that button is not displayed.
    /// The Enter key and the Ok button ends the screen with IsCancelled = false. The Cancel key or the Cancel
    /// button ends the screen with IsCancelled = true.
    /// </summary>
    class OkCancelScreen : FourLinesWithButtonsScreen, IHandleInput
    {
        /// <summary>
        /// The inputs used in this screen.
        /// </summary>
        static readonly List<UserInputs> _Inputs = new List<UserInputs>
        {
            UserInputs.Enter,
            UserInputs.Cancel,
            UserInputs.Button0,
            UserInputs.Button1,
        };

        /// <summary>
        /// OkCancelScreen constructor.
        /// </summary>
        /// <param name="lines">Up to four lines of text to display.</param>
        /// <param name="okText">Required. The text of the OK button.</param>
        /// <param name="cancelText">Optional. The text of the cancel button.
        /// If null then the button will not be shown.</param>
        public OkCancelScreen(string[] lines, string okText, string cancelText)
        {
            Init(lines, okText, cancelText, DisplayData.Alignment.Left);

        }

        /// <summary>
        /// OkCancelScreen constructor.
        /// </summary>
        /// <param name="lines">Up to four lines of text to display.</param>
        /// <param name="okText">Required. The text of the OK button.</param>
        /// <param name="cancelText">Optional. The text of the cancel button.
        /// If null then the button will not be shown.</param>
        /// <param name="alignment">Optional. Text alignment.</param>
        public OkCancelScreen(string[] lines, string okText, string cancelText, DisplayData.Alignment alignment)
        {
            Init(lines, okText, cancelText, alignment);

        }

        public void Init(string[] lines, string okText, string cancelText, DisplayData.Alignment alignment)
        {
            this.SetScreenLines(alignment, lines);

            if (cancelText.IsNotNullOrEmpty())
            {
                Left.Show(cancelText, Color.DarkRed, null);
            }

            Right.Show(okText, Color.DarkGreen, null);
        }

        InputsSpecification IHandleInput.AcceptedInputs => new InputsSpecification
        {
            Inputs  = _Inputs,
        };

        void IHandleInput.HandleInput(IEnumerable<char> input)
        {
            // The screen ends on the first input which is either a button press, a Enter or Cancel key or a timeout.

            var key = input.First();

            IsCancelled = key == InputCharacter.Cancel || key == InputCharacter.Button0;
        }
    }
}
