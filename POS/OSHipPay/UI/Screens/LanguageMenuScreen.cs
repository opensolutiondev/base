

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This menu enables the user to select a language for the user interface.
    /// The options are taken from the currently installed languages.
    /// </summary>
    class LanguageMenuScreen : MenuScreenBase
    {
        /// <summary>
        /// LanguageMenuScreen constructor.
        /// </summary>
        public LanguageMenuScreen()
        {
            Title = Strings.title_selectLanguage;

            var langs = ApplicationContext.Current.InstalledLanguages;

            Add("English", () => SetLanguage("en_GB"));
            if (langs.Contains("swe")) Add("Svenska", () => SetLanguage("sv_SE"));
            if (langs.Contains("fin")) Add("Suomi", () => SetLanguage("fi_FI"));
            if (langs.Contains("nor")) Add("Norsk", () => SetLanguage("nn_NO"));
            if (langs.Contains("dan")) Add("Dansk", () => SetLanguage("da_DK"));
            if (langs.Contains("pol")) Add("Polski", () => SetLanguage("pl_PL"));
            if (langs.Contains("deu") || langs.Contains("ger/deu")) //Should be removed when the PA sends the correct code
            {
                Add("Deutsch", () => SetLanguage("de_DE"));
            }
        }

        /// <summary>
        /// Changes the language to use and updates the registry with chosen language. 
        /// </summary>
        /// <param name="languageCode">Language code</param>
        void SetLanguage(string languageCode)
        {
            var ctx = ApplicationContext.Current;
            ctx.Strings.SetLanguage(languageCode);
            ctx.Settings.SelectedLanguage = languageCode;
        }
    }
}