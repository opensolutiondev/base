using System;
using System.Collections.Generic;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen handles purchases and refunds. The user is asked for an amount and
    /// then the appropriate transaction is created.
    /// </summary>
    class PurchaceRefundScreen : SimpleScreen
    {
        readonly TransactionType _transactionType;

        PurchaceRefundScreen()
        {

        }

        public PurchaceRefundScreen(TransactionType transactionType)
        {
            Logging.Log.Debug("PurchaceRefundScreen initializing with type " + transactionType.ToString(), new PurchaceRefundScreen());
            if (TransactionContext.Current.NextTransaction != null)
                throw new InvalidOperationException("A transaction has already been selected.");

            if (transactionType != TransactionType.Purchase &&
                transactionType != TransactionType.Refund)
                throw new ArgumentOutOfRangeException(nameof(transactionType));

            _transactionType = transactionType;

        }

        /// <summary>
        /// <see cref="SimpleScreen.Displayed"/>
        /// </summary>
        /// <param name="ui"></param>
        public override void Displayed(UserInterface ui)
        {
            Logging.Log.Debug("PurchaceRefundScreen.Displayed initializing ", new PurchaceRefundScreen());
            string caption = null; 

            switch (_transactionType)
            {
                case TransactionType.Purchase:
                    caption = Strings.Purchase;
                    break;
                case TransactionType.Refund:
                    caption = Strings.refund;
                    break;
                default:
                    break;
            }
   
            IList<POSTransactionDescriptor> posTransactionDescriptors = SessionContext.Current.Terminal.TerminalConfig.DebitCreditParameters_TransactionDescriptors;

            POSTransactionDescriptor posTransactionDescriptor = posTransactionDescriptors.FirstOrDefault(t => t.TransactionType == _transactionType);

            //Password protect transaction types dependent on PPL Configuration. 
            if (posTransactionDescriptor != null && (!posTransactionDescriptor.PasswordProtected || ui.CheckMerchantPassword(caption)))
            {
                if (!IsCancelled)
                {
                    if (_transactionType == TransactionType.Purchase) // && Standalone
                    {
                        uint totalAmount = ui.RequestAmountWithMenu(caption, Strings.enterAmount).GetValueOrDefault(0);
                        Logging.Log.Debug("PurchaceRefundScreen.Displayed amount "+totalAmount.ToString(), new PurchaceRefundScreen());
                        if (totalAmount > 0)
                        {
                            TransactionType transactionType = _transactionType;

                            TransactionContext.Current.NextTransaction = new TransactionData
                            {
                                Type = transactionType,
                                Amounts = new TransactionAmounts(totalAmount, 0 /*cashbackAmount.Value*/, 0 /*vatAmount.Value*/),
                            };
                        }
                        else
                        {
                            IsCancelled = true;
                        }
                    }
                    else
                    {
                        uint totalAmount = ui.RequestAmount(caption, Strings.enterAmount).GetValueOrDefault(0);
                        if (totalAmount > 0)
                        {
                            TransactionType transactionType = _transactionType;

                            TransactionContext.Current.NextTransaction = new TransactionData
                            {
                                Type = transactionType,
                                Amounts = new TransactionAmounts(totalAmount, 0 /*cashbackAmount.Value*/, 0 /*vatAmount.Value*/),
                            };
                        }
                        else
                        {
                            IsCancelled = true;
                        }
                    }
                }
            }
            else
            {
                IsCancelled = true;
            }
        }
    }
}