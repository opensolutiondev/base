﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen displays the value of a setting to the user. The user can choose to confirm the setting,
    /// choose to edit the setting of (if configured) cancel.
    /// </summary>
    class ConfirmSettingScreen : FourLinesWithButtonsScreen, IHandleInput
    {
        static readonly List<UserInputs> _Inputs = new List<UserInputs>
        {
            UserInputs.Button0,
            UserInputs.Button1,
            UserInputs.Enter,
            UserInputs.Backspace,
            UserInputs.Cancel
        };

        static readonly List<UserInputs> _InputsNoCancel = new List<UserInputs>
        {
            UserInputs.Button0,
            UserInputs.Button1,
            UserInputs.Enter,
            UserInputs.Backspace,
        };

        readonly bool _canCancel;

        public bool IsConfirmed { get; set; }

        /// <summary>
        /// ConfirmSettingScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="value">Value to confirm</param>
        /// <param name="canCancel">true/false</param>
        public ConfirmSettingScreen(string title, string value, bool canCancel)
        {
            Right.Show(UserInterface.Strings.accept, Color.Green, Color.White);
            Left.Show(UserInterface.Strings.change);
            Line1.SetValue(title).SizeLarge().AlignCentre();
            Line3.SetValue(value).AlignCentre();

            _canCancel = canCancel;
        }
        public ConfirmSettingScreen(string title, string value, bool canCancel, bool otherConstructor)
        {
            Right.Show(UserInterface.Strings.no, Color.Green, Color.White);
            Left.Show(UserInterface.Strings.yes);
            Line1.SetValue(title).SizeLarge().AlignCentre();
            Line3.SetValue(value).AlignCentre();

            _canCancel = canCancel;
        }
        public InputsSpecification AcceptedInputs => _canCancel ? _Inputs : _InputsNoCancel;

        public void HandleInput(IEnumerable<char> input)
        {
            var key = input.First();
            switch (key)
            {
                case InputCharacter.Button1:
                case InputCharacter.Enter:
                    // Value is ok
                    IsConfirmed = true;
                    break;

                case InputCharacter.Button0:
                case InputCharacter.Backspace:
                    // Value is not ok
                    IsConfirmed = false;
                    break;

                default:
                    // Cancel
                    IsCancelled = true;
                    break;
            }
        }
    }

    class CompletedEventArgs<T> : EventArgs where T : IScreen
    {
        public T Screen { get; set; }

        public CompletedEventArgs(T screen)
        {
            Screen = screen;
        }
    }
}
