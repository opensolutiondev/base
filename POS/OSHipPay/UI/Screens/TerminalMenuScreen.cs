using System.Collections.Generic;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen displays the Terminal Setup Menu.
    /// This screen is shown when the user selects Terminal Setup Menu in the transaction menu.
    /// </summary>
    class TerminalMenuScreen : MenuScreenBase
    {
        IScreen _selectedFunction;

        /// <summary>
        /// TerminalMenuScreen constructor.
        /// </summary>
        public TerminalMenuScreen()
        {
            Title = Strings.setupMenu_title;
           
            Add(Strings.setupMenu_language, () => _selectedFunction = new LanguageMenuScreen());
            Add(Strings.setupMenu_setup, () => _selectedFunction = new TerminalSettingsScreen());

            TerminalStatus tstat = SessionContext.Current.Terminal;

            if ((tstat.AvailableFeatures != null) &&
                (tstat.AvailableFeatures.Contains(TransactionFeatures.Tipping)))
            {
                Add(Strings.opt_tipping, () => _selectedFunction = new TipOptionScreen());
            }
            Add(Strings.opt_decimalsEnabled, () => _selectedFunction = new DisableDecimalsOptionScreen());
            Add(Strings.opt_merchantReceiptEnabled, () => _selectedFunction = new DisableMerchantReceiptOptionScreen());
            Add(Strings.setupMenu_managment, () => ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true);
        }

        /// <summary>
        /// <see cref="CompositeScreenBase.GetScreens"/>
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<IScreen> GetScreens()
        {
            return MyGetScreens(base.GetScreens());
        }

        /// <summary>
        /// Gets an enumerator of menupages.
        /// </summary>
        /// <param name="menuPages"></param>
        /// <returns></returns>
        IEnumerable<IScreen> MyGetScreens(IEnumerable<IScreen> menuPages)
        {
            foreach (var page in menuPages)
            {
                yield return page;
            }
            if (_selectedFunction != null)
            {
                yield return _selectedFunction;
            }
        }
    }
}