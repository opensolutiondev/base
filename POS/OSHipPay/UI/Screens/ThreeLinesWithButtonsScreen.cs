﻿using System.Collections.Generic;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// A screen with three lines of text and two buttons at the bottom.
    /// </summary>
    class ThreeLinesWithButtonsScreen : IScreen, IViewThreeLines, IViewWithButtons
    {
        /// <summary>
        /// Convenient access to the localized strings.
        /// </summary>
        protected static StringDefinitions Strings => ApplicationContext.Current.Strings;

        /// <summary>
        /// The first line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line1 { get; }

        /// <summary>
        /// The second line of text on the screen.
        /// </summary>
        public TextBlock Line2 { get; }

        /// <summary>
        /// The third line of text on the screen.
        /// </summary>
        public TextBlock Line3 { get; }

        /// <summary>
        /// The left button on the screen.
        /// </summary>
        public Button Left { get; }

        /// <summary>
        /// The right button on the screen.
        /// </summary>
        public Button Right { get; }

        public ThreeLinesWithButtonsScreen()
        {
            Line1 = new TextBlock(0);
            Line2 = new TextBlock(1);
            Line3 = new TextBlock(2);
            Left = new Button();
            Right = new Button();
        }

        /// <summary>
        /// When the screen is done this property indicates whether the user cancelled the screen or not.
        /// Note that only some screens can be cancelled.
        /// </summary>
        public bool IsCancelled { get; protected set; }

        /// <summary>
        /// Access all lines of text on the screen as an IEnumerable, for operations that work all the lines at once.
        /// </summary>
        public virtual IEnumerable<TextBlock> AllTextBlocks
        {
            get
            {
                yield return Line1;
                yield return Line2;
                yield return Line3;
            }
        }
    }

    /// <summary>
    /// A screen with four lines of text and two buttons at the bottom.
    /// </summary>
    class FourLinesWithButtonsScreen : ThreeLinesWithButtonsScreen, IViewFourLines
    {
        /// <summary>
        /// The fourth line of text on the screen.
        /// </summary>
        public TextBlock Line4 { get; }

        public FourLinesWithButtonsScreen()
        {
            Line4 = new TextBlock(0);
        }

        public override IEnumerable<TextBlock> AllTextBlocks => base.AllTextBlocks.Append(Line4);
    }

    /// <summary>
    /// A screen with five lines of text. There is no space for buttons on this screen.
    /// </summary>
    class FiveLinesScreen : IScreen, IViewFiveLines
    {
        /// <summary>
        /// The first line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line1 { get; }

        /// <summary>
        /// The second line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line2 { get; }

        /// <summary>
        /// The third line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line3 { get; }

        /// <summary>
        /// The fourth line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line4 { get; }

        /// <summary>
        /// The fifth line of text on the screen (at the top).
        /// </summary>
        public TextBlock Line5 { get; }

        /// <summary>
        /// When the screen is done this property indicates whether the user cancelled the screen or not.
        /// Note that only some screens can be cancelled.
        /// </summary>
        public bool IsCancelled { get; protected set; }

        /// <summary>
        /// Access all lines of text on the screen as an IEnumerable, for operations that work all the lines at once.
        /// </summary>
        IEnumerable<TextBlock> IScreen.AllTextBlocks
        {
            get
            {
                yield return Line1;
                yield return Line2;
                yield return Line3;
                yield return Line4;
                yield return Line5;
            }
        }

        public FiveLinesScreen()
        {
            Line1 = new TextBlock(0);
            Line2 = new TextBlock(1);
            Line3 = new TextBlock(2);
            Line4 = new TextBlock(3);
            Line5 = new TextBlock(4);
        }
    }
}