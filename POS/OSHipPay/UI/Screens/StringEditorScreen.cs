﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// The string editor screen enables the user to edit something. The StringEditorScreen class is the base class
    /// for all different kinds of editors and let's the user edit an arbitrary string. Subclasses might
    /// impose different restrictions on the edited value, e.g. ip-addresses.
    /// </summary>
    class StringEditorScreen : FourLinesWithButtonsScreen, IHandleInput
    {
        /// <summary>
        /// This event fires when the user presses Enter or the Ok button and the value is valid.
        /// </summary>
        public event EventHandler<EditorCompletedEventArgs> Completed;

        /// <summary>
        /// True if it's ok to return an empty value. If this is false and the initial value is empty
        /// then the screen cannot be cancelled. If this is false and the initial value is not empty
        /// then the screen can be cancelled.
        /// </summary>
        public bool IsOptional { get; set; }

        /// <summary>
        /// Gets or sets a string value
        /// </summary>
        public string Value { get; private set; }

        string _buffer;
        readonly bool _canCancel;
        public StringEditorScreen()
        {

        }

        /// <summary>
        /// StringEditorScreen constructor.
        /// </summary>
        /// <param name="title">A title to display at the top of the screen</param>
        /// <param name="initialValue">The initial value</param>
        /// <param name="isOptional">True if an empty result is acceptable</param>
        public StringEditorScreen(string title,
                                  string initialValue,
                                  bool isOptional)
        {
            Value = initialValue ?? String.Empty;
            _canCancel = isOptional || initialValue.IsNotNullOrEmpty();
            IsOptional = isOptional;

            Right.Show(Strings.okButton, Color.Green, Color.White);
            if (_canCancel)
            {
                Left.Show(Strings.cancel, Color.DarkRed, Color.White);
            }
            Line1.SetValue(title).SizeLarge().AlignCentre();
            Line3.SetValue(String.Empty);
        }

        /// <summary>
        /// StringEditorScreen constructor.
        /// </summary>
        /// <param name="title">A title to display at the top of the screen</param>
        /// <param name="initialValue">The initial value</param>
        /// <param name="isOptional">True if an empty result is acceptable</param>
        /// <param name="showMenu">True if gray menu button is shown</param>
        public StringEditorScreen(string title,
                                  string initialValue,
                                  bool isOptional,
                                  Boolean showMenu)
        {
            Value = initialValue ?? String.Empty;
            IsOptional = isOptional;
            _canCancel = true;
            Right.Show(Strings.payButton, Color.Green, Color.White);
            if (showMenu)
            {
                Left.Show(Strings.menuButton, Color.DarkGray, Color.White);
            }
            Line1.SetValue(title).SizeLarge().AlignCentre();
            Line3.SetValue(String.Empty);
        }


        public InputsSpecification AcceptedInputs => UserInputsLists.EditString;

        public void HandleInput(IEnumerable<char> inputStream)
        {
            _buffer = GetBuffer(Value);
            Line3.Update(FormatBufferForDisplay(_buffer));
            Edit(inputStream);
            if (!IsCancelled)
            {
                Value = FormatBufferForResult(_buffer);
                var handler = Completed;
                handler?.Invoke(this, new EditorCompletedEventArgs(Value));
            }
        }

        /// <summary>
        /// Reset the edited value.
        /// </summary>
        public virtual void Reset()
        {
            _buffer = String.Empty;
        }

        /// <summary>
        /// Format the buffer value for displaying on the screen. In most cases the buffer value is just
        /// displayed as it is but for some cases the display is different.
        /// Example: for IP addresses the buffer value "172016001002" should be displayed as "172.016.001.002".
        /// Example 2: passwords are displayed as asterisks.
        /// </summary>
        /// <param name="buffer">The current buffer value.</param>
        protected virtual string FormatBufferForDisplay(string buffer) { return buffer; }

        /// <summary>
        /// Create a buffer from an initial value. In most cases the buffer does not need formatting and
        /// the initial value is simply copied to the buffer.
        /// Example: for IP addresses the value "171.16.1.2" should get a buffer of "172016001002".
        /// </summary>
        /// <param name="value">The current buffer value</param>
        protected virtual string GetBuffer(string value) { return value; }

        /// <summary>
        /// Format the buffer value as a result. In most cases the buffer value is the same but
        /// sometimes the buffer value must be formatted before it's return from this screen.
        /// Example: for IP addresses the buffer "172016001002" should be returned as "172.16.1.2"
        /// </summary>
        /// <param name="buffer">The current buffer value</param>
        protected virtual string FormatBufferForResult(string buffer) { return buffer; }

        /// <summary>
        /// Return true if the current buffer is a valid.
        /// Return false if the current buffer is definetly invalid.
        /// Return null if the current buffer needs more input to become valid.
        /// </summary>
        /// <param name="buffer">The current buffer value</param>
        protected virtual bool? IsValidBuffer(string buffer) { return buffer.IsNotNullOrEmpty() || IsOptional; }

        void Edit(IEnumerable<char> inputStream)
        {
            foreach (var input in inputStream)
            {
                string newBuffer = null;
                switch (input)
                {
                    case InputCharacter.Enter:
                    case InputCharacter.Button1:
                    {
                        if (IsValidBuffer(_buffer).GetValueOrDefault(false))
                        {
                            return;
                        }
                        Beep.Angry();
                        break;
                    }

                    case InputCharacter.Cancel:
                    case InputCharacter.Button0:
                    {
                        if (_canCancel)
                        {
                            IsCancelled = true;
                            return;
                        }
                        Beep.Angry();
                        break;
                    }

                    case InputCharacter.Backspace:
                    {
                        if (_buffer.Length > 0)
                        {
                            newBuffer = _buffer.Substring(0, _buffer.Length - 1);
                        }
                        else
                        {
                            Beep.Angry();
                        }
                        break;
                    }

                    default:
                    {
                        if (input >= '0' && input <= '9')
                        {
                            newBuffer = _buffer + input;
                            bool newBufferIsInvalid = !IsValidBuffer(newBuffer).GetValueOrDefault(true);
                            if (newBufferIsInvalid)
                            {
                                Beep.Angry();
                                newBuffer = null;
                            }
                        }
                        break;
                    }
                }

                if (newBuffer != null)
                {
                    _buffer = newBuffer;
                    Line3.Update(FormatBufferForDisplay(_buffer));
                }
            }
        }
    }

    public class EditorCompletedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets a string value
        /// </summary>
        public string Value { get; set; }

        public EditorCompletedEventArgs(string value)
        {
            Value = value;
        }
    }
}