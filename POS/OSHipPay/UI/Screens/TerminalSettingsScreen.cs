﻿using System;
using System.Collections.Generic;
using System.Net;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen manages the terminal settings.
    /// </summary>
    class TerminalSettingsScreen : CompositeScreenBase
    {
        readonly Settings _settings;

        public TerminalSettingsScreen() : this(ApplicationContext.Current.Settings) { }

        public TerminalSettingsScreen(Settings settings)
        {
            _settings = settings;
        }
        
        /// <summary>
        /// <see cref="CompositeScreenBase.GetScreens"/>
        /// </summary>
        public override IEnumerable<IScreen> GetScreens()
        {
            // GetScreens2 defines the screens and the order we display them in.
            // Here the screens are displayed until done or cancelled.
            foreach (var screen in GetScreens2())
            {
                screen.Cancelled += delegate { IsCancelled = true; };
                yield return screen;

                if (IsCancelled)
                {
                    yield break;
                }
            }
        }

        public IEnumerable<EditSettingScreenBase> GetScreens2()
        {
            bool canCancel = ShouldTheUserBeAbleToCancel();

            yield return GetTerminalIdSetting(canCancel);
            yield return GetPplHostSetting(canCancel);
            yield return GetPplPortSetting(canCancel);
            yield return GetSpdhHostSetting(canCancel);
            yield return GetSpdhPortSetting(canCancel);
            yield return GetSpdhHost2Setting(canCancel);
            if (_settings.SPDHAddress2.IsNotNullOrEmpty())
            {
                // Only display the port setting if the host setting has a value.
                yield return GetSpdhPort2Setting(canCancel);
            }
            yield return GetDcappSetting(canCancel);
            //yield return GetWifiSSID(canCancel);
            //yield return GetWifiPassword(canCancel);

        }

        /// <summary>
        /// Determine if the user should be able to cancel editing. The reason this might be false
        /// is that some settings are mandatory and if any one of those settings does not have a value
        /// then the user must complete this view.
        /// </summary>
        /// <returns></returns>
        public bool ShouldTheUserBeAbleToCancel()
        {
            var terminalId = _settings.TerminalId;
            var pplHost = _settings.PPLAddress;
            var pplPort = _settings.PPLPort;
            var spdhHost = _settings.SPDHAddress;
            var spdhPort = _settings.SPDHPort;

            bool canCancel = terminalId.IsNotNullOrEmpty()
                             && pplHost.IsNotNullOrEmpty()
                             && pplPort > 0
                             && spdhHost.IsNotNullOrEmpty()
                             && spdhPort > 0;

            // We don't care about if these values are not set: seconday SPDH server, DCAPP override

            return canCancel;
        }

        public TerminalIdSettingScreen GetTerminalIdSetting(bool canCancel)
        {
            string terminalId = _settings.TerminalId;
            var editor = new TerminalIdSettingScreen(terminalId, canCancel);
            editor.Completed += (sender, args) =>
            {
                _settings.TerminalId = ((TerminalIdSettingScreen)sender).TerminalId;
            };
            return editor;
        }

        public IpAddressSettingScreen GetPplHostSetting(bool canCancel)
        {
            string pplHost = _settings.PPLAddress;
            var editor = new IpAddressSettingScreen(UserInterface.Strings.mgmt_setConfigIP, pplHost, canCancel);
            editor.Completed += (sender, args) =>
            {
                _settings.PPLAddress = ((IpAddressSettingScreen)sender).Address;
            };

            return editor;
        }

        public IpPortSettingScreen GetPplPortSetting(bool canCancel)
        {
            int pplPort = _settings.PPLPort;
            var editor = new IpPortSettingScreen(UserInterface.Strings.mgmt_setConfigPort, pplPort, canCancel);
            editor.Completed += (sender, args) =>
            {
                _settings.PPLPort = ((IpPortSettingScreen)sender).Port;
            };

            return editor;
        }

        public IpAddressSettingScreen GetSpdhHostSetting(bool canCancel)
        {
            string spdhHost = _settings.SPDHAddress;
            var editor = new IpAddressSettingScreen(UserInterface.Strings.setup_spdhHost, spdhHost, canCancel);
            editor.Completed += (sender, args) =>
            {
                _settings.SPDHAddress = ((IpAddressSettingScreen)sender).Address;
            };

            return editor;
        }

        public IpPortSettingScreen GetSpdhPortSetting(bool canCancel)
        {
            int spdhPort = _settings.SPDHPort;
            var editor = new IpPortSettingScreen(UserInterface.Strings.setup_spdhPort, spdhPort, canCancel);
            editor.Cancelled += delegate { IsCancelled = true; };
            editor.Completed += (sender, args) =>
            {
                _settings.SPDHPort = ((IpPortSettingScreen)sender).Port;
            };
            return editor;
        }

        public IpAddressSettingScreen GetSpdhHost2Setting(bool canCancel)
        {
            string spdhHost2 = _settings.SPDHAddress2;
            var editor = new IpAddressSettingScreen(UserInterface.Strings.setup_spdhHost2, spdhHost2, true) { IsOptional = true };
            editor.Completed += (sender, args) =>
            {
                _settings.SPDHAddress2 = ((IpAddressSettingScreen)sender).Address;
            };

            return editor;
        }

        public IpPortSettingScreen GetSpdhPort2Setting(bool canCancel)
        {
            int spdhPort2 = _settings.SPDHPort2;
            var editor = new IpPortSettingScreen(UserInterface.Strings.setup_spdhPort2, spdhPort2, true) { IsOptional = true };
            editor.Completed += (sender, args) =>
            {
                _settings.SPDHPort2 = ((IpPortSettingScreen)sender).Port;
            };
            return editor;
        }

        public IpAddressSettingScreen GetDcappSetting(bool canCancel)
        {
            var dcappHost = _settings.DCAPPHost != null ? _settings.DCAPPHost.ToString() : String.Empty;
            var editor = new IpAddressSettingScreen("DCAPP Override host", dcappHost, canCancel) {IsOptional = true};
            editor.Completed += (sender, args) =>
            {
                var address = ((IpAddressSettingScreen)sender).Address;
                _settings.DCAPPHost = address.IsNotNullOrEmpty() ? IPAddress.Parse(address) : null;
            };

            return editor;
        }
        public OSStringEditorScreen GetWifiPassword(bool canCancel)
        {
            string s = _settings.WifiPassword ?? string.Empty;
            var editor = new OSStringEditorScreen("Wifi Password", s, canCancel) { IsOptional = true };
            editor.Completed += (sender, args) =>
            {
                _settings.WifiPassword = ((OSStringEditorScreen)sender).GivenValue;
            };

            return editor;
        }

        public OSStringEditorScreen GetWifiSSID(bool canCancel)
        {
            string s = _settings.WifiSSID ?? String.Empty;
            var editor = new OSStringEditorScreen("Wifi SSID", s, canCancel) { IsOptional = true };
            editor.Completed += (sender, args) =>
            {
                _settings.WifiSSID = ((OSStringEditorScreen)sender).GivenValue;
            };

            return editor;
        }

    }
}