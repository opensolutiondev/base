using System.Collections.Generic;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// The transaction menu. Displayed when PreTransaction() is called.
    /// </summary>
    class TransactionMenuScreen : CompositeScreenBase
    {
        readonly MenuScreenBase _menu = new MenuScreenBase();

        IScreen _selectedFunction;

        public TransactionMenuScreen()
        {
            _menu.Title = Strings.tmenu_title;
            _menu.EnterSelectsFirstItem = true;

            AddTransactionTypesToScreen();          

            _menu.Add(Strings.setupMenu_title, () => _selectedFunction = new TerminalMenuScreen());
        }

        /// <summary>
        /// <see cref="CompositeScreenBase.GetScreens"/>
        /// </summary>
        public override IEnumerable<IScreen> GetScreens()
        {
            // Keep going until something was selected and that something was not cancelled.

            bool isDone = false;
            while (!isDone)
            {
                yield return _menu;

                if (_selectedFunction != null)
                {
                    yield return _selectedFunction;

                    isDone = !_selectedFunction.IsCancelled;

                    _selectedFunction = null;
                }
            }
        }

        /// <summary>
        /// Add transaction types dependent on PPL Configuration. 
        /// </summary>
        public void AddTransactionTypesToScreen()
        {
            //A list of transaction types is created from the configuration in PPL file DCPAR (Transaction Descriptors).
            IList<POSTransactionDescriptor> posTransactionDescriptors = SessionContext.Current.Terminal.TerminalConfig.DebitCreditParameters_TransactionDescriptors;

            if (posTransactionDescriptors != null)
            {
                //Transaction type Reversal is not configured in PPL file DCPAR. Add it to the list since it should always be available.
                AddTransactionTypeToList(ref posTransactionDescriptors, TransactionType.Reversal);

                //Order the list of transaction types in preferred order, to be shown on the screen. 
                posTransactionDescriptors = OrderListOfTransactionTypes(posTransactionDescriptors);

                foreach (POSTransactionDescriptor posTransactionDescriptor in posTransactionDescriptors)
                {
                    //If the transaction type is configured as allowed, add it to the menu.
                    if (posTransactionDescriptor.TransactionTypeAllowed)
                    {
                        switch (posTransactionDescriptor.TransactionType)
                        {
                            case TransactionType.Purchase:
                                {
                                    _menu.Add(Strings.tmenu_purchase, () => _selectedFunction = new PurchaceRefundScreen(TransactionType.Purchase));
                                    break;
                                }

                            case TransactionType.Refund:
                                {
                                    _menu.Add(Strings.tmenu_refund, () => _selectedFunction = new PurchaceRefundScreen(TransactionType.Refund));
                                    break;
                                }

                            case TransactionType.Reversal:
                                {
                                    _menu.Add(Strings.tmenu_reversal, () => _selectedFunction = new ReversalScreen());
                                    break;
                                }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Add a transaction type to the list of transaction descriptors.
        /// </summary>
        /// <param name="posTransactionDescriptors">A list of transaction types, defined as transaction descriptors in PPL file, DCPAR.</param>
        /// <param name="transactionType">Transaction type to add to the list of transaction descriptors.</param>
        public void AddTransactionTypeToList(ref IList<POSTransactionDescriptor> posTransactionDescriptors, TransactionType transactionType)
        {
            var posTransactionType = new POSTransactionDescriptor
            {
                TransactionType        = transactionType,
                PasswordProtected      = false,
                ProductGroupRequired   = false,
                TransactionTypeAllowed = true
            };
            posTransactionDescriptors.Add(posTransactionType);
        }

        /// <summary>
        /// Order a list of transaction types. For now the order is not configurable. It is a result of existing order and Interblocks requirements.
        /// </summary>
        /// <param name="posTransactionDescriptors">List to be ordered.</param>
        /// <returns>An ordered list.</returns>
        public IList<POSTransactionDescriptor> OrderListOfTransactionTypes(IList<POSTransactionDescriptor> posTransactionDescriptors)
        {
            Dictionary<TransactionType, int> sortingvalues = new Dictionary<TransactionType, int> 
            { 
                { TransactionType.Purchase, 10 },
                { TransactionType.CashAdvance, 20 },
                { TransactionType.Refund, 30 },
                { TransactionType.Reversal, 50 },
            };

            IList<POSTransactionDescriptor> orderedListOfTransactionTypes = posTransactionDescriptors.OrderBy(t => sortingvalues.GetValueOrDefault(t.TransactionType, 99)).ToList();

            return orderedListOfTransactionTypes;
        }
    }
}