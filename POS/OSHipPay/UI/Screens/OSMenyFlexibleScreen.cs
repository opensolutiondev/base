﻿using OSHipPay.Utilities;
using System.Collections.Generic;
using System.Drawing;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    class OSMenyFlexibleScreen 
    {

        public OSMenyFlexibleScreen(UserInterface UI, PayApp _paymentApp, StringDefinitions strings) {
            FlexibleDisplay fd = new FlexibleDisplay(7);   
            fd.Elements.Add(new FlexibleDisplayElement(strings.setupMenu_title, FlexibleDisplayElement.SizeLarge, 0, 100, false, 0) { HorizontalAlignment = StringAlignment.Center });
            fd.Elements.Add(new FlexibleDisplayElement(strings.tmenu_transactionlist, FlexibleDisplayElement.SizeRegular, 1, 100, true, 1000) { BackgroundColour = Color.Wheat, VisibleClick = true });
            fd.Elements.Add(new FlexibleDisplayElement(strings.diagTitle, FlexibleDisplayElement.SizeRegular, 2, 100, true, 1001) { BackgroundColour = Color.Wheat });
            fd.Elements.Add(new FlexibleDisplayElement(strings.setupMenu_setup, FlexibleDisplayElement.SizeRegular, 3, 100, true, 1002) { BackgroundColour = Color.Wheat });
            fd.Elements.Add(new FlexibleDisplayElement(strings.setupMenu_managment, FlexibleDisplayElement.SizeRegular, 3, 100, true, 1003) { BackgroundColour = Color.Wheat });
            fd.Elements.Add(new FlexibleDisplayElement(strings.cancel, FlexibleDisplayElement.SizeSmall, 4, 45, true, 1050) { BackgroundColour = Color.Red, TextColour = Color.White, HorizontalAlignment = StringAlignment.Center });
            fd.Elements.Add(new FlexibleDisplayElement(strings.okButton, FlexibleDisplayElement.SizeSmall, 4, -45, true, 1051) { BackgroundColour = Color.Green, TextColour = Color.White, HorizontalAlignment = StringAlignment.Center });
            List<UserInputs> keys = new List<UserInputs>
            {
                UserInputs.Enter,
                UserInputs.Cancel,
                UserInputs.SingleTap
            };
            UI.Display(fd);
            UserInputs input;
            do
            {
                SingleKeyInputter ski = new SingleKeyInputter((IPaymentApp)_paymentApp, ref _paymentApp._keyHandler, _paymentApp._keyHandlerLock, keys);
                input = ski.WaitForFinish(ref _paymentApp._keyHandler);
                switch (input)
                {
                    case UserInputs.Cancel:
                        break;
                    case UserInputs.Enter:
                        break;
                    case UserInputs.SingleTap:
                        Logging.Log.VSlog("ButtonId = " + ski.ButtonId, this);
                        if (ski.ButtonId == 1000) { 
                            ApplicationContext.Current.HandleTransactionListOnNextSession = true;
                            break;
                        }
                        else if (ski.ButtonId == 1001)
                        {
                            ApplicationContext.Current.DiagnosticsOnNextSession = true;
                            break;
                        }
                        else if (ski.ButtonId == 1002)
                        {
                            ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession = true;
                            break;
                        }
                        else if (ski.ButtonId == 1003)
                        {
                            ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true;
                            break;
                        }
                        else if (ski.ButtonId == 1050)
                        {
                            ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true;
                            break;
                        }
                        else if (ski.ButtonId == 1051)
                        {
                            ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true;
                            break;
                        }
                        continue;
                    default:
                        continue;

                }
            } while (input != UserInputs.Enter);

        }

    }
}