namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enable and disable the Tipping option.
    /// </summary>
    class TipOptionScreen : OptionScreen
    {
        /// <summary>
        /// TipOptionScreen constructor.
        /// </summary>
        public TipOptionScreen()
            : base(Strings.opt_tipping, Strings.opt_disableTipping, Strings.opt_enableTipping)
        {
        }

        /// <summary>
        /// <see cref="OptionScreen.CurrentValue"/>
        /// </summary>
        protected override bool CurrentValue
        {
            get => ApplicationContext.Current.Settings.Tipping;
            set => ApplicationContext.Current.Settings.Tipping = value;
        }
    }
}
