using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// A function that validates a password.
    /// </summary>
    /// <param name="password">A string.</param>
    /// <returns>True if the password is correct, otherwise false.</returns>
    public delegate bool PasswordValidationFunction(string password);

    /// <summary>
    /// A password check screen. The screen first displays a password editor screen. If the
    /// entered password is incorrect a message is displayed and the user has the option
    /// to try again.
    /// </summary>
    class PasswordCheckScreen : SimpleScreen
    {
        readonly string _caption;
        readonly PasswordValidationFunction _passwordValidator;

        /// <summary>
        /// True if the entered password is valid.
        /// </summary>
        public bool IsValid;

        /// <summary>
        /// PasswordCheckScreen constructor.
        /// </summary>
        /// <param name="caption">The text that will be shown at the top of the screen. It should describe
        /// what function the user is trying to access.</param>
        /// <param name="passwordValidator">A function that validates a password.</param>
        public PasswordCheckScreen(string caption, PasswordValidationFunction passwordValidator)
        {
            _caption = caption ?? String.Empty;
            _passwordValidator = passwordValidator;
        }

        /// <summary>
        /// <see cref="SimpleScreen.Displayed"/>
        /// </summary>
        /// <param name="ui"></param>
        public override void Displayed(UserInterface ui)
        {
            var passwordEntry = new PasswordEditorScreen(_caption, String.Empty, true);
            while (!IsValid && !IsCancelled)
            {
                // Let the user enter a password.
                passwordEntry.Reset();
                ui.Display(passwordEntry);

                IsCancelled = passwordEntry.IsCancelled;
                if (!IsCancelled)
                {
                    // Check if the entered password is ok.
                    IsValid = _passwordValidator(passwordEntry.Value);
                    if (!IsValid)
                    {
                        Beep.Angry();
                        IsCancelled = !ui.YesNo(Strings.invalidPassword, Strings.try_again + "?");
                    }
                }
            }
        }
    }
}