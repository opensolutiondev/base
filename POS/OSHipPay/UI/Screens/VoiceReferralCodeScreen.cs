﻿using System;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enter a voice referral code.
    /// </summary>
    class VoiceReferralCodeScreen : StringEditorScreen
    {
        /// <summary>
        /// VoiceReferralCodeScreen constructor.
        /// </summary>
        /// <param name="supplementaryText">Supplementary text</param>
        public VoiceReferralCodeScreen(string supplementaryText) : base(Strings.voiceReferral, String.Empty, true)
        {
            Line2.SetValue(Strings.callVoiceReferral1 + " " + supplementaryText).AlignCentre().SizeSmall();
            Line4.SetValue(Strings.callVoiceReferral2).AlignCentre();
        }

        /// <summary>
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="editValue">Value to validate</param>
        /// <returns></returns>
        protected override bool? IsValidBuffer(string editValue)
        {
            if (editValue.Length > 6)
                return false;
            if (editValue.Length == 6)
                return true;
            return null;
        }
    }
}