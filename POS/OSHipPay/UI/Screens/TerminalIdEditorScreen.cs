

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to edit the terminal id.
    /// </summary>
    class TerminalIdEditorScreen : StringEditorScreen
    {
        /// <summary>
        /// TerminalIdEditorScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="value">Initial value</param>
        /// <param name="isOptional">true/false</param>
        public TerminalIdEditorScreen(string title, string value, bool isOptional)
            : base(title, value, isOptional)
        {
        }

        /// <summary>
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="editValue">Value to validate</param>
        /// <returns></returns>
        protected override bool? IsValidBuffer(string editValue)
        {
            if (editValue.IsNullOrEmpty())
                return null;
            return editValue.Length < 20;
        }
    }
}