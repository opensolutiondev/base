﻿

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Screen for confirming and editing settings that are IPv4 ports.
    /// </summary>
    class IpPortSettingScreen : EditSettingScreenBase
    {
        readonly string _title;

        public int Port { get; set; }

        /// <summary>
        /// Indicates if an empty/null value is allowed for the address.
        /// </summary>
        public bool IsOptional;

        public IpPortSettingScreen(string title, int port, bool canCancel) : base(canCancel)
        {
            _title = title;
            Port = port;
        }

        /// <summary>
        /// <see cref="EditSettingScreenBase.Value"/>
        /// </summary>
        public override string Value => Port.ToString();

        /// <summary>
        /// <see cref="EditSettingScreenBase.ConfirmTitle"/>
        /// </summary>
        public override string ConfirmTitle => _title;

        /// <summary>
        /// <see cref="EditSettingScreenBase.GetEditor"/>
        /// </summary>
        /// <returns></returns>
        protected override StringEditorScreen GetEditor()
        {
            var editor = new IpPortEditorScreen(_title, Port, IsOptional);
            editor.Completed += (sender, args) => Port = args.Value.ToInt32(0);
            return editor;
        }
    }
}