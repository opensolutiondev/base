﻿using System;
using System.Linq;
using System.Text;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enter an IP adress with valid format.
    /// </summary>
    class DateEditorScreen : StringEditorScreen
    {
        public DateEditorScreen(string title, string value, bool isOptional)
            : base(title, value, isOptional)
        {
        }

        protected override string FormatBufferForDisplay(string buffer)
        {
            var text = new StringBuilder(buffer, 20);
            return text.ToString();
        }
        protected override string GetBuffer(string value)
        {
            if (value.IsNullOrEmpty())
                return String.Empty;
            return value;
        }

        protected override string FormatBufferForResult(string buffer)
        {
            if (buffer.IsNullOrEmpty())
                return String.Empty;
            var text = new StringBuilder(buffer, 10);
            if (text.Length >= 4)
                text.Insert(4, "/");
            if (text.Length >= 2)
                text.Insert(2, "/");
            return text.ToString();
        }

        protected override bool? IsValidBuffer(string value)
        {
            return true;
        }
    }
}