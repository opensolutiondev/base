﻿using System;
using System.Linq;
using System.Text;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// This screen enables the user to enter an IP adress with valid format.
    /// </summary>
    class IPAddressEditorScreen : StringEditorScreen
    {
        /// <summary>
        /// IPAddressEditorScreen constructor.
        /// </summary>
        /// <param name="title">Screen title</param>
        /// <param name="value">Initial value</param>
        /// <param name="isOptional">true/false</param>
        public IPAddressEditorScreen(string title, string value, bool isOptional)
            : base(title, value, isOptional)
        {
        }

        /// <summary>
        /// <see cref="StringEditorScreen.FormatBufferForDisplay"/>
        /// </summary>
        protected override string FormatBufferForDisplay(string buffer)
        {
            // Convert "172016000015" to "172.016.000.015"
            var text = new StringBuilder(buffer, 20);
            if (text.Length >= 9)
                text.Insert(9, ".");
            if (text.Length >= 6)
                text.Insert(6, ".");
            if (text.Length >= 3)
                text.Insert(3, ".");
            return text.ToString();
        }

        /// <summary>
        /// <see cref="StringEditorScreen.GetBuffer"/>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override string GetBuffer(string value)
        {
            if (value.IsNullOrEmpty())
                return String.Empty;

            // Convert an address like "172.16.0.15" to "172016000015"
            return value.Split('.')
                .Select(x => x.ToInt32(0).ToString("000"))
                .StringJoin("");
        }

        /// <summary>
        /// <see cref="StringEditorScreen.FormatBufferForResult"/>
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        protected override string FormatBufferForResult(string buffer)
        {
            if (buffer.IsNullOrEmpty())
                return String.Empty;

            int[] parts =
            {
                buffer.Substring(0, 3).ToInt32(0),
                buffer.Substring(3, 3).ToInt32(0),
                buffer.Substring(6, 3).ToInt32(0),
                buffer.Substring(9, 3).ToInt32(0),
            };
            return parts.StringJoin(".");
        }

        /// <summary>
        /// <see cref="StringEditorScreen.IsValidBuffer"/>
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override bool? IsValidBuffer(string value)
        {
            if (value.IsNullOrEmpty())
                return IsOptional ? true : (bool?)null;
            if (value.Length > 12)
                return false;
            if (value.Length >= 3)
                if (value.Substring(0, 3).ToInt32(256) > 255)
                    return false;
            if (value.Length >= 6)
                if (value.Substring(3, 3).ToInt32(256) > 255)
                    return false;
            if (value.Length >= 9)
                if (value.Substring(6, 3).ToInt32(256) > 255)
                    return false;
            if (value.Length == 12)
                return value.Substring(9, 3).ToInt32(256) <= 255;

            return null;
        }
    }
}