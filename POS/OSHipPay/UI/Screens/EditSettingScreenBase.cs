﻿using System;
using System.Collections.Generic;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Common base for all screens for editing settings.
    /// </summary>
    abstract class EditSettingScreenBase : CompositeScreenBase
    {
        readonly bool _canCancel;

        /// <summary>
        /// Gets value with preferred format.
        /// </summary>
        public abstract string Value { get; }

        /// <summary>
        /// Gets value.
        /// </summary>
        public abstract string ConfirmTitle { get; }

        /// <summary>
        /// This event is fired when the user presses cancel on the confirm screen.
        /// </summary>
        public event EventHandler Cancelled;
        protected virtual void RaiseCancelled()
        {
            var handler = Cancelled;
            handler?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// This event is fired when the screen is completed, i.e. the
        /// user has confirmed the setting or cancelled.
        /// </summary>
        public event EventHandler<CompletedEventArgs<EditSettingScreenBase>> Completed;
        protected virtual void RaiseCompleted()
        {
            var handler = Completed;
            handler?.Invoke(this, new CompletedEventArgs<EditSettingScreenBase>(this));
        }

        /// <summary>
        /// </summary>
        /// <param name="canCancel">
        /// Set this to true if the user should be able to cancel.
        /// </param>
        protected EditSettingScreenBase(bool canCancel)
        {
            _canCancel = canCancel;
        }

        /// <summary>
        /// <see cref="CompositeScreenBase.GetScreens"/>
        /// </summary>
        public override IEnumerable<IScreen> GetScreens()
        {
            // A setting is edited in two steps: confirmation and editing.
            // This screen can be exited only from the confirm view.
            bool isCompleted = false;
            bool isCancelled = false;

            while (!isCompleted && !isCancelled)
            {
                ConfirmSettingScreen confirm;
                if (this.GetType().Equals(typeof(DateSettingScreen)))
                    confirm = new ConfirmSettingScreen(ConfirmTitle, Value, _canCancel, true);
                else
                    confirm = new ConfirmSettingScreen(ConfirmTitle, Value, _canCancel);
                yield return confirm;

                isCompleted = confirm.IsConfirmed;
                isCancelled = confirm.IsCancelled;

                if (!isCompleted && !isCancelled)
                {
                    var editor = GetEditor();
                    yield return editor;
                }
            }

            if (isCancelled)
            {
                RaiseCancelled();
            }
            else if (isCompleted)
            {
                RaiseCompleted();
            }
        }

        /// <summary>
        /// Gets a string editor screen.
        /// </summary>
        /// <returns></returns>
        protected abstract StringEditorScreen GetEditor();
    }
}