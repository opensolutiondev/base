using System;
using System.Collections.Generic;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Base class for menus.
    /// </summary>
    class MenuScreenBase : CompositeScreenBase
    {
        protected IScreen SelectedScreen { get; set; }

        public string Title = String.Empty;

        readonly List<MenuPageScreen> _pages = new List<MenuPageScreen>();

        /// <summary>
        /// Add a menu item to the menu. This will automatically split the menu into pages
        /// if it grows too big.
        /// </summary>
        /// <param name="label">The text for the menu item.</param>
        /// <param name="handler">Something that will be executed if the user selects the menu item.</param>
        /// <seealso cref="MenuPageScreen.Add"/>
        public virtual void Add(string label, Action handler)
        {
            if (label.IsNullOrEmpty()) throw new ArgumentNullException(nameof(label));

            InitialisePages();

            var lastPage = _pages.Last();
            if (lastPage.IsFull)
            {
                AddPageBreak();
                lastPage = _pages.Last();
            }

            lastPage.Add(label, handler);
        }

        /// <summary>
        /// Add a new menu page, even if the current page has space for more items.
        /// </summary>
        public void AddPageBreak()
        {
            InitialisePages();

            var lastPage = _pages.Last();
            var nextPage = new MenuPageScreen(Title);
            lastPage.SetNextPage(nextPage);
            nextPage.SetPreviousPage(lastPage);
            _pages.Add(nextPage);
        }

        /// <summary>
        /// Add a timeout for each menu page. 
        /// </summary>
        /// <param name="timeout">The length of the timeout.</param>
        /// <param name="handler">A function that should be executed if the timeout occurs.</param>
        public virtual void AddTimeout(TimeSpan timeout, Action handler)
        {
            foreach (var page in _pages)
            {
                page.AddTimeout(timeout, handler);
            }
        }

        /// <summary>
        /// The default behaviour of the enter button (the green one) is to navigate to the next page.
        /// If this is set to true then pressing enter on the first page will instead select the first item.
        /// This only happens on the first page, the other pages keep the default behaviour.
        /// </summary>
        /// <remarks>
        /// This is desired on the first page of the transaction menu when it should be as frictionless as
        /// possible to start a new purchase transaction.
        /// </remarks>
        /// <seealso cref="MenuPageScreen.EnterSelectsFirstItem"/>
        public bool EnterSelectsFirstItem
        {
            set
            {
                InitialisePages();
                _pages[0].EnterSelectsFirstItem = true;
            }
        }

        public override IEnumerable<IScreen> GetScreens()
        {
            MenuPageScreen currentPage = _pages[0];

            while (currentPage != null)
            {
                yield return currentPage;

                IsCancelled = currentPage.IsCancelled;
                currentPage = currentPage.PageNavigatedTo;

                // If an item was selected, then we're done. PageNavigatedTo will be
                // null and this loop will exit.
            }
        }

        /// <summary>
        /// Add the first page if it hasn't been added already.
        /// </summary>
        void InitialisePages()
        {
            if (_pages.Count == 0)
            {
                _pages.Add(new MenuPageScreen(Title)); 
            }
        }

        /// <summary>
        /// A single page with up to three selectable items. Optionally a title and one or two navigation buttons.
        /// </summary>
        public class MenuPageScreen : FourLinesWithButtonsScreen, IHandleInput
        {
            public class MenuItem
            {
                public string Title;
                public Action Handler;
                public char Input;
            }

            /// <summary>
            /// If true then the Enter button selects the first item.
            /// </summary>
            /// <seealso cref="MenuScreenBase.EnterSelectsFirstItem"/>
            public bool EnterSelectsFirstItem = false;

            MenuPageScreen _nextPage;
            MenuPageScreen _prevPage;

            readonly List<MenuItem> _items = new List<MenuItem>();
            readonly List<UserInputs> _inputs = new List<UserInputs>();

            TimeSpan _timeout;
            Action _timeoutAction;

            /// <summary>
            /// Menu page screen.
            /// </summary>
            /// <param name="title">Screen title</param>
            public MenuPageScreen(string title)
            {
                if (title.IsNotNullOrEmpty())
                {
                    Line1.SetValue(title).AlignCentre();
                }

                _inputs.Add(UserInputs.Cancel); 
            }

            /// <summary>
            /// Set next menu page screen
            /// </summary>
            /// <param name="nextPage">Next menu page</param>
            public void SetNextPage(MenuPageScreen nextPage)
            {
                _nextPage = nextPage;
                _inputs.Add(UserInputs.Enter);
                _inputs.Add(UserInputs.Button1);
                Right.Show(Strings.more);
            }

            /// <summary>
            /// Set previous menu page screen
            /// </summary>
            /// <param name="prevPage">Previous menu page</param>
            public void SetPreviousPage(MenuPageScreen prevPage)
            {
                _prevPage = prevPage;
                Left.Show(Strings.back);
                _inputs.Add(UserInputs.Backspace);
                _inputs.Add(UserInputs.Button0);
            }

            /// <summary>
            /// True if more items cannot be added to this page.
            /// </summary>
            public bool IsFull => _items.Count >= 3;

            /// <summary>
            /// If the user navigates back or forward, this will be set to
            /// the page navigated to (either PrevPage or NextPage).
            /// </summary>
            public MenuPageScreen PageNavigatedTo { get; private set; }

            /// <summary>
            /// Add a menu item to the menu.
            /// </summary>
            /// <param name="title">The title of the menu item. This is displayed on the screen.</param>
            /// <param name="handler">A function that should be executed if the item is selected.</param>
            /// <seealso cref="MenuScreenBase.Add"/>
            public void Add(string title, Action handler)
            {
                if (IsFull)
                {
                    throw new InvalidOperationException("Menu page is full");
                }

                var item = new MenuItem
                {
                    Title = title,
                    Handler = handler,
                    Input = InputCharacter.AllDigits[_items.Count + 1],
                };

                _items.Add(item);
                _inputs.Add(InputCharacter.Reverse(item.Input));

                TextBlock nextLine = GetNextEmptyLine();
                nextLine.SetValue(_items.Count + ". " + item.Title).SizeExtraSmall();
            }

            /// <summary>
            /// Add a timeout handler to the menu.
            /// </summary>
            /// <param name="timeout">The length of the timeout.</param>
            /// <param name="handler">A function that should be executed if the timeout occurs.</param>
            public void AddTimeout(TimeSpan timeout, Action handler)
            {
                _timeout = timeout;
                _timeoutAction = handler;
            }

            /// <summary>
            /// Get next empty line.
            /// </summary>
            /// <returns>First textblock that is empty</returns>
            TextBlock GetNextEmptyLine()
            {
                return AllTextBlocks.First(x => x.Value.IsNullOrEmpty());
            }

            /// <summary>
            /// Get the accepted inputs of this screen.
            /// </summary>
            public InputsSpecification AcceptedInputs => new InputsSpecification
            {
                Inputs  = _inputs,
                Timeout = _timeout,
            };

            /// <summary>
            /// Handle input stream.
            /// </summary>
            /// <param name="inputStream">Input stream</param>
            public void HandleInput(IEnumerable<char> inputStream)
            {
                PageNavigatedTo = null;

                foreach (var input in inputStream)
                {
                    Action whatToDo = HandleSingleInput(input);
                    if (whatToDo != null)
                    {
                        whatToDo();
                        break;
                    }
                }
            }

            /// <summary>
            /// Handle one key or button press.
            /// </summary>
            /// <param name="input">Input character</param>
            /// <returns>An action that should be taken or null if the input was invalid and should be ignored.</returns>
            public Action HandleSingleInput(char input)
            {
                Action result = null;

                if ((InputCharacter.Enter == input) && EnterSelectsFirstItem)
                {
                    input = '1';
                }

                switch (input)
                {
                    case InputCharacter.Cancel:
                        return () => IsCancelled = true;

                    case InputCharacter.Button0:
                    case InputCharacter.Backspace:
                    {
                        if (_prevPage != null)
                        {
                            result = () => PageNavigatedTo = _prevPage;
                        }
                        break;
                    }

                    case InputCharacter.Button1:
                    case InputCharacter.Enter:
                    {
                        if (_nextPage != null)
                        {
                            result = () => PageNavigatedTo = _nextPage;
                        }
                        break;
                    }
  
                    case InputCharacter.Timeout:
                    {
                        result = _timeoutAction;
                        break;
                    }

                    default:
                    {
                        MenuItem selectedMenuItem = _items.FirstOrDefault(mi => mi.Input == input);

                        if (selectedMenuItem?.Handler != null)
                        {
                            result = selectedMenuItem.Handler;
                        }
                        break;
                    }
                }

                return result;
            }
        }
    }
}