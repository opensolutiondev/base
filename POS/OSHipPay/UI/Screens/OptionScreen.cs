using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using WestPA.Standalone;

namespace OSHipPay.UI.Screens
{
    /// <summary>
    /// Simple Yes / No screen that takes two text lines and presents a Yes / No choice with
    /// the OK button as Yes and the cancel button as No.
    /// </summary>
    abstract class OptionScreen : FourLinesWithButtonsScreen, IHandleInput
    {
        string _title;

        /// <summary>
        /// Valid input options for this screen
        /// </summary>
        static readonly List<UserInputs> ValidInputs = new List<UserInputs>
                                                       {
                                                           UserInputs.Button0,
                                                           UserInputs.Button1,
                                                           UserInputs.Enter,
                                                           UserInputs.Cancel
                                                       };

        /// <summary>
        /// OptionScreen constructor.
        /// </summary>
        ///<param name="title">Screen title</param> 
        ///<param name="disableText">Disable option text</param>
        ///<param name="enableText">Enable option text</param>
        protected OptionScreen(string title, string disableText, string enableText)
        {
            Initialise(title, disableText, enableText);
        }

        /// <summary>
        /// Default OptionScreen constructor, only for specialisations
        /// </summary>
        protected OptionScreen()
        {
        }

        /// <summary>
        /// Initialiser for cases where CurrentValue is not defined at construction time
        /// </summary>
        public void Initialise(string title, string disableText, string enableText)
        {
            _title = title;

            string currentSetting, changeSetting;

            if (CurrentValue)
            {
                currentSetting = _title + " : " + Strings.enabled;
                changeSetting = disableText;
            }
            else
            {
                currentSetting = _title + " : " + Strings.disabled;
                changeSetting = enableText;
            }

            this.SetScreenLines(currentSetting, changeSetting);

            Left.Show(Strings.no, Color.DarkRed, null);
            Right.Show(Strings.yes, Color.DarkGreen, null);
        }

        /// <summary>
        /// Gets or sets the value of the option.
        /// </summary>
        protected abstract bool CurrentValue { get; set; }

        /// <summary>
        /// Gives the input options
        /// </summary>
        InputsSpecification IHandleInput.AcceptedInputs => ValidInputs;

        /// <summary>
        /// Input handler
        /// </summary>
        /// <param name="input">Selected input</param>
        void IHandleInput.HandleInput(IEnumerable<char> input)
        {
            char key = input.First();

            if ((key == InputCharacter.Button1) ||
                (key == InputCharacter.Enter))
            {
                var currentValue = CurrentValue = !CurrentValue;

                string currentSetting = _title + " : " + (currentValue ? Strings.enabled : Strings.disabled);

                Line1.Update(currentSetting);
                Line2.Update(String.Empty);

                ApplicationContext.Current.UI.Pause(TimeSpan.FromSeconds(1.5));
            }
        }
    }
}