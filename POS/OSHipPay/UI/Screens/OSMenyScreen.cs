﻿using System.Collections.Generic;

namespace OSHipPay.UI.Screens
{
    class OSMenyScreen : MenuScreenBase
    {
        readonly IScreen _selectedFunction = null;

        /// <summary>
        /// TerminalMenuScreen constructor.
        /// </summary>
        public OSMenyScreen()
        {


            Title = Strings.setupMenu_title;

//           Add(Strings.tmenu_reversal, () => _selectedFunction = new PurchaceRefundScreen(WestPA.Standalone.TransactionType.Reversal));

//            Add(Strings.tmenu_refund, () => _selectedFunction = new PurchaceRefundScreen(WestPA.Standalone.TransactionType.Refund));

            Add(Strings.tmenu_transactionlist, () => ApplicationContext.Current.HandleTransactionListOnNextSession = true);

            Add(Strings.diagTitle, () => ApplicationContext.Current.DiagnosticsOnNextSession = true);

            Add(Strings.setupMenu_setup, () => ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession = true);

            Add(Strings.setupMenu_managment, () => ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true);

//            Add(Strings.setupMenu_setup, () => _selectedFunction = new TerminalMenuScreen());

            //            Add(Strings.setupMenu_title, () => _selectedFunction = new TerminalMenuScreen());

            //Not needed in terminal any more - Patric at West on 2.11.2018....
            //Add(Strings.tmenu_closeBatch, () => ApplicationContext.Current.CloseBatchOnNextSession = true);


        }

        /// <summary>
        /// <see cref="CompositeScreenBase.GetScreens"/>
        /// </summary>

        /// <returns></returns>
        public override IEnumerable<IScreen> GetScreens()
        {
            return MyGetScreens(base.GetScreens());
        }

        /// <summary>
        /// Gets an enumerator of menupages.
        /// </summary>
        /// <param name="menuPages"></param>
        /// <returns></returns>
        IEnumerable<IScreen> MyGetScreens(IEnumerable<IScreen> menuPages)
        {
            foreach (var page in menuPages)
            {
                yield return page;
            }
            if (_selectedFunction != null)
            {
                yield return _selectedFunction;
            }
        }
    }
}