﻿using System;
using System.Threading;
using WestPA.Standalone;

namespace OSHipPay.UI
{
    class InputHandler
    {
        readonly AutoResetEvent _waitHandle;
        readonly object _lock = new object();

        volatile bool _isOccupied;
        char _nextCharacter;


        public InputHandler()
        {
            _waitHandle = new AutoResetEvent(false);
        }

        /// <summary>
        /// Wait for the next user input.
        /// </summary>
        /// <param name="timeout">Optional timeout</param>
        /// <remarks>
        /// This will block until another thread calls HandleUserInput or until the timeout has been reached.
        /// If the timeout is triggered then InputCharacter.Timeout will be returned.
        /// </remarks>
        /// <exception cref="InvalidOperationException">Thrown if another thread is already waiting for input.</exception>
        /// <returns></returns>
        public char GetNextInput(TimeSpan timeout)
        {
            GetExclusiveAccessOrFail();

            int timeoutInMillseconds = timeout.TotalMilliseconds.ToInt32(0);
            if (timeoutInMillseconds > 0)
            {
                bool receivedEvent = _waitHandle.WaitOne(timeoutInMillseconds, false);
                if (!receivedEvent)
                {
                    _nextCharacter = InputCharacter.Timeout;
                }
            }
            else
            {
                _waitHandle.WaitOne();
            }

            _isOccupied = false;

            return _nextCharacter;
        }

        /// <summary>
        /// Receive input from the system.
        /// </summary>
        /// <param name="input">The UserInputs value.</param>
        /// <param name="keyCode">Optional keyCode value if input is UserInputs.Letter</param>
        public void HandleUserInput(UserInputs input, char keyCode)
        {
            lock (_lock)
            {
                if (_isOccupied)
                {
                    _nextCharacter = InputCharacter.Get(input, keyCode);

                    _waitHandle.Set();
                }
                else
                    Logging.Log.VSlog("HandleUserInput not accupied", Logging.Severity.Error);
            }
        }

        /// <summary>
        /// Make sure nothing else is already listening for input.
        /// </summary>
        void GetExclusiveAccessOrFail()
        {
            bool lockIsAquired = false;
            if (!_isOccupied)
                lock (_lock)
                    if (!_isOccupied)
                    {
                        _isOccupied = true;
                        lockIsAquired = true;
                    }

            if (!lockIsAquired)
                // Something is already waiting for input.
                throw new InvalidOperationException("Input handler is occupied.");
        }
    }
}