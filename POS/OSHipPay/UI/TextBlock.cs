﻿using System;
using WestPA.Standalone;

namespace OSHipPay.UI
{
    public class TextBlock
    {
        /// <summary>
        /// Fired when the text is updated through the Update method. The UserInterface class
        /// listens to this to push the changes to the payment application.
        /// </summary>
        public static event EventHandler<UpdateEventArgs> Updated;

        readonly int _lineNumber;

        public DisplayData.Alignment Alignment = DisplayData.Alignment.Left;
        public DisplayData.Size Size = DisplayData.Size.Regular;

        public string Value = String.Empty;

        public TextBlock(int lineNumber)
        {
            _lineNumber = lineNumber;
            Value = String.Empty;
        }

        /// <summary>
        /// Set the value to the given string. Returns the TextBlock for easy chaining.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public TextBlock SetValue(string text) { Value = text ?? String.Empty; return this; }

        public void Update(string text)
        {
            text = text ?? "";
            SetValue(text);
            Updated?.Invoke(this, new UpdateEventArgs(_lineNumber, Value));
        }

        public class UpdateEventArgs : EventArgs
        {
            public int Line { get; set; }

            /// <summary>
            /// Gets or sets a string value
            /// </summary>
            public string Value { get; set; }

            public UpdateEventArgs(int line, string value) { Line = line; Value = value; }
        }
    }

    static class TextBlockHelpers
    {
        public static TextBlock AlignCentre(this TextBlock line)
        {
            line.Alignment = DisplayData.Alignment.Centre;
            return line;
        }

        public static TextBlock AlignRight(this TextBlock line)
        {
            line.Alignment = DisplayData.Alignment.Right;
            return line;
        }

        public static TextBlock SizeExtraSmall(this TextBlock line)
        {
            line.Size = DisplayData.Size.ExtraSmall;
            return line;
        }

        public static TextBlock SizeLarge(this TextBlock line)
        {
            line.Size = DisplayData.Size.Large;
            return line;
        }

        public static TextBlock SizeSmall(this TextBlock line)
        {
            line.Size = DisplayData.Size.Small;
            return line;
        }
    }

}