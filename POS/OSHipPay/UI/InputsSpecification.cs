using System;
using System.Collections.Generic;
using WestPA.Standalone;

namespace OSHipPay.UI
{
    /// <summary>
    /// Specifies which inputs a component wants to receive.
    /// </summary>
    public struct InputsSpecification
    {
        /// <summary>
        /// A list of all inputs that should be reported by the system. If this is null
        /// then no inputs will be reported.
        /// </summary>
        public List<UserInputs> Inputs;

        /// <summary>
        /// An optional timeout. If this is a positive value and no input is received
        /// before the specified time then the Timeout input will be reported.
        /// </summary>
        public TimeSpan Timeout;

        /// <summary>
        /// Convenience operator for implicitly converting lists of UserInputs to InputSpecifications.
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        public static implicit operator InputsSpecification(List<UserInputs> inputs)
        {
            return new InputsSpecification {Inputs = inputs};
        }
    }
}