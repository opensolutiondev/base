﻿using Android.App;
using Android.Content;
using Android.Net.Wifi;
using Android.OS;
using OSHipPay.Configuration;
using OSHipPay.Core;
using OSHipPay.Logging;
using OSHipPay.UI;
using OSHipPay.UI.Screens;
using OSHipPay.Utilities;
using OSHipPay.WebSockets;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSockets;
using WestPA.Standalone;

namespace OSHipPay
{

    public delegate void UserInputHandler(UserInputs input, char character, int id);


    /// <summary>
    /// The POS component must implement the IComponent interface
    /// </summary>
    /// <remarks>
    /// IMPORTANT! The PA has to be configured in order to use this component.
    /// - ApplicationMode should be set to "STANDALONE"
    /// - POSComponentFilename should be set to the type name of this component,
    ///     suitable for <see cref="System.Type.GetType"/>,
    ///     e.g. "OSHipPay.PayApp, POS".
    /// </remarks>
    public class PayApp : IComponent
    {
        public UserInputHandler _keyHandler = null;
        public object _keyHandlerLock = new object();

        internal bool AreTheIdsUnique(string cashRegisterId, string cashRegisterPaymentId)
        {
            if (cashRegisterId.IsNullOrEmpty() || cashRegisterPaymentId.IsNullOrEmpty()) return false;
            return _transactionInfo.GetWithECRIDs(cashRegisterId, cashRegisterPaymentId)==null;
        }

        private OSHipPay.WebSockets.WebSocketServerBase.CallBackDelegate _callBackFunction => CallBackDelegate;

        /// <summary>
        /// Component name for splash screen
        /// </summary>
        public string ComponentName => "OSHipPay";

        /// <summary>
        /// The payment application instance, as exposed through the IPaymentApp interface
        /// </summary>
        IPaymentApp PaymentApp { get; set; }

        internal string GetReferenceNumber(string cashRegisterId, string cashRegisterPaymentId)
        {
            TransactionStatus tr = _transactionInfo.GetWithECRIDs(cashRegisterId, cashRegisterPaymentId);
            return tr?.ReferenceNumber;
        }
        internal TransactionStatus GetTransactionStatusWithIDs(string cashRegisterId, string cashRegisterPaymentId)
        {
            TransactionStatus tr = _transactionInfo.GetWithECRIDs(cashRegisterId, cashRegisterPaymentId);
            return tr;
        }

        internal void CreateErrorForLastTransactionResult(String error, TransactionType t)
        {
            lastTransactionResult = new Messages.TransactionResult
            {
                CashRegisterId = TransactionContext.Current.CashRegisterId,
                CashRegisterPaymentId = TransactionContext.Current.CashRegisterPaymentId,
                TranasctionType =  t.ToString()
            };
        }

        public Settings GetSettings()
        {
            return Settings;
        }

        private StatusCodes currentStatus = StatusCodes.Idle;
        public StatusCodes CurrentStatus { get => currentStatus; set => SetStatusForCurrentStatus(value); }

        void SetStatusForCurrentStatus(StatusCodes value)
        {
            currentStatus = value;
            WebSocketProcessor.DoUDPBroadtcast(currentStatus);
        }

        WebSocketServerBase _webSocketServer;
        UDPListener _udpListener;

        OSHipPay.WebSockets.Messages.TransactionResult lastTransactionResult = null;

        internal void SetContinue()
        {
            UI.SetContinue();
            WakeUpScreenAsync();
            SetStatusForCurrentStatus(StatusCodes.StartTransaction);
        }

        async void WakeUpScreenAsync()
        {
            var powerManager = (PowerManager)Application.Context.GetSystemService(Context.PowerService);
            var wakeLock = powerManager.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.AcquireCausesWakeup, "StackOverflow");
            wakeLock.Acquire();
            await Task.Delay(1000);
            wakeLock.Release();

        }

        WebSocketProcessor _webSocketProcessor;

        UserInterface UI { get; set; }

        Settings Settings { get; set; }
        SystemInfo Sysinfo { get; set; }

        ReceiptHandler Receipts { get; set; }

        Boolean TippingEnabled = false;

        private ITransactionRepository _transactionInfo;
//        private ITransactionIdentifiersRepository _transactionIdentifiers;

        /// <summary>
        /// Localised strings for the user interface.
        /// </summary>
        readonly StringDefinitions _strings = new StringDefinitions();

        internal bool IsStartTransactionFinished()
        {
            return currentStatus.Equals(StatusCodes.Idle);
        }

        /*
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logging.Log.Error("UnhandledException "+ e.ExceptionObject, new PayApp());
            Logging.Log.CloseAndFlush();
            if (e.IsTerminating)
            {
                Thread.Sleep(5000);
                System.Environment.Exit(1);
            }
        }
        */

        #region Setup and Support

        /// <summary>
        /// This function is called once by the payment application when it has completed
        /// its initialisation phase after power-up.
        /// </summary>
        /// <param name="paymentApp">An object that implements the IPaymentApp interface,
        /// i.e. a reference to the payment application.</param>
        /// <param name="sysinfo">Terminal and payment application information</param>
        /// <returns>true if initialisation was successful, otherwise false</returns>
        public bool Initialise(IPaymentApp paymentApp, SystemInfo sysinfo)
        {
            //MLA - not allowed to use - could get PA info in there...
            //            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            System.Diagnostics.Debug.Assert(paymentApp != null);
            System.Diagnostics.Debug.Assert(sysinfo != null);

            PaymentApp = paymentApp;
            Settings = new Settings(PaymentApp, sysinfo);
            Logging.Log.Init2(paymentApp);
            Config.Init(Settings, sysinfo);
            UI = new UserInterface(PaymentApp, _strings, Settings); 
            PA_AppSettings.Init(Settings, sysinfo);
            MerchantInfo.Init(Settings);
            TippingEnabled = Settings.Tipping;
            ApplicationContext.Initialise(UI, Settings, _strings, sysinfo); 
            _strings.SetLanguage(Settings.SelectedLanguage ?? ""); 

            Receipts = CreateReceiptHandler();

            PaymentApp.EventNotification += AppEventHandler;
            Sysinfo = sysinfo;
            //Logging.Log.Error("Testing OSHipPay", new PayApp());
            return true;
        }

        internal string GetTerminalStatusMessage()
        {
            return currentStatus.GetStatusText();
        }

        internal string GetTerminalStatus()
        {
            return currentStatus.GetStatusCode();
        }

        /// <summary>
        /// Create a receipt handler. Differs slightly between POS and CAPOS. 
        /// </summary>
        ReceiptHandler CreateReceiptHandler()
        {
            return new ReceiptHandler(PaymentApp, UI, _strings);
        }

        /// <summary>
        /// Handles user input events.
        /// </summary>
        /// <param name="item">A user input event</param>
        /// <param name="keyCode">The ASCII code of the input. Only applicable when
        /// the input is a letter or a digit.</param>
        /// <param name="id">Flexible display element id, not used by this component.</param>
        /// <seealso cref="IComponent.HandleUserInput"/>
        public void HandleUserInput(UserInputs item, char keyCode, int id)
        {
            Logging.Log.Debug("HandleUserInput " + item.ToString()+" "+keyCode+" "+id, new PayApp());
            if (_keyHandler != null)
            {
                _keyHandler(item, keyCode, id);
            } else
                UI?.HandleUserInput(item, keyCode);
        }

        /// <summary>
        /// Handles events from the payment application.
        /// </summary>
        /// <param name="evt">Event details</param>
        public void AppEventHandler(NotificationEventArgs evt) 
        {
            switch (evt.eventType)
            {
                case NotificationEventArgs.NotificationType.CardInserted:
                    SetStatusForCurrentStatus(StatusCodes.CardInserted);
                    Logging.Log.Debug("AppEventHandler Card Inserted ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.CardRemoved:
                    SetStatusForCurrentStatus(StatusCodes.CardRemoved);
                    Logging.Log.Informational("AppEventHandler Card removed ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.ConfigurationLoadFailed:
                    Logging.Log.Emergency("AppEventHandler configuration load failed ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.ConfigurationUpdateFailed:
                    Logging.Log.Error("AppEventHandler configuration update failed ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.StoreAndForwardTransactionSent:
                    Logging.Log.Informational("AppEventHandler Store And Forward Transaction Sent ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.TerminalBusyWithUpdates:
                    SetStatusForCurrentStatus(StatusCodes.TerminalUpdating);
                    Logging.Log.Informational("AppEventHandler Terminal Busy with Update ", new PayApp());
                    break;
                case NotificationEventArgs.NotificationType.TerminalReady:
                    SetStatusForCurrentStatus(StatusCodes.Idle);
                    Logging.Log.Informational("AppEventHandler Terminal ready", new PayApp());
                    break;
                default:
                    break;
            }
            //Notify the WebSockerProcessor if needed...
        }

        /// <summary>
        /// This function is called if the terminal has to restart because of a software
        /// upgrade or a change in settings in the management function menu.  The POS component
        /// should shut down cleanly when this is called.
        /// </summary>
        public void ShutdownPOS() 
        {
            Logging.Log.Debug("ShutdownPOS", new PayApp());
        }

        #endregion

        public void CallBackDelegate(WebSocket s, string str)
        {
            if (_webSocketProcessor != null)
                _webSocketProcessor.ProcessMessage(s, str, Settings);
            else Logging.Log.Error("Cannot answer messages since WebSocketProcessor is null", new PayApp());    
        }

        #region PreSession

        /// <summary>
        /// This fn is the Session entry point
        /// </summary>
        /// <param name="status"></param>
        /// <param name="session">Object to hold the session details (may be null)</param>
        public virtual void RunPreSession(TerminalStatus status, out SessionData session)
        {
            session = null;
            try
            {

                if (ApplicationContext.Current.ReconnectedNetwork)
                {
                    ApplicationContext.Current.ReconnectedNetwork = false;
                    if (_webSocketServer != null)
                        _webSocketServer.Close();
                    _webSocketServer = null;
                }
                Settings.Tipping = TippingEnabled;
                Logging.Log.Debug("RunPreSession starting ", new PayApp());
                if (Settings.LogLevel>0)
                    switch ((Severity)Settings.LogLevel)
                    {
                        case Severity.Warning:
                            status.LogLevel = TerminalStatus.LoggingLevels.Warning;
                            break;
                        case Severity.Error:
                            status.LogLevel = TerminalStatus.LoggingLevels.Error;
                            break;
                        case Severity.Informational:
                            status.LogLevel = TerminalStatus.LoggingLevels.Status;
                            break;
                        case Severity.Debug:
                            status.LogLevel = TerminalStatus.LoggingLevels.Debug;
                            break;
                        default:
                            status.LogLevel = TerminalStatus.LoggingLevels.Spam;
                            break;

                    }

#if DEBUG


                /*
                if (!PaymentApp.Display(new ImageData { ImageResourceName = "POS.Example.images.hippo.png" }))
                {
                    Logging.Log.Debug("RunPreTransaction Logo failed " + OSHipPay.Configuration.Config.imageData.ImageResourceName, new PayApp());
                }
                UI.Pause(TimeSpan.FromSeconds(2));
                */
                // Display who we are and our version. Only for testing.
                UI.Display(GetType().Name, GetType().Assembly.GetName().Version.ToString());
                UI.Pause(TimeSpan.FromSeconds(1));
#endif
                if (_transactionInfo == null)
                {
                    _transactionInfo = new TransactionRepositorySQLite(Path.Combine(Sysinfo.PersistentDirectory, "transactions.sqlite"));
#if DEBUG
                    ((TransactionRepositorySQLite)_transactionInfo).Initialise(); // a developer can re-create the table here
#else
                    ((TransactionRepositorySQLite)_transactionInfo).Initialise(false);
#endif
                    Logging.Log.Debug("RunPreSession initialized transactions ", new PayApp());
                }
#if DEBUG
                if (_transactionInfo != null)
                {
                    long count = _transactionInfo.GetCount();
                    Logging.Log.Spam("Count: " + count, new PayApp());
                    var list =  _transactionInfo.GetECRIdentifiers();
                    foreach (TransactionStatus t in list)
                    {
                        Logging.Log.Spam("Found ids: " + t.CashRegisterId + " " + t.CashRegisterPaymentId + " for " + t.Id, new PayApp());
                    }
                }
#endif
                /*
                if (_transactionIdentifiers == null)
                {
                    _transactionIdentifiers = new TransactionIdentifiersRepositorySQLite(Path.Combine(Sysinfo.PersistentDirectory, "transactionIdentifiers.sqlite"));
#if DEBUG
                    ((TransactionRepositorySQLite)_transactionInfo).Initialise(); // a developer can re-create the table here
#else
                    ((TransactionRepositorySQLite)_transactionInfo).Initialise(false);
#endif
                    Logging.Log.Debug("RunPreSession initialized transactionIdentifiers ", new PayApp());
                }
                */
                SessionContext.New();

                SessionContext.Current.Terminal = status;
                ApplicationContext.Current.CheckInstalledLanguages(status.LanguagesInstalled);

                bool shouldShowMenu = SessionContext.Last.IsValid && !SessionContext.Last.HasDoneSomething;
                if (ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession)
                {
                    ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession = false;
                    if (UI.CheckDailyPassword2(null))
                    {
                        ShowTerminalSettings();
                    }
                }
                else
                if (ApplicationContext.Current.DiagnosticsOnNextSession)
                {
                    ApplicationContext.Current.DiagnosticsOnNextSession = false;
                    ShowDiagnostics(Receipts);
                }
                else if (ApplicationContext.Current.HandleTransactionListOnNextSession)
                {
                    ApplicationContext.Current.HandleTransactionListOnNextSession = false;
                    AskForTransactionListDateAndHandleIt();
                } 

                // To prepare the session there are a number of things that need to be checked
                // and to make sure we don't forget to check everything in case something changes
                // we do this forever-loop. It exits if we should open the management menu or if we
                // know we have good session data.
                for (; ; )
                {
                    // First, check if we should open the managment functions.
                    if (ApplicationContext.Current.OpenManagmentFunctionsOnNextSession)
                    {
                        // The configuration managment should be opened.
                        // This is done by exiting with a null session.
                        ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = false;
                        if (UI.CheckDailyPassword2(null))
                        {
                            SessionContext.Current.HasDoneSomething = true;
                            session = null;
                        } else
                            session = GetSessionData();
                        break;
                    }

                    // Next, check if we have all required settings.
                    if (GetSessionData() == null)
                    {
                        SetStatusForCurrentStatus(StatusCodes.GeneralFailure);
                        Logging.Log.Debug("RunPreSession GetSettionData()==null -> show TerminalSettingsScreen ", new PayApp());
                        // Something's missing. Let the user check all settings and then try again.
                        if (UI.CheckDailyPassword2(null))
                        {
                            ShowTerminalSettings();
                        }
                        continue;
                    }

                    if (shouldShowMenu)
                    {
                        SetStatusForCurrentStatus(StatusCodes.GeneralFailure);
                        // Something went wrong after the last RunPreSession. In order to prevent
                        // the terminal from continously looping on RunPreSession, show the terminal menu now.
                        Logging.Log.Error("RunPreSession something went wrong -> show TerminalMenuScreen ", new PayApp());
                        if (UI.CheckDailyPassword2(null))
                        {
                            SessionContext.Current.HasDoneSomething = true;
                            session = null;
                            UI.Display(new TerminalMenuScreen());
                            shouldShowMenu = false;
                        }
                        continue;
                    }

                    if (PA_AppSettings.WifiOff)
                    {
                        /*
                        SetNetworkInterfacePowerLevel(NetworkInterfaceAdapterType.Wifi,
                            NetworkInterfacePower.Off);
                        */
                    }
                    else
                    {
                        try
                        {
                            var res = PaymentApp.GetNetworkInterfaceAdapterSettings(
                                out NetworkInterfaceAdapterSettings wifiAdapterSettings,
                                NetworkInterfaceAdapterType.Wifi); // it crashes when wifi network adapter is off
                                                                   // if(res) bug in PA: https://support.westint.se/hc/requests/4132
                            if (wifiAdapterSettings != null &&
                                !String.IsNullOrEmpty(Settings.WifiSSID))
                            {
                                PaymentApp.SetNetworkInterfacePowerLevel(NetworkInterfaceAdapterType.Wifi,
                                    NetworkInterfacePower.On);
                                //_paymentApp.ConnectToWifiNetwork(AppSettings.WifiSSID, AppSettings.WifiPassword);
                                var x = PaymentApp.AddWifiNetwork(Settings.WifiSSID, Settings.WifiPassword, true);
                                Log.VSlog(x.ToString(), this);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.VSlog(e.ToString(), this);
                            TryToReconnectWifi();
                            //this fails all the time since GetNetworkInterfaceAdapterSettings is not implemented...
                        }

                        if (!String.IsNullOrEmpty(Settings.WifiAddress))
                        {
                            try
                            {
                                SetNetworkSettings(Settings.WifiAddress, NetworkInterfaceAdapterType.Wifi);
                            }
                            catch (Exception e)
                            {
                                session = null;
                                Logging.Log.Error("Cannot set Wifi network settings: ", e, this);

                                UI.FourLinesAndOK(_strings.CannotSetNetworkSettings, "", "", "");

                                return;
                            }
                        }


                    }
                    // Get the session parameters again. If they are ok then we are done.
                    session = GetSessionData();
                    /*
                     *              if (Settings.CloseBatchTime==null)
                                    {
                                        Settings.CloseBatchTime = DateTime.Now.AddMinutes(1).TimeOfDay;
                                    }
                                    */
                    //                    session.CloseBatchTime = Settings.CloseBatchTime;
                    if (session != null)
                    {
                        if (PA_AppSettings.LogFileAddress != null)
                        {
                            session.LogfileUploadTarget = new FtpDestination(PA_AppSettings.LogFileAddress, "OSPayLogger",
                                "OSPayLogger", "");
                            Logging.Log.Debug("RunPreSession LogFileUploadTarget set to  " + PA_AppSettings.LogFileAddress, new PayApp());
                        }
                        if (PA_AppSettings.APN != null)
                            session.GsmConfiguration = new GsmSettings()
                            {
                                APN = PA_AppSettings.APN,
                                Description = "Ospay",
                                DialNumber = "*99#"
                            };
                        session.KeyBeepDefault = Settings.KeyBeepDefault;
                        SessionContext.Current.TerminalId = session.TerminalId;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Log.Error("RunPreSsession failed", e, this);
            }
        }


        private void ShowTerminalSettings()
        {
            UI.Display(new TerminalSettingsScreen());
            /*
            var WifiSSID =
                ShowStringInputDialog(ref _keyHandler, _keyHandlerLock, "WifiSSID", Settings.WifiSSID);
            if (WifiSSID != null)
                Settings.WifiSSID = WifiSSID;
            var WifiPassword =
                ShowStringInputDialog(ref _keyHandler, _keyHandlerLock, "WifiPassword", Settings.WifiPassword);
            if (WifiPassword != null)
                Settings.WifiPassword = WifiPassword;
             */
        }

        private void SetNetworkSettings(string settingsString, NetworkInterfaceAdapterType adapterType)
        {
            var settings = Utils.ParseNetworkSettings(settingsString);

            if (settings == null)
            {
                Logging.Log.Error("Cannot parse network settings " + settingsString + " for adapter: " + adapterType, this);
                return;
            }

            try
            {
                if (!PaymentApp.SetNetworkInterfaceAdapterSettings(settings, adapterType))
                    Logging.Log.Warning("Failed to set network settings " + settingsString + " for adapter: " + adapterType, this);
            }
            catch (Exception x)
            {
                Logging.Log.Warning("Exception in setting network settings " + settingsString + " for adapter: " + adapterType + " - " + x, this);
            }
        }


        private void AskForTransactionListDateAndHandleIt()
        {
            Logging.Log.Debug("AskForTransactionListDateAndHandleIt started", new PayApp());
            DateTime dt = DateTime.Now;
            bool cancel = false;
            var editor = new DateSettingScreen(UserInterface.Strings.GetDate,"", true);
            Logging.Log.Debug("AskForTransactionListDateAndHandleIt editor "+editor, new PayApp());
            editor.Cancelled += (sender, args) => { cancel = true; };
            editor.Completed += (sender, args) =>
            {
                if (((DateSettingScreen) sender).date.IsNotNullOrEmpty())
                {
                    Logging.Log.Debug(
                        "AskForTransactionListDateAndHandleIt editor  Completed " + dt.ToShortDateString(),
                        new PayApp());
                    dt = DateTime.Parse(((DateSettingScreen) sender).date, _strings.GetCultureInfo().DateTimeFormat);
                    HandleTransactionList(dt.StartOfDay(), dt.EndOfDay());
                    cancel = true;
                }
            };
            UI.Display(editor);
            if (cancel) return;
            Logging.Log.Debug("AskForTransactionListDateAndHandleIt going to HandleTransactionList with default DT ", new PayApp());
            HandleTransactionList(dt.StartOfDay().AddDays(-Settings.TransactionDaysLimit), DateTime.Now);
        }

        private void SetNetworkInterfacePowerLevel(NetworkInterfaceAdapterType adapterType, NetworkInterfacePower State)
        {
            Logging.Log.Debug("SetNetworkInterfacePowerLevel "+adapterType+" "+State, new PayApp());
            PaymentApp.SetNetworkInterfacePowerLevel(adapterType, State);
        }

        internal Messages.TransactionResult GetTransactionResultForRef(string sRef)
        {
            TransactionStatus tr = _transactionInfo.GetTransaction(sRef);
            if (tr != null)
               return new Messages.TransactionResult(tr);
            return null;
        }

        bool CheckIfPrinterIsAvailable()
        {
            PaymentApp.GetPrinterStatus(out PrinterState ps);
            if (ps != null)
            {
                if (ps.PaperOK && ps.PrinterOnline)
                    return true;
            }

            return false;
        }


        private void ShowDiagnostics(ReceiptHandler rh)
        {
            Logging.Log.Debug("ShowDiagnostics starting ", new PayApp());
            ReceiptBuilder rb = null;
            bool bNoPrinter = Settings.NoPrinter && !CheckIfPrinterIsAvailable();
            if (!bNoPrinter)
                rb = new ReceiptBuilder(PaymentApp, _strings.GetCultureInfo(), Settings);
            try
            {
                long connTime;
                if (rb != null) {
                    rb.AddString(rb.FormatDateTime(DateTime.Now));
                    rb.AddString(MerchantInfo.Name);
                    rb.AddString(MerchantInfo.OrganisationNumber);
                }
                FlexibleDisplay fd = new FlexibleDisplay(5);
                int id = 1;
                var leftWidth = 25.0f;
                var rightWitdh = 75.0f;
                connTime = License.GetAndPingDefaultGateway(out IPAddress gwaddress);
                fd.Elements.Add(new FlexibleDisplayElement("IP", FlexibleDisplayElement.SizeLarge, 0, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("WAITING", FlexibleDisplayElement.SizeLarge, 0, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("GW", FlexibleDisplayElement.SizeLarge, 1, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("WAITING", FlexibleDisplayElement.SizeLarge, 1, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("SPDH", FlexibleDisplayElement.SizeLarge, 2, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("WAITING", FlexibleDisplayElement.SizeLarge, 2, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("PPL", FlexibleDisplayElement.SizeLarge, 3, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("WAITING", FlexibleDisplayElement.SizeLarge, 3, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("LIC.", FlexibleDisplayElement.SizeLarge, 4, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("WAITING", FlexibleDisplayElement.SizeLarge, 4, rightWitdh, false, id));
                UI.Display(fd);
                License.GetIPAddress(out string address);
                string gw = gwaddress.ToString() + " " + ((connTime >= 0) ? ("OK" + ", " + connTime.ToString() + "ms") : "FAIL");
                string spdh = (Utils.IsConnectionPossible(PA_AppSettings.SPDHHost, out connTime) ? "OK" + ", " + connTime.ToString() + "ms" : "FAIL") ;
                string ppl = (Utils.IsConnectionPossible(PA_AppSettings.PPLHost, out connTime) ? "OK" + ", " + connTime.ToString() + "ms" : "FAIL") ;
                string license = (Utils.IsConnectionPossible(PA_AppSettings.LicenseServer, out connTime) ? "OK" + ", " + connTime.ToString() + "ms" : "FAIL") ;
                StringBuilder stb = new StringBuilder();
                stb.Append("ShowDiagnostics: ");
                stb.Append("IP: " + address+" ");
                stb.Append("GW: " + gw + " ");
                stb.Append("SPDH: " + spdh + " ");
                stb.Append("PPL: " + ppl + " ");
                stb.Append("LICENSE: " + license + " ");
                stb.Append("SSID: " + Settings.WifiSSID + " ");
                UI.UpdateFlexibleItem(1, address, null, null);
                UI.UpdateFlexibleItem(2, gw, null, null);
                UI.UpdateFlexibleItem(3, spdh, null, null);
                UI.UpdateFlexibleItem(4, ppl, null, null);
                UI.UpdateFlexibleItem(5, license, null, null);
                Logging.Log.Informational(stb.ToString(), new PayApp());
                if (rb!=null)
                    rb.AddString(" ");
                    rb.AddString("IP: " + address);
                    rb.AddString("GW: " + gw);
                if (rb != null) {
                    rb.AddString("SPDH: " + spdh);
                }
                if (rb != null) {
                    rb.AddString("PPL: " + ppl);
                }
                if (rb != null)
                {
                    rb.AddString("LICENSE: " + license);
                    rb.AddString("SERIAL NUMBER:" + PA_AppSettings.SerialNumber);
                    rb.AddString("TERMINAL ID:" + PA_AppSettings.TerminalId);
                    rb.AddString("PA v." + PA_AppSettings.GetVersion());
                    rb.AddString("APP v." + License.GetVersion());
                    rb.AddString(("MAC: "+Utils.GetMacAddress()));
                    /*
                    foreach (var m in Utils.GetNetworkInterfaces(NetworkInterfaceAdapterType.Wifi))
                    {
                        rb.AddString("WIFI : " + m.GetPhysicalAddress().ToString());
                    }
                    foreach (var m in Utils.GetNetworkInterfaces(NetworkInterfaceAdapterType.Lan))
                    {
                        rb.AddString("LAN : " + m.GetPhysicalAddress().ToString());
                    }
                    */
#if DEBUG
                    rb.AddString(Utils.LogMemoryStatus(true));
#endif
                    rh.Print(rb);
                }

            }
            catch (Exception e)
            {
                Logging.Log.Error("ShowDiagnostics Failed", e, this);
            }
            char ch = UI.InputKey(Utils.GetEnumValues<UserInputs>());
            if (ch=='\r')
                   ShowTerminalInfo(rh);

        }


        private void ShowTerminalInfo(ReceiptHandler rh)
        {
            Logging.Log.Debug("ShowTerminalInfo starting ", new PayApp());
            try
            {
                FlexibleDisplay fd = new FlexibleDisplay(5);
                fd.Elements.Add(new FlexibleDisplayElement("SERIAL NUMBER", FlexibleDisplayElement.SizeRegular, 0, 100, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement(PA_AppSettings.SerialNumber, FlexibleDisplayElement.SizeRegular, 1, 100, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement("TERMINAL ID", FlexibleDisplayElement.SizeRegular, 2, 100, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement(PA_AppSettings.TerminalId, FlexibleDisplayElement.SizeRegular, 3, 100, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement(" ", FlexibleDisplayElement.SizeRegular, 4, 100, false, 0));
                PaymentApp.Display(fd);
            }
            catch (Exception e)
            {
                Logging.Log.Error("ShowTerminalInfo failed", e, this);
            }
            char ch = UI.InputKey(Utils.GetEnumValues<UserInputs>());
            if (ch == '\r')
                ShowCurrentNetworkSettings(rh);
        }

        private void ShowCurrentNetworkSettings(ReceiptHandler rh)
        {
            Logging.Log.Debug("ShowCurrentNetworkSettings starting ", new PayApp());
            try
            {
                ReceiptBuilder rb = null;
                bool bNoPrinter = Settings.NoPrinter && !CheckIfPrinterIsAvailable();
                if (!bNoPrinter)
                    rb = new ReceiptBuilder(PaymentApp, _strings.GetCultureInfo(), Settings);
                FlexibleDisplay fd = new FlexibleDisplay(6);
                int id = 1;
                var leftWidth = 25.0f;
                var rightWitdh = 75.0f;

                var subnet = "";
                var gw = "";
                var lease = "";
                var dhcpActive = false;
                var mac = Utils.GetMacAddress();
                var key = "";
                WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);

                License.GetIPAddress(out string ip);
                subnet = License.GetSubnetMask(ip);
                gw = License.GetDefaultGateway();
                lease = "" + wifiManager.DhcpInfo.LeaseDuration / 1000;
                dhcpActive = wifiManager.DhcpInfo.LeaseDuration > 0;
                License.GetIPAddress(out ip);
                if (rb != null) { 
                    bool bFound = false;
                    foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        if (nic.GetPhysicalAddress().ToString().Equals(mac))
                        {
                            key = nic.Name.ToString().ToUpper();
                            var info = Utils.GetInterfaceInfoString(nic);
                            foreach (String s in info.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None))
                            {
                                rb.AddString(s);

                            }
                            Logging.Log.Informational(info, this);
                            bFound = true;
                            break;
                        }
                        if (bFound) break;
                    }
                    /*
                                        foreach (var iface in Utils.GetNetworkInterfaces(NetworkInterfaceAdapterType.Wifi))
                                        {
                                            if (iface != null)
                                            {

                                                foreach (UnicastIPAddressInformation unicastIPAddressInformation in iface.GetIPProperties().UnicastAddresses)
                                                {
                                                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                                                    {
                                                        if (ip.Equals(unicastIPAddressInformation.Address.ToString()))
                                                        {
                                                            //mac = iface.GetPhysicalAddress().ToString();
                                                            key = iface.Name.ToString().ToUpper();
                                                            var info = Utils.GetInterfaceInfoString(iface);
                                                            foreach (String s in info.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None))
                                                            {
                                                                rb.AddString(s);

                                                            }
                                                            Logging.Log.Informational(info, this);
                                                            bFound = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (bFound) break;
                                        }
                    */
                    if (bFound)
                        rh.Print(rb);
                }
                fd.Elements.Add(new FlexibleDisplayElement(key, FlexibleDisplayElement.SizeLarge, 0, leftWidth, false, 0));
                fd.Elements.Add(new FlexibleDisplayElement(ip, FlexibleDisplayElement.SizeLarge, 0, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("SUBNET", FlexibleDisplayElement.SizeLarge, 1, leftWidth, false, -1));
                fd.Elements.Add(new FlexibleDisplayElement(subnet, FlexibleDisplayElement.SizeLarge, 1, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("GW", FlexibleDisplayElement.SizeLarge, 2, leftWidth, false, -1));
                fd.Elements.Add(new FlexibleDisplayElement(gw, FlexibleDisplayElement.SizeLarge, 2, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("DHCP", FlexibleDisplayElement.SizeLarge, 3, leftWidth, false, -1));
                fd.Elements.Add(new FlexibleDisplayElement(dhcpActive.ToString().ToUpper(), FlexibleDisplayElement.SizeLarge, 3, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("LEASE", FlexibleDisplayElement.SizeLarge, 4, leftWidth, false, -1));
                fd.Elements.Add(new FlexibleDisplayElement(dhcpActive ? lease : String.Empty, FlexibleDisplayElement.SizeLarge, 4, rightWitdh, false, id++));
                fd.Elements.Add(new FlexibleDisplayElement("MAC", FlexibleDisplayElement.SizeLarge, 5, leftWidth, false, -1));
                fd.Elements.Add(new FlexibleDisplayElement(mac, FlexibleDisplayElement.SizeLarge, 5, rightWitdh, false, id++));
                PaymentApp.Display(fd);
                StringBuilder stb = new StringBuilder();
                stb.Append("ShowCurrentNetworkSettings: ");
                stb.Append("Key: " + key+ " ");
                stb.Append("IP: " + ip + " ");
                stb.Append("Subnet: " + subnet + " ");
                stb.Append("GW: " + gw + " ");
                stb.Append("DHCP: " + dhcpActive.ToString().ToUpper() + " ");
                stb.Append("MAC: " + mac + " ");
                stb.Append("SSID: " + Settings.WifiSSID + " ");
                Logging.Log.Informational(stb.ToString(), new PayApp());
            }
            catch (Exception e)
            {
                Logging.Log.Error("ShowCurrentNetworkSettings failed", e, this);
            }
            char ch = UI.InputKey(Utils.GetEnumValues<UserInputs>());
        }

        private bool CheckForNetwork()
        {
            if (Settings.WifiSSID!=null)
            try
            {
                
            } catch (Exception e)
            {
                Logging.Log.Error("CheckForNetwork " + e.Message, new PayApp());
                return false;
            }
            else if (!PA_AppSettings.WifiOff)
                Logging.Log.Debug("CheckForNetwork no WifiSSID defined ", new PayApp());

            return true;
        }

        private void TryToReconnectWifi()
        {
//            Logging.Log.Debug("TryToReconnectWifi initializing ", new PayApp());
            if (Settings.WifiSSID != null)
                try
                {
                    WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
                    WifiInfo wifiInfo = wifiManager.ConnectionInfo;
                    if (wifiInfo.NetworkId == -1)
                    {
                        Logging.Log.Debug("TryToReconnectWifi wifi wasn't connected ",
                            new PayApp());
                    }
                    else return;
                    int netId = -1;
                    String configureSSID = Settings.WifiSSID.ToUpper();
                    foreach (var id in wifiManager.ConfiguredNetworks)
                    {
                        String ssid = id.Ssid.ToUpper();
                        ssid = ssid.Replace("\"", String.Empty);
                        Logging.Log.Debug("CheckForNetwork SSID " + ssid, new PayApp());
                        if (ssid.Equals(configureSSID))
                        {
                            Logging.Log.Debug("TryToReconnectWifi found the defined SSID " + Settings.WifiSSID, new PayApp());
                            netId = id.NetworkId;
                            break;
                        }
                    }

                    if (netId > -1)
                    {
                        Logging.Log.Debug("TryToReconnectWifi trying to reconnect to " + Settings.WifiSSID, new PayApp());
                        wifiManager.Disconnect();
                        wifiManager.EnableNetwork(netId, true);
                        wifiManager.Reconnect();
                        ApplicationContext.Current.ReconnectedNetwork = true;
                        Logging.Log.Debug("TryToReconnectWifi tried to reconnect to " + Settings.WifiSSID, new PayApp());
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Error("TryToReconnectWifi " + e.Message, new PayApp());
                }
            else if (!PA_AppSettings.WifiOff)
                Logging.Log.Debug("TryToReconnectWifi no WifiSSID defined ", new PayApp());
        }

        /// <summary>
        /// Retrieves the terminal data stored in the registry.  The dat is used in a SessionData object.
        /// </summary>
        SessionData GetSessionData()
        {
            // DCAPP always uses the FTP port.
            const int DCAPPPort = 21;

            try
            {
                string tspId = Settings.TerminalId;
                string pplAddress = Settings.PPLAddress;
                int pplPort = Settings.PPLPort;
                string spdhHost = Settings.SPDHAddress;
                int spdhPort = Settings.SPDHPort;
                string spdhHost2 = Settings.SPDHAddress2;
                int spdhPort2 = Settings.SPDHPort2;

                if ((pplPort != 0) &&
                    (spdhPort != 0) &&
                    (!string.IsNullOrEmpty(pplAddress)) &&
                    (!string.IsNullOrEmpty(spdhHost)) &&
                    (!string.IsNullOrEmpty(tspId)))
                {
                    pplAddress = pplAddress.SanitiseIPAddress();
                    spdhHost = spdhHost.SanitiseIPAddress();
                    if (!String.IsNullOrEmpty(spdhHost2))
                    {
                        spdhHost2 = spdhHost2.SanitiseIPAddress();
                    }
                    IPEndPoint ppl = null;
                    try
                    {
                        ppl = new IPEndPoint(IPAddress.Parse(pplAddress), pplPort);
                    } catch (Exception e2)
                    {
                        Logging.Log.Error("Exception in PPLAddress " + pplAddress, e2, new PayApp());
                    }
                    IPEndPoint spdh = null;
                    try
                    {
                        spdh = new IPEndPoint(IPAddress.Parse(spdhHost), spdhPort);
                    }
                    catch (Exception e2)
                    {
                        Logging.Log.Error("Exception in spdhHost " + spdhHost, e2, new PayApp());
                    }
                    IPEndPoint spdh2 = null;
                    try
                    {
                            spdh2 = spdhHost2.IsNotNullOrEmpty()
                                           ? new IPEndPoint(IPAddress.Parse(spdhHost2), spdhPort2)
                                           : null;
                    }
                    catch (Exception e2)
                    {
                        Logging.Log.Error("Exception in spdhHost2 " + spdhHost2, e2, new PayApp());
                    }
                    IPEndPoint dcapp = null;
                    try
                    {
                        if (Settings.DCAPPHost != null)
                        {
                            dcapp = new IPEndPoint(Settings.DCAPPHost, DCAPPPort);
                        }
                    }
                    catch (Exception e2)
                    {
                        Logging.Log.Error("Exception in DCAPPHost " + Settings.DCAPPHost, e2, new PayApp());
                    }
                    return new SessionData(tspId, ppl, spdh, spdh2, dcapp, DateTime.UtcNow, null, _strings.GetIso639_2());
                }
            }
            catch
            {
                // ignore
            }

            return null;
        }

        #endregion

        #region PreTransaction


        /// <summary>
        /// This fn enables the operator to select / start a transaction
        /// </summary>
        /// <param name="termStat">Terminal status</param>
        /// <param name="transaction">Object to hold the transaction details</param>
        public void RunPreTransaction(TerminalStatus termStat, out TransactionData transaction)
        {
            Logging.Log.Debug("RunPreTransaction initializing - stat: "+termStat.ToString(), new PayApp());
            try
            {
                SessionContext.Current.HasDoneSomething = true;
                SessionContext.Current.Terminal = termStat;

                TransactionContext.New();

                if (!CheckForNetwork())
                {
                    //Network not connected....
                }

                if (_webSocketProcessor == null)
                    _webSocketProcessor = new WebSocketProcessor(this, UI, Settings);

                try
                {
                    if (_webSocketServer == null)
                    {
                         _webSocketServer = new WebSocketServerBase(Settings.WebSocketPort, _callBackFunction);
                    }
                }
                catch (Exception e)
                {
                    if (_webSocketServer != null)
                        _webSocketServer.Close();
                    _webSocketServer = null;
                    Logging.Log.Error("RunPreTransaction WebSocket failed", e, this);
                }
                if (Settings.UDPPort > 0)
                {
                    UDP.SetPort(Settings.UDPPort);
                    UDPListener.SetPort(Settings.UDPPort);
                }
                try
                {
                    if (_udpListener == null)
                    {
                        _udpListener = new UDPListener();
                    }

                }
                catch (Exception e)
                {
                    if (_udpListener != null)
                        _udpListener.Close();
                    _udpListener = null;
                    Logging.Log.Error("RunPreTransaction UDPListener failed", e, this);
                }
                /*
                if (ApplicationContext.Current.CloseBatchOnNextSession)callB                {

                    IScreen scr = new UI.Screens.CloseBatchScreen();
                    UI.Display(scr);
                    HandleBatchData(PaymentApp.GetCurrentBatchTotals());
                    ApplicationContext.Current.CloseBatchOnNextSession = false;
                }
                */
                if (TransactionContext.Current.NextTransaction == null)
                {
                    if (Settings.IsPinPadMode())
                    {
                        OSHipPay.Configuration.Config.imageData.ImageResourceName = "POS.Example.images."+PA_AppSettings.Logo+".png";
                        new OSHipPay.Configuration.Config();
                        if (!UI.Display(OSHipPay.Configuration.Config.imageData))
                        {
                            Logging.Log.Debug("RunPreTransaction Logo failed "+ OSHipPay.Configuration.Config.imageData.ImageResourceName, new PayApp());
                            DisplayData d = new DisplayData(DisplayData.DisplayTemplate.FourLines);
                            d.AddLine(new DisplayData.TextLine("OSHipPay", DisplayData.Alignment.Centre, DisplayData.Size.Large));
                            d.AddLine(new DisplayData.TextLine(_strings.SplashScreen_Waiting, DisplayData.Alignment.Centre, DisplayData.Size.Regular));
                            UI.Display(d);

                        }
                        SetStatusForCurrentStatus(StatusCodes.Idle);
                        char ch = InputCharacter.Timeout;
                        List<UserInputs> keys = new List<UserInputs>
                        {
                            UserInputs.Tab
                        };
                        while ( (ch = UI.InputKeyWithTimeOut(keys)) == InputCharacter.Timeout) {
                            Thread.Sleep(100);
                        }
                        if (ch == InputCharacter.Tab )
                        {
                            SetStatusForCurrentStatus(StatusCodes.Setup);
                            //Cancel for now... Show Meny...
                            //UI.Display(new OSMenyScreen());
                            ShowOsMenuFlexible();
                        }
                        else if(ch == InputCharacter.Continue)
                        {
                            SetStatusForCurrentStatus(StatusCodes.StartTransaction);
                        }
                        Logging.Log.Debug("RunPreTransaction "+ch, new PayApp());
                    }
                    else
                    {
                        //Show a different screen in standalone mode...
                        UI.Screens.PurchaceRefundScreen scr = new UI.Screens.PurchaceRefundScreen(TransactionType.Purchase);
                        SetStatusForCurrentStatus(StatusCodes.StartTransaction);
                        UI.Display(scr);
                        if (scr.IsCancelled)
                        {
                            SetStatusForCurrentStatus(StatusCodes.Setup);
                            //Cancel for now... Show Meny...
                            UI.Display(new OSMenyScreen());
                            //new OSMenyFlexibleScreen(UI);

                        }
                    }
                }

            } catch (Exception e)
            {
                Logging.Log.Error("RunPreTransaction ", e, new PayApp());
            }
            transaction = TransactionContext.Current.NextTransaction;

            if (transaction != null)
            {
                if (!ApplicationContext.Current.Settings.Tipping)
                {
                    transaction.Options = DisabledTippingOptions;
                }
            }

        }

        private void ShowOsMenuFlexible()
        {
            FlexibleDisplay fd = new FlexibleDisplay(7);
            fd.Elements.Add(new FlexibleDisplayElement(_strings.setupMenu_title, FlexibleDisplayElement.SizeLarge, 0, 100, false, 0) { HorizontalAlignment = StringAlignment.Center });
            fd.Elements.Add(new FlexibleDisplayElement("1. "+_strings.tmenu_transactionlist, FlexibleDisplayElement.SizeRegular, 1, 100, true, 1000) { BackgroundColour = Color.LightBlue, VisibleClick = true });
            fd.Elements.Add(new FlexibleDisplayElement("2. " + _strings.diagTitle, FlexibleDisplayElement.SizeRegular, 2, 100, true, 1001) { BackgroundColour = Color.LightBlue });
            fd.Elements.Add(new FlexibleDisplayElement("3. " + _strings.setupMenu_setup, FlexibleDisplayElement.SizeRegular, 3, 100, true, 1002) { BackgroundColour = Color.LightBlue });
            fd.Elements.Add(new FlexibleDisplayElement("4. " + _strings.setupMenu_managment, FlexibleDisplayElement.SizeRegular, 4, 100, true, 1003) { BackgroundColour = Color.LightBlue });
            fd.Elements.Add(new FlexibleDisplayElement(_strings.cancel, FlexibleDisplayElement.SizeSmall, 5, 45, true, 1050) { BackgroundColour = Color.Red, TextColour = Color.White, HorizontalAlignment = StringAlignment.Center });
            //fd.Elements.Add(new FlexibleDisplayElement(_strings.okButton, FlexibleDisplayElement.SizeSmall, 5, -45, true, 1051) { BackgroundColour = Color.Green, TextColour = Color.White, HorizontalAlignment = StringAlignment.Center });
            List<UserInputs> keys = new List<UserInputs>
            {
                UserInputs.One, UserInputs.Two, UserInputs.Three, UserInputs.Four,
                UserInputs.Enter,
                UserInputs.Cancel,
                UserInputs.SingleTap
            };
            UI.Display(fd);
            UserInputs input;
            SingleKeyInputter ski = new SingleKeyInputter((IPaymentApp)PaymentApp, ref _keyHandler, _keyHandlerLock, keys);
            input = ski.WaitForFinish(ref _keyHandler);
            switch (input)
            {
                case UserInputs.One:
                    ApplicationContext.Current.HandleTransactionListOnNextSession = true;
                    break;
                case UserInputs.Two:
                    ApplicationContext.Current.DiagnosticsOnNextSession = true;
                    break;
                case UserInputs.Three:
                    ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession = true;
                    break;
                case UserInputs.Four:
                    ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true;
                    break;
                case UserInputs.SingleTap:
                    Logging.Log.VSlog("ButtonId = " + ski.ButtonId, this);
                    if (ski.ButtonId == 1000)
                    {
                        ApplicationContext.Current.HandleTransactionListOnNextSession = true;
                        break;
                    }
                    else if (ski.ButtonId == 1001)
                    {
                        ApplicationContext.Current.DiagnosticsOnNextSession = true;
                        break;
                    }
                    else if (ski.ButtonId == 1002)
                    {
                        ApplicationContext.Current.OpenTerminalSettingsScreenOnNextSession = true;
                        break;
                    }
                    else if (ski.ButtonId == 1003)
                    {
                        ApplicationContext.Current.OpenManagmentFunctionsOnNextSession = true;
                        break;
                    }
                    break;
                default:
                    break;

            }

        }

        #endregion

        #region Callbacks / Miscellaneous

        /// <summary>
        /// This method provides a way for the POs component to attempt to perform asynchronous
        /// operations once the transaction has begun, e.g. cancelling the transaction due to
        /// some external event.
        /// </summary>
        /// <param name="operation">The operation to perform</param>
        /// <returns>True if the operation is possible at this time, false if it cannot be performed</returns>
        public bool RequestAsynchronousOperation(AsynchronousOperations operation)
        {
            return PaymentApp.RequestAsynchronousOperation(operation);
        }


        /// <summary>
        /// Asks one of a fixed set of questions of the terminal operator, e.g. verify signature, get payment
        /// code, etc.
        /// </summary>
        /// <param name="request">Specifies data required by the payment application</param>
        /// <param name="response">Returned data</param>
        public void GetSpecificData(SpecificRequest request, out SpecificResponse response)
        {
            switch (request.RequiredInformation)
            {
                case SpecificRequest.QueryType.SignatureVerification:
                {
                    response = HandleSignatureVerification(request.Receipt);
                    break;
                }

                case SpecificRequest.QueryType.VoiceReferralCode:
                {
                    response = HandleVoiceReferralCode(request);
                    break;
                }

                case SpecificRequest.QueryType.PaymentCode:
                {
                    response = HandlePaymentCode(request);
                    break;
                }

                case SpecificRequest.QueryType.ParameterUpdateConfirmation:
                {
                    bool ok = UI.YesNo(_strings.q_updateParameters);
                    response = SpecificResponse.CreateParameterUpdateConfirmation(ok);
                    break;
                }

                case SpecificRequest.QueryType.VATAmount:
                {
                    response = HandleVATAmount(request);
                    break;
                }

                case SpecificRequest.QueryType.EmbossedCardImprintTaken:
                {
                    throw new NotImplementedException();
                }

                case SpecificRequest.QueryType.FallbackToMagstripe:
                {
                    bool ok = UI.YesNo(_strings.fallbackToMagstripe1, _strings.fallbackToMagstripe2);
                    response = SpecificResponse.CreateFallbackToMagstripeResponse(ok);
                    break;
                }

                default:
                {
                    // If we get here then we signal our dissatisfaction with a null
                    response = null;
                    break;
                }
            }
        }

        SpecificResponse HandleVATAmount(SpecificRequest request)
        {
            throw new NotImplementedException();
        }

        SpecificResponse HandlePaymentCode(SpecificRequest request)
        {
            throw new NotImplementedException();
        }

        SpecificResponse HandleVoiceReferralCode(SpecificRequest required)
        {
            var screen = new VoiceReferralCodeScreen(required.SupplementaryText);
            UI.Display(screen);

            string code = (screen.Value ?? "").ToUpper();
            SpecificResponse response = SpecificResponse.CreateVoiceReferralResponse(!screen.IsCancelled, code);

            return response;
        }

        /// <summary>
        /// The customer has chosen signature verification and the PA needs to know if the signature is ok or not.
        /// The request contains receipt data and we are expected to print the merchant receipt so that the customer
        /// has sign it.
        /// </summary>
        /// <param name="receipt">The receipt data for the merchant receipt.</param>
        SpecificResponse HandleSignatureVerification(ReceiptData receipt)
        {
            bool isSignatureOk = false;

            if (receipt != null && !Settings.NoPrinter)
            {
                bool isMerchantReceiptPrinted = Receipts.PrintMerchantReceipt(receipt, Settings);
                if (isMerchantReceiptPrinted)
                {
                    TransactionContext.Current.IsMerchantReceiptPrinted = true;

                    // The operator has to verify that the signature is ok.
                    isSignatureOk = UI.OkCancel(_strings.verifySignature);
                }
                else
                {
                    // The merchant receipt was not printed.
                    UI.Display(_strings.technicalError);
                }
            }

            return SpecificResponse.CreateSignatureVerificationResponse(isSignatureOk);
        }


        /// <summary>
        /// Confirms that a card is acceptable to the POS component
        /// </summary>
        /// <param name="card">Card details</param>
        /// <param name="terminalId">Terminal id used in the transaction</param>
        /// <returns></returns>
        public CardValidationResult VerifyCard(CardInformation card, string terminalId)
        {
            SetStatusForCurrentStatus(StatusCodes.CardAccepted);
            // For our purposes we will accept any card.
            return new CardValidationResult(CardStatus.OK);
        }

        /// <summary>
        /// Indicates the outcome of a transaction.
        /// </summary>
        /// <param name="report">Transaction results</param>
        public void TransactionComplete(TransactionReport report)
        {
            Logging.Log.Debug("TransactionComplete", new PayApp());
            try
            {
                TransactionContext tr = TransactionContext.Current;

                ReceiptData r = tr.ReceiptData;
                TransactionStatuses status = new TransactionStatuses();
                status = TransactionStatuses.UNKNOWN;
                switch (report.Result)
                {
                    case TransactionResult.Success:
                        status = TransactionStatuses.ACCEPTED;
                        break;
                    case TransactionResult.TransactionCancelled:
                        status = TransactionStatuses.CANCELLED;
                        break;
                    case TransactionResult.TransactionDeclined:
                        status = TransactionStatuses.DECLINED;
                        break;
                    default:
                        status = TransactionStatuses.UNKNOWN;
                        break;
                }
                if (status == TransactionStatuses.ACCEPTED || status == TransactionStatuses.DECLINED)
                {
                    try
                    {
                        _transactionInfo.AddOrUpdate(DateTime.Now.ToString(), r.Transaction.TransactionType, status, tr.NextTransaction.Amounts,
                            r.CardType, tr.ReferenceNumber, r, true,  TransactionContext.Current.Cashier, TransactionContext.Current.CashRegisterId, TransactionContext.Current.CashRegisterPaymentId);
                    }
                    catch (Exception e2)
                    {
                        Logging.Log.Error("TransactionComplete TransactionResult saveto SQLite failed ", e2, new PayApp());
                    }
                    try
                    {
                        lastTransactionResult = new Messages.TransactionResult(r, tr.NextTransaction)
                        {
                            CashRegisterId = TransactionContext.Current.CashRegisterId,
                            CashRegisterPaymentId = TransactionContext.Current.CashRegisterPaymentId,
                            Cashier = TransactionContext.Current.Cashier
                        };
                    }
                    catch (Exception e2)
                    {
                        Logging.Log.Error("TransactionComplete TransactionResult JSON failed ", e2, new PayApp());
                    }
                }
                else
                {
                    lastTransactionResult = new Messages.TransactionResult(r, tr.NextTransaction)
                    {
                        CashRegisterId = TransactionContext.Current.CashRegisterId,
                        CashRegisterPaymentId = TransactionContext.Current.CashRegisterPaymentId,
                        Cashier = TransactionContext.Current.Cashier,
                        Approved = false,
                        DenialText = _strings.tr_Cancelled,
                        TranasctionType = TransactionContext.Current.NextTransaction.Type.ToString()
                    };

                }
                SetStatusForCurrentStatus(StatusCodes.TransactionResultFinished);
            }
            catch (Exception e)
            {
                Logging.Log.Error("TransactionComplete failed ", e, new PayApp());
            }
        }

        public void SetLastTranasctionResultToNull()
        {
            lastTransactionResult = null;
        }

        public Messages.TransactionResult GetLastTransactionResult()
        {
            return lastTransactionResult;
        }

        /// <summary>
        /// Handles batch data sent from the payment application.  The purpose of this is to print a 
        /// batch report. This can happen as a scheduled activity outside of a transaction as well
        /// as during an explicit close batch transaction.
        /// </summary>
        public void HandleTransactionList(DateTime start, DateTime end)
        {
            try
            {
                Logging.Log.Debug("HandleTransactionList starting", new PayApp());
                if (TransactionContext.Current == null) return;
                SessionContext.Current.TerminalId = Settings.TerminalId;
//                bool bNoPrinter = Settings.NoPrinter && !CheckIfPrinterIsAvailable();
//                if (!bNoPrinter)
//                    Receipts.PrintTransactionList(_transactionInfo.GetForPeriod(start, end), Settings);
//                else
                {
                    StringBuilder stb = new StringBuilder();
                    IEnumerable<TransactionStatus> orders = _transactionInfo.GetForPeriod(start, end);
                    UserInputs input;
                    List<UserInputs> keys = new List<UserInputs>
                    {
                        UserInputs.Enter,
                        UserInputs.Cancel,
                        UserInputs.Swipe, UserInputs.SingleTap
                    };
                    int rows = 9;
                    int i = 0;
                    do
                    {
                        i = DoTransactionListLines(ref i, rows, orders.OrderByDescending(m => m.ReceiptData.Timestamp));
                        SingleKeyInputter ski = new SingleKeyInputter((IPaymentApp) PaymentApp, ref _keyHandler,
                            _keyHandlerLock, keys);
                        Logging.Log.Debug("HandleTransactionList waiting for input", new PayApp());
                        input = ski.WaitForFinish(ref _keyHandler);
                        Logging.Log.Debug("HandleTransactionList checking input", new PayApp());
                        switch (input)
                        {
                            case UserInputs.Swipe:
                                Logging.Log.Debug("HandleTransactionList swipe", new PayApp());
                                return;
                            case UserInputs.Cancel:
                                break;
                            case UserInputs.SingleTap:
                                Logging.Log.Debug("HandleTransactionList - ButtonId = " + ski.ButtonId, new PayApp());
                                if (ski.ButtonId == 1001)
                                {
                                    if (i - rows >= -1)
                                        i -= rows * 2;
                                    break;
                                }
                                else if (ski.ButtonId == 1002)
                                {
                                    //if (i + rows < orders.Count() + 1)
                                    //    i += rows;
                                    break;
                                }

                                break;
                            default:
                                Logging.Log.Error("HandleTransactionList - input = " + input, new PayApp());
                                break;
                        }
                    } while (input != UserInputs.Enter && input != UserInputs.Cancel);
                    if (input==UserInputs.Enter && CheckIfPrinterIsAvailable())
                    {
                       Receipts.PrintTransactionList(_transactionInfo.GetForPeriod(start, end), Settings);
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Log.Error("HandleTransactionList - Exception = " + e.Message, new PayApp());
            }
        }


        private int DoTransactionListLines(ref int j, int rows, IEnumerable<TransactionStatus> orders)
        {
            Logging.Log.Debug("DoTransactionListLines - j = " + j, new PayApp());
            FlexibleDisplay fd = new DefaultFlexibleDisplay();
            int i = 0;

            fd.Elements.Add(new DefaultFlexibleDisplayElement(_strings.transactionList.ToUpper(), (uint)i, 100, i) { ShowBorder = true, IsClickable = false, HorizontalAlignment = StringAlignment.Center });
            i++;
            fd.Elements.Add(new DefaultFlexibleDisplayElement(_strings.title_tidpunkt, (uint)i, 25.0f, i) { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Near });
            fd.Elements.Add(new DefaultFlexibleDisplayElement(_strings.title_typstatus, (uint)i, 50.0f, i) { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Center });
            fd.Elements.Add(new DefaultFlexibleDisplayElement(_strings.title_belop, (uint)i, 25.0f, i) { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Far });
            String sDate = null;
            List<TransactionStatus> subset = new List<TransactionStatus>();
            if (j >= 0 && j < orders.Count())
            {
                subset = orders.Skip(j).Take(rows).ToList();
            } 

            foreach (var ord in subset)
            {
                i++;
                String sCurrentDate = ((ord.ReceiptData.Timestamp).ToString(
                    _strings.GetCultureInfo().DateTimeFormat.ShortDatePattern, _strings.GetCultureInfo()));
                if (!sCurrentDate.Equals(sDate))
                {
                    sDate = sCurrentDate;
                    fd.Elements.Add(new DefaultFlexibleDisplayElement("", (uint)i, 25.0f, i)
                    { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Near });
                    fd.Elements.Add(new DefaultFlexibleDisplayElement(sDate, (uint)i, 50.0f, i)
                    {
                        ShowBorder = false,
                        IsClickable = false,
                        HorizontalAlignment = StringAlignment.Center
                    });
                    fd.Elements.Add(new DefaultFlexibleDisplayElement("", (uint)i, 25.0f, i)
                    { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Far });
                    i++;
                }

                String s = "";
                switch (ord.ReceiptData.Transaction.TransactionType)
                {
                    case TransactionType.CashAdvance:
                        s = _strings.receipt_cashadvance;
                        break;
                    case TransactionType.Purchase:
                        s = _strings.trList_Purchase;
                        break;
                    case TransactionType.Refund:
                        s = _strings.trList_Refund;
                        break;
                    case TransactionType.Reversal:
                        s = _strings.trList_Reversal;
                        break;
                    case TransactionType.PreAuth:
                        s = _strings.trList_PreAuth;
                        break;
                    case TransactionType.PreAuthSettle:
                        s = _strings.trList_PreAuthSettle;
                        break;
                    default:
                        continue;
                }

                switch (ord.Status)
                {
                    case TransactionStatuses.ACCEPTED:
                        s = s + "/" + _strings.trList_Accepted;
                        break;
                    case TransactionStatuses.DECLINED:
                        s = s + "/" + _strings.trList_Declined;
                        break;
                    case TransactionStatuses.CANCELLED:
                        s = s + "/" + _strings.trList_Cancelled;
                        break;
                    case TransactionStatuses.UNKNOWN:
                        s = s + "/" + _strings.trList_Unknown;
                        break;
                }

                fd.Elements.Add(
                    new DefaultFlexibleDisplayElement(
                            ord.ReceiptData.Timestamp.ToString(
                                _strings.GetCultureInfo().DateTimeFormat.ShortTimePattern,
                                _strings.GetCultureInfo()), (uint)i, 25.0f, i)
                    { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Near });
                fd.Elements.Add(new DefaultFlexibleDisplayElement(s, (uint)i, 50.0f, i)
                { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Center });
                fd.Elements.Add(
                    new DefaultFlexibleDisplayElement(
                            (ord.ReceiptData.AuthorisedAmounts.TotalAmount / 100m).ToString("F2",
                                _strings.GetCultureInfo()), (uint)i, 25.0f, i)
                    { ShowBorder = false, IsClickable = false, HorizontalAlignment = StringAlignment.Far });
            }

            fd.MinRows = i;
            if (i < rows)
            {
                Logging.Log.Spam("DoTransactionListLines - adding extra rows ", new PayApp());
            }
            while (i < rows)
            {
                i++;
                fd.Elements.Add(new DefaultFlexibleDisplayElement(" ", (uint)i, 25.0f, i)
                    { ShowBorder = true, IsClickable = false, HorizontalAlignment = StringAlignment.Near });
                fd.Elements.Add(new DefaultFlexibleDisplayElement(" ", (uint)i, 50.0f, i)
                    { ShowBorder = true, IsClickable = false, HorizontalAlignment = StringAlignment.Center });
                fd.Elements.Add(new DefaultFlexibleDisplayElement(" ", (uint)i, 25.0f, i)
                    { ShowBorder = true, IsClickable = false, HorizontalAlignment = StringAlignment.Far });
                fd.MinRows++;
            }
            if (orders.Count() > rows-2 || i>=rows-1)
            {
                i++;
                fd.MinRows ++;
                if (j>0)
                    fd.Elements.Add(new DefaultFlexibleDisplayElement("<<", (uint)i, 50.0f, 1001, Color.LightBlue) { ShowBorder = true });
                else
                    fd.Elements.Add(new DefaultFlexibleDisplayElement("<<", (uint)i, 50.0f, 0, Color.LightGray) { ShowBorder = true, IsClickable = false});
                if (j<orders.Count()-rows)
                    fd.Elements.Add(new DefaultFlexibleDisplayElement(">>", (uint)i, 50.0f, 1002, Color.LightBlue) { ShowBorder = true });
                else
                    fd.Elements.Add(new DefaultFlexibleDisplayElement(">>", (uint)i, 50.0f, 0, Color.LightGray) { ShowBorder = true, IsClickable = false });
            }
            else
                fd.MinRows = rows + 2;
            Logging.Log.Debug("DoTransactionListLines - Starting to show ", new PayApp());
            UI.Display(fd);
            return j+rows;
        }

        /// <summary>
        /// Handles batch data sent from the payment application.  The purpose of this is to print a 
        /// batch report. This can happen as a scheduled activity outside of a transaction as well
        /// as during an explicit close batch transaction.
        /// </summary>
        /// <param name="batch">Batch data</param>
        //removed from SDK 245
        /*
                public void HandleBatchData(BatchData batch)
                {
                    if (batch == null) return;
                    if (TransactionContext.Current == null) return;

                    TransactionContext.Current.BatchData = batch; 
                    Receipts.PrintBatchData(batch);
                }
        */
        /// <summary>
        /// This fn is the entry point for receipt printing
        /// </summary>
        /// <param name="receipt">Object holding the receipt data</param>
        public void HandleReceiptData(ReceiptData receipt)
        {
            if (receipt == null) return;

            // Store the receipt data
            TransactionContext.Current.ReceiptData = receipt;
           

            bool shouldPrintMerchantReceipt = !Settings.DisableMerchantReceipt                     &&
                                              !TransactionContext.Current.IsMerchantReceiptPrinted &&
                                              receipt.Approved;

            bool shouldPrintCardHolderReceipt = !Settings.DisableCardHolderReceipt ||
                                              !receipt.Approved;

            bool isSuccess = true;

            if (Settings.NoPrinter)
            {
                shouldPrintCardHolderReceipt = false;
                shouldPrintMerchantReceipt = false;
            }

            // Only do a merchant receipt if it is a successful transaction
            if (shouldPrintMerchantReceipt)
            {
                isSuccess = Receipts.PrintMerchantReceipt(receipt, Settings);

                if (isSuccess)
                {
                    // We need to tell the operator to take the merchant receipt.
                    UI.Alert(_strings.TakeReceipt1, _strings.TakeReceipt2);
                    UI.PleaseWait(String.Empty);
                }
            }

            if (isSuccess && shouldPrintCardHolderReceipt)
            {
                Receipts.PrintCardholderReceipt(receipt, Settings);
            }
        }

        #endregion

        /// <summary>
        /// The TransactionOptions used if tipping is disabled.
        /// </summary>
        static readonly TransactionOptions DisabledTippingOptions =
            new TransactionOptions
            {
                DisabledFeatures = new List<TransactionFeatures> {TransactionFeatures.Tipping}
            };
    }
}



