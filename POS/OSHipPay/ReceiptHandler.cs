﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using OSHipPay.Core;
using OSHipPay.Logging;
using OSHipPay.UI;
using WestPA.Standalone;

namespace OSHipPay
{
    /// <summary>
    /// The receipt handler class contains methods for creating and printing receipts.
    /// </summary>
    public class ReceiptHandler
    {
        /// <summary>
        /// Indicates if the concerned receipt is for the merchant or the cardholder.
        /// </summary>
        public enum ReceiptType
        {
            MerchantReceipt,
            CardholderReceipt
        }

        readonly IPaymentApp _app;
        readonly UserInterface _ui;
        protected readonly StringDefinitions _strings;

        /// <summary>
        /// ReceiptHandler constructor.
        /// </summary>
        /// <param name="app">A reference to the payment application.</param>
        /// <param name="ui">A reference to the UserInterface object.</param>
        /// <param name="strings">The localised strings to use.</param>
        public ReceiptHandler(IPaymentApp app, UserInterface ui, StringDefinitions strings)
        {
            _app = app;
            _ui = ui;
            _strings = strings;
        }

        public bool Print(ReceiptBuilder rb)
        {
            bool result = false;
            PrinterState ps = CheckPrinterState();
            if (ps!=null)
            {
                result = Print(rb, ps, ReceiptType.MerchantReceipt);
            }
            return result;
        }

        /// <summary>
        /// Handles batch data sent from the payment application.  The purpose of this is to print a 
        /// batch report. This can happen as a scheduled activity outside of a transaction as well
        /// as during an explicit close batch transaction.
        /// </summary>
        /// <param name="orders">Receipt data</param>
        public bool PrintTransactionList(IEnumerable<TransactionStatus> orders, Settings settings)
        {
            bool result = false;
            PrinterState ps = CheckPrinterState();
            if (ps != null)
            {
                ReceiptBuilder rb = new ReceiptBuilder(_app, _strings.GetCultureInfo(), settings);
                
                if (BuildTransactionList(orders/*, batch*/, rb))
                {
                    result = Print(rb, ps, ReceiptType.MerchantReceipt);
                }
            }
            return result;
        }

        /// <summary>
        /// Handles batch data sent from the payment application.  The purpose of this is to print a 
        /// batch report. This can happen as a scheduled activity outside of a transaction as well
        /// as during an explicit close batch transaction.
        /// </summary>
        /// <param name="batch">Batch data</param>
        /*
        public bool PrintBatchData(BatchData batch)
        {
            bool result = false;
            PrinterState ps = CheckPrinterState();
            if (ps != null)
            {
                ReceiptBuilder rb = new ReceiptBuilder(_app, _strings.GetCultureInfo());
                if (BuildCloseBatchReceipt(batch, rb))
                {
                    result = Print(rb, ps, ReceiptType.MerchantReceipt);
                }
            }
            return result;
        }

        */
        private string FormatToColumns(string[] s, int[] w, StringAlignment[] stringAlignments)
        {

            StringBuilder str = new StringBuilder("");
            for (int i = 0; i < s.Length; i++) {
                try
                {
                    switch (stringAlignments[i])
                    {
                        case StringAlignment.Center:
                            str.Append(String.Format("{0,-" + w[i] + "}",
                                String.Format("{0," + ((w[i] + s[i].Length) / 2).ToString() + "}", s[i])));
                            break;
                        case StringAlignment.Far:
                            str.Append(String.Format("{0," + w[i] + "}", s[i]));
                            break;
                        case StringAlignment.Near:
                            String p = String.Format("{0,-"+w[i]+"}", s[i]);
                            str.Append(p);
                            break;
                    }
                } catch (Exception e)
                {
                    Log.Error(e.Message, e);
                }
            }
            return str.ToString();
        }

        public virtual bool BuildTransactionList(IEnumerable<TransactionStatus> orders, StringBuilder rb, ReceiptBuilder RcpB)
        {
            bool result = false;
            try
            {

                                rb.Append(_strings.transactionList.ToUpper());
                                rb.Append("\n");
                
                string sDate = "";
                bool bTitles = false;
                int[] columns = new int[3] { 10, 20, 10 };
                StringAlignment[] aligns = new StringAlignment[3] { StringAlignment.Near, StringAlignment.Center, StringAlignment.Far };
                foreach (TransactionStatus r in orders)
                {
                    if (!bTitles)
                    {
                        bTitles = true;
/*
                        rb.AddString(r.ReceiptData.MerchantInformation.Name);
                        rb.AddString(r.ReceiptData.MerchantInformation.date);
                        rb.AddString(r.ReceiptData.MerchantInformation.ZipCode + " " + r.ReceiptData.MerchantInformation.City);
                        rb.AddString(r.ReceiptData.MerchantInformation.PhoneNumber);
                        rb.AddString(_strings.receipt_organisation + " " + r.ReceiptData.MerchantInformation.OrganisationNumber);
                        rb.AddString(" ");
                        rb.AddString(r.ReceiptData.MerchantInformation.BankAgentName);
                        rb.AddString(_strings.From + ": " + r.ReceiptData.Timestamp.ToString("yyyy-MM-dd HH:mm"));
                        rb.AddString(_strings.To + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm"));
                        rb.AddString(" ");
                        rb.AddString(_strings.receipt_terminal + ": " + SessionContext.Current.TerminalId);
                        rb.AddString(" ");
                        */
                        rb.Append(FormatToColumns(new string[3] { _strings.title_tidpunkt, _strings.title_typstatus, _strings.title_belop }, columns, aligns));
                        rb.Append("\n");
                    }
                    String sCurrentDate = RcpB.FormatDate(r.ReceiptData.Timestamp);
                    if (!sCurrentDate.Equals(sDate))
                    {
                        sDate = sCurrentDate;
                        rb.Append(sDate);
                        rb.Append("\n");
                    }
                    String s = "";
                    switch (r.ReceiptData.Transaction.TransactionType)
                    {
                        case TransactionType.CashAdvance:
                            s = _strings.receipt_cashadvance;
                            break;
                        case TransactionType.Purchase:
                            s = _strings.trList_Purchase;
                            break;
                        case TransactionType.Refund:
                            s = _strings.trList_Refund;
                            break;
                        case TransactionType.Reversal:
                            s = _strings.trList_Reversal;
                            break;
                        case TransactionType.PreAuth:
                            s = _strings.trList_PreAuth;
                            break;
                        case TransactionType.PreAuthSettle:
                            s = _strings.trList_PreAuthSettle;
                            break;
                        default:
                            continue;
                    }
                    switch (r.Status)
                    {
                        case TransactionStatuses.ACCEPTED:
                            s = s + "/" + _strings.trList_Accepted;
                            break;
                        case TransactionStatuses.DECLINED:
                            s = s + "/" + _strings.trList_Declined;
                            break;
                        case TransactionStatuses.CANCELLED:
                            s = s + "/" + _strings.trList_Cancelled;
                            break;
                        case TransactionStatuses.UNKNOWN:
                            s = s + "/" + _strings.trList_Unknown;
                            break;
                    }
                    rb.Append(FormatToColumns(new string[3] { " " + RcpB.FormatTime(r.ReceiptData.Timestamp), s, RcpB.FormatCurrency(r.ReceiptData.AuthorisedAmounts.TotalAmount) }, columns, aligns));
                    rb.Append("\n");
                }
                rb.Append("\n");
                result = true;
            }
            catch
            {
                // There shouldn't be any exceptions in this function but in case there are we just return false;
                result = false;
            }
            return result;
        }
        /// <summary>
        /// Build the close batch receipt. Note that this is overridden in the cash advance POS.
        /// </summary>
        /// <param name="batch">The receipt data.</param>
        /// <param name="rb">A receipt builder instance.</param>
        /// <returns>True if successful.</returns>
        public virtual bool BuildTransactionList(IEnumerable<TransactionStatus> orders/*, BatchData batch*/, ReceiptBuilder rb)
        {
            bool result = false;
            try
            {
                
                rb.AddString(_strings.transactionList.ToUpper(), StringAlignment.Center);
                string sDate = "";
                bool bTitles = false;
                int[] columns = new int[3] { 10, 20, 10 };
                StringAlignment[] aligns = new StringAlignment[3] { StringAlignment.Near, StringAlignment.Center, StringAlignment.Far };
                foreach (TransactionStatus r in orders)
                {
                    if (!bTitles)
                    {
                        bTitles = true;
                        rb.AddString(r.ReceiptData.MerchantInformation.Name);
                        rb.AddString(r.ReceiptData.MerchantInformation.Address);
                        rb.AddString(r.ReceiptData.MerchantInformation.ZipCode + " " + r.ReceiptData.MerchantInformation.City);
                        rb.AddString(r.ReceiptData.MerchantInformation.PhoneNumber);
                        rb.AddString(_strings.receipt_organisation + " " + r.ReceiptData.MerchantInformation.OrganisationNumber);
                        rb.AddString(" ");
                        rb.AddString(r.ReceiptData.MerchantInformation.BankAgentName);
                        rb.AddString(_strings.From + ": " + r.ReceiptData.Timestamp.ToString("yyyy-MM-dd HH:mm"));
                        rb.AddString(_strings.To + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm"));
                        rb.AddString(" ");
                        rb.AddString(_strings.receipt_terminal + ": " + SessionContext.Current.TerminalId);
                        rb.AddString(" ");
                        rb.AddString(FormatToColumns(new string[3] { _strings.title_tidpunkt, _strings.title_typstatus, _strings.title_belop }, columns, aligns));
                    }
                    String sCurrentDate = rb.FormatDate(r.ReceiptData.Timestamp);
                    if (!sCurrentDate.Equals(sDate))
                    {
                        sDate = sCurrentDate;
                        rb.AddString(sDate);
                    }
                    String s = "";
                    switch (r.ReceiptData.Transaction.TransactionType)
                    {
                        case TransactionType.CashAdvance:
                            s = _strings.receipt_cashadvance;
                            break;
                        case TransactionType.Purchase:
                            s = _strings.trList_Purchase;
                            break;
                        case TransactionType.Refund:
                            s = _strings.trList_Refund;
                            break;
                        case TransactionType.Reversal:
                            s = _strings.trList_Reversal;
                            break;
                        case TransactionType.PreAuth:
                            s = _strings.trList_PreAuth;
                            break;
                        case TransactionType.PreAuthSettle:
                            s = _strings.trList_PreAuthSettle;
                            break;
                        default: 
                            continue;
                    }
                    switch (r.Status)
                    {
                        case TransactionStatuses.ACCEPTED:
                            s = s + "/" + _strings.trList_Accepted;
                            break;
                        case TransactionStatuses.DECLINED:
                            s = s + "/" + _strings.trList_Declined;
                            break;
                        case TransactionStatuses.CANCELLED:
                            s = s + "/" + _strings.trList_Cancelled;
                            break;
                        case TransactionStatuses.UNKNOWN:
                            s = s + "/" + _strings.trList_Unknown;
                            break;
                    }
                    rb.AddString(FormatToColumns(new string[3] { " "+rb.FormatTime(r.ReceiptData.Timestamp), s, rb.FormatCurrency(r.ReceiptData.AuthorisedAmounts.TotalAmount) }, columns, aligns));
                }
                rb.AddString(" ");
                rb.AddString(" ");
                result = true;
            }
            catch
            {
                // There shouldn't be any exceptions in this function but in case there are we just return false;
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Build the close batch receipt. Note that this is overridden in the cash advance POS.
        /// </summary>
        /// <param name="batch">The receipt data.</param>
        /// <param name="rb">A receipt builder instance.</param>
        /// <returns>True if successful.</returns>
        /// 
        /*
        public virtual bool BuildCloseBatchReceipt(BatchData batch, ReceiptBuilder rb)
        {
            bool result = false;
            try
            {
                rb.AddString(_strings.receipt_BATCHREPORT.ToUpper(), StringAlignment.Center);
                rb.AddString(batch.MerchantInformation.Name);
                rb.AddString(batch.MerchantInformation.date);
                rb.AddString(batch.MerchantInformation.ZipCode + " " + batch.MerchantInformation.City);
                rb.AddString(batch.MerchantInformation.PhoneNumber);
                rb.AddString(_strings.receipt_organisation + " " + batch.MerchantInformation.OrganisationNumber);
                rb.AddString(" ");
                rb.AddString(batch.MerchantInformation.BankAgentName);
                rb.AddString(_strings.From + ": " + batch.Start.ToString("yyyy-MM-dd HH:mm"));
                rb.AddString(_strings.To + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm"));
                rb.AddString(" ");
                rb.AddString(_strings.receipt_batchNumber + ": " + batch.BatchNumber);
                rb.AddString(_strings.receipt_terminal + ": " + SessionContext.Current.TerminalId);
                rb.AddString(_strings.receipt_batchClosed + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd"));
                rb.AddString(" ");
                foreach (BatchData.BatchSubTotal bst in batch.SubTotals)
                {
                    rb.AddString(bst.FinancialInstitution);
                    rb.AddStringNoIncrement("   " + _strings.Purchase + " (" + bst.Total.NumberOfDebits + ")", StringAlignment.Near);
                    rb.AddString(FormatAmount(bst.Total.ValueOfDebits), StringAlignment.Far);
                    rb.AddStringNoIncrement("   " + _strings.refund + " (" + bst.Total.NumberOfCredits + ")", StringAlignment.Near);
                    rb.AddString(FormatAmount(bst.Total.ValueOfCredits), StringAlignment.Far);
                    rb.AddStringNoIncrement("   " + _strings.Total + " (" +
                                            (bst.Total.NumberOfCredits + bst.Total.NumberOfDebits) + ")",
                                            StringAlignment.Near);
                    rb.AddString(FormatAmount(bst.Total.ValueOfDebits - bst.Total.ValueOfCredits),
                                 StringAlignment.Far);
                }
                rb.AddString(" ");

                rb.AddString(_strings.Total.ToUpper());
                rb.AddStringNoIncrement("   " + _strings.Purchase + " (" + batch.Total.NumberOfDebits + ")", StringAlignment.Near);
                rb.AddString(FormatAmount(batch.Total.ValueOfDebits - batch.Total.GratuityTotal), StringAlignment.Far);
                rb.AddStringNoIncrement("   " + _strings.refund + " (" + batch.Total.NumberOfCredits + ")", StringAlignment.Near);
                rb.AddString(FormatAmount(batch.Total.ValueOfCredits), StringAlignment.Far);
                if (batch.Total.GratuityTotal > 0)
                {
                    rb.AddStringNoIncrement("   " + _strings.opt_tipping, StringAlignment.Near);
                    rb.AddString(FormatAmount(batch.Total.GratuityTotal), StringAlignment.Far);
                }
                rb.AddStringNoIncrement("   " + _strings.Total + " (" +
                                        (batch.Total.NumberOfCredits + batch.Total.NumberOfDebits) + ")",
                                        StringAlignment.Near);
                rb.AddString(FormatAmount(batch.Total.ValueOfDebits - batch.Total.ValueOfCredits),
                             StringAlignment.Far);
                rb.AddString(" ");
                if (batch.Successful)
                {
                    if (batch.HostResponse != null)
                    {
                        if (!batch.Total.Equals(batch.HostResponse))
                        {
                            rb.AddString(_strings.hostResponseDiffers.ToUpper());
                            rb.AddString(_strings.call + " " + batch.HelpdeskNumber);
                            rb.AddStringNoIncrement("   " + _strings.Purchase + " (" +
                                                    batch.HostResponse.NumberOfDebits + ")",
                                                    StringAlignment.Near);
                            rb.AddString(FormatAmount(batch.HostResponse.ValueOfDebits),
                                         StringAlignment.Far);
                            rb.AddStringNoIncrement("   " + _strings.refund + " (" +
                                                    batch.HostResponse.NumberOfCredits + ")",
                                                    StringAlignment.Near);
                            rb.AddString(FormatAmount(batch.HostResponse.ValueOfCredits),
                                         StringAlignment.Far);
                            rb.AddStringNoIncrement("   " + _strings.Total + " (" +
                                                    (batch.HostResponse.NumberOfCredits +
                                                     batch.HostResponse.NumberOfDebits) + ")",
                                                    StringAlignment.Near);
                            rb.AddString(FormatAmount(batch.HostResponse.ValueOfDebits -
                                                      batch.HostResponse.ValueOfCredits),
                                         StringAlignment.Far);
                            rb.AddString(" ");
                        }
                    }
                    if (batch.OnStoreAndForward)
                    {
                        rb.AddString(_strings.closeBatchAddedToSF.ToUpper());
                    }
                    else
                    {
                        rb.AddString(_strings.closeBatchCompletedOnline.ToUpper());
                    }
                }
                else
                {
                    rb.AddString(_strings.technicalError.ToUpper());
                    rb.AddString(_strings.call + " " + batch.HelpdeskNumber);
                    rb.AddString(_strings.closeBatchNOTAPPROVED.ToUpper());
                }

                result = true;
            }
            catch
            {
                // There shouldn't be any exceptions in this function but in case there are we just return false;
                result = false;
            }
            return result;
        }
        */
        /// <summary>
        /// Print the cardholder copy of the receipt.
        /// Note that this method will display dialogs if there are problems with the printer.
        /// </summary>
        /// <param name="receipt">The receipt contents.</param>
        /// <returns>True if the operation was successful, false if there was a problem or the user cancelled.</returns>
        public bool PrintCardholderReceipt(ReceiptData receipt, Settings settings)
        {
            return PrintReceipt(receipt, ReceiptType.CardholderReceipt, settings);
        }

        /// <summary>
        /// Print the merchant copy of the receipt.
        /// Note that this method will display dialogs if there are problems with the printer.
        /// </summary>
        /// <param name="receipt">The receipt contents.</param>
        /// <returns>True if the operation was successful, false if there was a problem or the user cancelled.</returns>
        public bool PrintMerchantReceipt(ReceiptData receipt, Settings settings)
        {
            return PrintReceipt(receipt, ReceiptType.MerchantReceipt, settings);
        }

        /// <summary>
        /// Printer the merchant or customer receipt.
        /// </summary>
        /// <param name="receipt">The receipt contents</param>
        /// <param name="rcptType">The type of receipt to print</param>
        /// <returns>True if the operation was successful, false if there was a problem or the user cancelled.</returns>
        protected virtual bool PrintReceipt(ReceiptData receipt, ReceiptType rcptType, Settings settings)
        {
            bool result = false;
            var ps = CheckPrinterState();
            if (ps != null)
            {
                ReceiptBuilder rb = new ReceiptBuilder(_app, _strings.GetCultureInfo(), settings);
                if (BuildMerchantOrCustomerReceipt(receipt, rb, rcptType))
                {
                    result = Print(rb, ps, ReceiptType.MerchantReceipt);
                }
            }
            return result;
        }

        /// <summary>
        /// Examine the state of the printer. If there are some problems, the user is given the option to check the printer
        /// and try again.
        /// </summary>
        /// <returns>
        /// If the printer is available and free then a PrinterState object is returned.
        /// Otherwise the function returns null.
        /// </returns>
        protected virtual PrinterState CheckPrinterState()
        {
            PrinterState result = null;

            try
            {
                bool retry = true;
                while (retry)
                {
                    _app.GetPrinterStatus(out PrinterState ps);
                    bool isPrinterAvailable = ps != null
                                              && ps.PrinterOnline
                                              && ps.PaperOK;
                    if (!isPrinterAvailable)
                    {
                        retry = _ui.OkCancel(_strings.printer_trouble, _strings.check_printer, _strings.ok_to_try_again);
                        _ui.PleaseWait(String.Empty);
                    }
                    else
                    {
                        retry = false;
                        result = ps;
                    }
                }
            }
            catch
            {
                // There shouldn't be any exceptions here but in case there we just return a null.
                result = null;
            }

            return result;
        }

        /// <summary>
        /// This method prints a receipt
        /// </summary>
        /// <param name="item">Object holding the receipt data</param>
        /// <param name="rb">A receipt builder instance.</param>
        /// <param name="rcptType">The type of receipt to print.</param>
        /// <returns>True if the receipt was sent to the printer, false otherwise</returns>
        public bool BuildMerchantOrCustomerReceipt( ReceiptData item,
                                                    ReceiptBuilder rb,
                                                    ReceiptType rcptType)
        {
            bool result = false;
            try
            {
                var strings = _strings; 

                if (ReceiptType.CardholderReceipt == rcptType)
                {
                    rb.AddString(strings.receipt_CARDHOLDERCOPY, StringAlignment.Center);
                }
                else
                {
                    rb.AddString(strings.receipt_MERCHANTCOPY, StringAlignment.Center);
                }
                rb.AddString(" ");
                rb.AddString(item.MerchantInformation.Name);
                rb.AddString(item.MerchantInformation.Address);
                rb.AddString(item.MerchantInformation.ZipCode + " " + item.MerchantInformation.City);
                rb.AddString(item.MerchantInformation.PhoneNumber);
                rb.AddString(strings.receipt_organisation + " " + item.MerchantInformation.OrganisationNumber);
                rb.AddString(" ");
                rb.AddString(item.MerchantInformation.BankAgentName);
                rb.AddString(strings.receipt_acquirer + ": " + item.AcquirerReference);
                rb.AddString(strings.receipt_terminal + ": " + SessionContext.Current.TerminalId);
                rb.AddString(item.Timestamp.ToString(strings.GetCultureInfo()));
                rb.AddString(GetTransactionType(item, strings));
                rb.AddStringNoIncrement(item.CurrencyCode);
                rb.AddString(FormatAmount(item.AuthorisedAmounts.NetAmount), StringAlignment.Far);
                rb.AddPositiveAmount(strings.vat, item.AuthorisedAmounts.VatAmount);
                rb.AddPositiveAmount(strings.receipt_acquirer_fee, item.AuthorisedAmounts.CashAdvanceChargeAmount);
                rb.AddPositiveAmount(strings.receipt_gratuity_amount, item.AuthorisedAmounts.GratuityAmount);
                rb.AddPositiveAmount(strings.receipt_cash_back, item.AuthorisedAmounts.CashbackAmount);
                rb.AddAmount(strings.Total, item.AuthorisedAmounts.TotalAmount);
                if ((item.Verification == VerificationMethod.PINOffline) ||
                    (item.Verification == VerificationMethod.PINOnline) ||
                    (item.Verification == VerificationMethod.Combined))
                {
                    // Add 'PIN used' line
                    rb.AddString(strings.receipt_pin_used);
                }
                rb.AddString(" ");
                if (ReceiptType.CardholderReceipt == rcptType)
                {
                    // Need to obscure the card number more than it already is
                    // We preserve the last four digits though.
                    string customerVersion = item.Transaction.PaymentCardNumber;
                    if (customerVersion != null)
                    {
                        int firstAsterisk = item.Transaction.PaymentCardNumber.IndexOf('*');
                        if (firstAsterisk > 0)
                        {
                            customerVersion = new string('*', firstAsterisk) +
                                              item.Transaction.PaymentCardNumber.Substring(firstAsterisk);
                        }
                    }
                    rb.AddString(customerVersion);
                }
                else
                {
                    rb.AddString(item.Transaction.PaymentCardNumber);
                }
                rb.AddString(" ");
                rb.AddString(item.CardType);

                // According to Swedbank bulletin Contactless 1.0 r4, req M901043, we must indicate on the receipt if contactless
                // technology was used.
                if (CardInformation.EntryMethod.Contactless == item.Transaction.CardEntryMethod)
                {
                    rb.AddString(_strings.receipt_contactless);
                }
                if (!String.IsNullOrEmpty(item.Transaction.PaymentCode))
                {
                    rb.AddString(strings.receipt_payment_code + ": " + item.Transaction.PaymentCode);
                }
                switch (item.DebitCredit)
                {
                    case ReceiptData.DebitCreditSelection.Credit:
                    {
                        rb.AddString(strings.credit);
                    }
                    break;

                    case ReceiptData.DebitCreditSelection.Debit:
                    {
                        rb.AddString(strings.debit);
                    }
                    break;

                    default:
                    {
                        // Nothing to do.
                    }
                    break;
                }
                rb.AddString(" ");
                // No signature lines on reversals
                if (!item.IsReversal)
                {
                    if ((item.Verification == VerificationMethod.Signature) ||
                        (item.Verification == VerificationMethod.Combined))
                    {
                        // Merchant receipt gets signature lines
                        bool shouldPrintSignatureLines = (item.Transaction.TransactionType == TransactionType.Purchase
                                                          || item.Transaction.TransactionType == TransactionType.CashAdvance)
                                                         && item.Approved
                                                         && ReceiptType.MerchantReceipt == rcptType;
                        if (shouldPrintSignatureLines)
                        {
                            AddWrapped32String(rb, strings.receipt_debit_authorisation);
                            rb.AddString(" ");
                            rb.AddString(" ");
                            rb.AddString(" ");
                            rb.AddString((strings.id.ToUpper() + ":  ").PadRight(45, '.'));
                            rb.AddString(" ");
                            rb.AddString(" ");
                            rb.AddString(" ");
                            rb.AddString((strings.sign.ToUpper() + ": ").PadRight(45, '.'));
                            rb.AddString(" ");
                        }
                    }
                    if (item.Transaction.TransactionType == TransactionType.Refund)
                    {
                        if (ReceiptType.CardholderReceipt == rcptType)
                        {
                            if (item.Approved)
                            {
                                // Cardholder receipt gets signature lines
                                AddWrapped32String(rb, strings.receipt_credit_authorisation);
                                rb.AddString(" ");
                                rb.AddString(" ");
                                rb.AddString(" ");
                                rb.AddString(".......................................");
                                rb.AddString(strings.receipt_cashier_signature);
                                rb.AddString(" ");
                                rb.AddString(" ");
                                rb.AddString(" ");
                                rb.AddString(".......................................");
                                rb.AddString(strings.receipt_cashier_name);
                                rb.AddString(" ");
                            }
                        }
                    }
                }
                if (item.Approved)
                {
                    if (item.Authorisation == AuthorisationMethod.Online)
                    {
                        rb.AddString(strings.receipt_approved_online);
                    }
                    else
                    {
                        rb.AddString(strings.receipt_approved_offline);
                    }
                }
                else
                {
                    if (item.Authorisation == AuthorisationMethod.Online)
                    {
                        rb.AddString(strings.receipt_declined_online);
                    }
                    else
                    {
                        rb.AddString(strings.receipt_declined_offline);                        
                    }
                    rb.AddString(item.DenialText);
                }
                rb.AddString(GetCodeString(item));
                rb.AddString(" ");
                rb.AddString(strings.receipt_ref_no + ": " + item.ReferenceNumber);
                rb.AddString(" ");
                if (item.EMV != null)
                {
                    rb.AddString("AID: " + EMVElements.StringOf(item.EMV.AID));
                    rb.AddString("TVR: " + EMVElements.StringOf(item.EMV.TVR));
                    rb.AddString("TSI: " + EMVElements.StringOf(item.EMV.TSI));
                }
                rb.AddString(" ");
                result = true;
            }
            catch
            {
                // There shouldn't be any exceptions in this function but in case there are we just return false;
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Adds a string to a receipt builder. 
        /// </summary>
        /// <param name="rb">IReceiptBuilder instance to take the lines of text</param>
        /// <param name="txt">Source text to add</param>
        void AddWrapped32String(ReceiptBuilder rb, string txt)
        {
            if (txt != null)
            {
                rb.AddString(txt);
            }
        }

        /// <summary>
        /// Get the code string for the receipt.
        /// </summary>

        string GetCodeString(ReceiptData receipt)
        {
            var codeString = new StringBuilder(20);

            codeString.Append(GetEntryMethodCharacter(receipt.Transaction.CardEntryMethod));
            codeString.Append(CharOf(receipt.Verification));
            codeString.Append(CharOf(receipt.Authorisation));
            codeString.Append(' ');
            codeString.Append(CharOf(receipt.AuthEntity));
            codeString.Append(' ');
            codeString.Append(receipt.AuthorisationResponseCode);
            codeString.Append(' ');
            codeString.Append(receipt.FinancialInstitution);
            codeString.Append(' ');
            codeString.Append(receipt.ApprovalCode);

            return codeString.ToString();
        }

        /// <summary>
        /// Get the character that represents the entry method.
        /// </summary>
        /// <param name="entryMethod">A card entry method value.</param>
        /// <returns>The character that goes on the receipt.</returns>
        static char GetEntryMethodCharacter(CardInformation.EntryMethod entryMethod)
        {
            char result;
            switch (entryMethod)
            {
                case CardInformation.EntryMethod.ChipInserted:
                {
                    result = 'C';
                    break;
                }
                case CardInformation.EntryMethod.Swiped:
                {
                    result = 'D';
                    break;
                }
                case CardInformation.EntryMethod.Contactless:
                {
                    result = 'K';
                    break;
                }
                default:
                {
                    // Default to manual entry.
                    result = 'T';
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Get the description of the transaction type, e.g. PURCHASE or REFUND.
        /// </summary>
        /// <param name="data">The receipt data</param>
        /// <param name="strings">The string translations.</param>
        static string GetTransactionType(ReceiptData data, StringDefinitions strings)
        {
            string ttype = null;
            switch (data.Transaction.TransactionType)
            {
                case TransactionType.Purchase:
                ttype = strings.receipt_purchase;
                break;
                case TransactionType.Refund:
                ttype = strings.receipt_return;
                break;
                case TransactionType.Reversal:
                ttype = strings.receipt_reversal;
                break;
                case TransactionType.CashAdvance:
                ttype = strings.receipt_cashadvance;
                break;
                default:
                ttype = data.Transaction.TransactionType.ToString();
                break;
            }

            if ((data.IsReversal) && (data.Transaction.TransactionType != TransactionType.Reversal))
            {
                ttype += " " + strings.reversal;
            }
            return ttype.ToUpper();
        }

        /// <summary>
        /// This fn returns a string representation of the object
        /// </summary>
        /// <param name="val">Object to convert to a string</param>
        /// <returns>string represention of the input parameter</returns>
        static string CharOf(object val)
        {
            try
            {
                return ((char)((int)Convert.ChangeType(val, Enum.GetUnderlyingType(val.GetType()), null))).ToString();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Print a receipt. If a printer is present and ready the receipt will be printed on
        /// that printer. An image will also be saved on file, regardless of printer availability.
        /// </summary>
        /// <param name="rb">A ReceiptBuilder containing the receipt data to print.</param>
        /// <param name="printerState">A PrinterState object.</param>
        /// <param name="rcptType">The type of receipt printed. This only affects which file the image is saved to.</param>
        protected bool Print(ReceiptBuilder rb, PrinterState printerState, ReceiptType rcptType)
        {
            bool result = false;
            // We try to print the receipt until it succeeds or the user cancels.
            bool retry = true;
            while (retry)
            {
                result = _app.RcptPrint(TearOffLength.Normal);
                retry = !result && _ui.OkCancel(_strings.printer_trouble, _strings.check_printer, _strings.try_again_q);
            }
            return result;
        }

        /// <summary>
        /// Format an amount as a string. This function handles decimals and cultures.
        /// </summary>
        /// <param name="value">An amount with cents, e.g. 110 SEK = 11000</param>
        /// <returns>The amount as a string, e.g. "110,00"</returns>
        protected string FormatAmount(long value)
        {
            return (value / 100m).ToString("F2", _strings.GetCultureInfo());
        }
    }
}
