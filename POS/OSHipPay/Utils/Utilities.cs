﻿using OSHipPay.Core;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using WestPA.Standalone;
using System.Linq;
using System.Text;
using Android.Net.Wifi;
using Android.Content;
using Android.App;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace OSHipPay.Utilities
{
    class Utils
    {

        //ToDo - actual memory status logging....
        /// <summary>
        /// Logs the current memory status
        /// </summary>
        public static string LogMemoryStatus(bool shortOutput)
        {
            /*
            int used = (int)(.memoryUsed) / 1024;
                int free = (int)(internalReport.ullAvailPhys / 1024);
                long gcAllocated = GC.GetTotalMemory(true) / 1024;
                if (shortOutput)
                    return ("Free: " + free + "Kb, used: " + used + " Kb");
                return ("Physical memory available: " + free + " Kb, used: " + used + " Kb  (Managed = " +
                    gcAllocated + " Kb, Unmanaged = " + (used - gcAllocated) + " Kb");
            */
            return String.Empty;
        }

        public static int SocketConnectTimeoutSeconds = 3;

        public static bool IsConnectionPossible(System.Net.IPEndPoint ipEndPoint)
        {
            return IsConnectionPossible(ipEndPoint, out long dontcare);
        }


        public static bool IsConnectionPossible(System.Net.IPEndPoint ipEndPoint, out long cTime)
        {
            cTime = -1;
            if (ipEndPoint!=null)
                try
                {
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    bool connected = false;
                    System.Threading.Thread t = new Thread(() =>
                    {
                        connected = IsConnectionPossiblePrivate(ipEndPoint);
                    });
                    sw.Start();
                    t.Start();
                    t.Join(SocketConnectTimeoutSeconds * 1000);
                    if (connected)
                        cTime = sw.ElapsedMilliseconds;
                    else cTime = -1;
                }
                catch (Exception e)
                {
                    Logging.Log.Error(
                        "SocketExtensions cannot to " + ipEndPoint.Address + ":" + ipEndPoint.Port + " " + e.Message,
                        new Utils());
                    cTime = -1;
                }
            if (cTime > -1) return true;
            else return false;
        }

        private static bool IsConnectionPossiblePrivate(System.Net.IPEndPoint ipEndPoint)
        {
            bool connectStatus = false;
            if (ipEndPoint == null)
            {
 //               cTime = -1;
                return false;
            }
            System.Net.Sockets.Socket clientSocket = null;
//            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            try
            {
                Logging.Log.Spam("SocketExtensions connecting to " + ipEndPoint.Address + ":" + ipEndPoint.Port,
                    new Utils());
                clientSocket = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork,
                    System.Net.Sockets.SocketType.Stream,
                    System.Net.Sockets.ProtocolType.Tcp);
                //sw.Start();
                Logging.Log.Spam(
                    "SocketExtensions starting to connect to " + ipEndPoint.Address + ":" + ipEndPoint.Port,
                    new Utils());

                SocketExtensions.Connect(clientSocket, ipEndPoint.Address.ToString(), ipEndPoint.Port,
                    SocketConnectTimeoutSeconds * 1000);
                Logging.Log.Spam("SocketExtensions starting to connect " + ipEndPoint.Address + ":" + ipEndPoint.Port,
                    new Utils());
                connectStatus = clientSocket.Connected;
                Logging.Log.Spam(
                    "SocketExtensions starting to status for " + ipEndPoint.Address + ":" + ipEndPoint.Port +
                    " connected " + connectStatus, new Utils());
            }
            catch (System.Net.Sockets.SocketException sx)
            {
                Logging.Log.Spam("SocketException " + ipEndPoint.Address + ":" + ipEndPoint.Port+" "+sx.Message,
                    new Utils());
                Logging.Log.VSlog(sx.Message, new Utils());
                connectStatus = false;
            }
            catch (Exception ex)
            {
                Logging.Log.Spam("Exception " + ipEndPoint.Address + ":" + ipEndPoint.Port + " " + ex.Message,
                    new Utils());
//                cTime = sw.ElapsedMilliseconds;
                Logging.Log.VSlog(ex.Message, new Utils());
                connectStatus = false;
            }
            //            cTime = sw.ElapsedMilliseconds;
            if (clientSocket != null)
                clientSocket.Close();
            return connectStatus;
        }

        /// <summary>
        /// Finds the MAC address of the NIC with maximum speed.
        /// </summary>
        /// <returns>The MAC address.</returns>
        public static string GetMacAddress()
        {
            const int MIN_MAC_ADDR_LENGTH = 12;
            string macAddress = string.Empty;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                Logging.Log.Debug(
                    "Found MAC date: " + nic.GetPhysicalAddress() +
                    " Type: " + nic.NetworkInterfaceType, new Utils());

                string tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed && nic.OperationalStatus==OperationalStatus.Up &&
                    !string.IsNullOrEmpty(tempMac) &&
                    tempMac.Length >= MIN_MAC_ADDR_LENGTH)
                {
                    Logging.Log.Debug("New Max Speed = " + nic.Speed + ", MAC: " + tempMac, new Utils());
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }

            return macAddress;
        }

        public static List<NetworkInterface> GetNetworkInterfaces(NetworkInterfaceAdapterType type)
        {
            List<NetworkInterface> dic = new List<NetworkInterface> ();
            try
            {
                var interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (var iface in interfaces)
                {
                    if (string.Equals(iface.Name, "SAIO WIRELESS MODEM", StringComparison.InvariantCultureIgnoreCase))
                        dic.Add(iface);
                    else if (iface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 && type==NetworkInterfaceAdapterType.Wifi)
                        dic.Add(iface);
                    else if (type == NetworkInterfaceAdapterType.Lan)
                        dic.Add(iface);
                }
            }
            catch (Exception x) { Logging.Log.Debug(x.ToString(), new Utils()); }
            return dic;
        }

        public static List<T> GetEnumValues<T>() where T : new()
        {
            T valueType = new T();
            return typeof(T).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                .Select(fieldInfo => (T)fieldInfo.GetValue(valueType))
                .Distinct()
                .ToList();
        }

        public static String FromIntToIPString(int i) {
            return (i & 0xFF) + "." +
                        ((i >> 8) & 0xFF) + "." +
                        ((i >> 16) & 0xFF) + "." +
                        ((i >> 24) & 0xFF);
        }

        public static string GetInterfaceInfoString(NetworkInterface iface)
        {
            var sb = new StringBuilder();
            WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
            sb.AppendLine("");
            var host = Dns.GetHostEntry(Dns.GetHostName());
            var ip = "";
            foreach (var ipp in host.AddressList)
            {
                if (ipp.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip = ipp.ToString();

                }
            }
            long speed = 0;
            var subnet = "";
            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
                {
                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        if (ip.Equals(unicastIPAddressInformation.Address.ToString()))
                        {
                            speed = adapter.Speed;
                            subnet = unicastIPAddressInformation.IPv4Mask.ToString();
                            break;
                        }
                    }
                }
            }
            sb.AppendLine("IP: " + ip);
            sb.AppendLine("SUBNET: " + subnet);
            sb.AppendLine("GATEWAY: " + FromIntToIPString(wifiManager.DhcpInfo.Gateway));
            sb.AppendLine("DHCP: " + (wifiManager.DhcpInfo.LeaseDuration>0));
            sb.AppendLine("SPEED: " + (speed / 1000000) + " Mbps");
            sb.AppendLine("MAC: " + iface.GetPhysicalAddress());
            var sStatus = "DOWN";
            if (iface.OperationalStatus == OperationalStatus.Up)
                sStatus = "UP";
            sb.AppendLine("STATUS: " + wifiManager.ConnectionInfo.Frequency + "/" + sStatus);
            if (iface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
            {
                sb.AppendLine("AP: " + wifiManager.ConnectionInfo.SSID);
                sb.AppendLine("AP signal: " + wifiManager.ConnectionInfo.Rssi);
                sb.AppendLine("AP MAC: " + wifiManager.ConnectionInfo.BSSID);
                sb.AppendLine("Configured APs:");
                foreach (var p in wifiManager.ConfiguredNetworks)
                    sb.AppendLine("*     " + p.Ssid);
            }
            return sb.ToString();
        }
        public static NetworkInterfaceAdapterSettings ParseNetworkSettings(string settings)
        {
            if (String.IsNullOrEmpty(settings))
                return null;

            var chunks = settings.Split(';');
            if (chunks.Length < 3)
                return null;

            try
            {
                var res = new NetworkInterfaceAdapterSettings
                {
                    IPAddress = IPAddress.Parse(chunks[ 0 ]),
                    Netmask = IPAddress.Parse(chunks[ 1 ]),
                    GatewayAddress = IPAddress.Parse(chunks[ 2 ])
                };

                if (chunks.Length == 4)
                {
                    if (chunks[ 3 ].Equals(Boolean.TrueString, StringComparison.InvariantCultureIgnoreCase) ||
                        chunks[ 3 ].Equals(Boolean.FalseString, StringComparison.InvariantCultureIgnoreCase))
                    {
                        res.DHCPEnabled = Boolean.Parse(chunks[ 3 ]);
                    }
                }
                return res;
            }
            catch
            {
                return null;
            }
        }

    }
}