﻿

using OSHipPay.Configuration;
using OSHipPay.Core;
using OSHipPay.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using WebSockets;
using WestPA.Standalone;

namespace OSHipPay.WebSockets
{
    public class WebSocketProcessor
    {
        static PayApp _app;
        readonly UserInterface _ui;
        static Settings _set;
        List<Thread> socketThread = new List<Thread>();

        bool ContinuePayment = true;

        bool GetContinuePayment()
        {
            return ContinuePayment;
        }


        private WebSocketProcessor() { }

        OSHipPay.WebSockets.Messages.TerminalStatusBroadcast _deftaultError = new OSHipPay.WebSockets.Messages.TerminalStatusBroadcast();

        public WebSocketProcessor(PayApp app, UserInterface UI, Settings settings) 
        {
            _app = app;
            _set = settings;
            _ui = UI;
            _deftaultError.StatusCode = StatusCodes.UnknownError.GetStatusCode();
            _deftaultError.StatusMessage = StatusCodes.UnknownError.GetStatusText();
        }

        void ProcessStartTransaction(WebSocket s, OSHipPay.WebSockets.Messages.StartTransaction msg, Settings set)
        {
            if (!_app.GetTerminalStatus().Equals(StatusCodes.Idle.GetStatusCode()))
            {
                OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
                {
                    StatusCode = StatusCodes.NotIdle.GetStatusCode(),
                    StatusMessage = StatusCodes.NotIdle.GetStatusText()
                };
                String ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                Logging.Log.Debug("Sending ProcessPurchaceOrReversal status " + ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
                return;
            }
            if (msg.AllowTipping!=null)
                ApplicationContext.Current.Settings.Tipping = msg.AllowTipping.Value;
            TransactionType t = TransactionType.Purchase;
            if (msg.TransactionType!=null && msg.TransactionType.ToLower().Equals(TransactionType.Refund.ToString().ToLower()))
            {
                t = TransactionType.Refund;
            }
            TransactionContext.Current.Cashier = msg.Cashier;
            TransactionContext.Current.CashRegisterId = msg.CashRegisterId;
            TransactionContext.Current.CashRegisterPaymentId = msg.CashRegisterPaymentId;
            if (!_app.AreTheIdsUnique(msg.CashRegisterId, msg.CashRegisterPaymentId))
            {
                OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
                {
                    StatusCode = StatusCodes.NotUniqueIDs.GetStatusCode(),
                    StatusMessage = StatusCodes.NotUniqueIDs.GetStatusText()
                };
                String ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                Logging.Log.Debug("Sending ProcessPurchaceOrReversal status " + ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
                ToSend = null;
                Thread.Sleep(100);
                _app.CreateErrorForLastTransactionResult(StatusCodes.NotUniqueIDs.GetStatusText(), t);
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(_app.GetLastTransactionResult());
                Logging.Log.Informational(ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
                Logging.Log.Informational(ToSend, new WebSocketProcessor());
//                Logging.Log.Debug("Sending ProcessPurchaceOrReversal response " + ToSend, new WebSocketProcessor());
                ToSend = null;
                Logging.Log.Debug("Finished ProcessPurchaceOrReversal with error", new WebSocketProcessor());
                return;
            }
            TransactionContext.Current.NextTransaction = new TransactionData
            {
                Type = t,
                Amounts = new TransactionAmounts((uint)(msg.Amount * 100), 0 /*cashbackAmount.Value*/, 0 /*vatAmount.Value*/),
            };

            if (msg.Amount == 0) { //SwipeAhead....
                SetContinuePayment(false);
                TransactionContext.Current.NextTransaction.Amounts = null;
            }

            ProcessPurchaceOrReversal(s, false);
        }

        internal static void ProcessECRBroadCast(IPEndPoint groupEP)
        {
            try
            {
                Messages.CashRegisterBroadcastResponse crbr = new Messages.CashRegisterBroadcastResponse()
                {
                    TerminalId = _set.TerminalId,
                    TerminalName = _set.TerminalName
                };
                string ToSend =
                     OSHipPay.WebSockets.JSONProcessor.ConvertToJson(crbr);
                if (ToSend != null && ToSend.IsNotNullOrEmpty())
                {
                    Logging.Log.Debug("CashRegisterBroadcast answer " + ToSend, new WebSocketProcessor());
                    OSHipPay.Configuration.UDP.Send(ToSend);
                }

            }
            catch (Exception e)
            {
                    Logging.Log.Error("ProcessECRBroadCast failed", e, new WebSocketProcessor());
            }


        }

        void ProcessReversal(WebSocket s, OSHipPay.WebSockets.Messages.Reversal msg, Settings set)
        {
            String sRef = _app.GetReferenceNumber(msg.CashRegisterId, msg.CashRegisterPaymentId);
            if (sRef.IsNullOrEmpty())
            {
                OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
                {
                    StatusCode = StatusCodes.NotUniqueIDs.GetStatusCode(),
                    StatusMessage = StatusCodes.NotUniqueIDs.GetStatusText()
                };
                String ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                Logging.Log.Debug("Sending ProcessReversal status " + ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
                ToSend = null;
                Thread.Sleep(100);
                _app.CreateErrorForLastTransactionResult(StatusCodes.NotUniqueIDs.GetStatusText(), TransactionType.Reversal);
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(_app.GetLastTransactionResult());
                Logging.Log.Informational(ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
//                Logging.Log.Debug("Sending ProcessReversal response " + ToSend, new WebSocketProcessor());
                ToSend = null;
                Logging.Log.Debug("Finished ProcessReversal with error", new WebSocketProcessor());
                return;
            }
            TransactionContext.Current.NextTransaction = new TransactionData
            {
                Type = TransactionType.Reversal,
                RetrievalReference = sRef,
            };
            ProcessPurchaceOrReversal(s, true);
        }


        private void ProcessPurchaceOrReversal(WebSocket s, bool Reversal)
        {
            Logging.Log.Debug("Starting ProcessPurchaceOrReversal ", new WebSocketProcessor());
            _app.SetContinue();
            String StatusCode = _app.GetTerminalStatus();
            String ToSend = null;
            _app.SetLastTranasctionResultToNull();
            bool bContinued = !GetContinuePayment();
            while (!GetContinuePayment())
            {
                OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn2 = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
                {
                    StatusCode = StatusCodes.WaitingForFinalAmount.GetStatusCode(),
                    StatusMessage = StatusCodes.WaitingForFinalAmount.GetStatusText()
                };
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn2);
                Thread.Sleep(500);
            }
            OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
            {
                StatusCode = StatusCodes.StartTransaction.GetStatusCode(),
                StatusMessage = StatusCodes.StartTransaction.GetStatusText()
            };
            ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
            Thread.Sleep(50);


            if (bContinued)
                Logging.Log.Debug("Continuing ProcessPurchaceOrReversal ", new WebSocketProcessor());
            for (; ; )
            {
                if (!StatusCode.Equals(_app.GetTerminalStatus()))
                {
                    StatusCode = _app.GetTerminalStatus();
                    OSHipPay.WebSockets.Messages.TransactionStatusNotification tsn2 = new OSHipPay.WebSockets.Messages.TransactionStatusNotification
                    {
                        StatusCode = _app.GetTerminalStatus(),
                        StatusMessage = _app.GetTerminalStatusMessage()
                    };
                    ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn2);
                }

                if (ToSend.IsNotNullOrEmpty())
                {
                    Logging.Log.Debug("Sending ProcessPurchaceOrReversal "+ToSend, new WebSocketProcessor());
                    s.SendTextAsync(ToSend, true, CancellationToken.None);
                    ToSend = null;
                }
                Thread.Sleep(50);
                if (_app.IsStartTransactionFinished())
                {
                    ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(_app.GetLastTransactionResult());
                    s.SendTextAsync(ToSend, true, CancellationToken.None);
                    ToSend = null;
                    Logging.Log.Debug("Finished ProcessPurchaceOrReversal ", new WebSocketProcessor());
                    break;
                }
            }
        }

        internal static void DoUDPBroadtcast(StatusCodes currentStatus)
        {
            Messages.TerminalStatusBroadcast tsb = new Messages.TerminalStatusBroadcast(currentStatus);
            string ToSend =
                 OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsb);
            if (ToSend!=null && ToSend.IsNotNullOrEmpty())
            {
                Logging.Log.Debug("Sending TerminalStatusBroadcast " + ToSend, new WebSocketProcessor());
                OSHipPay.Configuration.UDP.Send(ToSend);
            }
        }

        void ProcessAbort(WebSocket s, OSHipPay.WebSockets.Messages.Abort msg, Settings set)
        {
            Logging.Log.Debug("Starting ProcessAbort ", new WebSocketProcessor());
            String ToSend = null;
            for (; ; )
            {
                bool b = false;
                if (TransactionContext.Current.CashRegisterId.Equals(msg.CashRegisterId) && TransactionContext.Current.CashRegisterPaymentId.Equals(msg.CashRegisterPaymentId)) {
                    Logging.Log.Debug("Starting Async to WestPA", new WebSocketProcessor());
                    b = _app.RequestAsynchronousOperation(WestPA.Standalone.AsynchronousOperations.RequestTransactionAbort);
                    Logging.Log.Debug("Finished Async to WestPA", new WebSocketProcessor());
                    if (b)
                    {
                        OSHipPay.WebSockets.Messages.AbortResult tsn = new OSHipPay.WebSockets.Messages.AbortResult
                        {
                            StatusCode = AbortStatusCodes.OK.GetStatusCode(),
                            StatusMessage = AbortStatusCodes.OK.GetStatusText()
                        };
                        ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                        SetContinuePayment(true);
                    }
                    else
                    {
                        OSHipPay.WebSockets.Messages.AbortResult tsn = new OSHipPay.WebSockets.Messages.AbortResult
                        {
                            StatusCode = AbortStatusCodes.Denied.GetStatusCode(),
                            StatusMessage = AbortStatusCodes.Denied.GetStatusText()
                        };
                        ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                    }
                }
                else
                {
                    OSHipPay.WebSockets.Messages.AbortResult tsn = new OSHipPay.WebSockets.Messages.AbortResult
                    {
                        StatusCode = AbortStatusCodes.Incorrect.GetStatusCode(),
                        StatusMessage = AbortStatusCodes.Incorrect.GetStatusText()
                    };
                    ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tsn);
                }
                if (ToSend.IsNotNullOrEmpty())
                {
                    Logging.Log.Debug("Sending ProcessAbort " + ToSend, new WebSocketProcessor());
                    s.SendTextAsync(ToSend, true, CancellationToken.None);
                    ToSend = null;
                }
                Logging.Log.Debug("Finished ProcessAbort ", new WebSocketProcessor());
                break;
            }
        }

        private void SetContinuePayment(bool v)
        {
            ContinuePayment = v;
        }

        internal void ProcessMessage(WebSocket s, string str, Settings Settings)
        {
            string ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(_deftaultError);
            var obj = JSONProcessor.ParseMessageFromJson(str);
            Logging.Log.Debug("Got JSON " + str, new WebSocketProcessor());
            if (obj==null) {
                OSHipPay.WebSockets.Messages.TerminalStatusBroadcast set = new OSHipPay.WebSockets.Messages.TerminalStatusBroadcast
                {
                    StatusCode = StatusCodes.UnknownError.GetStatusCode(),
                    StatusMessage = StatusCodes.UnknownError.GetStatusText()
                };
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);
            }
            else if (obj is OSHipPay.WebSockets.Messages.StartTransaction tr)
            {
                ToSend = null;
                Thread t = new Thread(() => ProcessStartTransaction(s, tr, Settings));
                socketThread.Add(t);
                t.Start();
            }
            else if (obj is OSHipPay.WebSockets.Messages.TransactionAmountKnown tak)
            {
                ToSend = null;
                Thread t = new Thread(() => ProcessTransactionAmountKnown(s, tak, Settings));
                socketThread.Add(t);
                t.Start();
                
            }
            else if (obj is OSHipPay.WebSockets.Messages.BonusCardRead)
            {
            }
            else if (obj is OSHipPay.WebSockets.Messages.GetTransactionResult gtr)
            {
                ToSend = null;
                Thread t = new Thread(() => ProcessGetTransactionResult(s, gtr, Settings));
                socketThread.Add(t);
                t.Start();

            }
            else if (obj is OSHipPay.WebSockets.Messages.GetTransactionsForGivenDay)
            {

            }
            else if (obj is OSHipPay.WebSockets.Messages.GetTransactionsForGivenDayResult)
            {

            }
            else if (obj is OSHipPay.WebSockets.Messages.Reversal reversal)
            {
                ToSend = null;
                Thread t = new Thread(() => ProcessReversal(s, reversal, Settings));
                socketThread.Add(t);
                t.Start();

            }
            else if (obj is OSHipPay.WebSockets.Messages.Abort abort)
            {
                ToSend = null;
                Thread t = new Thread(() => ProcessAbort(s, abort, Settings));
                socketThread.Add(t);
                t.Start();
            }
            else if (obj is OSHipPay.WebSockets.Messages.QueryGiftCardBalance)
            {

            }
            else if (obj is OSHipPay.WebSockets.Messages.QueryGiftCardBalanceResponse)
            {

            }
            else if (obj is OSHipPay.WebSockets.Messages.StartGiftCardTransaction)
            {

            }
            else if (obj is OSHipPay.WebSockets.Messages.GetTerminalStatus)
            {
                OSHipPay.WebSockets.Messages.TerminalStatusBroadcast set = new OSHipPay.WebSockets.Messages.TerminalStatusBroadcast
                {
                    StatusCode = _app.GetTerminalStatus(),
                    StatusMessage = _app.GetTerminalStatusMessage()
                };
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);
            }
            else if (obj is OSHipPay.WebSockets.Messages.CashRegisterBroadcast)
            {
                OSHipPay.WebSockets.Messages.CashRegisterBroadcastResponse set = new OSHipPay.WebSockets.Messages.CashRegisterBroadcastResponse
                {
                    TerminalName = Settings.TerminalName,
                    TerminalId = Settings.TerminalId
                };
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);

            }
            else if (obj is OSHipPay.WebSockets.Messages.GetConfiguration)
            {
                OSHipPay.WebSockets.Messages.GetConfigurationResponse set = new Messages.GetConfigurationResponse
                {
                    TerminalName = Settings.TerminalName,
                    TippingEnabled = Settings.Tipping,
                    TerminalId = Settings.TerminalId
                };
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);

            }
            else if (obj is OSHipPay.WebSockets.Messages.SetConfiguration sc)
            {
                bool bOK = false;
                if (sc.TerminalName != null)
                {
                    Settings.TerminalName = sc.TerminalName;
                    bOK = true;
                }
                if (sc.TippingEnabled != null)
                {
                    Settings.Tipping = sc.TippingEnabled.Value;
                    //Settings.SavePASetting("EnableTipping", Settings.Tipping);
                    bOK = true;
                }
                if (sc.GUID!=null)
                {
                    bOK = true;
                    Settings.Guid = sc.GUID;
                    Logging.Log.SetGUID(sc.GUID);
                }
                if (sc.LogLevel>0)
                {
                    bOK = true;
                    Settings.LogLevel = sc.LogLevel;
                    Logging.Log.SetLogLevel(Settings.LogLevel);
                }
                if (sc.KeyBeepDefault != null) {
                    bOK = true;
                    Settings.KeyBeepDefault = sc.KeyBeepDefault.Value;
                }
                if (sc.Logo != null)
                {
                    bOK = true;
                    PA_AppSettings.Logo = sc.Logo;
                    Settings.Logo = sc.Logo;
                }
                if (bOK)
                {
                    OSHipPay.WebSockets.Messages.SetConfigurationResponse set = new OSHipPay.WebSockets.Messages.SetConfigurationResponse
                    {
                        StatusCode = StatusCodes.SettingsSaved.GetStatusCode(),
                        StatusMessage = StatusCodes.SettingsSaved.GetStatusText()
                    };
                    ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);
                } else
                {
                    OSHipPay.WebSockets.Messages.SetConfigurationResponse set = new OSHipPay.WebSockets.Messages.SetConfigurationResponse
                    {
                        StatusCode = StatusCodes.SettingsFailed.GetStatusCode(),
                        StatusMessage = StatusCodes.SettingsFailed.GetStatusText()
                    };
                    ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);

                }
            }
            else if (obj is OSHipPay.WebSockets.Messages.GetLog gl)
            {
                OSHipPay.WebSockets.Messages.GetLogResponse set = new OSHipPay.WebSockets.Messages.GetLogResponse();
                StringBuilder stb = new StringBuilder();
                if (gl.TerminalLog)
                {
                    set.TerminalLog = Logging.Log.GetLastEntries().Split('\n');
                    stb.Append("TerminalLog: ").Append('\n');
                    stb.Append(Logging.Log.GetLastEntries());
                }
                if (gl.LogCat)
                {
                    set.LogCat = Logging.Log.GetLogCat().Split('\n');
                    stb.Append("LogCat: ").Append('\n');
                    stb.Append(Logging.Log.GetLogCat());
                }
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);
                if (gl.SendTo.IsNotNullOrEmpty() && stb.Length>0)
                {
                    if (gl.SendTo.ToLower().Equals("LumberJack") || gl.SendTo.ToLower().Equals("OSAgent"))
                    {
                        Logging.Log.Informational("Dumped: "+s.ToString(), new WebSocketProcessor());
                    } else if (gl.SendTo.ToLower().Contains("@"))
                    {
                        Logging.Log.MailAsAttachment(gl.SendTo, stb);
                    }
                }
            }
            else if (obj is OSHipPay.WebSockets.Messages.GetSettings gs)
            {
                OSHipPay.WebSockets.Messages.GetSettingsResponse set = new OSHipPay.WebSockets.Messages.GetSettingsResponse();
                StringBuilder stb = new StringBuilder();
                /*
                if (gs.PASettings)
                {
                    set.PASettings = Settings.GetPASettings().Split('\n');
                    stb.Append("PASettings: ").Append('\n');
                    stb.Append(Settings.GetPASettings());
                }
                */
                if (gs.POSComponentSettings)
                {
                    set.POSComponentSettings = Settings.GetPOSComponentSettings().Split('\n');
                    stb.Append("LogCat: ").Append('\n');
                    stb.Append(Settings.GetPOSComponentSettings());
                }
                set.Version = License.GetVersion();
                set.AppName = License.GetAppName();
                ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(set);
                if (gs.SendTo.IsNotNullOrEmpty() && stb.Length > 0)
                {
                    if (gs.SendTo.ToLower().Equals("LumberJack") || gs.SendTo.ToLower().Equals("OSAgent"))
                    {
                        Logging.Log.Informational("Dumped: " + s.ToString(), new WebSocketProcessor());
                    }
                    else if (gs.SendTo.ToLower().Contains("@"))
                    {
                        Logging.Log.MailAsAttachment(gs.SendTo, stb);
                    }
                }
            }
            if (ToSend != null) { 
                Logging.Log.Debug("Sending ProcessAbort " + ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
            }
        }

        private void ProcessTransactionAmountKnown(WebSocket s, Messages.TransactionAmountKnown tak, Settings settings)
        {

            Logging.Log.Debug("ProcessTransactionAmountKnown", new WebSocketProcessor());
            TransactionContext.Current.NextTransaction.Amounts = new TransactionAmounts((uint)tak.Amount*100, (uint)tak.CashbackAmount*100, (uint)tak.VatAmount*100);
            SetContinuePayment(true);
        }

        private void ProcessGetTransactionResult(WebSocket s, Messages.GetTransactionResult gtr, Settings settings)
        {
            TransactionStatus tr2 = _app.GetTransactionStatusWithIDs(gtr.CashRegisterId, gtr.CashRegisterPaymentId);
            Messages.TransactionResult tr = new Messages.TransactionResult(tr2);
            string ToSend = OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tr);
            if (ToSend != null) { 
                Logging.Log.Debug("Sending ProcessGetTransactionResult " + ToSend, new WebSocketProcessor());
                s.SendTextAsync(ToSend, true, CancellationToken.None);
            }
}
    }
}