﻿using Newtonsoft.Json;
using System;
using static OSHipPay.WebSockets.Messages;

namespace OSHipPay.WebSockets
{
    public class JSONProcessor
    {

        public static Object ParseMessageFromJson(string s)
        {
            try
            {
                var Object = JsonConvert.DeserializeObject<BaseMessage>(s);
                string sName = ((BaseMessage)Object).Message;
                if (sName.Equals(typeof(StartTransaction).Name))
                {
                    return JsonConvert.DeserializeObject<StartTransaction>(s);
                }
                else if (sName.Equals(typeof(TransactionResult).Name))
                {
                    return JsonConvert.DeserializeObject<TransactionResult>(s);
                }
                else if (sName.Equals(typeof(TransactionAmountKnown).Name))
                {
                    return JsonConvert.DeserializeObject<TransactionAmountKnown>(s);
                }
                else if (sName.Equals(typeof(BonusCardRead).Name))
                {
                    return JsonConvert.DeserializeObject<BonusCardRead>(s);
                }
                else if (sName.Equals(typeof(GetTransactionResult).Name))
                {
                    return JsonConvert.DeserializeObject<GetTransactionResult>(s);
                }
                else if (sName.Equals(typeof(GetTransactionsForGivenDay).Name))
                {
                    return JsonConvert.DeserializeObject<GetTransactionsForGivenDay>(s);
                }
                else if (sName.Equals(typeof(GetTransactionsForGivenDayResult).Name))
                {
                    return JsonConvert.DeserializeObject<GetTransactionsForGivenDayResult>(s);
                }
                else if (sName.Equals(typeof(Reversal).Name))
                {
                    return JsonConvert.DeserializeObject<Reversal>(s);
                }
                else if (sName.Equals(typeof(TransactionStatusNotification).Name))
                {
                    return JsonConvert.DeserializeObject<TransactionStatusNotification>(s);
                }
                else if (sName.Equals(typeof(Abort).Name))
                {
                    return JsonConvert.DeserializeObject<Abort>(s);
                }
                else if (sName.Equals(typeof(AbortResult).Name))
                {
                    return JsonConvert.DeserializeObject<AbortResult>(s);
                }
                else if (sName.Equals(typeof(QueryGiftCardBalance).Name))
                {
                    return JsonConvert.DeserializeObject<QueryGiftCardBalance>(s);
                }
                else if (sName.Equals(typeof(QueryGiftCardBalanceResponse).Name))
                {
                    return JsonConvert.DeserializeObject<QueryGiftCardBalanceResponse>(s);
                }
                else if (sName.Equals(typeof(StartGiftCardTransaction).Name))
                {
                    return JsonConvert.DeserializeObject<StartGiftCardTransaction>(s);
                }
                else if (sName.Equals(typeof(GetTerminalStatus).Name))
                {
                    return JsonConvert.DeserializeObject<GetTerminalStatus>(s);
                }
                else if (sName.Equals(typeof(TerminalStatusBroadcast).Name))
                {
                    return JsonConvert.DeserializeObject<TerminalStatusBroadcast>(s);
                }
                else if (sName.Equals(typeof(CashRegisterBroadcast).Name))
                {
                    return JsonConvert.DeserializeObject<CashRegisterBroadcast>(s);
                }
                else if (sName.Equals(typeof(CashRegisterBroadcastResponse).Name))
                {
                    return JsonConvert.DeserializeObject<CashRegisterBroadcastResponse>(s);
                }
                else if (sName.Equals(typeof(GetConfiguration).Name))
                {
                    return JsonConvert.DeserializeObject<GetConfiguration>(s);
                }
                else if (sName.Equals(typeof(GetConfigurationResponse).Name))
                {
                    return JsonConvert.DeserializeObject<GetConfigurationResponse>(s);
                }
                else if (sName.Equals(typeof(SetConfiguration).Name))
                {
                    return JsonConvert.DeserializeObject<SetConfiguration>(s);
                }
                else if (sName.Equals(typeof(SetConfigurationResponse).Name))
                {
                    return JsonConvert.DeserializeObject<SetConfigurationResponse>(s);
                }
                else if (sName.Equals(typeof(GetLog).Name))
                {
                    return JsonConvert.DeserializeObject<GetLog>(s);
                }
                else if (sName.Equals(typeof(GetSettings).Name))
                {
                    return JsonConvert.DeserializeObject<GetSettings>(s);
                }
                else
                {
                    Logging.Log.Error("Failed to parse JSON object " + s, new JSONProcessor());
                }
            }
            catch (Exception e)
            {
                Logging.Log.Error("Failed to parse JSON object " + s, e, new JSONProcessor());
            }
            return null;
        }

        public static string ConvertToJson(Object o)
        {
            return JsonConvert.SerializeObject(o);
        }
    }
}