﻿using Newtonsoft.Json;
using OSHipPay.Core;
using System;
using System.Collections.Generic;
using System.Text;
using WestPA.Standalone;

namespace OSHipPay.WebSockets
{
    public class Messages
    {

        public class BaseMessage
        {
            [JsonProperty(Order = 1)]
            public string Message { get; set; }
        }

        public class BaseMessageWithCashRegisterId: BaseMessage
        {
            [JsonProperty(Order = 2)]
            public string CashRegisterId { get; set; }
            [JsonProperty(Order = 3)]
            public string CashRegisterPaymentId { get; set; }
        }
        public class StartTransaction : BaseMessageWithCashRegisterId
        {

            public string WayOfPayment { get; set; }
            public string TransactionType { get; set; }
            public decimal Amount { get; set; }
            public string Currency { get; set; }
            public bool ReadBonusCard { get; set; }
            public DateTime CashRegisterDateTime { get; set; }
            public bool ForceOnline { get; set; }
            public bool AllowDCC { get; set; }
            public Boolean? AllowTipping { get; set; }
            public string Cashier { get; set; }

            public StartTransaction()
            {
                Message = "StartTransaction";

            }
        }

        public class TransactionResult : BaseMessageWithCashRegisterId
        {
            public decimal? Amount { get; set; }
            public decimal? TipAmount { get; set; }
            public decimal? CashbackAmount { get; set; }
            public decimal? VatAmount { get; set; }
            public decimal? CashAdvanceChargeAmount { get; set; }
            public decimal? GratuityAmount { get; set; }
            public string TerminalReferenceNumber { get; set; }
            public string TranasctionType { get; set; }
            public string PaymentCardNumber { get; set; }
            public string PaymentCode { get; set; }
            public string CardEntryMethod { get; set; }
            public DCCData DCCData { get; set; }
            public string ExpiryDate { get; set; }
            public string CardholderName { get; set; }
            public bool IsReversal { get; set; }
            public string DebitCreditSelection { get; set; }
            public MerchantData MerchantData { get; set; }
            public string CurrencyCode { get; set; }
            public DateTime Timestamp { get; set; }
            public string CardType { get; set; }
            public string DenialText { get; set; }
            public string FinancialInstitution { get; set; }
            public string ReferenceNumber { get; set; }
            public string AuthorisationResponseCode { get; set; }
            public string AuthorisingEntity { get; set; }
            public string AuthorisationMethod { get; set; }
            public string VerificationMethod { get; set; }
            public string ApprovalCode { get; set; }
            public bool Approved { get; set; }
            public string AID { get; set; }
            public string TVR { get; set; }
            public string TSI { get; set; }
            public string EmvAuthorisationResponseCode { get; set; }
            public string Cashier { get; set; }

            public TransactionResult()
            {
                Message = "TransactionResult";
            }

            private Decimal? FromCents(uint amount)
            {
                if (amount!=0)
                    return (decimal)(Convert.ToDecimal(amount) / 100);
                else
                    return null;
            }

            private string BytesToString(byte[] a)
            {
                StringBuilder stb = new StringBuilder();
                foreach(byte b in a)
                {
                    stb.Append((char)b);
                }
                return stb.ToString();
            }

            public TransactionResult(ReceiptData r, TransactionData nextTransaction)
            {
                Message = "TransactionResult";
                try
                {
                    if (nextTransaction.Amounts != null) { 
                         Amount = FromCents(nextTransaction.Amounts.TotalAmount);
                         TipAmount = FromCents(nextTransaction.Amounts.GratuityAmount);
                         CashbackAmount = FromCents(nextTransaction.Amounts.CashbackAmount);
                         VatAmount = FromCents(nextTransaction.Amounts.VatAmount);
                         CashAdvanceChargeAmount = FromCents(nextTransaction.Amounts.CashAdvanceChargeAmount);
                         GratuityAmount = FromCents(nextTransaction.Amounts.GratuityAmount);
                    }
                    if (r != null)
                    {
                        if (r.AuthorisedAmounts!= null)
                        {
                            Amount = FromCents(r.AuthorisedAmounts.TotalAmount);
                            TipAmount = FromCents(r.AuthorisedAmounts.GratuityAmount);
                            CashbackAmount = FromCents(r.AuthorisedAmounts.CashbackAmount);
                            VatAmount = FromCents(r.AuthorisedAmounts.VatAmount);
                            CashAdvanceChargeAmount = FromCents(r.AuthorisedAmounts.CashAdvanceChargeAmount);
                            GratuityAmount = FromCents(r.AuthorisedAmounts.GratuityAmount);
                        }
                        TerminalReferenceNumber = r.ReferenceNumber;
                        TranasctionType = r.Transaction.TransactionType.ToString();
                        PaymentCardNumber = r.Transaction.PaymentCardNumber;
                        PaymentCode = r.Transaction.PaymentCode;
                        CardEntryMethod = r.Transaction.CardEntryMethod.ToString();
                        if (r.DCC != null)
                            DCCData = new DCCData(r.DCC);
                        else
                            DCCData = new DCCData();
                        ExpiryDate = r.ExpiryDate;
                        CardholderName = r.CardholderName;
                        IsReversal = r.IsReversal;
                        DebitCreditSelection = r.DebitCredit.ToString();
                        if (r.MerchantInformation != null)
                            MerchantData = new MerchantData(r.MerchantInformation);
                        else
                            MerchantData = new MerchantData();
                        CurrencyCode = r.CurrencyCode;
                        Timestamp = r.Timestamp;
                        CardType = r.CardType;
                        DenialText = r.DenialText;
                        FinancialInstitution = r.FinancialInstitution;
                        ReferenceNumber = r.ReferenceNumber;
                        AuthorisationResponseCode = r.AuthorisationResponseCode;
                        AuthorisingEntity = r.AuthEntity.ToString();
                        AuthorisationMethod = r.Authorisation.ToString();
                        VerificationMethod = r.Verification.ToString();
                        ApprovalCode = r.ApprovalCode;
                        Approved = r.Approved;
                        AID = EMVElements.StringOf(r.EMV.AID);
                        TVR = EMVElements.StringOf(r.EMV.TVR);
                        TSI = EMVElements.StringOf(r.EMV.TSI);
                        EmvAuthorisationResponseCode = r.EMV.EmvAuthorisationResponseCode;
                    }
                    else Approved = false;
                } catch (Exception e)
                {
                    Logging.Log.Error("TransactionResult contructor failed", e, new TransactionResult());
                }
            }
            public TransactionResult(TransactionStatus tr)
            {
                Message = "TransactionResult";
                try
                {
                    if (tr.Amounts != null)
                    {
                        Amount = FromCents(tr.Amounts.TotalAmount);
                        TipAmount = FromCents(tr.Amounts.GratuityAmount);
                        CashbackAmount = FromCents(tr.Amounts.CashbackAmount);
                        VatAmount = FromCents(tr.Amounts.VatAmount);
                        CashAdvanceChargeAmount = FromCents(tr.Amounts.CashAdvanceChargeAmount);
                        GratuityAmount = FromCents(tr.Amounts.GratuityAmount);
                    }
                    ReceiptData r = tr.ReceiptData;
                    if (r != null)
                    {
                        TerminalReferenceNumber = r.ReferenceNumber;
                        TranasctionType = r.Transaction.TransactionType.ToString();
                        PaymentCardNumber = r.Transaction.PaymentCardNumber;
                        PaymentCode = r.Transaction.PaymentCode;
                        CardEntryMethod = r.Transaction.CardEntryMethod.ToString();
                        if (r.DCC != null)
                            DCCData = new DCCData(r.DCC);
                        else
                            DCCData = new DCCData();
                        ExpiryDate = r.ExpiryDate;
                        CardholderName = r.CardholderName;
                        IsReversal = r.IsReversal;
                        DebitCreditSelection = r.DebitCredit.ToString();
                        if (r.MerchantInformation != null)
                            MerchantData = new MerchantData(r.MerchantInformation);
                        else
                            MerchantData = new MerchantData();
                        CurrencyCode = r.CurrencyCode;
                        Timestamp = r.Timestamp;
                        CardType = r.CardType;
                        DenialText = r.DenialText;
                        FinancialInstitution = r.FinancialInstitution;
                        ReferenceNumber = r.ReferenceNumber;
                        AuthorisationResponseCode = r.AuthorisationResponseCode;
                        AuthorisingEntity = r.AuthEntity.ToString();
                        AuthorisationMethod = r.Authorisation.ToString();
                        VerificationMethod = r.Verification.ToString();
                        ApprovalCode = r.ApprovalCode;
                        Approved = r.Approved;
                        AID = EMVElements.StringOf(r.EMV.AID);
                        TVR = EMVElements.StringOf(r.EMV.TVR);
                        TSI = EMVElements.StringOf(r.EMV.TSI);
                        EmvAuthorisationResponseCode = r.EMV.EmvAuthorisationResponseCode;
                    }
                    else Approved = false;
                }
                catch (Exception e)
                {
                    Logging.Log.Error("TransactionResult contructor failed", e, new TransactionResult());
                }
            }
        }

        public class DCCData
        {
            public string CurrencyCode { get; set; }
            public long ConvertedAmount { get; set; }
            public long ConvertedExtra { get; set; }
            public int Exponent { get; set; }
            public decimal ExchangeRate { get; set; }
            public string RateProvider { get; set; }
            public string Source { get; set; }
            public DateTime TimeStamp { get; set; }
            public string MarkupFormatted { get; set; }
            public char CardScheme { get; set; }
            public string MerchantDisclaimer { get; set; }
            public string CardholderDisclaimer { get; set; }

            public DCCData(WestPA.Standalone.DCCData d)
            {
                CurrencyCode = d.CurrencyCode;
                ConvertedAmount = d.ConvertedAmount;
                ConvertedExtra = d.ConvertedExtra;
                Exponent = d.Exponent;
                RateProvider = d.RateProvider;
                ExchangeRate = d.ExchangeRate;
                Source = d.Source;
                TimeStamp = d.TimeStamp;
                MarkupFormatted = d.MarkupFormatted;
                CardScheme = d.CardScheme;
                MerchantDisclaimer = d.MerchantDisclaimer;
                CardholderDisclaimer = d.CardholderDisclaimer;
            }

            public DCCData() { }

        }

        public class MerchantData
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string ZipCode { get; set; }
            public string PhoneNumber { get; set; }
            public string OrganisationNumber { get; set; }
            public string CategoryCode { get; set; }
            public string BankAgentName { get; set; }

            public MerchantData(WestPA.Standalone.MerchantData m)
            {
                    Name = m.Name;
                    Address = m.Address;
                    City = m.City;
                    ZipCode = m.ZipCode;
                    PhoneNumber = m.PhoneNumber;
                    OrganisationNumber = m.OrganisationNumber;
                    CategoryCode = m.CategoryCode;
                    BankAgentName = m.BankAgentName;
            }

            public MerchantData() { }

        }

        public class BaseMessageWithStatusCode : BaseMessage
        {
            public string StatusCode { get; set; }
            public string StatusMessage { get; set; }
        }

        public class BonusCardRead : BaseMessage
        {
            public string CardType { get; set; }
            public string CardSubType { get; set; }
            public string CardNumber { get; set; }

            public BonusCardRead()
            {
                Message =  "BonusCardRead";
            }
        }

        public class TransactionAmountKnown : BaseMessageWithCashRegisterId
        {
            public double Amount { get; set; }
            public int TipAmount { get; set; }
            public double CashbackAmount { get; set; }
            public int VatAmount { get; set; }
            public int CashAdvanceChargeAmount { get; set; }
            public int GratuityAmount { get; set; }
            public TransactionAmountKnown()
            {
                Message = "TransactionAmountKnown";
            }
        }
        public class GetTransactionResult : BaseMessageWithCashRegisterId
        {
            public GetTransactionResult()
            {
                Message = "GetTransactionResult";
            }
        }

        public class GetTransactionsForGivenDay : BaseMessage
        {
            public DateTime TimeStamp { get; set; }
            public GetTransactionsForGivenDay()
            {
                Message = "GetTransactionsForGivenDay";
            }
        }

        public class TransactionIDs
        {
            public string CashRegisterId { get; set; }
            public string CashRegisterPaymentId { get; set; }
        }

        public class GetTransactionsForGivenDayResult : BaseMessage
        {
            public List<TransactionIDs> Transactions { get; set; }
            public GetTransactionsForGivenDayResult()
            {
                Message = "GetTransactionsForGivenDayResult";
            }
        }

        public class Reversal : BaseMessageWithCashRegisterId
        {
            public Reversal()
            {
                Message = "Reversal";
            }
        }

        public class TransactionStatusNotification : BaseMessageWithStatusCode
        {
            public TransactionStatusNotification()
            {
                Message = "TransactionStatusNotification";
            }
        }

        public class Abort : BaseMessageWithCashRegisterId
        {
            public Abort()
            {
                Message = "Abort";
            }
        }

        public class AbortResult : BaseMessageWithStatusCode
        {
            public AbortResult()
            {
                Message = "AbortResult";
            }
        }

        public class QueryGiftCardBalance : BaseMessage
        {
            public string CardNumber { get; set; }
            public QueryGiftCardBalance()
            {
                Message = "QueryGiftCardBalance";
            }
        }

        public class QueryGiftCardBalanceResponse : BaseMessageWithStatusCode
        {
            public decimal Amount { get; set; }
            public QueryGiftCardBalanceResponse()
            {
                Message = "QueryGiftCardBalanceResponse";
                Amount = 0;
            }
        }
        public class StartGiftCardTransaction : StartTransaction
        {
            public string CardNumber { get; set; }
            public StartGiftCardTransaction()
            {
                Message = "StartGiftCardTransaction";
            }
        }
        public class GetTerminalStatus : BaseMessage
        {
            public GetTerminalStatus()
            {
                Message = "GetTerminalStatus";
            }
        }

        public class TerminalStatusBroadcast : BaseMessageWithStatusCode
        {

            public TerminalStatusBroadcast()
            {
                Message = "TerminalStatusBroadcast";
            }

            public TerminalStatusBroadcast(StatusCodes currentStatus)
            {
                Message = "TerminalStatusBroadcast";
                StatusCode = currentStatus.GetStatusCode();
                StatusMessage = currentStatus.GetStatusText();

            }
        }
        public class CashRegisterBroadcast : BaseMessage
        {
            public CashRegisterBroadcast()
            {
                Message = "CashRegisterBroadcast";
            }
        }

        public class CashRegisterBroadcastResponse : BaseMessage
        {
            public string TerminalId { get; set; }
            public string TerminalName { get; set; }

            public CashRegisterBroadcastResponse()
            {
                Message = "CashRegisterBroadcastResponse";
            }
        }

        public class GetConfiguration : BaseMessage
        {
            public GetConfiguration()
            {
                Message = "GetConfiguration";
            }
        }

        public class GetConfigurationResponse : BaseMessage
        {
            public string TerminalName { get; set; }
            public bool TippingEnabled { get; set; }
            public string TerminalId { get; set; }

            public GetConfigurationResponse()
            {
                Message = "GetConfigurationResponse";
            }
        }
        
        public class SetConfiguration : BaseMessage
        {
            public string TerminalName { get; set; }
            public Boolean? TippingEnabled { get; set; }

            public string GUID { get; set; }
            public int LogLevel { get; set; }

            public Boolean? KeyBeepDefault { get; set; }

            public string Logo { get; set; }

            public SetConfiguration()
            {
                Message = "SetConfiguration";
            }
        }

        public class SetConfigurationResponse : BaseMessageWithStatusCode
        {
            public SetConfigurationResponse()
            {
                Message = "SetConfigurationResponse";
            }
        }

        public class GetLog : BaseMessage
        {
            public bool TerminalLog { get; set; }
            public bool LogCat { get; set; }

            public string SendTo { get; set; }

            public GetLog()
            {
                Message = "GetLog";
            }
        }
        public class GetLogResponse : BaseMessage
        {
            public String[] TerminalLog { get; set; }
            public String[] LogCat { get; set; }

            public GetLogResponse()
            {
                Message = "GetLogResponse";
            }
        }

        public class GetSettings : BaseMessage
        {
            public bool PASettings { get; set; }
            public bool POSComponentSettings { get; set; }

            public string SendTo { get; set; }

            public GetSettings()
            {
                Message = "GetSettings";
            }
        }
        public class GetSettingsResponse : BaseMessage
        {
            public String[] PASettings { get; set; }
            public String[] POSComponentSettings { get; set; }

            public string AppName { get; set; }
            public string Version { get; set; }

            public GetSettingsResponse()
            {
                Message = "GetSettingsResponse";
            }
        }


    }
}