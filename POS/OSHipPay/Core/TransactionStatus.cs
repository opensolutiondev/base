﻿using System;
using WestPA.Standalone;

namespace OSHipPay.Core
{
    public enum TransactionStatuses { UNKNOWN, ACCEPTED, DECLINED, CANCELLED };

    public class TransactionStatus
    {
        public string Id { get; set; }
        public TransactionType TransactionType { get; set; }
        public TransactionStatuses Status { get; set; }
        public TransactionAmounts Amounts { get; set; }
        public string CardName { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime UpdateDate { get; set; }
        public ReceiptData ReceiptData { get; set; }
        public bool Verified { get; set; }
        private string _cashier = "1";
        public string Cashier { get { return _cashier; } set { _cashier = value; } }
        public UInt32 OfflinetransactionId { get; set; }
        public string CashRegisterId { get; set; }
        public string CashRegisterPaymentId { get; set; }

        internal TransactionStatus() { /* This ctor is only used for serialization */ }

        public TransactionStatus(string transactionId, TransactionType type, TransactionStatuses status, TransactionAmounts amounts,
            string cardName, string referenceNumber, ReceiptData receipt, bool verified, string cashier, string cashRegisterId, string cashRegisterPaymentId)
        {
            Id = transactionId;
            TransactionType = type;
            Status = status;
            Amounts = amounts;
            CardName = cardName;
            ReferenceNumber = referenceNumber;
            UpdateDate = DateTime.Now;
            ReceiptData = receipt;
            Verified = verified;
            Cashier = cashier;
            CashRegisterId = cashRegisterId;
            CashRegisterPaymentId = cashRegisterPaymentId;
        }

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("Id = " + Id);
            sb.AppendLine("TransactionType = " + TransactionType);
            sb.AppendLine("Status = " + Status);
            sb.AppendLine("TotalAmount = " + Amounts.TotalAmount);
            sb.AppendLine("CardName = " + CardName);
            sb.AppendLine("ReferenceNumber = " + ReferenceNumber);
            sb.AppendLine("UpdateDate = " + UpdateDate);
            sb.AppendLine("Cashier = " + Cashier);
            sb.AppendLine("CashRegisterId = " + CashRegisterId);
            sb.AppendLine("CashRegisterPaymentId = " + CashRegisterPaymentId);
            return sb.ToString();
        }
    }
}