﻿using System;
using System.Data;
using Mono.Data.Sqlite;
using OSHipPay.Logging;

namespace OSHipPay.Core
{
    internal class ExceptionHandler
    {
        private uint _count = 0;
        private SqliteConnection _q;
        readonly private Action _initialiseAction;

        internal ExceptionHandler(SqliteConnection q, Action initialiseAction)
        {
            _q = q;
            _initialiseAction = initialiseAction;
        }

        internal bool Handle(Exception sx)
        {
            
            if (_count == 0)
                Log.Error(sx.ToString(), this);
            
            //retry
            if (_count < 3)
            {
                _count++;
                System.Threading.Thread.Sleep(1000);
                Log.Warning("Retrying", this);
                return true;
            }
            //enough with the retries. Let's go berserk :-)
            else if (_count < 5)
            {
                _count++;
                /*
                var fName = "";
                try
                {
                    System.Threading.Thread.Sleep(1000);
                    fName = _q.ConnectionString.Split('=')[1].Split(';')[0].TrimStart('"').TrimEnd('"');
                    var connstring = _q.ConnectionString;
                    _q.Close();
                    try { System.IO.File.Delete(fName); }
                    catch (System.IO.FileNotFoundException) { }
                    Log.Warning("File removed: " + fName, this);
                    SqliteConnection newq = new SqliteConnection(connstring);
                    newq.Open();

                    _q = newq;
                    try
                    {
                        _initialiseAction();
                    }
                    catch (Exception x) { Log.Error(x.ToString(), x, this); }
                }
                catch (Exception x){  Log.Error("Failed to remove: " + fName + " - " + x.ToString(), this);}
                */
                return true;
            }
            return false;
        }
    }

    public class Connection : IDisposable
    {
        private ExceptionHandler _exceptionHandler;
        internal SqliteConnection _connection;
        internal Action _action;

        public ConnectionState State { get { return _connection.State; } }

        public Connection(string connectionString, Action initialiseAction)
        {
            _connection = new SqliteConnection(connectionString + _pragmaStatement);
            _action = initialiseAction;
            _exceptionHandler = new ExceptionHandler(_connection, _action);
        }

        public void Open()
        {
            if (_connection.State == System.Data.ConnectionState.Open)
                return;

            retry:
            try { _connection.Open(); }
            catch (SqliteException q) { if (_exceptionHandler.Handle(q)) goto retry; }
            catch (Exception x) { Log.Error(x.ToString(), x, this);}
        }

        public void Close()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
                return;

            retry:
            try { _connection.Close(); }
            catch (SqliteException q) { if (_exceptionHandler.Handle(q)) goto retry; }
            catch (Exception x) { Log.Error(x.ToString(), x, this); }
        }

        void IDisposable.Dispose()
        {
            _connection.Dispose();
        }

        private const string _pragmaStatement =
            "soft_heap_limit=409600;locking_mode=EXCLUSIVE;page_size=1024;cache_size=128;";
    } // end class

    public class Command : IDisposable
    {
        private SqliteCommand _command;
        private ExceptionHandler _exceptionHandler;
        readonly private SqliteConnection _connection;
        public SqliteParameterCollection Parameters { get { return _command.Parameters; } }

        public Command(string commandText, Connection connection)
        {
            _connection = connection._connection;
            _exceptionHandler = new ExceptionHandler(_connection, connection._action);
            _command = new SqliteCommand(commandText, _connection);
        }

        public int ExecuteNonQuery()
        {
        retry:
            try { return _command.ExecuteNonQuery(); }
            catch (SqliteException q) { if (_exceptionHandler.Handle(q)) goto retry; }
            catch (Exception x) { Log.Error(x.ToString(), x, this); }
            return 0;
        }

        public SqliteDataReader ExecuteReader()
        {
        retry:
            try { return _command.ExecuteReader(); }
            catch (SqliteException q) { if (_exceptionHandler.Handle(q)) goto retry; }
            catch (Exception x) { Log.Error(x.ToString(), x, this); }
            return null;
        }

        void IDisposable.Dispose()
        {
            Parameters.Clear();
            _command.Dispose();
        }
    } // end class

}