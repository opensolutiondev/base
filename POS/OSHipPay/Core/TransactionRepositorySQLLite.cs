﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using WestPA.Standalone;
using OSHipPay.Configuration;
using OSHipPay.Logging;

namespace OSHipPay.Core
{
    public class TransactionRepositorySQLite : ITransactionRepository
    {
        private readonly string _connectionString;
        private int _currentTransactionCount = int.MinValue;
        private string _currenttransactionId;
        private XmlSerializer _serializer = new XmlSerializer(typeof(TransactionStatus));
        private readonly XmlWriterSettings _serializerSettings = new XmlWriterSettings();
        private readonly XmlSerializerNamespaces _serializerNamespaces = new XmlSerializerNamespaces();
        private readonly Connection _sqliteConnection;
        TransactionRepositorySQLite()
        {

        }
        public TransactionRepositorySQLite(string fileName)
        {
            Logging.Log.Debug("Initialise ", new TransactionRepositorySQLite());
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentNullException("fileName");

            _connectionString = "Data Source=\"" + fileName + "\";Version=3;";
            _sqliteConnection = new Connection(_connectionString, Initialise);
            _sqliteConnection.Open();
            _serializerSettings.OmitXmlDeclaration = true;
            _serializerNamespaces.Add("", "");

            Logging.Log.Debug("connected ", new TransactionRepositorySQLite());
            try
            {
                CheckAndCreateSchema(false);
                Logging.Log.Debug("CheckAndCreateSchema ", new TransactionRepositorySQLite());
            }
            catch (Exception e)
            {
            
                Log.Error("Cannot initialize SQLite. File " + fileName + " will be removed.", e, this);

                try
                {
                    File.Delete(fileName);
                }
                catch { }
            }
        }

        /*
        ~TransactionRepositorySQLite()
        {
            if (_sqliteConnection!=null)
                _sqliteConnection.Close();
        }
        */

        public void Initialise()
        {
            Logging.Log.Debug("Initialise ", new TransactionRepositorySQLite());
            CheckAndCreateSchema(false);
        }
        public void Initialise(bool recreateTable)
        {
            Logging.Log.Debug("Initialise() ", new TransactionRepositorySQLite());
            CheckAndCreateSchema(recreateTable);
        }

        public TransactionStatus AddOrUpdate(string transactionId, TransactionType type, TransactionStatuses status, TransactionAmounts amounts,
            string cardName, string referenceNumber, ReceiptData receipt, bool verified, string cashier, string cashRegisterId, string cashRegisterPaymentId)
        {
            Logging.Log.Debug("AddOrUpdate " + transactionId, new TransactionRepositorySQLite());
            if (String.IsNullOrEmpty(transactionId))
                return null;

            var tr = new TransactionStatus
            {
                Id = transactionId,
                TransactionType = type,
                Status = status,
                Amounts = amounts,
                CardName = cardName,
                ReferenceNumber = referenceNumber,
                ReceiptData = receipt,
                Verified = verified,
                Cashier = cashier,
                CashRegisterId = cashRegisterId,
                CashRegisterPaymentId = cashRegisterPaymentId,
                UpdateDate = DateTime.Now
            };

            return AddOrUpdate(tr);
        }

        public TransactionStatus AddOrUpdate(TransactionStatus tr)
        {
            Logging.Log.Debug("AddOrUpdate with tr" , new TransactionRepositorySQLite());

            if (tr == null || String.IsNullOrEmpty(tr.Id))
                return null;

            var count = GetCount();

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            if (count >= PA_AppSettings.TransactionNumbersLimit)
            {
                var numberToRemove = count - PA_AppSettings.TransactionNumbersLimit + 1;
                using (var command = new Command(DeleteByNumberStatement, _sqliteConnection))
                {
                    command.Parameters.AddWithValue("@number", numberToRemove);
                    command.ExecuteNonQuery();
                    _currentTransactionCount = int.MinValue;
                }
            }
            /*
            using (var command = new Command(DeleteDateTimeStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@dt", DateTime.Today.AddDays(-PA_AppSettings.TransactionDaysLimit));
                command.ExecuteNonQuery();
                _currentTransactionCount = int.MinValue;
            }
            */
            GetCount();

            using (var command2 = new Command(InsertOrReplaceStatement, _sqliteConnection))
            {
                command2.Parameters.AddWithValue("@id", tr.Id);
                command2.Parameters.AddWithValue("@cashRegiserId", tr.CashRegisterId);
                command2.Parameters.AddWithValue("@cashRegisterPaymentId", tr.CashRegisterPaymentId);
                command2.Parameters.AddWithValue("@udt", tr.UpdateDate.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                command2.Parameters.AddWithValue("@status", tr.Status.ToString());
                command2.Parameters.AddWithValue("@data", Serialize(tr));
                command2.ExecuteNonQuery();
                if (!tr.Id.Equals(_currenttransactionId))
                    _currentTransactionCount++;
                _currenttransactionId = tr.Id;
            }
            Logging.Log.Informational(OSHipPay.WebSockets.JSONProcessor.ConvertToJson(tr), new TransactionRepositorySQLite());
            Logging.Log.Debug("AddOrUpdate with tr Id = "+tr.Id, new TransactionRepositorySQLite());
            return tr;
        }

        public void SetVerified(string transactionId)
        {
            var tr = GetTransaction(transactionId);
            if (tr == null)
                return;

            tr.Verified = true;
            AddOrUpdate(tr);
        }

        public void Clear()
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(ClearStatement, _sqliteConnection))
            {
                command.ExecuteNonQuery();
                _currentTransactionCount = 0;
            }
        }

        public bool ContainsId(string transactionId)
        {
            if (String.IsNullOrEmpty(transactionId))
                return false;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(ContainsIdStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@transactionId", transactionId);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                        return true;
                }
            }
            return false;
        }

        public TransactionStatus GetTransaction(string transactionId)
        {
            if (String.IsNullOrEmpty(transactionId))
                return null;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetTransactionStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@transactionId", transactionId);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                        return Deserialize((string)reader["data"]);
                }
            }
            return null;
        }

        public TransactionStatus GetLastAccepted()
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetLastStatusStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@status", TransactionStatuses.ACCEPTED.ToString());

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return Deserialize((string)reader["data"]);
                    }
                }
            }
            return null;
        }

        public long GetCount()
        {
            if (_currentTransactionCount != int.MinValue)
                return _currentTransactionCount;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetCountStatement, _sqliteConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var tmp = reader.GetInt32(0);
                        _currentTransactionCount = tmp;
                        return tmp;
                    }
                }
            }
            return 0;
        }

        public long GetCountForPeriod(DateTime from, DateTime to)
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetCountForPeriodStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader.GetInt64(0);
                    }
                }
            }
            return 0;
        }

        public TransactionStatus GetFirst()
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetFirstStatement, _sqliteConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return Deserialize((string)reader["data"]);
                    }
                }
            }
            return null;
        }

        public TransactionStatus GetLast()
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetLastStatement, _sqliteConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return Deserialize((string)reader["data"]);
                    }
                }
            }
            return null;
        }

        public IEnumerable<TransactionStatus> GetForPeriod(DateTime from, DateTime to)
        {
            var res = new List<TransactionStatus>();
            var count = GetCount();

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            if (count >= PA_AppSettings.TransactionNumbersLimit)
            {
                var numberToRemove = count - PA_AppSettings.TransactionNumbersLimit + 1;
                using (var command = new Command(DeleteByNumberStatement, _sqliteConnection))
                {
                    command.Parameters.AddWithValue("@number", numberToRemove);
                    command.ExecuteNonQuery();
                    _currentTransactionCount = int.MinValue;
                }
            }
            GetCount();
            using (var command = new Command(GetForPeriodStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var tr = Deserialize((string)reader["data"]);
                        if (tr != null)
                            res.Add(tr);
                    }
                }
            }
            return res;
        }

        public IEnumerable<TransactionStatus> GetECRIdentifiers()
        {
            var res = new List<TransactionStatus>();
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetECRIdentifiersStatemt, _sqliteConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var tr = Deserialize((string)reader["data"]);
                        if (tr != null)
                            res.Add(tr);
                    }
                }
            }
            return res;
        }

        public IList<string> GetIdentifiersForPeriod(DateTime from, DateTime to)
        {
            var res = new List<string>();
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetIdentifiersStatemt, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        res.Add((string)reader["transactionId"]);
                    }
                }
            }
            return res;
        }

        private void CheckAndCreateSchema(bool recreateTable)
        {
            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            if (recreateTable)
            {
                using (var command = new Command(DeleteTableStatement, _sqliteConnection))
                {
                    command.ExecuteNonQuery();
                    _currentTransactionCount = 0;
                }
            }

            using (var command = new Command(CreateTableStatement, _sqliteConnection))
            {
                command.ExecuteNonQuery();
            }

            using (var command = new Command(CreateUpdateDateTimeIndexStatement, _sqliteConnection))
            {
                command.ExecuteNonQuery();
            }
        }

        // mark it internal only for testing purposes
        internal string Serialize(TransactionStatus tr)
        {
            if (tr == null)
                return String.Empty;

            using (var stream = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stream, _serializerSettings))
                {
                    _serializer.Serialize(xmlWriter, tr, _serializerNamespaces);
                    xmlWriter.Flush();
                    return stream.ToString();
                }
            }
        }

        private TransactionStatus Deserialize(string data)
        {
            //try
            //{
                using (var stream = new StringReader(data))
                {
                    return (TransactionStatus)_serializer.Deserialize(stream);
                }
            /*
            }
            catch (Exception e)
            {
                //Log.Error("Cannot deserialize an tr", e, this);
            }
            */
            //return null;
        }


        public TransactionStatus GetWithECRIDs(string cashRegisterId, string cashRegisterPaymentId)
        {
            if (String.IsNullOrEmpty(cashRegisterId) || String.IsNullOrEmpty(cashRegisterPaymentId))
                return null;

            if (GetCount()==0) return null;

            if (_sqliteConnection.State == System.Data.ConnectionState.Closed)
                _sqliteConnection.Open();

            using (var command = new Command(GetTransactionWithECRIDsStatement, _sqliteConnection))
            {
                command.Parameters.AddWithValue("@cashRegisterId", cashRegisterId);
                command.Parameters.AddWithValue("@cashRegisterPaymentId", cashRegisterPaymentId);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                        return Deserialize((string)reader["data"]);
                }
            }
            return null;
        }

        #region Command statements

        private const string CreateTableStatement =
            "CREATE TABLE if not exists [transactions] (" +
            "   [transactionId] string primary key ASC," +
            "   [CashRegisterId] string," +
            "   [CashRegisterPaymentId] string," +
            "   [updateDateTime] datetime," +
            "   [status] text, " +
            "   [data] text );";

        private const string CreateUpdateDateTimeIndexStatement =
            "CREATE INDEX if not exists [updateDateTime] " +
            "   on [transactions] ([updateDateTime]);";

        private const string DeleteTableStatement =
            "DROP TABLE if exists [transactions];";

        // mark it internal only for testing purposes
        internal const string InsertOrReplaceStatement =
            "INSERT OR REPLACE into [transactions] " +
            "   ([transactionId], [updateDateTime], [status], [CashRegisterId], [CashRegisterPaymentId], [data])" +
            "   values (@id, @udt, @status,  @cashRegiserId, @cashRegisterPaymentId, @data);";

        private const string GetTransactionStatement =
            "SELECT [data]" +
            "  from [transactions]" +
            "  where [transactionId] = @transactionId";

        private const string GetTransactionWithECRIDsStatement =
            "SELECT [data]" +
            "  from [transactions]" +
            "  where [CashRegisterId] = @cashRegisterId and [CashRegisterPaymentId] = @cashRegisterPaymentId";

        private const string GetLastStatusStatement =
            "SELECT [data]" +
            "  from [transactions]" +
            "  where [status] = @status" +
            "  tr by [updateDateTime] desc" +
            "  limit 1;";

        private const string DeleteByNumberStatement =
            "DELETE from [transactions]" +
            "  where [transactionId] in" +
            "    (select [transactionId] from [transactions] tr by [updateDateTime] asc limit @number);";

        private const string GetECRIdentifiersStatemt =
            "SELECT [data] " +
            "  from [transactions]";

        private const string GetIdentifiersStatemt =
            "SELECT [transactionId]" +
            "  from [transactions]" +
            "  where [updateDateTime] >= @from and [updateDateTime] <= @to" +
            "  tr by [updateDateTime] desc";

        private const string DeleteDateTimeStatement =
            "DELETE from [transactions] where [updateDateTime] < @dt";

        private const string ClearStatement =
            "DELETE from [transactions]";

        private const string GetFirstStatement =
            "SELECT [data] from [transactions] tr by [updateDateTime] asc";

        private const string GetLastStatement =
            "SELECT [data] from [transactions] tr by [updateDateTime] desc";

        private const string GetForPeriodStatement =
            "SELECT [data] from [transactions] where [updateDateTime] >= @from and [updateDateTime] <= @to;";

        private const string GetCountForPeriodStatement =
            "SELECT count(*) as [count] from [transactions] where [updateDateTime] >= @from and [updateDateTime] <= @to;";

        private const string GetCountStatement =
            "SELECT count(*) as [count] from [transactions]";

        private const string ContainsIdStatement =
            "SELECT [transactionId] from transactions where [transactionId]=@transactionId";

        #endregion
    }
}