﻿using System;
using System.Collections.Generic;
using WestPA.Standalone;

namespace OSHipPay.Core
{
    public interface ITransactionRepository
    {
        TransactionStatus AddOrUpdate(string orderId, TransactionType orderFunction, TransactionStatuses status, TransactionAmounts amounts,
            string cardName, string referenceNumber, ReceiptData receipt, bool verified, string cashier, string cashRegisterId, string cashRegisterPaymentId);

        TransactionStatus AddOrUpdate(TransactionStatus order);

        void SetVerified(string orderId);

        void Clear();

        bool ContainsId(string orderId);

        TransactionStatus GetTransaction(string orderId);

        TransactionStatus GetLastAccepted();

        long GetCount();

        long GetCountForPeriod(DateTime from, DateTime to);

        TransactionStatus GetFirst();
        TransactionStatus GetLast();
        IEnumerable<TransactionStatus> GetForPeriod(DateTime from, DateTime to);
        TransactionStatus GetWithECRIDs(string cashRegisterId, string cashRegisterPaymentId);

        IEnumerable<TransactionStatus> GetECRIdentifiers();


        IList<string> GetIdentifiersForPeriod(DateTime from, DateTime to);
    }
}
