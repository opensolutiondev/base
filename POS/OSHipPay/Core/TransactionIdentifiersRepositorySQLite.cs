﻿using System;
using Mono.Data.Sqlite;
using System.IO;
using System.Collections.Generic;
using OSHipPay.Logging;

namespace OSHipPay.Core
{
    public class TransactionIdentifiersRepositorySQLite : ITransactionIdentifiersRepository
    {
        private readonly string _connectionString;


        TransactionIdentifiersRepositorySQLite()
        {

        }

        public TransactionIdentifiersRepositorySQLite(string fileName)
        {
            Logging.Log.Debug("TransactionIdentifiersRepositorySQLite initializing " + fileName, new TransactionIdentifiersRepositorySQLite());
            if (String.IsNullOrEmpty(fileName))
                throw new ArgumentNullException("fileName");

            _connectionString = "Data Source=\"" + fileName + "\";Version=3;";

            try
            {
                CheckAndCreateSchema(false);
            }
            catch (Exception e)
            {
                Log.Error("Cannot initialize SQLite. File " + fileName + " will be removed.", e, this);

                try
                {
                    File.Delete(fileName);
                }
                catch { }
            }
        }

        public void Initialise()
        {
            Logging.Log.Debug("TransactionIdentifiersRepositorySQLite initialise() " , new TransactionIdentifiersRepositorySQLite());
            CheckAndCreateSchema(false);
        }
        public void Initialise(bool recreateTable)
        {
            Logging.Log.Debug("TransactionIdentifiersRepositorySQLite initialise(bool) ", new TransactionIdentifiersRepositorySQLite());
            CheckAndCreateSchema(recreateTable);
        }

        public long GetCount()
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                using (var command = new SqliteCommand(GetCountStatement, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt64(0);
                        }
                    }
                }
            }
            return 0;
        }

        public void Add(string transactionId)
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (var command = new SqliteCommand(InsertStatement, connection))
                    {
                        command.Parameters.AddWithValue("@tr", transactionId);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception x) { Log.Error(transactionId + " - " + x.ToString(), this); }
            }
        }

        public IList<string> GetIdentifiers()
        {
            var trs = new List<string>();

            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                using (var command = new SqliteCommand(GetAllStatement, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            trs.Add((string)reader["transactionId"]);
                        }
                    }
                }
            }

            return trs;
        }

        public void Clear()
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                using (var command = new SqliteCommand(ClearStatement, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private void CheckAndCreateSchema(bool recreateTable)
        {
            using (var connection = new SqliteConnection(_connectionString))
            {
                connection.Open();

                if (recreateTable)
                {
                    using (var command = new SqliteCommand(DeleteTableStatement, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }

                using (var command = new SqliteCommand(CreateTableStatement, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private const string CreateTableStatement =
            "CREATE TABLE if not exists [std_transactionids] (" +
            "   [transactionId] string primary key ASC);";

        private const string DeleteTableStatement =
            "DROP TABLE if exists [std_transactionids];";

        private const string InsertStatement =
            "INSERT into [std_transactionids] (transactionId) values (@tr);";

        private const string ClearStatement =
            "DELETE from [std_transactionids];";

        private const string GetAllStatement =
            "select [transactionId] from [std_transactionids];";

        private const string GetCountStatement =
            "select count(*) as [count] from [std_transactionids];";
    }
}
