﻿using System;

namespace OSHipPay.Core
{
    public static class EnumExtensions
    {
        public static string GetStatusCode(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            return !(Attribute.GetCustomAttribute(field, typeof(StatusCodeAttribute)) is StatusCodeAttribute attr) ? null : attr.StatusCode;
        }

        public static string GetStatusText(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            return !(Attribute.GetCustomAttribute(field, typeof(StatusTextAttribute)) is StatusTextAttribute attr) ? null : attr.StatusText;
        }
    }
}