﻿using System.Collections.Generic;

namespace OSHipPay.Core
{
    public interface ITransactionIdentifiersRepository
    {
        void Add(string orderId);
        long GetCount();
        IList<string> GetIdentifiers();
        void Clear();
    }
}
