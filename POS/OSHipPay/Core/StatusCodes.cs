﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSHipPay.Core
{
    class StatusCodeAttribute : Attribute
    {
        public string StatusCode;
        public StatusCodeAttribute(string statusCode) { StatusCode = statusCode; }
    }

    class StatusTextAttribute: Attribute
    {
        public string StatusText;
        public StatusTextAttribute(string statusText) { StatusText = statusText; }
    }

    public enum StatusCodes
    {
        [StatusCode("I00")]
        [StatusText("Showing idle screen")]
        Idle = -1,

        [StatusCode("S00")]
        [StatusText("Showing setup screen")]
        Setup = -2,

        [StatusCode("000")]
        [StatusText("Accepted/OK/No errors")]
        OK = 0,

        [StatusCode("001")]
        [StatusText("General failure")]
        GeneralFailure = 1,

        [StatusCode("002")]
        [StatusText("StartTransaction initialized")]
        StartTransaction = 2,

        [StatusCode("003")]
        [StatusText("TransactionResult finished")]
        TransactionResultFinished = 3,

        [StatusCode("004")]
        [StatusText("Waiting for final amount")]
        WaitingForFinalAmount = 4,

        [StatusCode("005")]
        [StatusText("Card read and accepted")]
        CardAccepted = 5,

        [StatusCode("006")]
        [StatusText("Card removed")]
        CardRemoved = 6,

        [StatusCode("007")]
        [StatusText("Card inserted")]
        CardInserted = 7,

        [StatusCode("E01")]
        [StatusText("Terminal Updating")]
        TerminalUpdating = 1001,

        [StatusCode("E02")]
        [StatusText("Unknown Error")]
        UnknownError = 1002,

        [StatusCode("E03")]
        [StatusText("Given IDs are not unique")]
        NotUniqueIDs = 4,

        [StatusCode("E05")]
        [StatusText("Unknown card")]
        CardUnknown = 1005,

        [StatusCode("E06")]
        [StatusText("Terminal busy")]
        NotIdle = 1006,

        [StatusCode("S01")]
        [StatusText("Saved settings")]
        SettingsSaved = 2001,

        [StatusCode("S02")]
        [StatusText("Failed to save settings")]
        SettingsFailed = 2002

    }

    public enum AbortStatusCodes
    {

        [StatusCode("A00")]
        [StatusText("Abort initializing")]
        Initializing = 0,

        [StatusCode("A01")]
        [StatusText("Abort accepted")]
        OK = 1,

        [StatusCode("A02")]
        [StatusText("Abort denied")]
        Denied = 2,

        [StatusCode("A03")]
        [StatusText("Incorrect parameters")]
        Incorrect = 3
    }

}