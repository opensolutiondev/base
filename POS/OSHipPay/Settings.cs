﻿using OSHipPay.Logging;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using WestPA.Standalone;

namespace OSHipPay
{
    /// <summary>
    /// Access to the settings used by the POS component.
    /// </summary>
    public class Settings
    {
        readonly IPaymentApp _app;

        SystemInfo Sysinfo { get; set; }

        /// <summary>
        /// The ip address of the DCAPP host
        /// </summary>
        public int LogTimeout
        {
            get => GetInt32("LogTimeout");
            set => SetInt32("LogTimeout", value);
        }
        /// <summary>
        /// The ip address of the DCAPP host
        /// </summary>
        public IPAddress DCAPPHost
        {
            get => GetIPAddress("DCAPPHost");
            set => SetIPAddress("DCAPPHost", value);
        }

        /// <summary>
        /// Disable decimals when requesting amounts? If true then the user cannot enter decimal values.
        /// </summary>
        public bool DisableDecimals
        {
            get => GetString("DisableDecimals").ToBoolean(false);
            set => SetString("DisableDecimals", value.ToString());
        }

        /// <summary>
        /// The ip address of the PPL host
        /// </summary>
        public string PPLAddress
        {
            get => GetString("PplHostIpAddress");
            set => SetString("PplHostIpAddress", value);
        }

        /// <summary>
        /// The port used on the PPL host
        /// </summary>
        public int PPLPort
        {
            get => GetInt32("PplHostPort");
            set => SetInt32("PplHostPort", value);
        }

        /// <summary>
        /// The selected interface language
        /// </summary>
        public string SelectedLanguage
        {
            get => GetString("SelectedLanguage");
            set => SetString("SelectedLanguage", value);
        }

        /// <summary>
        /// The IP address of the SPDH host
        /// </summary>
        public string SPDHAddress
        {
            get => GetString("SpdhHostIpAddress");
            set => SetString("SpdhHostIpAddress", value);
        }

        /// <summary>
        /// The IP address of the secondary SPDH host
        /// </summary>
        public string SPDHAddress2
        {
            get => GetString("SpdhHostIpAddress2");
            set => SetString("SpdhHostIpAddress2", value);
        }

        /// <summary>
        /// The port used on the SPDH host
        /// </summary>
        public int SPDHPort
        {
            get => GetInt32("SpdhHostIpPortNumber");
            set => SetInt32("SpdhHostIpPortNumber", value);
        }

        /// <summary>
        /// The port used on the secondary SPDH host
        /// </summary>
        public int SPDHPort2
        {
            get => GetInt32("SpdhHostIpPortNumber2");
            set => SetInt32("SpdhHostIpPortNumber2", value);
        }

        /// <summary>
        /// Terminal id
        /// </summary>
        public string TerminalId
        {
            get => GetString("ApplicationTerminalIdentifier");
            set => SetString("ApplicationTerminalIdentifier", value);
        }

        /// <summary>
        /// Tipping enabled or disabled from the POS component
        /// </summary>
        public bool Tipping
        {
            get => GetString("Tipping").ToBoolean(false);
            set => SetString("Tipping", value.ToString());
        }

        /// <summary>
        /// Whether or not the merchant's receipt is printed
        /// </summary>
        public bool DisableMerchantReceipt
        {
            get => GetBool("DisableMerchantReceipt", false);
            set => SetBool("DisableMerchantReceipt", value);
        }

        public bool DisableCardHolderReceipt
        {
            get => GetBool("DisableCardHolderReceipt", false);
            set => SetBool("DisableCardHolderReceipt", value);
        }

        

        public string APN
        {
            get => GetString("APN");
            set => SetString("APN", value);
        }

        public string WifiAddress
        {
            get => GetString("WifiAddress");
            set => SetString("WifiAddress", value);
        }
        /// <summary>
        /// WifiSSID
        /// </summary>
        public string WifiSSID
        {
            get => GetString("WifiSSID");
            set => SetString("WifiSSID", value);
        }

        /// <summary>
        /// WifiSSID
        /// </summary>
        public string WifiPassword
        {
            get => GetString("WifiPassword");
            set => SetString("WifiPassword", value);
        }

        /// <summary>
        /// WifiSSID
        /// </summary>
        public System.TimeSpan CloseBatchTime
        {
            get => GetDateTime("CloseBatchTime");
            set => SetDateTime("CloseBatchTime", value);
        }
        public string License
        {
            get => GetString("License");
            set => SetString("License", value);
        }
        public string Guid
        {
            get => GetString("Guid");
            set => SetString("Guid", value);
        }

        public bool FirstInitNotRun
        {
            get => GetBool("FirstInitNotRun", false);
            set => SetBool("FirstInitNotRun", value);
        }

        public string GetTimeZone
        {
            get => GetString("TimeZone");
            set => SetString("TimeZone", value);
        }

        public bool UseLumberJack
        {
            get => GetBool("UseLumberJack", true);
            set => SetBool("UseLumberJack", value);
        }

        /// <summary>
        /// The port used on the websocket connections
        /// </summary>
        public int WebSocketPort
        {
            get => GetInt32("WebSocketPort");
            set => SetInt32("WebSocketPort", value);
        }

        /// <summary>
        /// The port used if we are a StandAlone terminal
        /// </summary>
        public bool StandAlone
        {
            get => GetBool("StandAlone", false);
            set => SetBool("StandAlone", value);
        }

        /// <summary>
        /// The port used as name for terminal
        /// </summary>
        public string TerminalName
        {
            get => GetString("TerminalName");
            set => SetString("TerminalName", value);
        }

        /// <summary>
        /// The port used on the UDPPort connections
        /// </summary>
        public int UDPPort
        {
            get => GetInt32("UDPPort");
            set => SetInt32("UDPPort", value);
        }

        public long CurrentVersion
        {
            get => GetLong("CurrentVersion");
            set => SetLong("CurrentVersion", value);
        }

        public int LogLevel
        {
            get => GetInt32("LogLevel");
            set => SetInt32("LogLevel", value);
        }
        public bool NoPrinter
        {
            get => GetBool("NoPrinter", true);
            set => SetBool("NoPrinter", value);
        }
        public bool KeyBeepDefault
        {
            get => GetBool("KeyBeepDefault", true);
            set => SetBool("KeyBeepDefault", value);
        }

        /// <summary>
        /// The Logo used for terminal
        /// </summary>
        public string Logo
        {
            get => GetString("Logo");
            set => SetString("Logo", value);
        }

        public string LogFileEndPoint
        {
            get => GetString("LogFileEndPoint");
            set => SetString("LogFileEndPoint", value);
        }
        
        public int TransactionDaysLimit
        {
            get => GetInt32("TransactionDaysLimit");
            set => SetInt32("TransactionDaysLimit", value);
        }
        public int TransactionNumbersLimit
        {
            get => GetInt32("TransactionNumbersLimit");
            set => SetInt32("TransactionNumbersLimit", value);
        }

        public Settings(IPaymentApp app, SystemInfo sysinfo)
        {
            _app = app;
            if (LogTimeout < 1)
            {
                LogTimeout = 30000;
            }
            this.Sysinfo = sysinfo;
            Logging.Log.Init(this, sysinfo);
            Logging.Log.Debug("Initializing", new Settings());
            if (WebSocketPort < 0)
                WebSocketPort = 3333;
            if (UDPPort < 0)
                UDPPort = 3334;
            if (TerminalName == null || TerminalName.IsNotNullOrEmpty())
            {
                TerminalName = "Not set";
            }
#if DEBUG
            if (!FirstInitNotRun)
            {
                Logging.Log.Debug("Initializing - setting default settings as a onetime thing", new Settings());
                if (PPLAddress == null)
                {
                    PPLAddress = "185.27.171.42";
                    Logging.Log.Error("PPLAddress was null defaulting to " + PPLAddress, new Settings());
                }
                if (PPLPort < 1)
                {
                    PPLPort = 55133;
                    Logging.Log.Error("PPLPort was <1 defaulting to " + PPLPort, new Settings());
                }
                if (SPDHAddress.IsNullOrEmpty())
                {
                    SPDHAddress = "185.27.171.42";
                    Logging.Log.Error("SPDHAddress was null defaulting to " + SPDHAddress, new Settings());
                }
                if (SPDHPort < 1)
                {
                    SPDHPort = 55144;
                    Logging.Log.Error("SPDHPort was <1 defaulting to " + SPDHPort, new Settings());
                }
                if (TerminalId == null)
                {
                    TerminalId = "80000155";
                    Logging.Log.Error("Terminal was null defaulting to " + TerminalId, new Settings());
                }
                WebSocketPort = 3333;
                UDPPort = 3334;
                DisableCardHolderReceipt = true;
                DisableDecimals = true;
                FirstInitNotRun = true;
            }
#endif
            /*
            PropertyInfo[] properties = this.GetType().GetProperties();
            try
            {
                foreach (PropertyInfo pi in properties)
                {
                    Logging.Log.Debug(pi.Name + " " + pi.GetValue(this, null), new Settings());
                }
            } catch (Exception e)
            {
                Logging.Log.Error("Settings init exception ", e, new Settings());
            }
            */
            Logging.Log.Init(this, sysinfo);
            Logging.Log.Debug("Initialized", new Settings());
        }

        protected Settings() { /* empty constructor for mockups */ }

        int GetInt32(string key) => _app.GetRegistryValue(key, "").ToInt32(0);

        long GetLong(string key) => _app.GetRegistryValue(key, "").ToInt64(0);
        void SetLong(string key, long value) => _app.StoreRegistryValue(key, value.ToString());

        IPAddress GetIPAddress(string key)
        {
            var text = GetString(key);
            if (text.IsNotNullOrEmpty())
                try
                {
                    return IPAddress.Parse(text);
                }
                catch
                {
                    // ignore
                }
            return null;
        }

        protected System.TimeSpan GetDateTime(string key) {
            System.TimeSpan? dt = null;
            string s = _app.GetRegistryValue(key);
            if (s.IsNotNullOrEmpty())
            {
                dt = System.TimeSpan.Parse(s);
            }
            return dt ?? dt.Value;
        }
        protected virtual void SetDateTime(string key, System.TimeSpan value)
        {
            _app.StoreRegistryValue(key, value.ToString());
        }

        protected string GetString(string key) => _app.GetRegistryValue(key);

        void SetInt32(string key, int value) => _app.StoreRegistryValue(key, value.ToString());

        void SetIPAddress(string key, IPAddress value)
        {
            var text = value != null
                ? value.ToString()
                : "";
            SetString(key, text);
        }

        protected virtual void SetString(string key, string value)
        {
            _app.StoreRegistryValue(key, value);
        }

        /// <summary>
        /// Gets a boolean value from the registry.  Booleans are stored as strings and should
        /// be "true" or "false".
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="defaultValue">Default value if the key is not found</param>
        /// <returns>The interpreted value of the regisry data</returns>
        bool GetBool(string key, bool defaultValue) => GetString(key).ToBoolean(defaultValue);

        /// <summary>
        /// Sets a boolean value in the registry.  Booleans are stored as strings and should
        /// be "true" or "false".
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="value">Value to store</param>
        void SetBool(string key, bool value) => _app.StoreRegistryValue(key, value.ToString());

        internal bool IsPinPadMode()
        {
            return !StandAlone && WebSocketPort > 0;
        }


        internal string GetPOSComponentSettings()
        {
            StringBuilder stb = new StringBuilder();
            
            string path = Path.Combine(Sysinfo.PersistentDirectory, "../../LocalConfig/posconfig.d/");
            try
            {
                var files = System.IO.Directory.GetFiles(path);
                if (!files.Any())
                    return stb.ToString();

                foreach (var file in files)
                {
                    string fullFileName = Path.Combine(path, file);

                    using (var streamReader = new StreamReader(fullFileName))
                    {
                        string content = streamReader.ReadToEnd();

                        stb.Append(file + ": " + content+" \n");
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Log.VSlog(e.Message, Severity.Error);
            }
            return stb.ToString();
        }

        //Not allowed to change PA Settings...
        /*
        internal string GetPASettings()
        {
            StringBuilder stb = new StringBuilder();
            string path = Path.Combine(Sysinfo.PersistentDirectory, "../../LocalConfig/config.d/");
            try
            {
                var files = System.IO.Directory.GetFiles(path);
                if (!files.Any())
                    return stb.ToString();

                foreach (var file in files)
                {
                    string fullFileName = Path.Combine(path, file);

                    using (var streamReader = new StreamReader(fullFileName))
                    {
                        string content = streamReader.ReadToEnd();

                        stb.Append(file + ": " + content + " \n");
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Log.VSlog(e.Message, Severity.Error);
            }
            return stb.ToString();
        }

        internal void SavePASetting(string v, Object valueToSave)
        {
            String fileName = Path.Combine(Sysinfo.PersistentDirectory, "../../LocalConfig/config.d/") + v;
            String value = valueToSave.ToString() ;
            try
            {
                String savedData = "";
                if (new Java.IO.File(fileName).Exists())
                {
                    Java.IO.FileReader rdr = new Java.IO.FileReader(fileName);
                    char[] inputBuffer = new char[1024];//get Block size as buffer

                    int charRead = rdr.Read(inputBuffer);
                    for (int k = 0; k < charRead; k++)
                    {
                        savedData += inputBuffer[k];
                    }
                    rdr.Close();
                    new Java.IO.File(fileName).Delete();
                }
                if (!savedData.Equals(value))
                {
                    Java.IO.FileWriter write = new Java.IO.FileWriter(fileName);
                    write.Append(value);
                    write.Close();
                }
            }
            catch (Exception e)
            {
                Logging.Log.VSlog("Setting failed "+v+" " + e.Message, Logging.Severity.Error);
            }
        }
        */
    }
}
