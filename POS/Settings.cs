﻿using System.Net;
using WestPA.Standalone;

namespace POS.Example
{
    /// <summary>
    /// Access to the settings used by the POS component.
    /// </summary>
    public class Settings
    {
        readonly IPaymentApp _app;

        /// <summary>
        /// The ip address of the DCAPP host
        /// </summary>
        public IPAddress DCAPPHost
        {
            get => GetIPAddress("DCAPPHost");
            set => SetIPAddress("DCAPPHost", value);
        }

        /// <summary>
        /// Disable decimals when requesting amounts? If true then the user cannot enter decimal values.
        /// </summary>
        public bool DisableDecimals
        {
            get => GetString("DisableDecimals").ToBoolean(false);
            set => SetString("DisableDecimals", value.ToString());
        }

        /// <summary>
        /// The ip address of the PPL host
        /// </summary>
        public string PPLAddress
        {
            get => GetString("PplHostIpAddress");
            set => SetString("PplHostIpAddress", value);
        }

        /// <summary>
        /// The port used on the PPL host
        /// </summary>
        public int PPLPort
        {
            get => GetInt32("PplHostPort");
            set => SetInt32("PplHostPort", value);
        }

        /// <summary>
        /// The selected interface language
        /// </summary>
        public string SelectedLanguage
        {
            get => GetString("SelectedLanguage");
            set => SetString("SelectedLanguage", value);
        }

        /// <summary>
        /// The IP address of the SPDH host
        /// </summary>
        public string SPDHAddress
        {
            get => GetString("SpdhHostIpAddress");
            set => SetString("SpdhHostIpAddress", value);
        }

        /// <summary>
        /// The IP address of the secondary SPDH host
        /// </summary>
        public string SPDHAddress2
        {
            get => GetString("SpdhHostIpAddress2");
            set => SetString("SpdhHostIpAddress2", value);
        }

        /// <summary>
        /// The port used on the SPDH host
        /// </summary>
        public int SPDHPort
        {
            get => GetInt32("SpdhHostIpPortNumber");
            set => SetInt32("SpdhHostIpPortNumber", value);
        }

        /// <summary>
        /// The port used on the secondary SPDH host
        /// </summary>
        public int SPDHPort2
        {
            get => GetInt32("SpdhHostIpPortNumber2");
            set => SetInt32("SpdhHostIpPortNumber2", value);
        }

        /// <summary>
        /// Terminal id
        /// </summary>
        public string TerminalId
        {
            get => GetString("ApplicationTerminalIdentifier");
            set => SetString("ApplicationTerminalIdentifier", value);
        }

        /// <summary>
        /// Tipping enabled or disabled from the POS component
        /// </summary>
        public bool Tipping
        {
            get => GetString("Tipping").ToBoolean(false);
            set => SetString("Tipping", value.ToString());
        }

        /// <summary>
        /// Whether or not the merchant's receipt is printed
        /// </summary>
        public bool DisableMerchantReceipt
        {
            get => GetBool("DisableMerchantReceipt", false);
            set => SetBool("DisableMerchantReceipt", value);
        }

        public Settings(IPaymentApp app)
        {
            _app = app;
        }

        protected Settings() { /* empty constructor for mockups */ }

        int GetInt32(string key) => _app.GetRegistryValue(key, "").ToInt32(0);

        IPAddress GetIPAddress(string key)
        {
            var text = GetString(key);
            if (text.IsNotNullOrEmpty())
                try
                {
                    return IPAddress.Parse(text);
                }
                catch
                {
                    // ignore
                }
            return null;
        }

        protected string GetString(string key) => _app.GetRegistryValue(key);

        void SetInt32(string key, int value) => _app.StoreRegistryValue(key, value.ToString());

        void SetIPAddress(string key, IPAddress value)
        {
            var text = value != null
                ? value.ToString()
                : "";
            SetString(key, text);
        }

        protected virtual void SetString(string key, string value)
        {
            _app.StoreRegistryValue(key, value);
        }

        /// <summary>
        /// Gets a boolean value from the registry.  Booleans are stored as strings and should
        /// be "true" or "false".
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="defaultValue">Default value if the key is not found</param>
        /// <returns>The interpreted value of the regisry data</returns>
        bool GetBool(string key, bool defaultValue) => GetString(key).ToBoolean(defaultValue);

        /// <summary>
        /// Sets a boolean value in the registry.  Booleans are stored as strings and should
        /// be "true" or "false".
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="value">Value to store</param>
        void SetBool(string key, bool value) => _app.StoreRegistryValue(key, value.ToString());
    }
}
