﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace POS.Example
{
    static class Helpers
    {
        /// <summary>
        /// Get a value from a dictionary. If the key is not found in the dictionary then the defaultValue is returned.
        /// </summary>
        /// <typeparam name="TKey">The type of the keys in the dictionary</typeparam>
        /// <typeparam name="TValue">The type of the values in the dictionary</typeparam>
        /// <param name="dict">The dictionary</param>
        /// <param name="key">The key of the value to get.</param>
        /// <param name="defaultValue">The value to return if the key is not found.</param>
        /// <returns></returns>
        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key,
            TValue defaultValue)
        {
            TValue result;
            if (dict == null || !dict.TryGetValue(key, out result))
                result = defaultValue;
            return result;
        }

        /// <summary>
        /// Get a value from an array. If the request index is outside the bounds of the array,
        /// or the array is null, then the default value for type T is returned.
        /// </summary>
        /// <typeparam name="T">The type of items in the array.</typeparam>
        /// <param name="array">An array.</param>
        /// <param name="index">The index of requested item.</param>
        public static T GetValueOrDefault<T>(this T[] array, int index)
        {
            return array != null && array.Length > index
                       ? array[index]
                       : default(T);
        }

        /// <summary>
        /// Return the index of an item in an array.
        /// </summary>
        /// <typeparam name="T">Type type of objects in the array.</typeparam>
        /// <param name="array">The array to search in.</param>
        /// <param name="value">The value to search for.</param>
        /// <returns>The first index of the value or -1 if not found.</returns>
        public static int IndexOf<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++)
                if (Equals(array[i], value))
                    return i;
            return -1;
        }

        /// <summary>
        /// Join a sequence of objects together in a string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">A list of objects. Each object's ToString()-representation will be used.</param>
        /// <param name="separator">A string that will be inserted between each object in the result.</param>
        /// <returns></returns>
        public static string StringJoin<T>(this IEnumerable<T> source, string separator)
        {
            string s = "";
            var result = new StringBuilder();
            if (source != null)
                foreach (var item in source)
                {
                    result.Append(s);
                    result.Append(item);
                    s = separator;
                }
            return result.ToString();
        }

        /// <summary>
        /// Parse a value as a boolean. <seealso cref="bool.Parse"/>
        /// </summary>
        /// <param name="value">An object. Its string representation will be parsed to see if it's "True" or "False".</param>
        /// <param name="defaultValue">The value that should be returned if the parsing fails.</param>
        public static bool ToBoolean(this object value, bool defaultValue)
        {
            bool result = defaultValue;
            if (value != null)
            {
                try
                {
                    result = bool.Parse(value.ToString());
                }
                catch
                {
                    // Ignore parsing errors.
                }
            }
            return result;
        }

        /// <summary>
        /// Parse a string or object as an integer. Returns a default value if parsing fails.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(this object o, int defaultValue)
        {
            try
            {
                if (o != null)
                {
                    var s = o as string;
                    if (s == "")
                        return defaultValue;
                    if (s == null)
                        s = o.ToString();
                    return Int32.Parse(s, NumberStyles.Integer);
                }
            }
            catch
            {
                // ignore
            }

            return defaultValue;
        }

        /// <summary>
        /// Parse a string or object as an long integer. Returns a default value if parsing fails.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToInt64(this object o, long defaultValue)
        {
            try
            {
                if (o != null)
                {
                    var s = o as string;
                    if (s == "")
                        return defaultValue;
                    if (s == null)
                        s = o.ToString();
                    return Int64.Parse(s, NumberStyles.Integer);
                }
            }
            catch
            {
                // ignore
            }

            return defaultValue;
        }

        
        /// <summary>
        /// Parse a string or object as an unsigned integer. Returns a default value if parsing fails.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static uint ToUInt32(this object o, uint defaultValue)
        {
            try
            {
                if (o != null)
                {
                    var s = o as string;
                    if (s == "")
                        return defaultValue;
                    if (s == null)
                        s = o.ToString();
                    return UInt32.Parse(s, NumberStyles.Integer);
                }
            }
            catch
            {
                // ignore
            }

            return defaultValue;
        }
    }

    static class EnumerableHelpers
    {
        /// <summary>
        /// Append one or more items to a sequence.
        /// </summary>
        /// <typeparam name="T">The type of items in the sequence</typeparam>
        /// <param name="source">The sequence. Can be null.</param>
        /// <param name="items">The items that should be appended.</param>
        /// <returns></returns>
        public static IEnumerable<T> Append<T>(this IEnumerable<T> source, params T[] items)
        {
            if (source != null)
                foreach (var item in source)
                    yield return item;
            foreach (var item in items)
                yield return item;
        }
    }

    static class StringHelpers
    {
        /// <summary>
        /// String.Format as an extension method on strings.
        /// </summary>
        /// <param name="fmt"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Fmt(this string fmt, params object[] args)
        {
            return String.Format(fmt, args);
        }

        /// <summary>
        /// String.IsNullOrEmpty as an extension method on strings.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string s) { return String.IsNullOrEmpty(s); }

        /// <summary>
        /// !String.IsNullOrEmpty as an extension method on strings.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string s) { return !String.IsNullOrEmpty(s); }

        /// <summary>
        /// We can't enter decimal points in the terminal, so some parts of the address end up with leading zeroes
        /// if they are one or two digits.  Unfortunately these are parsed as octal in the IPAddress parse function,
        /// so we need to re-decimalise them by stripping the leading zeroes.
        /// </summary>
        /// <param name="original">The entered IP address</param>
        /// <returns>The IP address with leading zeroes removed</returns>
        public static string SanitiseIPAddress(this string original)
        {
            string result = string.Empty;

            try
            {
                string[] parts = original.Split('.');
                for (int i = 0; i < parts.Length; ++i)
                {
                    int part = int.Parse(parts[i]);
                    result += part.ToString();
                    if (i < (parts.Length - 1))
                    {
                        result += ".";
                    }
                }
            }
            catch (Exception)
            {
                result = "0.0.0.0";   // Failsafe
            }

            return result;
        }

        /// <summary>
        /// Parse a string as a timespan.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="fmt"></param>
        /// <returns></returns>
        public static TimeSpan? ToTimeSpan(this string s, string fmt)
        {
            if (fmt == null) throw new ArgumentNullException(nameof(fmt));

            if (s.IsNotNullOrEmpty())
            {
                try
                {
                    var d = DateTime.ParseExact(s, fmt, null);
                    return d - d.Date;
                }
                catch
                {
                    // ignore
                }
            }
            return null;
        }
    }
}
