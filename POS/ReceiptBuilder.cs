using System.Drawing;
using System.Globalization;
using WestPA.Standalone;

namespace POS.Example
{
    public class ReceiptBuilder
    {
        /// <summary>
        /// The culture specifies how amounts are formatted.
        /// </summary>
        readonly CultureInfo _culture;

        /// <summary>
        /// reference to the payment app, needed to start receipts and add text blocks to them
        /// </summary>
        readonly IPaymentApp _app;

        /// <summary>
        /// ReceiptBuilder constructor for default font type (variable width)
        /// </summary>
        /// <param name="appInstance">reference to the payment app, needed to start receipts and add text blocks to them</param>
        /// <param name="culture">The culture info to use for amount formatting.</param>
        public ReceiptBuilder(IPaymentApp appInstance, CultureInfo culture)
        {
            _culture = culture;
            _app = appInstance;
            _app.RcptStart();
        }

        /// <summary>
        /// Adds a string of text to the receipt
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Horizontal alignment on the receipt</param>
        /// <param name="incrementCursor">True if the cursor should be incremented, i.e. this line
        /// is now complete, false if more text will be added to this line</param>
        void AddString(string text, StringAlignment alignment, bool incrementCursor)
        {
            _app.RcptAddTextBlock(text, FontSelection.VariableWidth_Normal, 0, 0, alignment, incrementCursor);
        }

        /// <summary>
        /// Adds a piece of text aligned to left, middle or right of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Position of the text on the receipt</param>
        public void AddString(string text, StringAlignment alignment)
        {
            AddString(text, alignment, true);
        }

        /// <summary>
        /// Adds a piece of text on the near (left) side of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <returns>The number of vertical pixels that the cursor has moved on by.</returns>
        public void AddString(string text)
        {
            AddString(text, StringAlignment.Near, true);
        }

        /// <summary>
        /// Adds a piece of text on the near (left) side of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        public void AddStringNoIncrement(string text)
        {
            AddString(text, StringAlignment.Near, false);
        }

        /// <summary>
        /// Adds a piece of text aligned to left, middle or right of the receipt.
        /// The vertical cursor is not changed in this instance.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Position of the text on the receipt</param>
        public void AddStringNoIncrement(string text, StringAlignment alignment)
        {
            AddString(text, alignment, false);
        }

        /// <summary>
        /// Add a line with a label and an amount if the amount is non-zero.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="amount">The amount. It will be formatted with cents and according to culture.</param>
        public void AddPositiveAmount(string label, uint amount)
        {
            if (amount > 0)
            {
                AddAmount(label, amount);
            }
        }

        /// <summary>
        /// Add a line with a label and an amount.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="amount">The amount. It will be formatted with cents and according to culture.</param>
        public void AddAmount(string label, uint amount)
        {
            string sAmount = (amount / 100m).ToString("F2", _culture);
            AddStringNoIncrement(label + ":");
            AddString(sAmount, StringAlignment.Far);
        }
    
    }
}
