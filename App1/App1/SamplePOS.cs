﻿using Java.IO;
using System;
using WestPA.Standalone;

namespace WestPA.SamplePOS
{

    //This project is only needed for terminal that we want to reset POSComponentFilename from SamplePos to OSHipPay...
    public class SamplePOSComponent : IComponent
    {

        string IComponent.ComponentName => "Example POS";

        void IComponent.GetSpecificData(SpecificRequest required, out SpecificResponse response)
        {
            throw new NotImplementedException();
        }

        void IComponent.HandleReceiptData(ReceiptData item)
        {
            throw new NotImplementedException();
        }

        void IComponent.HandleUserInput(UserInputs item, char keyCode, int id)
        {
            throw new NotImplementedException();
        }

        public static void VSlog(string text, object category)
        {
            if (System.Diagnostics.Debugger.IsAttached &&
                !string.IsNullOrEmpty(text) &&
                category != null)
            {
                System.Diagnostics.Trace.WriteLine(text, category.GetType().ToString());
            }
        }


        bool IComponent.Initialise(IPaymentApp paymentApplication, SystemInfo sysinfo)
        {
            VSlog("Wrong settings in POSComponentFilename. Trying to rewrite them", new SamplePOSComponent());
            String conf = "/sdcard/West/BIT_OW/LocalConfig/config.d/POSComponentFilename";
            String component = "OSHipPay.PayApp, POS\n";
            try
            {
                String savedData = "";
                if (new File(conf).Exists()) { 
                    FileReader rdr = new FileReader(conf);
                    char[] inputBuffer = new char[1024];//get Block size as buffer
                    
                    int charRead = rdr.Read(inputBuffer);
                    for (int k = 0; k < charRead; k++)
                    {
                        savedData += inputBuffer[k];
                    }
                    rdr.Close();
                    new File(conf).Delete();
                }
                if (!savedData.Equals(component))
                {
                    FileWriter write = new FileWriter(conf);
                    write.Append(component);
                    write.Close();
                }

            }
            catch (Exception e)
            {
                VSlog("Failed to write the POSComponentFilename " + e.Message + " " + e.StackTrace, new SamplePOSComponent());
            }
            VSlog("Restarting...", new SamplePOSComponent());
            Java.Lang.JavaSystem.Exit(0);
            throw new Exception("Restart Terminal for changes to take effect");
        }

        void IComponent.RunPreSession(TerminalStatus status, out SessionData session)
        {
            throw new NotImplementedException();
        }

        void IComponent.RunPreTransaction(TerminalStatus status, out TransactionData transaction)
        {
            throw new NotImplementedException();
        }

        void IComponent.ShutdownPOS()
        {
            throw new NotImplementedException();
        }

        void IComponent.TransactionComplete(TransactionReport report)
        {
            throw new NotImplementedException();
        }

        public CardStatus VerifyCard(CardInformation card)
        {
            throw new NotImplementedException();
        }

        public void HandleBatchData(BatchData batch)
        {
            throw new NotImplementedException();
        }
    }
}