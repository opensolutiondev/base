﻿/* **********************************************************************************
 * @file
 *
 * @brief West International Payment Application POS Component interface
 *
 * This source file contains the definition of the POS component interface
 *
 */
/* **********************************************************************************
 *
 * This drawing/document is copyright and the property of West International.
 * It must not be copied (in whole or in part), used for manufacture or
 * otherwise disclosed without prior written consent. Any copies of this
 * drawing/document made by any method must also include a copy of this legend.
 * This document is supplied without liability for errors or omissions.
 *
 * (c) Copyright 2013, West International
 *
 ********************************************************************************** */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Drawing;
using System.Text;
using System.Globalization;

// Disable warnings about unused variables in classes
#pragma warning disable 169

// The namespace here is designed for portability, and does not reflect the module ownership of this code
namespace WestPA.Standalone
{
    #region User input and Display section

    /// <summary>
    /// Constants used to divide the values of
    /// the UserInputs enumeration into blocks for
    /// simpler processing.
    /// </summary>
    public static class UserInputBases
    {
        /// <summary>Digit keys</summary>
        public const int Digit = 0x10;

        /// <summary>Control keys, e.g. Enter, Tab, Cancel, Backspace</summary>
        public const int Control = 0x20;

        /// <summary>On-screen buttons</summary>
        public const int Button = 0x40;

        /// <summary>Letters derived from the keypad</summary>
        public const int Letter = 0x80;
    }

    /// <summary>
    /// Enumeration of all possible user inputs from the keypad and on-screen buttons.
    /// The values are derived from the UserInputBases constant members so that the
    /// processing of input events can be simplified.
    /// </summary>
    public enum UserInputs
    {
        /// <summary>Used only where an input can be specified but is not needed</summary>
        NotUsed = 0,

        // Digit keys
        /// <summary>Zero key</summary>
        Zero = UserInputBases.Digit + 0,
        /// <summary>One key</summary>
        One = UserInputBases.Digit + 1,
        /// <summary>Two key</summary>
        Two = UserInputBases.Digit + 2,
        /// <summary>Three key</summary>
        Three = UserInputBases.Digit + 3,
        /// <summary>Four key</summary>
        Four = UserInputBases.Digit + 4,
        /// <summary>Five key</summary>
        Five = UserInputBases.Digit + 5,
        /// <summary>Six key</summary>
        Six = UserInputBases.Digit + 6,
        /// <summary>Seven key</summary>
        Seven = UserInputBases.Digit + 7,
        /// <summary>Eight key</summary>
        Eight = UserInputBases.Digit + 8,
        /// <summary>Nine key</summary>
        Nine = UserInputBases.Digit + 9,
        /// <summary>Only used in specifying valid keys, indicates all digit keys should be included</summary>
        AllDigits = UserInputBases.Digit + 10,

        // Control keys
        /// <summary>Enter key</summary>
        Enter = UserInputBases.Control + 0,
        /// <summary>Backspace key</summary>
        Backspace = UserInputBases.Control + 1,
        /// <summary>Cancel key</summary>
        Cancel = UserInputBases.Control + 2,
        /// <summary>Tab key</summary>
        Tab = UserInputBases.Control + 3,
        /// <summary>Virtual question mark key sent instead of a digit
        /// key when the F1 key must be pressed</summary>
        QuestionMark = UserInputBases.Control + 4,
        /// <summary>Only used in specifying valid keys, indicates Enter, Backspace and Cancel should be included</summary>
        EnterBackspaceAndCancel = UserInputBases.Control + 5,

        // On-screen buttons / clickable items
        /// <summary>Left side on-screen button in fixed layout screen</summary>
        Button0 = UserInputBases.Button + 0,
        /// <summary>Right side on-screen button in fixed layout screen</summary>
        Button1 = UserInputBases.Button + 1,
        /// <summary>Clickable control in flexible layout</summary>
        SingleTap = UserInputBases.Button + 2,
        /// <summary>Swipe gesture</summary>
        Swipe = UserInputBases.Button + 3,

        // Letter keys derived from the keypad digits
        /// <summary>Indicates a letter key</summary>
        LetterKey = UserInputBases.Letter,
        /// <summary>Indicates a symbol</summary>
        Symbol = UserInputBases.Letter + 1
    }

    /// <summary>
    /// Encapsulates all the elements needed to display a full screen of text
    /// </summary>
    public class DisplayData
    {
        /// <summary>
        /// Enumeration of screen templates
        /// </summary>
        public enum DisplayTemplate
        {
            /// <summary>Four lines of text with no buttons</summary>
            FourLines,
            /// <summary>Three lines of text with two buttons on the bottom of the screen</summary>
            ThreeLinesWithButtons,
            /// <summary>Five lines of text with no buttons</summary>
            FiveLines,
            /// <summary>Four lines of text with two buttons on the bottom of the screen</summary>
            FourLinesWithButtons
        }

        /// <summary>
        /// Enumeration of text alignment options
        /// </summary>
        public enum Alignment
        {
            /// <summary>Align text to the left of the line</summary>
            Left = 0,
            /// <summary>Align text to the centre of the line</summary>
            Centre = 1,
            /// <summary>Align text to the right of the line</summary>
            Right = 2
        }

        /// <summary>
        /// Text size enumeration
        /// </summary>
        public enum Size
        {
            /// <summary>Extra-small text size</summary>
            ExtraSmall,
            /// <summary>Small text size</summary>
            Small,
            /// <summary>Regular text size</summary>
            Regular,
            /// <summary>Large text size</summary>
            Large
        }

        /// <summary>
        /// Button identity enumeration
        /// </summary>
        public enum ButtonId
        {
            /// <summary>
            /// Identifies the button on the left side of the screen
            /// </summary>
            LeftButton,
            /// <summary>
            /// Identifies the button on the right side of the screen
            /// </summary>
            RightButton
        }

        /// <summary>
        /// Encapsulates data required to display a clickable button on screen
        /// </summary>
        public class ButtonData
        {
            /// <summary>
            /// The background colour of the button.  Set to null to use the default background colour.
            /// </summary>
            public Color? BackgroundColour = null;

            /// <summary>
            /// The colour of the button text.  Set to null to use the default text colour.
            /// </summary>
            public Color? TextColour = null;

            /// <summary>
            /// The text of the button.  A button with no text defined will not be displayed.
            /// </summary>
            public string Text;

            /// <summary>
            /// Constructor with no colour specified
            /// </summary>
            /// <param name="text">The text of the button.  A button with no text defined will not be displayed.</param>
            public ButtonData(string text)
            {
                this.Text = text;
            }

            /// <summary>
            /// Constructor to set all data elements
            /// </summary>
            /// <param name="text">The text of the button.  A button with no text defined will not be displayed.</param>
            /// <param name="background">The background colour of the button.  Set to null to use the default background colour.</param>
            /// <param name="foreground">The colour of the button text.  Set to null to use the default text colour.</param>
            public ButtonData(string text, Color? background, Color? foreground)
                : this(text)
            {
                BackgroundColour = background;
                TextColour = foreground;
            }
        }

        /// <summary>
        /// Encapsulates all the data for a line of text on the display.
        /// </summary>
        public class TextLine
        {
            /// <summary>
            /// The alignment of this line of text
            /// </summary>
            public Alignment Alignment = Alignment.Centre;

            /// <summary>
            /// The size of this line of text.
            /// </summary>
            public Size Size = Size.Regular;

            /// <summary>
            /// The text to display
            /// </summary>
            public string Text;

            /// <summary>
            /// Constructor for all data elements
            /// </summary>
            /// <param name="text">Text to display in this line</param>
            /// <param name="alignment">Text alignment for this line</param>
            /// <param name="size">Text size for this line</param>
            public TextLine(string text, Alignment alignment, Size size)
                : this(text)
            {
                this.Alignment = alignment;
                this.Size = size;
            }

            /// <summary>
            /// Constructor for text with default alignment (centre) and size (regular)
            /// </summary>
            /// <param name="text">Text to display in this line</param>
            public TextLine(string text)
            {
                this.Text = text;
            }
        }

        /// <summary>
        /// The template used for this display definition
        /// </summary>
        public DisplayTemplate Template = DisplayTemplate.FiveLines;

        /// <summary>
        /// The lines of text for this display definition
        /// </summary>
        public List<TextLine> Lines = null;

        /// <summary>
        /// On-screen buttons to use for this display definition
        /// </summary>
        public Dictionary<ButtonId, ButtonData> Buttons = null;


        /// <summary>
        /// Constructor with no defined text lines and with default on-screen
        /// button setup.
        /// </summary>
        /// <param name="template">The display template to use</param>
        public DisplayData(DisplayTemplate template)
        {
            Buttons = null;

            this.Template = template;
        }

        /// <summary>
        /// Constructor taking set of text lines
        /// </summary>
        /// <param name="template">The display template to use</param>
        /// <param name="lines">The text lines to display</param>
        public DisplayData(DisplayTemplate template, List<TextLine> lines)
            : this(template)
        {
            this.Lines = lines;
        }

        /// <summary>
        /// Adds a text line to an existing display definition
        /// </summary>
        /// <param name="line">The line of text to add</param>
        public void AddLine(TextLine line)
        {
            if (Lines == null)
            {
                Lines = new List<TextLine>();
            }
            Lines.Add(line);
        }

        /// <summary>
        /// Adds a text line to an existing display definition
        /// </summary>
        /// <param name="text">The text to add</param>
        public void AddLine(string text)
        {
            if (text != null)
            {
                TextLine line = new TextLine(text);
                AddLine(line);
            }
        }

        /// <summary>
        /// Defines the contents of an on-screen button
        /// </summary>
        /// <param name="id">Button ID to define</param>
        /// <param name="contents">Button data to add</param>
        public void SetButton(ButtonId id, ButtonData contents)
        {
            if (Buttons == null)
            {
                Buttons = new Dictionary<ButtonId, ButtonData>();
            }
            else if (Buttons.ContainsKey(id))
            {
                Buttons.Remove(id);
            }

            Buttons.Add(id, contents);
        }

        /// <summary>
        /// Defines the contents of an on-screen button with text and default
        /// colours.
        /// </summary>
        /// <param name="id">Button ID to define</param>
        /// <param name="text">Button text</param>
        public void SetButton(ButtonId id, string text)
        {
            SetButton(id, new ButtonData(text));
        }
    }

    /// <summary>
    /// Encapsulates information about an image to be displayed on screen.
    /// </summary>
    public class ImageData
    {
        /// <summary>
        /// The image has to be stored as an embedded resource inside the same assembly
        /// that implemtents the <seealso cref="IComponent"/> interface.
        /// This field should contain the full name of the resource. That means that the
        /// name has to include both the namespace prefix of the assembly and the image path
        /// and filename.
        /// For example, if a project's default namespace is TermCo.POSComp and the images
        /// are stored in a directory called Images under the project's  root directory then
        /// the resource name for an image named 'Startup.jpg' from that directory would be
        /// "TermCo.POSComp.Images.Startup.jpg".
        /// </summary>
        public string ImageResourceName = string.Empty;
    }

    public class FlexibleDisplayElement
    {
        /// <summary>
        /// Text to display in this element
        /// </summary>
        public string Text;

        /// <summary>
        /// Text size in ems.  It is recommended to use one of the size constants here.
        /// </summary>
        public int SizeEms;

        /// <summary>
        /// Control ID.  This is returned in user input events when the control is clicked,
        /// and also is used to refer to the control if the properties have to be changed.
        /// </summary>
        public int ControlId;

        /// <summary>
        /// The row number that this element should appear on.  The flexible display mechanism
        /// can have multiple items on a row.
        /// </summary>
        public int Row;

        /// <summary>
        /// The percentage of the accessible display width that this element should occupy.
        /// A two-column menu, for example, would have each element set to occupy 50% of
        /// the available width, i.e. this property would be set to 50.
        /// </summary>
        public float WidthPercentage;

        /// <summary>
        /// Indicates if this element will generate a user input event back to the POS
        /// component if clicked.
        /// </summary>
        public bool IsClickable;

        /// <summary>
        /// Indicates if a border should be shown around the element.  This is useful for fields
        /// that are editable and for buttons, but less so for static text and clickable menu options.
        /// </summary>
        public bool ShowBorder = false;

        /// <summary>
        /// Indicates if a clickable element should show visible feedback that it has been clicked.
        /// The feedback is that the background colour of the element is darkened after clicking.  It is not changed
        /// back to the 'normal' background colour by the payment application - the expectation is that the display
        /// will change after clicking, so reverting to the normal colour is not required.
        /// </summary>
        public bool VisibleClick = false;

        /// <summary>
        /// Horizonal alignment of the text within the containing space.  Near = Left, Far = Right.
        /// </summary>
        public StringAlignment HorizontalAlignment = StringAlignment.Near;

        /// <summary>
        /// Vertical alignment of the text within the containing space.  Near = Top, Far = Bottom.
        /// </summary>
        public StringAlignment VerticalAlignment = StringAlignment.Center;

        /// <summary>
        /// Text style for this element
        /// </summary>
        public FontStyle Style = FontStyle.Regular;

        /// <summary>
        /// Text colour
        /// </summary>
        public Color TextColour = Color.Black;

        /// <summary>
        /// Fill colour of the containing space
        /// </summary>
        public Color BackgroundColour = Color.Transparent;

        /// <summary>
        /// Utility constants: full width of the containing space
        /// </summary>
        public const float WidthFull = 100f;

        /// <summary>
        /// Utility constants: half the width of the containing space
        /// </summary>
        public const float WidthHalf = 50f;

        /// <summary>
        /// Utility constants: one third of the width of the containing space
        /// </summary>
        public const float WidthThird = 33.3f;

        /// <summary>
        /// Utility constants: one quarter of the width of the containing space
        /// </summary>
        public const float WidthQuarter = 25f;


        /// <summary>
        /// Utility constants: extra small text size
        /// </summary>
        public const int SizeExtraSmall = 18;

        /// <summary>
        /// Utility constants: small text size
        /// </summary>
        public const int SizeSmall = 24;

        /// <summary>
        /// Utility constants: regular text size
        /// </summary>
        public const int SizeRegular = 30;

        /// <summary>
        /// Utility constants: large text size
        /// </summary>
        public const int SizeLarge = 38;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">Text to display</param>
        /// <param name="size">Size in ems.  It is recommended to use one of the size constants defined here.</param>
        /// <param name="rowNumber">Row number</param>
        /// <param name="widthPct">Width of the containing space as a percentage</param>
        /// <param name="clickable">Is this text clickable?</param>
        /// <param name="id">Control ID</param>
        public FlexibleDisplayElement(string text, int size, int rowNumber, float widthPct, bool clickable, int id)
        {
            Text = text;
            SizeEms = size;
            ControlId = id;
            Row = rowNumber;
            WidthPercentage = widthPct;
            IsClickable = clickable;
        }
    }

    /// <summary>
    /// Contains a list of FlexibleDisplayElements.
    /// </summary>
    public class FlexibleDisplay
    {
        /// <summary>
        /// Minimum number of rows in the display.  The POS component
        /// may wish to have a consistent appearance, e.g. every menu has five rows in it.  If,
        /// however, only two rows are populated then the flexible display will make those two
        /// rows fill the screen unless a minimum number of rows (5 in this example) is specified.
        /// </summary>
        public int MinRows;

        /// <summary>
        /// Specify the width and height of additional padding for the container element.  Leave
        /// null to use the system defaults.
        /// </summary>
        public Size? ContainerMargins = null;

        /// <summary>
        /// Specify the width and height of padding for the controls within the container.  Leave
        /// null to use the system defaults.
        /// </summary>
        public Size? ElementMargins = null;

        /// <summary>
        /// Elements of the display.  Elements on the same row are added from left to right, i.e.
        /// if two elements are added on the same row then the first element will appear to the
        /// left of the second.
        /// </summary>
        public List<FlexibleDisplayElement> Elements = new List<FlexibleDisplayElement>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="minimumRows">Minimum number of rows to display</param>
        public FlexibleDisplay(int minimumRows)
        {
            MinRows = minimumRows;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public FlexibleDisplay()
            : this(0)
        {
        }
    }

    #endregion

    #region Event notification and asynchronous operations

    /// <summary>
    /// This class defines the event arguments passed with the Notification event
    /// </summary>
    public class NotificationEventArgs : EventArgs
    {
        /// <summary>
        /// Enumeration of notification types that can be sent to the POS component during
        /// pre-session or pre-transaction processing.
        /// </summary>
        public enum NotificationType
        {
            /// <summary>The terminal is updating parameters as part of a scheduled or requested update</summary>
            TerminalBusyWithUpdates,

            /// <summary>The terminal has finished updating parameters and is ready to run transactions</summary>
            TerminalReady,

            /// <summary>The terminal has attempted to load parameters but the attempt failed</summary>
            ConfigurationLoadFailed,

            /// <summary>The terminal has attempted to update parameters but the attempt failed</summary>
            ConfigurationUpdateFailed,

            /// <summary>
            /// A card was inserted.
            /// </summary>
            CardInserted,

            /// <summary>
            /// A card was removed.
            /// </summary>
            CardRemoved,

            /// <summary>
            /// A transaction has been successfully posted online for authorisation.  Not that reversals do
            /// not trigger this notification.
            /// </summary>
            StoreAndForwardTransactionSent
        }

        /// <summary>
        /// The event being notified
        /// </summary>
        public NotificationType eventType { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="eventToNotify">The event being notified</param>
        public NotificationEventArgs(NotificationType eventToNotify)
        {
            eventType = eventToNotify;
        }

        /// <summary>
        /// Details of an online transaction
        /// </summary>
        public class OnlineTransactionDetails
        {
            /// <summary>
            /// The transactions reference number, supplied by the POS component when the transaction is created
            /// </summary>
            public string RetrievalReference;

            /// <summary>
            /// True if the transaction was accepted by the host, false if it was rejected or some other error occurred
            /// </summary>
            public bool Result;
        }

        /// <summary>
        /// When the event type is StoreAndForwardTransactionSent then the transaction details are
        /// given here.
        /// </summary>
        public OnlineTransactionDetails TransactionDetails = null;
    }

    /// <summary>
    /// Definition for delegate functions that handle notification events
    /// </summary>
    /// <param name="evt">NotificationEventsArgs event</param>
    public delegate void NotificationHandler(NotificationEventArgs evt);

    /// <summary>
    /// Definition for a delegate function to handle screen status events
    /// </summary>
    /// <param name="screenId">The screen ID, typically from the Cardholder and Operator Interface
    /// Specification (CHAOI).  Since CHAOI is inadequate for the full range of displays used,
    /// the values here will go beyond the displays in CHAOI.  The intention here is not to
    /// document a full range of screen IDs, but instead the POS component can look for significant
    /// screen IDs if it needs to communicate status to some other system.  For example, if screen
    /// ID 652 is given here then the POS component may wish to display "Enter PIN" on an attached
    /// cash register.</param>
    public delegate void ScreenStatusHandler(int screenId);

    /// <summary>
    /// Asynchronous operations that the POS component can attempt to run during the
    /// course of a transaction.
    /// </summary>
    public enum AsynchronousOperations
    {
        /// <summary>Request to abort the current transaction</summary>
        RequestTransactionAbort
    }

    #endregion

    #region Initialisation section


    /// <summary>
    /// Encapsulates information about the terminal and the POS component operating environment
    /// </summary>
    public class SystemInfo
    {
        /// <summary>
        /// The directory that should be used to store persistent data files.
        /// </summary>
        public string PersistentDirectory;

        /// <summary>
        /// The directory that should be used to store temporary data files.
        /// </summary>
        public string TemporaryDirectory;

        /// <summary>
        /// The payment application name and version.  This should be used for
        /// logging and diagnostic purposes, and should not be parsed.
        /// </summary>
        public string PaymentAppVersion;

        /// <summary>
        /// The terminal serial number
        /// </summary>
        public string SerialNumber;

        /// <summary>
        /// The POS component version number, taken from the DLL version information and
        /// formatted according to the payment application's versioning rules.
        /// </summary>
        public string ComponentVersion;
    }

    #endregion

    #region Session data section

    /// <summary>
    /// Encapsulates useful information on the terminal status
    /// </summary>
    public class TerminalStatus
    {
        /// <summary>
        /// The number of transactions currently in the store and forward queue.
        /// </summary>
        public int TransactionsInStoreAndForward = 0;

        /// <summary>
        /// Terminal configuration information from PPL files.  If this is null then no configuration
        /// has been loaded yet.
        /// </summary>
        public TerminalConfiguration TerminalConfig = null;

        /// <summary>
        /// A list of transaction features that are available in the terminal, based on current
        /// configuration.  If no configuration has been loaded then this list may be empty or null.
        /// </summary>
        public List<TransactionFeatures> AvailableFeatures = null;

        /// <summary>
        /// A list of installed languages codes from the ISO639-2 specification
        /// at http://en.wikipedia.org/wiki/List_of_ISO_639-2_codes.  Examples
        /// include "swe", "eng", etc.
        /// </summary>
        public List<string> LanguagesInstalled;

        /// <summary>
        /// Contains GSM / GPRS / 3G configuration data if available
        /// </summary>
        public GsmSettings GsmConfiguration = null;

        /// <summary>
        /// Current terminal configuration status regarding updates, application fallback,
        /// </summary>
        public ConfigurationStatus ConfigStatus = null;

        /// <summary>
        /// Possible logging levels.
        /// </summary>
        public enum LoggingLevels
        {
            Status = 0,
            Error = 1,
            Warning = 2,
            Debug = 3,
            Spam = 4
        }

        /// <summary>
        /// Active logging level in the terminal.  Normal choices are Debug for a development /
        /// troubleshooting situation or Warning for normal operation.
        /// </summary>
        public LoggingLevels LogLevel;

        /// <summary>
        /// Used by Interblocks. List of instalment periods. Used when performing an EPP purchase. Configured in registry. 
        /// </summary>
        public uint[] EPPInstalmentPeriods;

        /// <summary>
        /// Used by Interblocks. List of product groups. Used when performing an EPP purchase. 
        /// </summary>
        public IDictionary<string, string> ProductGroups;

    }

    /// <summary>
    /// Relevant parts of the terminal's configuration, taken from PPL configuration files
    /// </summary>
    public class TerminalConfiguration
    {
        /// <summary>
        /// Configured country code taken from the MASPAR configuration data.  This is the ISO 3166
        /// numeric representation. (See https://en.wikipedia.org/wiki/ISO_3166-1_numeric).  If zero
        /// then the MASPAR country code could not be parsed as a number.
        /// </summary>
        public int CountryCode;

        /// <summary>
        /// Merchant data, if available from configuration data.  This is taken from the MASPAR
        /// configuration file.
        /// </summary>
        public MerchantData Merchant = null;

        /// <summary>
        /// Operating currency of the terminal, derived from the MASPAR configuration file.
        /// </summary>
        public CurrencyDefinition OperatingCurrency;

        /// <summary>
        /// If multi currency is enabled, this list will contain the currencies that can be selected. 
        /// The selected currency's ISO value is set in TransactionOptions.SelectedCurrency
        /// </summary>
        public List<CurrencyDefinition> AvailableCurrencies; 

        /// <summary>
        /// Used by Interblocks. List of transaction types with descriptors, configured in PPL file, DCPAR.
        /// </summary>
        public IList<POSTransactionDescriptor> DebitCreditParameters_TransactionDescriptors;      

    }

    /// <summary>
    /// Parameters to control SPDH communications behaviour
    /// </summary>
    public class SPDHComms
    {
        /// <summary>
        /// If set to true, the payment application will attempt to process the store and forward queue
        /// at the end of an offline transaction, but only if online authorisation was not attempted.
        /// If online authorisation was attempted but failed then it is likely that the attempt to clear
        /// the store and forward queue will also fail.
        /// </summary>
        public bool SendStoreAndForwardImmediately = false;

        /// <summary>
        /// Defines the number of retry attempts that the payment application will make if there is a
        /// communications failure.  This setting only applies to authorisations performed by the terminal
        /// over a TCP/IP network connection.  The range here is from 0 to 2.  A value below 0 or above 2
        /// will be ignored.
        /// </summary>
        public int AuthorisationRetries = 0;

        /// <summary>
        /// When the terminal connects to the SPDH server is expects to see an ENQ control character.
        /// This timeout defines how long, in milliseconds, the payment application will wait for this ENQ.
        /// Values less than 1000 (i.e. 1 second) will be ignored.  The standard timeout for this is 40
        /// seconds, i.e. a value of 40000.
        /// </summary>
        public int ENQTimeoutMilliseconds = 0;

        /// <summary>
        /// The timeout for a socket connection to be made, in seconds.  Leave this at zero to use the system default.
        /// The valid range here is 1s to 120s.  This only applies to sockets in TCP/IP connections.
        /// Note that the system default is not necessarily the same for all network devices.
        /// </summary>
        public int SocketConnectTimeoutSeconds = 0;
    }

    /// <summary>
    /// Holds settings related to GSM / GPRS / 3G wireless communications
    /// </summary>
    public class GsmSettings
    {
        /// <summary>
        /// GPRS / 3G access point name
        /// </summary>
        public string APN = string.Empty;

        /// <summary>
        /// GPRS / 3G dial number
        /// </summary>
        public string DialNumber = string.Empty;

        /// <summary>
        /// Description of this APN
        /// </summary>
        public string Description = string.Empty;
    }

    /// <summary>
    /// Encapsulates the settings required to upload a file to a FTP server.
    /// </summary>
    public class FtpDestination
    {
        /// <summary>
        /// Destination FTP server
        /// </summary>
        public IPEndPoint Server;

        /// <summary>
        /// Login id
        /// </summary>
        public string User;

        /// <summary>
        /// Password
        /// </summary>
        public string Password;

        /// <summary>
        /// Directory where files should be copied.  This does not include the filename itself.
        /// </summary>
        public string Directory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="server">Destination server</param>
        /// <param name="user">Username for FTP login</param>
        /// <param name="password">Password for FTP login</param>
        /// <param name="directory">Directory on server</param>
        public FtpDestination(IPEndPoint server, string user, string password, string directory)
        {
            Server = server;
            User = user;
            Password = password;
            Directory = directory;
        }
    }

    /// <summary>
    /// Class for network adapter settings
    /// </summary>
    public class NetworkInterfaceAdapterSettings
    {
        /// <summary>Network adapter name. Can be left empty. The name is anyway set
        /// by the PA and depends on the hardware model.</summary>
        public string AdapterName;

        /// <summary>Adapter's IP address</summary>
        public IPAddress IPAddress;

        /// <summary>Gateway IP address</summary>
        public IPAddress GatewayAddress;

        /// <summary>Netmask for this adapter</summary>
        public IPAddress Netmask;

        /// <summary>Is DHCP enabled for this adapter</summary>
        public bool DHCPEnabled;
    }

    /// <summary>
    /// Holds the current configuration status in terms of non-financial file updates,
    /// known as XFA files.
    /// </summary>
    public class ConfigurationStatus
    {
        /// <summary>
        /// List of configuration files that have been either processed or have failed in some
        /// way, i.e. these are files that should not be downloaded again.  This is not a complete
        /// history of updates in the terminal, only the most recent files for each file type.
        /// </summary>
        public List<string> UsedConfigurationFiles;

        /// <summary>
        /// List of PPL parameter files that are currently in use in the terminal
        /// </summary>
        public List<string> ActiveConfigurationFiles;

        /// <summary>
        /// The current file prefix, either "0000" or "WSTA".  At time of writing this
        /// convention is based on the service provider.  Evry used "0000" and Swedbank use
        /// "WSTA".
        /// </summary>
        public string FilePrefix;

        /// <summary>
        /// Indicates if there is a previous / alternate version of the payment application
        /// available on the terminal.  If so the POS component can choose to abandon the
        /// current application and fall back to the previous / alternate version.  This will
        /// delete the current payment application's files, but settings will be retained.
        /// </summary>
        public bool ApplicationFallbackPossible;
    }

    /// <summary>
    /// Encapsulates a firewall port number and the purpose that the port is opened for.
    /// </summary>
    public class FirewallPort
    {
        /// <summary>
        /// Port number
        /// </summary>
        public ushort PortNumber;

        /// <summary>
        /// Purpose of this port, must be consistent to allow a port that was previously
        /// opened for the same purpose to be closed if the port number changes.
        /// </summary>
        public string Purpose;
    }

    /// <summary>
    /// Default values of parameters that relate to transactions
    /// </summary>
    public class TransactionDefaults
    {
        /// <summary>
        /// Warning threshold tip value as a percentage of the purchase value
        /// </summary>
        public long TipWarningThresholdPercentage = 30;

        /// <summary>
        /// Maximum tip value as a percentage of the purchase value
        /// </summary>
        public long TipMaximumPercentage = 100;

        /// <summary>
        /// Number of seconds that the payment application should keep the transaction
        /// outcome on screen after completion.  If the POS component will display the
        /// transaction results then this can be set at zero to minimise the delay.
        /// </summary>
        public int OutcomeDisplayTime = 3;

        /// <summary>
        /// Number of milliseconds the signature button must be pressed.
        /// </summary>
        public int SignatureButtonActivationTime = 0;
    }

    /// <summary>
    /// Encapsulates the data required to initialise a session
    /// </summary>
    public class SessionData
    {
        /// <summary>
        /// Terminal ID, or TSP ID.  This is the ID by which the terminal is known in
        /// respect of PPL and SPDH operations.  This may not be null or empty.
        /// </summary>
        public string TerminalId;

        /// <summary>
        /// Identifies the PPL server that the terminal should use.  This is required
        /// for the terminal's first startup since it must download configuration files
        /// from a PPL server.  The configuration files will contain the details of
        /// a PPL server to use so once the terminal has been configured this member
        /// can be null.
        /// </summary>
        public IPEndPoint PPLServer;

        /// <summary>
        /// Identifies an alternate PPL server that the terminal can use if the primary
        /// PPL server cannot be used.  This can be left null if no fallback server is
        /// available.  This will be ignored in the primary PPL server is not defined.
        /// NOTE:  The process of retrying a PPL download with the fallback server is only enabled
        /// if the terminal is configured to support retries.  This is a setting in the payment
        /// application that must be defined separately.
        /// </summary>
        public IPEndPoint FallbackPPLServer;

        /// <summary>
        /// Identifies a primary SPDH server that the terminal should use for authorisations, etc.
        /// If this is null then the terminal will use the SPDH server specified in the
        /// PPL configuration files.  If specified, the server here will take priority.
        /// If this is null then the SecondarySPDHServer member is ignored.
        /// </summary>
        public IPEndPoint PrimarySPDHServer;

        /// <summary>
        /// Identifies a secondary SPDH server that the terminal should use for authorisations, etc.
        /// Only specify this if SPDHServer is also specified (otherwise it will be ignored).
        /// If specified then this server till take priority over the PPL configuration
        /// (but not over SPDHServer).
        /// </summary>
        public IPEndPoint SecondarySPDHServer;

        /// <summary>
        /// Identifies a PPL server that is used to override the IP address specified in
        /// DCAPP control files.  This is used where the files referenced by DCAPP are
        /// stored in a different location to the DCAPP file itself, but where the 
        /// IP addresses in the DCAPP file are not necessarily valid. This can be left null
        /// if no alternative DCAPP server is required.
        /// </summary>
        public IPEndPoint DCAPPServer;

        /// <summary>
        /// Defines some parameters for SPDH server communications.  Some parameters are
        /// only relevant to specific communications methods.  See the documentation for the
        /// SPDHComms class for more details.
        /// </summary>
        public SPDHComms SPDHCommsSettings;

        /// <summary>
        /// This member can be used to set the date and time of the terminal.  If this is
        /// null then it will be ignored.  The terminal will, whether this is supplied or not,
        /// set its date and time from data in the SPDH login response.
        /// </summary>
        public DateTime? CurrentDateTime;

        /// <summary>
        /// Contains a list of firewall ports to open for inbound TCP/IP connections.  This can
        /// be left null if no firewall ports are needed.
        /// </summary>
        public List<FirewallPort> FirewallPorts;

        /// <summary>
        /// Contains the ISO 639-2 language code to use for this session.  This must be from the
        /// list of language codes supplied in the TerminalStatus data, otherwise the terminal
        /// will default to the default language according to configuration.  This can, therefore,
        /// be left empty or null to use the default option.
        /// </summary>
        public string LanguageCode;

        /// <summary>
        /// Controls whether the key beep is enabled by default when EnableKeypadInputs is called
        /// without the beep parameter.  Set to true to enable key beeps, or false to disable them.
        /// The same setting controls on-screen button beeps.
        /// </summary>
        public bool KeyBeepDefault = true;

        /// <summary>
        /// GSM / GPRS / 3G settings.  If this data is provided and the contents are not empty then
        /// it will be used to configure the 3G modem if available.  If the 3G modem is not available
        /// then this data will be ignored.
        /// </summary>
        public GsmSettings GsmConfiguration = null;

        /// <summary>
        /// A list of GSM / GPRS / 3G settings that will be written to the terminal configuration.
        /// This list will then be available for selection in the payment application.  This list
        /// can be left null if not required.  Note that this does not actively apply these settings
        /// to the terminal, they only make them available for selection in the payment application.
        /// </summary>
        public List<GsmSettings> StoredGsmConfigurations = null;

        /// <summary>
        /// Destination for log file uploads via the terminal's management menu.  This can be left null
        /// if there is no customised destination required.
        /// </summary>
        public FtpDestination LogfileUploadTarget = null;

        /// <summary>
        /// A set of default transaction settings.  These values can be overridden by the POS component.
        /// </summary>
        public TransactionDefaults TransactionSettings = new TransactionDefaults();

        /// <summary>
        /// Constructor for convenience
        /// </summary>
        /// <param name="tspId">This is stored in the TerminalId member</param>
        /// <param name="ppl">This is stored in the PPLServer member</param>
        /// <param name="spdhPrimary">This is stored in the SPDHServer member</param>
        /// <param name="spdhSecondary">This is stored in the SPDHServer member</param>
        /// <param name="dcapp">IP endpoint to use to retrieve DCAPP files</param>
        /// <param name="now">This is stored in the CurrentDateTime member</param>
        /// <param name="ports">This is stored in the FirewallPorts member</param>
        /// <param name="language">Language that the PA should use</param>
        public SessionData(string tspId, IPEndPoint ppl, IPEndPoint spdhPrimary, IPEndPoint spdhSecondary,
            IPEndPoint dcapp, DateTime? now, List<FirewallPort> ports, string language)
        {
            TerminalId = tspId;
            PPLServer = ppl;
            PrimarySPDHServer = spdhPrimary;
            SecondarySPDHServer = spdhSecondary;
            CurrentDateTime = now;
            FirewallPorts = ports;
            DCAPPServer = dcapp;
            LanguageCode = language;
        }
    }

    #endregion

    #region Transaction data section

    /// <summary>
    /// Enumeration of possible transaction types. Configured in PPL file, DCPAR.
    /// Used by Interblocks: EPP, PreAuth, PreAuthSettle, MKE
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// Purchase transaction
        /// </summary>
        Purchase,

        /// <summary>
        /// Refund transaction
        /// </summary>
        Refund,

        /// <summary>
        /// Reverse the last transaction
        /// </summary>
        Reversal,

        /// <summary>
        /// SPDH login transaction.
        /// </summary>
        Login,

        /// <summary>
        /// Cash advance transaction
        /// </summary>
        CashAdvance,

        /// <summary>
        /// Used by Interblocks. Easy Payment Plan. (Extended purchase transaction type)
        /// </summary>
        EPP,

        /// <summary>
        /// Used by Interblocks. Pre-Authorization. (Extended purchase transaction type)
        /// </summary>
        PreAuth,

        /// <summary>
        /// Used by Interblocks. Pre-Authorization Settlement (Sale Completion). (Extended purchase transaction type) 
        /// </summary>
        PreAuthSettle,

        /// <summary>
        /// Used by Interblocks. Manually Key Entered. (Extended purchase transaction type)
        /// </summary>
        MKE,

        /// <summary>
        /// Reversed Acquiring. Implements the merchant payment transaction, see WP-891.
        /// </summary>
        ReversedAcquiring, 

        None
    }

    /// <summary>
    /// Holds the amounts information needed for a transaction.  In all cases the amount
    /// is specified in the smallest denomination of the currency in use, i.e. a value
    /// of 590 would be SEK 5.90 if the terminal was using SEK.
    /// The totalAmount member includes cashback, VAT, gratuity and cash advance charge. Those other members
    /// should not be added to the total.
    /// Note that Cash Advance only uses the TotalAmount &amp; CashAdvanceChargeAmount properties.
    /// </summary>
    public class TransactionAmounts
    {
        /// <summary>
        /// The total amount of the transaction.  If there is an amount specified
        /// for cashback, VAT or gratuity then these are all components of this
        /// total amount.
        /// </summary>
        public uint TotalAmount = 0;

        /// <summary>
        /// The cashback component of the total amount.
        /// </summary>
        public uint CashbackAmount = 0;

        /// <summary>
        /// The VAT component of the total amount.
        /// </summary>
        public uint VatAmount = 0;

        /// <summary>
        /// The cash advance charge component of the total amount.
        /// </summary>
        public uint CashAdvanceChargeAmount = 0;

        /// <summary>
        /// The amount of any gratuity that the customer added.  This is only used when the
        /// payment application supplies receipt information to the POS component.  The
        /// payment application will ignore this field when the POS component supplies this
        /// data as part of a transaction.
        /// </summary>
        public uint GratuityAmount = 0;

        /// <summary>
        /// Three digit numeric ISO 4217 value of selected currency if other than the default currency. 
        /// Leave as zero to use default currency.
        /// </summary>
        public int SelectedCurrency = 0;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="total">This is stored in the TotalAmount member</param>
        /// <param name="cashback">This is stored in the CashbackAmount member</param>
        /// <param name="vat">This is stored in the VatAmount member</param>
        public TransactionAmounts(uint total, uint cashback, uint vat)
            : this(total, cashback, vat, 0, 0)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="total">This is stored in the TotalAmount member</param>
        /// <param name="cashback">This is stored in the CashbackAmount member</param>
        /// <param name="vat">This is stored in the VatAmount member</param>
        /// <param name="gratuity">This is stored in the GratuityAmount member</param>
        public TransactionAmounts(uint total, uint cashback, uint vat, uint gratuity)
            : this(total, cashback, vat, gratuity, 0)

        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="total">This is stored in the TotalAmount member</param>
        /// <param name="cashback">This is stored in the CashbackAmount member</param>
        /// <param name="vat">This is stored in the VatAmount member</param>
        /// <param name="gratuity">This is stored in the GratuityAmount member</param>
        /// <param name="charge">This is stored in the CashAdvanceCharge member</param>
        public TransactionAmounts(uint total, uint cashback, uint vat, uint gratuity, uint charge)
        {
            TotalAmount = total;
            CashbackAmount = cashback;
            VatAmount = vat;
            GratuityAmount = gratuity;
            CashAdvanceChargeAmount = charge;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="total">This is stored in the TotalAmount member</param>
        /// <param name="cashback">This is stored in the CashbackAmount member</param>
        /// <param name="vat">This is stored in the VatAmount member</param>
        /// <param name="gratuity">This is stored in the GratuityAmount member</param>
        /// <param name="charge">This is stored in the CashAdvanceCharge member</param>
        /// <param name="currency">Three digit numeric ISO 4217 value for the selected currency for the transaction.
        /// If the default currency is used, leave as zero.</param>
        public TransactionAmounts(uint total, uint cashback, uint vat, uint gratuity, uint charge, int currency)
        {
            TotalAmount = total;
            CashbackAmount = cashback;
            VatAmount = vat;
            GratuityAmount = gratuity;
            CashAdvanceChargeAmount = charge;
            SelectedCurrency = currency;
        }

        /// <summary>
        /// Private constructor to allow serialisation
        /// </summary>
        private TransactionAmounts()
        {
        }

        /// <summary>
        /// The net amount of the transaction, i.e. the total amount less the cashback, VAT,
        /// tip and cash advance charge
        /// </summary>
        public uint NetAmount
        {
            get
            {
                return TotalAmount - (CashbackAmount + VatAmount + GratuityAmount + CashAdvanceChargeAmount);
            }
        }

        /// <summary>
        /// The amount of the transaction with VAT included, i.e. the total amount less the cashback
        /// and tip.
        /// </summary>
        public uint AmountWithVat
        {
            get
            {
                return TotalAmount - (CashbackAmount + GratuityAmount);
            }
        }

        /// <summary>
        /// Add each of the amount fields.
        /// </summary>
        /// <param name="ta1"></param>
        /// <param name="ta2"></param>
        /// <returns>New object where each field is the sum.</returns>
        public static TransactionAmounts operator +(TransactionAmounts ta1, TransactionAmounts ta2)
        {
            return new TransactionAmounts(ta1.TotalAmount + ta2.TotalAmount,
                ta1.CashbackAmount + ta2.CashbackAmount,
                ta1.VatAmount + ta2.VatAmount, ta1.GratuityAmount + ta2.GratuityAmount,
                ta1.CashAdvanceChargeAmount + ta2.CashAdvanceChargeAmount);
        }
    }

    /// <summary>
    /// Enumeration of transaction features that can be disabled on a per-transaction basis.
    /// </summary>
    public enum TransactionFeatures
    {
        /// <summary>ChipXpress functionality to bypass PIN entry for small amounts</summary>
        ChipXpress,

        /// <summary>Tipping / gratuity functionality</summary>
        Tipping,

        /// <summary>Dynamic currency conversion</summary>
        DCC,

        /// <summary>PIN bypass, i.e. signature option at PIN entry. Signature authorisation may still be required in other areas.</summary>
        PinBypass,

        /// <summary>Contactless as method to read a card.</summary>
        Contactless
    }

    /// <summary>
    /// Tip customisation options
    /// </summary>
    public class TipCustomisation
    {
        /// <summary>
        /// The maximum tip amount that can be added to this transaction.  Leave at -1 for the
        /// default, which is allow up to 100% of the transaction value
        /// </summary>
        public long MaximumTip = -1;

        /// <summary>
        /// Available on-screen prompts for tip entry
        /// </summary>
        public enum Prompts
        {
            AmountPlusExtra,    // Shows "Amount + Extra" on screen for tip entry
            TotalAmount         // Shows "Total amount" on screen for tip entry
        }

        /// <summary>
        /// Defines the prompt to be shown for tip entry.
        /// </summary>
        public Prompts TipPrompt = Prompts.AmountPlusExtra;

        /// <summary>
        /// Set to true to allow tip entry to be bypassed by pressing the OK button with
        /// the "0.00" value still on screen.  Set to false to require that an amount is entered,
        /// even if that amount is the actual purchase amount.
        /// </summary>
        public bool AllowSkipOnOk = true;
    }

    /// <summary>
    /// Transaction options to be applied to a transaction.  Some options are dependent
    /// on context, e.g. disabling features on a reversal transaction is meaningless.  In
    /// such cases the options will be ignored.
    /// </summary>
    public class TransactionOptions
    {
        /// <summary>
        /// List of features that should be disabled for this transaction.  Not all features
        /// are relevant to all transactions, e.g. ChipXpress does not apply to a refund
        /// transaction, and combinations like that will be ignored.  A null value will be
        /// treated the same as an empty list, i.e. the normal set of transaction features
        /// will be available.
        /// </summary>
        public List<TransactionFeatures> DisabledFeatures = null;

        /// <summary>
        /// Tip customisation for this transaction.  Leave this null to use the default behaviour.
        /// </summary>
        public TipCustomisation TippingRules = null;

        /// <summary>
        /// Contains the ISO 639-2 language code to use for this transaction.  This must be from the
        /// list of language codes supplied in the TerminalStatus data, otherwise the terminal
        /// will default to the default language according to configuration.  This can, therefore,
        /// be left empty or null to use the default option.
        /// </summary>
        public string LanguageCode;

        /// <summary>
        /// Optional custom data sent in the authorisation request to the host. Maximum size is 200 ASCII characters.
        /// Can not contain any SPDH control characters (0x1C,0x1E, 0x1F, 0xFF) or any other character with value below 0x20. 
        /// The response to this data will be provided in ReceiptData.AuthorisationData.
        /// Leave this null to use default behavior.
        /// </summary>
        public string AuthorisationData;

        /// <summary>
        /// Three digit numeric ISO 4217 value of selected currency if other than the default currency. Leave as zero to use default currency.
        /// </summary>
        public int SelectedCurrency = 0;

    }

    /// <summary>
    /// Extended transaction types. Some of them are used by Interblocks. 
    /// Theese transaction types is handled as WestPA ordinary purchase transactions in WestPA. 
    /// Theese transaction types can have their unique configuration in PPL.
    /// Theese transaction types can have their unique SPDH data. 
    /// </summary>
    public enum ExtendedTransactionTypes
    {
        None,

        /// <summary>
        /// Used by Interblocks. Easy Payment Plan
        /// </summary>
        EPP,

        /// <summary>
        /// Used by Interblocks. Pre-Authorization
        /// </summary>
        PreAuth,

        /// <summary>
        /// Used by Interblocks. Pre-Authorization Settlement (Sale Completion)
        /// </summary>
        PreAuthSettle,

        /// <summary>
        /// Used by Interblocks. Manually Key Entered
        /// </summary>
        MKE,

        /// <summary>
        /// Reversed Acquiring. Implements the merchant payment transaction.
        /// </summary>
        ReversedAcquiring
    }

    /// <summary>
    /// Used by Interblocks. PPL configuration of transaction types.
    /// </summary>
    public class POSTransactionDescriptor
    {
        public TransactionType TransactionType { get; set; }
        public bool TransactionTypeAllowed { get; set; }
        public bool PasswordProtected { get; set; }
        public bool ProductGroupRequired { get; set; }
    }

    /// <summary>
    /// Optional extended data used by the payment application.
    /// </summary>
    public class TransactionExtensions
    {
        /// <summary>
        /// Extend a transaction type with a sub-transaction type. 
        /// </summary>
        public ExtendedTransactionTypes ExtendedTransactionType;

        /// <summary>
        /// Used by Interblocks. EPP instalment period. Selected when performing an EPP purchase.
        /// </summary>
        public uint EPPInstalmentPeriod;

        /// <summary>
        /// Used by Interblocks. Product Group. Selected for configured transaction types for categorization. 
        /// </summary>
        public string ProductGroup;

         /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="extendedTransactionType">Extended transaction type.</param>
        /// <param name="eppInstalmentPeriod">EPP instalment period.</param>
        /// <param name="productGroup">Product Group.</param>
        public TransactionExtensions(ExtendedTransactionTypes extendedTransactionType, uint eppInstalmentPeriod, string productGroup)
        {
            ExtendedTransactionType = extendedTransactionType;
            EPPInstalmentPeriod = eppInstalmentPeriod;
            ProductGroup = productGroup;
        }
    }

    /// <summary>
    /// The data required by the payment application to begin a transaction.
    /// </summary>
    public class TransactionData
    {
        /// <summary>
        /// The type of transaction to begin.  These may be financial transactions or
        /// administrative functions.
        /// </summary>
        public TransactionType Type;

        /// <summary>
        /// The transaction amounts, if known.  These are not used in the reversal
        /// case, only for payments and refunds.  If this parameter is null, or if
        /// the totalAmount member of this parameter is zero, then the payment
        /// application will require the amounts to be supplied at a later stage
        /// in the transaction.  This enables the POS component to interface to an
        /// external device, e.g. cash register, to obtain amount information.
        /// Note that the cashback amount is not used in a refund transaction.
        /// </summary>
        public TransactionAmounts Amounts;

        /// <summary>
        /// Where <c>Type</c> is 'Reversal' then the payment application will use the
        /// retrieval reference number to verify that the transaction being reversed is
        /// the correct one.  The retrieval reference number is supplied as part of the
        /// receipt information for a transaction.
        /// </summary>
        public string RetrievalReference;

        /// <summary>
        /// A set of transaction options that should apply to this transaction.  Leave null
        /// for the default behaviour controlled by configuration.  Note that some existing
        /// configuration options in the terminal may take preference over settings here.
        /// </summary>
        public TransactionOptions Options = null;

        /// <summary>
        /// Optional extended data used by the payment application. 
        /// </summary>
        public TransactionExtensions Extensions = null;
    }

    /// <summary>
    /// Holds calculated totals from a number of transactions per financial institution. 
    /// </summary>
    public class TransactionSummary
    {
        /// <summary>
        /// Financial institution, acquirer
        /// </summary>
        public string FinancialInstitution;

        /// <summary>
        /// Holds the calculated amounts
        /// </summary>
        public TransactionAmounts TransactionAmountsSummary;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="financialInstitution"> Name of acquirer</param>
        /// <param name="amounts">Calculated totals for a number of transactions</param>
        public TransactionSummary(string financialInstitution, TransactionAmounts amounts)
        {
            FinancialInstitution = financialInstitution;
            TransactionAmountsSummary = amounts;
        }

        /// <summary>
        /// Format each field to make a report.
        /// </summary>
        /// <returns>String where each field is written to a new row.</returns>
        public override string ToString()
        {
            //Mainly for internal debug purposes. No translation needed.
            string text = string.Format("{0}\nTotal: {1:0.##}\nCashback: {2:0.##}\nVAT: {3:0.##}\nGratuity: {4:0.##}\nCashAdvanceCharge: {5:0.##}\n",
                FinancialInstitution, (double)TransactionAmountsSummary.TotalAmount / 100,
                (double)TransactionAmountsSummary.CashbackAmount / 100,
                (double)TransactionAmountsSummary.VatAmount / 100,
                (double)TransactionAmountsSummary.GratuityAmount / 100,
                (double)TransactionAmountsSummary.CashAdvanceChargeAmount / 100);
            return text;
        }
    }
    #endregion

    #region Card validity checking section

    /// <summary>
    /// Enumeration of outcomes from the POS component card validity check
    /// </summary>
    public enum CardStatus
    {
        /// <summary>
        /// Card is acceptable to the POS component
        /// </summary>
        OK,

        /// <summary>
        /// Card is not acceptable to the POS component
        /// </summary>
        Reject,

        /// <summary>
        /// Card is not acceptable to the POS component and the payment
        /// application should end the transaction.
        /// </summary>
        RejectAndEndTransaction
    }

    /// <summary>
    /// Info from the POS component from card validation. Contains card status and optional alternative routing info.
    /// </summary>
    public class CardValidationResult
    {
        /// <summary>
        /// Information on the cards validity.
        /// </summary>
        public CardStatus CardStatus;

        /// <summary>
        /// Optional custom data sent in the authorisation request to the host. 
        /// NOTE: If this value is set, the value of AuthorisationData in TransactionOptions will be overwritten. 
        /// Maximum size is 200 ASCII characters.
        /// Can not contain any SPDH control characters (0x1C,0x1E, 0x1F, 0xFF) or any other character with value below 0x20. 
        /// The response to this data will be provided in ReceiptData.AuthorisationData.
        /// Leave this null to use default behavior.
        /// </summary>
        public string AuthorisationData;

        /// <summary>
        /// (Optional) If the card validated should be sent to a different host than usual,
        /// supply info here on host address and terminal id to use. 
        /// Leave as null if the normal host and terminal id should be used.
        /// </summary>
        public RoutingInfo AlternativeRoutingInfo = null;


        public CardValidationResult(CardStatus cardStatus)
        {
            CardStatus = cardStatus;
        }
    }

    /// <summary>
    /// Info needed to send the transaction to a different host than the usual.
    /// </summary>
    public class RoutingInfo
    {
        /// <summary>
        /// Terminal id used for requests sent to this host
        /// </summary>
        public string TerminalId;

        public IPEndPoint HostAddress;

        /// <summary>
        /// Create an instance of RoutingInfo
        /// </summary>
        /// <param name="tspId">Terminal id used for requests sent to this host. Stored in TerminalId</param>
        /// <param name="hostAddress">IP address to the host</param>
        public RoutingInfo(string tspId, IPEndPoint hostAddress)
        {
            TerminalId = tspId;
            HostAddress = hostAddress;
        }
    }


    /// <summary>
    /// Encapsulates information about a card that has been presented to the payment application.
    /// </summary>
    public class CardInformation
    {
        /// <summary>
        /// Enumeration of different card types supported by the payment application
        /// </summary>
        public enum CardType
        {
            /// <summary>
            /// The card is a pure payment card
            /// </summary>
            Payment,

            /// <summary>
            /// The card is a pure bonus card
            /// </summary>
            Bonus,

            /// <summary>
            /// The card is configured as a payment card and a bonus card
            /// </summary>
            Combined,

            /// <summary>
            /// The card is not configured as a payment card or as a bonus card.
            /// </summary>
            OtherNonPCI
        }

        /// <summary>
        /// Enumeration of different possible card presentation methods
        /// </summary>
        public enum EntryMethod
        {
            /// <summary>
            /// Card was inserted into the smartcard reader
            /// </summary>
            ChipInserted = 'C',

            /// <summary>
            /// The card was swiped in magnetic card reader
            /// </summary>
            Swiped = 'D',

            /// <summary>
            /// The card data was entered manually on the terminal keypad
            /// </summary>
            Manual = 'T',

            /// <summary>
            /// Card was tapped and read by the rfid reader.
            /// </summary>
            /// <remarks>
            /// As defined by Swedbank bulletin Contactless 1.0 revision 4, req M202392
            /// </remarks>
            Contactless = 'K',
        }

        /// <summary>
        /// Indicates the type of card that has been presented.
        /// </summary>
        public CardType PresentedCardType;

        /// <summary>
        /// Indicates the method by which the card has been presented.
        /// </summary>
        public EntryMethod CardEntryMethod;

        /// <summary>
        /// The number of the presented card.  For payment cards this is
        /// obscured with asterisks except for the first 6 and last 4 digits.
        /// For cards that are not payment cards then this is the full card number.
        /// </summary>
        public string CardNumber;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="type">This is copied to the PresentedCardType member</param>
        /// <param name="method">This is copied to the CardEntryMethod member</param>
        /// <param name="number">This is copied to the CardNumber method</param>
        public CardInformation(CardType type, EntryMethod method, string number)
        {
            PresentedCardType = type;
            CardEntryMethod = method;
            CardNumber = number;
        }

    }
    #endregion

    #region Receipt data section

    /// <summary>
    /// Cardholder verification method, encoded as per CHAOI 8.1.3
    /// </summary>
    public enum VerificationMethod
    {
        /// <summary>Cardholder was verified by signature</summary>
        Signature = '@',

        /// <summary>Cardholder was verified by online PIN</summary>
        PINOnline = 'A',

        /// <summary>Cardholder was verified by offline PIN</summary>
        PINOffline = 'a',

        /// <summary>No cardholder verification was performed (chip only)</summary>
        NoVerification = '-',

        /// <summary>The specific 'No CVM' rule was performed (chip only)</summary>
        NoCVM = '/',

        /// <summary>Cardholder verification failed. Chip only.</summary>
        CVMFailed = ' ',

        /// <summary>A combined CVM rule has been performed (signature and offline PIN). Chip only.</summary>
        Combined = 'b',

        /// <summary>
        /// Cardholder was verified by a consumer device, e.g. a mobile phone.
        /// NOTE: the actual verification method printed on the receipt should
        /// be 'a' for PINOffline, according to Swedbank rules.
        /// Also, according to Visa, there must be a text saying "VERIFIED BY DEVICE".
        /// </summary>
        /// <remarks>
        /// Note that the value '%' was chosen arbitrarily, it's not used in any
        /// specification.
        /// See Swedbank Contactless Bulletin 1.0 R4, requirement M901042. 
        /// See payWave Terminal Test Procedures, v3.3.1.
        /// </remarks>
        ConsumerDevice = '%',
    }

    /// <summary>
    /// Authorisation method that was used, as per CHAOI 8.1.4
    /// </summary>
    public enum AuthorisationMethod
    {
        /// <summary>Transaction was authorised online</summary>
        Online = '1',

        /// <summary>Transaction was authorised offline</summary>
        Offline = '2',

        /// <summary>Transaction was authorised by phone call</summary>
        Phone = '4',

        /// <summary>Transaction was not authorised</summary>
        None = '5',

        /// <summary>Transaction was declined before authorisation was attempted</summary>
        DeclinedBeforeAuth = '-'
    }

    /// <summary>
    /// Authorising entity, as per CHAOI 8.1.5
    /// </summary>
    public enum AuthorisingEntity
    {
        /// <summary>Authorised by the DPC</summary>
        DPC0 = '0',

        /// <summary>Authorised locally in the terminal</summary>
        LocalDevice = '1',

        /// <summary>Authorised by the DPC</summary>
        DPC3 = '3',

        /// <summary>Authorised by the DPC</summary>
        DPC4 = '4',

        /// <summary>Authorised by the card issuer</summary>
        CardIssuer = '5',

        /// <summary>Interchange interface process</summary>
        InterchangeInterface = '6',

        /// <summary>Interchange (VISA or EPSS)</summary>
        InterchangeVisaEpss = '7',

        /// <summary>Authorised by merchant (acquiring) host</summary>
        Merchant = '9',

        /// <summary>Approval code was manually entered</summary>
        ManualEntry = 'M',

        /// <summary>Transaction was declined before authorisation was attempted</summary>
        DeclinedBeforeAuth = '-'
    }

    /// <summary>
    /// A list of possible transaction results.  This gives the POS component information on
    /// why a transaction did not complete successfully.  A successful transaction will use the
    /// 'Success' value.
    /// </summary>
    public enum TransactionResult
    {
        /// <summary>The transaction was successful</summary>
        Success,

        /// <summary>The transaction was declined by the authorisation host</summary>
        TransactionDeclined,

        /// <summary>The transaction failed but not specific reason was available</summary>
        UnspecifiedFailure,

        /// <summary>The transaction was cancelled</summary>
        TransactionCancelled,

        /// <summary>The transaction could not be run because the store and forward
        /// queue was full and could not be emptied</summary>
        StoreAndForwardQueueFull,

        /// <summary>The terminal must log in to the authorisation host before
        /// it can run a transaction</summary>
        SPDHLoginRequired,

        /// <summary>The parameters supplied for the transaction to begin were
        /// not correct</summary>
        InvalidTransactionParameters,

        /// <summary>The card used in the transaction was invalid or was
        /// rejected by the authorisation host</summary>
        InvalidCard,

        /// <summary>The transaction had to be authorised online
        /// but the attempt to go online failed.</summary>
        OnlineAuthorisationAttemptFailed,

        /// <summary>The application on a chip card is blocked</summary>
        ApplicationBlocked,

        /// <summary>The chip card is blocked</summary>
        CardBlocked
    }

    /// <summary>
    /// The different contactless card schemes supported by the payment application.
    /// </summary>
    public enum ContactlessCardSchemes
    {
        /// <summary>
        /// The transaction did not use a contactless card.
        /// </summary>
        None = 0,

        /// <summary>
        /// The transaction used a contactless card, but we don't know what kind.
        /// </summary>
        /// <remarks>
        /// This will probably never occurr but we should be prepared for the eventuality.
        /// </remarks>
        ContactlessOther = 1,

        /// <summary>
        /// The transaction used a contactless Visa card.
        /// </summary>
        ContactlessVisa = 2,

        /// <summary>
        /// The transaction used a contactless Mastercard card.
        /// </summary>
        ContactlessMastercard = 3,
    }

    /// <summary>
    /// Encapsulates EMV data required for receipt printing
    /// </summary>
    public class EMVElements
    {
        /// <summary>Application ID</summary>
        public byte[] AID;

        /// <summary>Terminal verification results</summary>
        public byte[] TVR;

        /// <summary>Transaction status indicator</summary>
        public byte[] TSI;

        /// <summary>EMV authorisation response code, from tag 8A.  Only valid for EMV transactions that
        /// reached the authorisation phase.  May be used on receipts in place of the host's authorisation
        /// response code when the transaction did not go online.</summary>
        public string EmvAuthorisationResponseCode;

        /// <summary>
        /// Constructor for all elements
        /// </summary>
        /// <param name="applicationId">Application ID</param>
        /// <param name="verificationResults">Terminal verification results</param>
        /// <param name="transactionStatus">Transaction status indicator</param>
        /// <param name="authResponseCode">EMV authorisation response code</param>
        public EMVElements(byte[] applicationId, byte[] verificationResults, byte[] transactionStatus, string authResponseCode)
        {
            AID = applicationId;
            TVR = verificationResults;
            TSI = transactionStatus;
            EmvAuthorisationResponseCode = authResponseCode;
        }

        /// <summary>
        /// Private constructor to allow serialisation
        /// </summary>
        private EMVElements()
        {
        }

        /// <summary>
        /// Generates a string representation of a byte array.  This is useful
        /// for receipts and logging.
        /// </summary>
        /// <param name="element">The array to convert to a string representation</param>
        public static string StringOf(byte[] element)
        {
            string result = null;
            if (element != null)
            {
                var sb = new StringBuilder(element.Length*2);
                for (int i = 0; i < element.Length; i++)
                {
                    sb.Append(element[i].ToString("X2"));
                }
                result = sb.ToString();
            }
            return result ?? String.Empty;
        }
    }

    /// <summary>
    /// Encapsulates merchant data for receipt printing
    /// </summary>
    public class MerchantData
    {
        /// <summary>
        /// Merchant name
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Merchant address
        /// </summary>
        public string Address = string.Empty;

        /// <summary>
        /// Merchant city
        /// </summary>
        public string City = string.Empty;

        /// <summary>
        /// Merchant ZIP code
        /// </summary>
        public string ZipCode = string.Empty;

        /// <summary>
        /// Merchant phone number
        /// </summary>
        public string PhoneNumber = string.Empty;

        /// <summary>
        /// Merchant organisation number
        /// </summary>
        public string OrganisationNumber = string.Empty;

        /// <summary>
        /// Merchant category code
        /// </summary>
        public string CategoryCode = string.Empty;

        /// <summary>
        /// Bank agent name
        /// </summary>
        public string BankAgentName = string.Empty;
    }

    /// <summary>
    /// Receipt data related to Dynamic Currency Conversion
    /// </summary>
    public class DCCData
    {
        /// <summary>
        /// Three-letter ISO currency code
        /// </summary>
        public string CurrencyCode;

        /// <summary>
        /// The converted amount in sub-units.  It is not safe to assume that
        /// the converted currency uses two decimal places.
        /// </summary>
        public long ConvertedAmount;

        /// <summary>
        /// The converted tip amount in sub-units.  It is not safe to assume that
        /// the converted currency uses two decimal places.
        /// </summary>        
        public long ConvertedExtra;

        /// <summary>
        /// Number of decimal places used by the converted currency
        /// </summary>
        public int Exponent;

        /// <summary>
        /// Exchange rate used in the conversion
        /// </summary>
        public decimal ExchangeRate;

        /// <summary>
        /// The currency service provider
        /// </summary>
        public string RateProvider;

        /// <summary>
        /// Exchange rate source
        /// </summary>
        public string Source;

        /// <summary>
        /// Date and time of the conversion
        /// </summary>
        public DateTime TimeStamp;

        /// <summary>
        /// The markup formatted as a string with percentage symbol
        /// </summary>
        public string MarkupFormatted;

        /// <summary>
        /// Card scheme.  "V" = Visa, "M" = Mastercard.
        /// </summary>
        public char CardScheme;

        /// <summary>
        /// Disclaimer to print on a merchant's DCC receipt, may not be supplied
        /// </summary>
        public string MerchantDisclaimer;

        /// <summary>
        /// Disclaimer to print on a cardholder's DCC receipt, may not be supplied
        /// </summary>
        public string CardholderDisclaimer;
    }

    /// <summary>
    /// Encapsulates the data required for receipt printing
    /// </summary>
    public class ReceiptData
    {
        /// <summary>
        /// Indicates the Debit / Credit selection for the transaction
        /// </summary>
        public enum DebitCreditSelection
        {
            /// <summary>Debit was selected</summary>
            Debit,

            /// <summary>Credit was selected</summary>
            Credit,

            /// <summary>The BINPAR for the card did not require a Debit / Credit selection to be made</summary>
            NoSelection
        }

        /// <summary>
        /// Provides essential details of the transaction for receipt purposes
        /// </summary>
        public struct TransactionInfo
        {
            /// <summary>
            /// Type of transaction
            /// </summary>
            public TransactionType TransactionType;

            /// <summary>
            /// Payment card number, obscured with '*' characters
            /// </summary>
            public string PaymentCardNumber;

            /// <summary>
            /// Payment code, can be null or empty if no code was involved
            /// </summary>
            public string PaymentCode;

            /// <summary>
            /// Card entry method for the transaction
            /// </summary>
            public CardInformation.EntryMethod CardEntryMethod;

            /// <summary>
            /// The card's PAN sequence number.  Valid values are greater than zero.
            /// </summary>
            public byte PanSequenceNumber;
        }

        /// <summary>Transaction information</summary>
        public TransactionInfo Transaction;

        /// <summary>Transaction outcome</summary>
        public bool Approved;

        /// <summary>Approval code for authorised transactions</summary>
        public string ApprovalCode;

        /// <summary>Financial amounts used in this transaction</summary>
        public TransactionAmounts AuthorisedAmounts;

        /// <summary>Verification method used in this transaction</summary>
        public VerificationMethod Verification;

        /// <summary>Authorisation method used in this transaction</summary>
        public AuthorisationMethod Authorisation;

        /// <summary>Authorising entity for this transaction</summary>
        public AuthorisingEntity AuthEntity;

        /// <summary>Authorisation response code for this transaction</summary>
        public string AuthorisationResponseCode;

        /// <summary>Transaction reference</summary>
        public string ReferenceNumber;

        /// <summary>Financial institution</summary>
        public string FinancialInstitution;

        /// <summary>Reason for denial if transaction was not approved</summary>
        public string DenialText;

        /// <summary>EMV data elements in this transaction</summary>
        public EMVElements EMV;

        /// <summary>Payment card type</summary>
        public string CardType;

        /// <summary>The date and time of the transaction</summary>
        public DateTime Timestamp;

        /// <summary>Three letter currency code, e.g. SEK, EUR, NOK</summary>
        public string CurrencyCode;

        /// <summary>Details of the merchant</summary>
        public MerchantData MerchantInformation;

        /// <summary>Acquirer reference number</summary>
        public string AcquirerReference;

        /// <summary>Debit / Credit selection details</summary>
        public DebitCreditSelection DebitCredit;

        /// <summary>Indicates if this is a receipt for a reversal</summary>
        public bool IsReversal = false;

        /// <summary>Cardholder name, may not be available for all cards</summary>
        public string CardholderName;

        /// <summary>The card expiry date.</summary>
        public string ExpiryDate;    

        /// <summary>DCC transaction data if applicable.  Null if not used.</summary>
        public DCCData DCC;

        /// <summary>Used by Interblocks. EPP instalment period. Null if not used.</summary>
        public uint EPPInstalmentPeriod;

        /// <summary>
        /// Indicates the type of contactless card used in the transaction.
        /// Defaults to <see cref="ContactlessCardSchemes.None"/> is the transaction
        /// did not use a contactless card.
        /// </summary>
        /// <remarks>
        /// There are specifications that the receipt must clearly indicate when the
        /// transaction was made with a contactless card.
        /// Visa requires that the receipt contains the text "Visa contactless" if
        /// the transaction was made using a Visa contactless card.
        /// Swedbank requires the text "Contactless" if the transaction was made using
        /// any contactless card.
        /// </remarks>
        public ContactlessCardSchemes ContactlessScheme = ContactlessCardSchemes.None;

        /// <summary>
        /// Custom authorisation data sent by the host in response to data sent in 
        /// TransactionOptions.AuthorisationData. 
        /// Will have a maximum length of 200 ASCII characters.
        /// </summary>
        public string AuthorisationData;
    }

    /// <summary>
    /// Class for building a receipt
    /// </summary>
    public class ReceiptBuilder
    {
        /// <summary>
        /// Text styles
        /// </summary>
        public enum TextStyle
        {
            Normal = 0,
            Bold = 1,
            Large = 2,
            Double = 3
        }

        /// <summary>
        /// Active font selection
        /// </summary>
        private FontSelection activeFontSelection;

        /// <summary>
        /// Indicates if we are using variable or fixed width fonts
        /// </summary>
        private bool variableFont;

        private IPaymentApp app;

        /// <summary>
        /// Constructor for default font type (variable width)
        /// </summary>
        /// <param name="appInstance">Application instance</param>
        public ReceiptBuilder(IPaymentApp appInstance)
            : this(appInstance, true)
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="appInstance">Application instance</param>
        /// <param name="useVariableFont">True if a variable pitch font should be used, false otherwise</param>
        public ReceiptBuilder(IPaymentApp appInstance, bool useVariableFont)
        {
            app = appInstance;
            variableFont = useVariableFont;
            SelectFont(TextStyle.Normal);

            appInstance.RcptStart();
        }

        /// <summary>
        /// Selects a font and pitch
        /// </summary>
        /// <param name="style"></param>
        /// <param name="useVariableWidth">True to use variable width, false to use fixed</param>
        public void SelectFont(TextStyle style, bool useVariableWidth)
        {
            variableFont = useVariableWidth;
            SelectFont(style);
        }

        /// <summary>
        /// Select the active font
        /// </summary>
        /// <param name="style"></param>
        public void SelectFont(TextStyle style)
        {
            if (variableFont)
            {
                switch (style)
                {
                    case TextStyle.Normal:
                        {
                            activeFontSelection = FontSelection.VariableWidth_Normal;
                        }
                        break;

                    case TextStyle.Bold:
                        {
                            activeFontSelection = FontSelection.VariableWidth_Bold;
                        }
                        break;

                    case TextStyle.Large:
                        {
                            activeFontSelection = FontSelection.VariableWidth_Large;
                        }
                        break;

                    case TextStyle.Double:
                        {
                            activeFontSelection = FontSelection.VariableWidth_Double;
                        }
                        break;
                }
            }
            else
            {
                switch (style)
                {
                    case TextStyle.Normal:
                        {
                            activeFontSelection = FontSelection.FixedWidth_Normal;
                        }
                        break;

                    case TextStyle.Bold:
                        {
                            activeFontSelection = FontSelection.FixedWidth_Bold;
                        }
                        break;

                    case TextStyle.Large:
                        {
                            activeFontSelection = FontSelection.FixedWidth_Large;
                        }
                        break;

                    case TextStyle.Double:
                        {
                            activeFontSelection = FontSelection.FixedWidth_Double;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Draw a horizonal line the full width of the receipt
        /// </summary>
        /// <param name="solid">True for a solid line, false for dashed</param>
        public void HorizontalLine(bool solid)
        {
            app.RcptAddHorizontalLine(!solid);
        }

        /// <summary>
        /// Increments the Y cursor by one full lineheight of the current font
        /// </summary>
        public void AddBlankLine()
        {
            app.RcptAddTextBlock(string.Empty, activeFontSelection, 0, 0, StringAlignment.Near, true);
        }

        /// <summary>
        /// Adds a block of text, splitting the line into sensible sized fragments
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Alignment</param>
        public void AddMultiLine(string text, StringAlignment alignment)
        {
            AddString(text, alignment);
        }

        /// <summary>
        /// Add a line with a left and a right component, typically used in reports but also receipts
        /// </summary>
        /// <param name="left">Text to display on the left</param>
        /// <param name="right">Text to display on the right</param>
        public void AddLine(string left, string right)
        {
            AddStringNoIncrement(left, StringAlignment.Near);
            AddString(right, StringAlignment.Far);
        }

        /// <summary>
        /// Add a line with a left and a right component, typically used in reports but also receipts.
        /// The left component has a colon added first
        /// </summary>
        /// <param name="left">Text to display on the left</param>
        /// <param name="right">Text to display on the right</param>
        public void Colonise(string left, string right)
        {
            AddLine(left + ":", right);
        }

        /// <summary>
        /// Adds a string of text to the receipt
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Horizontal alignment on the receipt</param>
        /// <param name="incrementCursor">True if the cursor should be incremented, i.e. this line
        /// is now complete, false if more text will be added to this line</param>
        protected virtual void AddString(string text, StringAlignment alignment, bool incrementCursor)
        {
            app.RcptAddTextBlock(text, activeFontSelection, 0, 0, alignment, incrementCursor);
        }

        /// <summary>
        /// Adds a string of text to the receipt
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Alignment</param>
        /// <param name="leftPctMargin">Left margin as a percentage of the total width</param>
        /// <param name="rightPctMargin">Right margin as a percentage of the total width</param>
        /// <param name="incrementCursor">True to advance the cursor afterwards</param>
        public void AddString(string text, StringAlignment alignment, int leftPctMargin, int rightPctMargin, bool incrementCursor)
        {
            app.RcptAddTextBlock(text, activeFontSelection, leftPctMargin, rightPctMargin, alignment, incrementCursor);
        }

        /// <summary>
        /// Adds a string at a position determined by a percentage measurement.
        /// A positive percentage means the text is left-aligned and positioned
        /// at that percentage distance from the left side.
        /// A negative percentage means the text is right-aligned and positioned
        /// at that percentage distance from the right side.
        /// </summary>
        /// <param name="text">Text to show</param>
        /// <param name="percentageX">Percentage for positioning</param>
        /// <param name="incrementCursor">True to move the Y cursor after adding the text</param>
        public void AddString(string text, int percentageX, bool incrementCursor)
        {
            int leftMargin = 0;
            int rightMargin = 0;
            StringAlignment alignment = StringAlignment.Near;

            if (percentageX > 0)
            {
                leftMargin = percentageX;
            }
            else if (percentageX < 0)
            {
                alignment = StringAlignment.Far;
                rightMargin = -percentageX;
            }

            app.RcptAddTextBlock(text, activeFontSelection, leftMargin, rightMargin, alignment, incrementCursor);
        }

        /// <summary>
        /// Adds a piece of text aligned to left, middle or right of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Position of the text on the receipt</param>
        public virtual void AddString(string text, StringAlignment alignment)
        {
            app.RcptAddTextBlock(text, activeFontSelection, 0, 0, alignment, true);
        }
        /// <summary>
        /// Adds a piece of text on the near (left) side of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <returns>The number of vertical pixels that the cursor has moved on by.</returns>
        public void AddString(string text)
        {
            AddString(text, StringAlignment.Near, true);
        }

        /// <summary>
        /// Adds a piece of text on the near (left) side of the receipt.
        /// The vertical cursor is incremented by the lineheight after this.
        /// </summary>
        /// <param name="text">Text to add</param>
        public void AddStringNoIncrement(string text)
        {
            AddString(text, StringAlignment.Near, false);
        }

        /// <summary>
        /// Adds a piece of text aligned to left, middle or right of the receipt.
        /// The vertical cursor is not changed in this instance.
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="alignment">Position of the text on the receipt</param>
        public void AddStringNoIncrement(string text, StringAlignment alignment)
        {
            AddString(text, alignment, false);
        }

        /// <summary>
        /// Print the accumulated bitmap
        /// </summary>
        /// <param name="tearoff">Tearoff length</param>
        /// <returns>True on success, false otherwise</returns>
        public bool Print(TearOffLength tearoff)
        {
            return app.RcptPrint(tearoff);
        }

        /// <summary>
        /// Add a line with a label and an amount if the amount is non-zero.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="amount">The amount. It will be formatted with cents and according to culture.</param>
        /// <param name="culture">Culture to use</param>
        public void AddPositiveAmount(string label, uint amount, CultureInfo culture)
        {
            if (amount > 0)
            {
                AddAmount(label, amount, culture);
            }
        }

        /// <summary>
        /// Add a line with a label and an amount.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="amount">The amount. It will be formatted with cents and according to culture.</param>
        /// <param name="culture">Culture to use</param>
        public void AddAmount(string label, uint amount, CultureInfo culture)
        {
            string sAmount = (amount / 100m).ToString("F2", culture);
            AddStringNoIncrement(label + ":");
            AddString(sAmount, StringAlignment.Far);
        }

    }

    /// <summary>
    /// Encapsulates the result of a transaction
    /// </summary>
    public class TransactionReport
    {
        /// <summary>
        /// The transaction outcome
        /// </summary>
        public TransactionResult Result;

        /// <summary>
        /// A text string that may give diagnostic information in failure cases.  This is
        /// usually intended for logging rather than for customer display.  This parameter may be null.
        /// If the transaction result is Success then this text should be ignored.
        /// When the transaction is to extract the terminal log file then, if successful, this parameter will contain the path
        /// to the extracted log file.
        /// </summary>
        public string DiagnosticText;

        /// <summary>
        /// True if ChipXpress was used, false if it was not used.
        /// </summary>
        public bool ChipXpressUsed = false;

        /// <summary>
        /// The ChipXpress threshold configured for the card that was used in the transaction.
        /// ChipXpress will only be possible for purchase amounts of this amount or below.
        /// If the value here is zero then ChipXpress is disabled for this card, either because
        /// the threshold is explicitly set to zero or because it is not configured at all.
        /// </summary>
        public uint ChipXpressThreshold = 0;
    }

    /// <summary>
    /// Holds data that is used by the payment application to ask for more information from
    /// the POS component.  The response from the POS component is supplied in a SpecificResponse instance.
    /// </summary>
    public class SpecificRequest
    {
        /// <summary>
        /// Enumeration of different types of information that can be requested by the payment application
        /// </summary>
        public enum QueryType
        {
            /// <summary>Signature verification needed.  NOTE:  The POS component should print the approved
            /// receipt if signature verification is successful.  If signature verification is not successful
            /// then the payment application will take responsibility for printing the declined receipt and
            /// will call HandleReceiptData with the updated receipt information.
            /// <para>Request fields used: <c>Receipt</c> contains the receipt data for the merchant receipt</para>
            /// <para>Response fields used: <c>YesNo</c> indicates if the signature verification
            /// was successful.</para></summary>
            SignatureVerification,

            /// <summary>Voice referral authorisation code needed.
            /// <para>Request fields used: <c>SupplementaryText</c> contains the phone number to call.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates if the voice referral process was
            /// successful, <c>ResponseText</c> contains the authorisation
            /// code in a successful outcome.</para></summary>
            VoiceReferralCode,

            /// <summary>The POS Component should confirm if a parameter update should be run
            /// following a 13x trigger from the SPDH host.
            /// <para>Request fields used: none.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates if the terminal should proceed
            /// with the parameter update or continue with the current parameters.</para></summary>
            ParameterUpdateConfirmation,

            /// <summary>Cashier should take an embossed imprint of the payment card.
            /// <para>Request fields used: none.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates that the embossed imprint was
            /// taken.</para></summary>
            EmbossedCardImprintTaken,

            /// <summary>The POS Component should supply a payment code.
            /// <para>Request fields used: <c>SupplementaryNumber</c> indicates the maximum length
            /// of the payment code.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates that the payment code was
            /// supplied, <c>ResponseNumber</c> contains the payment code, if supplied.</para></summary>
            PaymentCode,

            /// <summary>The POS Component should supply a VAT amount for the transaction.
            /// <para>Request fields used: <c>SupplementaryNumber</c> indicates the suggested VAT
            /// amount in sub-units.</para>
            /// <para>Response fields used: <c>ResponseNumber</c> contains the actual VAT amount
            /// in sub-units.</para></summary>
            VATAmount,

            /// <summary>When a chip card is swiped, the terminal needs confirmation that it is
            /// acceptable to use the card as a magnetic stripe card rather than a chip card.
            /// <para>Request fields used: none.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates if the card may be used.</para>
            /// </summary>
            FallbackToMagstripe,

            /// <summary>
            /// A refund transaction on a card that is eligible for DCC must check if the original purchase
            /// was performed using DCC for a specific currency.  POS components that do not support DCC should
            /// return No to this.  The answer is based on visual inspection of the customer's receipt.
            /// <para>Request fields used: <c>SupplementaryText</c> contains the three-letter currency code to ask about.</para>
            /// <para>Response fields used: <c>YesNo</c> indicates if DCC was used in the purchase.</para>
            /// </summary>
            DccOnOriginalPurchase
        }

        /// <summary>
        /// Indicates the data that is being requested
        /// </summary>
        public QueryType RequiredInformation;

        /// <summary>
        /// May contain text relevant to the request. This is not always used and the meaning here
        /// is dependent on the value of <c>RequiredInformation</c>
        /// </summary>
        public string SupplementaryText = string.Empty;

        /// <summary>
        /// May contain a number relevant to the request. This is not always used and the meaning here
        /// is dependent on the value of <c>RequiredInformation</c>
        /// </summary>
        public ulong SupplementaryNumber = 0;

        /// <summary>
        /// Receipt data required for printing the merchant receipt in signature verification
        /// </summary>
        public ReceiptData Receipt = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="query">Request type</param>
        /// <param name="text">Supplementary request text</param>
        /// <param name="number">Supplementary request number</param>
        /// <param name="receipt">Optional receipt data</param>
        public SpecificRequest(QueryType query, string text, ulong number, ReceiptData receipt)
        {
            RequiredInformation = query;
            SupplementaryText = text;
            SupplementaryNumber = number;
            Receipt = receipt;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="query">Request type</param>
        public SpecificRequest(QueryType query)
            : this(query, string.Empty, 0, null)
        {
        }

        /// <summary>
        /// Constructor covering all parameters
        /// </summary>
        /// <param name="query">Request type</param>
        /// <param name="text">Supplementary request text</param>
        public SpecificRequest(QueryType query, string text)
            : this(query, text, 0, null)
        {
        }

        /// <summary>
        /// Constructor covering all parameters
        /// </summary>
        /// <param name="query">Request type</param>
        /// <param name="number">Supplementary request number</param>
        public SpecificRequest(QueryType query, ulong number)
            : this(query, string.Empty, number, null)
        {
        }

        /// <summary>
        /// Constructor for a signature verification request
        /// </summary>
        /// <param name="receipt">Receipt data</param>
        public SpecificRequest(ReceiptData receipt)
            : this(QueryType.SignatureVerification, string.Empty, 0, receipt)
        {
        }

    }

    /// <summary>
    /// Contains the response data that should be supplied following a specific data request.
    /// This class cannot be instantiated directly; instead, use one of the <c>Create...</c> methods
    /// to return an instance of this class.
    /// </summary>
    public class SpecificResponse
    {
        /// <summary>
        /// The type of query for which this response has been made.
        /// </summary>
        public SpecificRequest.QueryType RequestType { get; private set; }

        /// <summary>
        /// Outcome of the request, Yes = true, No = false
        /// </summary>
        public bool YesNo { get; private set; }

        /// <summary>
        /// Text part of the response, where appropriate
        /// </summary>
        public string ResponseText { get; private set; }

        /// <summary>
        /// Numeric part of the response, where appropriate
        /// </summary>
        public ulong ResponseNumber { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Query">Request type</param>
        /// <param name="Result">Result of the query</param>
        /// <param name="Text">Text data</param>
        /// <param name="Number">Numeric data</param>
        private SpecificResponse(SpecificRequest.QueryType Query, bool Result, string Text, ulong Number)
        {
            RequestType = Query;
            YesNo = Result;
            ResponseText = Text;
            ResponseNumber = Number;
        }

        /// <summary>
        /// Creates a signature verification response
        /// </summary>
        /// <param name="SignatureVerified">True if the signature was verified, false if the transaction should be aborted</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateSignatureVerificationResponse(bool SignatureVerified)
        {
            return new SpecificResponse(SpecificRequest.QueryType.SignatureVerification, SignatureVerified, string.Empty, 0);
        }

        /// <summary>
        /// Creates a voice referral response
        /// </summary>
        /// <param name="Authorised">True if the transaction was authorised, false if the transaction should be aborted</param>
        /// <param name="AuthorisationCode">Authorisation code, maximum 6 characters.  This is ignored if <c>Authorised</c> is false.</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateVoiceReferralResponse(bool Authorised, string AuthorisationCode)
        {
            return new SpecificResponse(SpecificRequest.QueryType.VoiceReferralCode, Authorised, AuthorisationCode, 0);
        }

        /// <summary>
        /// Creates a parameter update response
        /// </summary>
        /// <param name="UpdateParametersNow">True if the parameter update should be run now, false if it should be delayed</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateParameterUpdateConfirmation(bool UpdateParametersNow)
        {
            return new SpecificResponse(SpecificRequest.QueryType.ParameterUpdateConfirmation, UpdateParametersNow, string.Empty, 0);
        }

        /// <summary>
        /// Creates an embossed card imprint response
        /// </summary>
        /// <param name="ImprintMadeOK">True if the card was imprinted, false if the transaction should be aborted</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateEmbossedCardImprintTakenResponse(bool ImprintMadeOK)
        {
            return new SpecificResponse(SpecificRequest.QueryType.EmbossedCardImprintTaken, ImprintMadeOK, string.Empty, 0);
        }

        /// <summary>
        /// Creates a fallback to magstripe response
        /// </summary>
        /// <param name="FallbackOK">Indicates if fallback to magstripe is acceptable in this instance</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateFallbackToMagstripeResponse(bool FallbackOK)
        {
            return new SpecificResponse(SpecificRequest.QueryType.FallbackToMagstripe, FallbackOK, string.Empty, 0);
        }

        /// <summary>
        /// Creates a payment code response
        /// </summary>
        /// <param name="PaymentCodeSupplied">True if the payment code was supplied, false otherwise</param>
        /// <param name="PaymentCode">The supplied payment code. This is ignored if <c>PaymentCodeSupplied</c> is false</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreatePaymentCodeResponse(bool PaymentCodeSupplied, ulong PaymentCode)
        {
            return new SpecificResponse(SpecificRequest.QueryType.PaymentCode, PaymentCodeSupplied, string.Empty, PaymentCode);
        }

        /// <summary>
        /// Creates a VAT amount response
        /// </summary>
        /// <param name="VATAmountSupplied">True if the VAT amount is valid, false otherwise</param>
        /// <param name="VATAmountSubUnits">The VAT amount in sub-units</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateVATAmountResponse(bool VATAmountSupplied, ulong VATAmountSubUnits)
        {
            return new SpecificResponse(SpecificRequest.QueryType.VATAmount, VATAmountSupplied, string.Empty, VATAmountSubUnits);
        }

        /// <summary>
        /// Creates a response to the 'was DCC used on the original purchase' question for refunds
        /// </summary>
        /// <param name="DccUsed">True if DCC was used, false if it was not</param>
        /// <returns>A SpecificResponse instance</returns>
        public static SpecificResponse CreateDccOnRefundResponse(bool DccUsed)
        {
            return new SpecificResponse(SpecificRequest.QueryType.DccOnOriginalPurchase, DccUsed, string.Empty, 0);
        }
    }

    #endregion

    #region Currency Section

    /// <summary>
    /// A structure that represents an ISO 4217 currency.
    /// </summary>
    public struct CurrencyDefinition
    {
        /// <summary>
        /// The three-letter currency code
        /// </summary>
        public string Code;

        /// <summary>
        /// The three-digit currency code
        /// </summary>
        public int IsoValue;

        /// <summary>
        /// Culture info for displaying currencies
        /// </summary>
        public CultureInfo Culture;

        /// <summary>
        /// Multiplier to get from sub-units (e.g. cents) to units (e.g. dollars), i.e. 10 to the power of (decimal places)
        /// </summary>
        public decimal Multiplier;

        /// <summary>
        /// Number of digits after the decimal separator
        /// </summary>
        public int DecimalPlaces;

        /// <summary>
        /// Indicates whether the tipping process enters values in pound or pence equivalent
        /// </summary>
        public bool TipSmallestUnit;
    }


    #endregion

    #region Printer section

    /// <summary>
    /// Provides information on the current state of the printer.  If no printer is attached then
    /// the <c>PrinterOnline</c> member will be false and the other members of this class can then be
    /// ignored.
    /// </summary>
    public class PrinterState
    {
        /// <summary>Indicates if there a printer attached.  If false then other members can be ignored.</summary>
        public bool PrinterOnline;

        /// <summary>Indicates if the printer has paper.</summary>
        public bool PaperOK;

        /// <summary>Indicates if the printer is currently busy with a print job.</summary>
        public bool PrinterBusy;

        /// <summary>Indicates if the printer has a print job pending</summary>
        public bool PrintJobPending;

        /// <summary>Indicates if the printer temperature sensor is within acceptable limits</summary>
        public bool TemperatureOK;

        /// <summary>Indicates if the printer power supply is normal or not</summary>
        public bool PowerSupplyOK;

        /// <summary>Indicates if the printer buffer is full or not</summary>
        public bool BufferFull;

        /// <summary>Supplies the width of the printable area in pixels</summary>
        public int PrintWidthPixels;
    }

    /// <summary>
    /// Available font styles in the receipt printer functions
    /// </summary>
    public enum FontSelection
    {
        VariableWidth_Normal = 0,
        VariableWidth_Bold = 1,
        VariableWidth_Large = 2,
        FixedWidth_Normal = 3,
        FixedWidth_Bold = 4,
        FixedWidth_Large = 5,
        VariableWidth_Double = 6,
        FixedWidth_Double = 7,
    }

    /// <summary>
    /// Tear-off lengths using in the receipt printer functions
    /// </summary>
    public enum TearOffLength
    {
        None = 0,
        Normal = 1,
        Long = 2
    }

    #endregion

    #region Network interface

    /// <summary>
    /// Enumeration of possible network interface power levels.
    /// </summary>
    public enum NetworkInterfacePower
    { 
        /// <summary>
        /// Set power to on
        /// </summary>
        On,

        /// <summary>
        /// Set power to off
        /// </summary>
        Off
    }

    /// <summary>
    /// Enumeration of possible network interface types.
    /// </summary>
    public enum NetworkInterfaceAdapterType
    { 
        /// <summary>
        /// Wireless
        /// </summary>
        Wifi,

        /// <summary>
        /// Gsm/3G
        /// </summary>
        Gsm,

        /// <summary>
        /// Local area network (wired)
        /// </summary>
        Lan
    }

    #endregion

    #region Logging section

    /// <summary>
    /// List of different log file types that may be available on the terminal, depending
    /// on configuration
    /// </summary>
    public enum LogFileTypes
    {
        PaymentApplicationLogFile
    }

    #endregion

    #region Interface definitions

    /// <summary>
    /// The POS component must implement this interface.  The interface defines how the
    /// payment application interacts with the POS component.
    /// </summary>
    public interface IComponent
    {
        /// <summary>
        /// This function is called once by the payment application when it has completed
        /// its initialisation phase after power-up.  It is used to initialise the POS component.
        /// </summary>
        /// <param name="paymentApplication">An object that implements the IPaymentApp interface,
        /// i.e. a reference to the payment application.  The interface provides the means for
        /// the POS component to interact with the terminal and the payment application</param>
        /// <returns>The POS component should return true if initialisation was successful,
        /// or false if some error occurred that prevents the terminal from operating.</returns>
        /// <param name="sysinfo">Terminal and payment application information</param>
        bool Initialise(IPaymentApp paymentApplication, SystemInfo sysinfo);

        /// <summary>
        /// Provides the POS component name to the payment application for logging and display.  This
        /// can be retrieved at any time, including before the Initialise method is called, so the data here
        /// should not be dependent on any other part of the POS component having been used.
        /// </summary>
        string ComponentName { get; }

        /// <summary>
        /// This functions is called from a dedicated thread and allows the POS component to
        /// set up a new session.
        /// </summary>
        /// <param name="status">Status information on the terminal that is useful for the POS component</param>
        /// <param name="session">Details of the new session, or null to launch the management functions menu</param>
        void RunPreSession(TerminalStatus status, out SessionData session);

        /// <summary>
        /// This functions is called from a dedicated thread and allows the POS component to
        /// do what is required before running a transaction.
        /// </summary>
        /// <param name="status">Current terminal status</param>
        /// <param name="transaction">Data for a new transaction to run, or null to end the current session</param>
        void RunPreTransaction(TerminalStatus status, out TransactionData transaction);

        /// <summary>
        /// This function is called when a card has been presented to the payment
        /// application and the card's details have been validated.  The POS component
        /// should indicate to the payment application whether or not this card is
        /// acceptable.  This is most likely to be used for bonus cards or special
        /// purpose cards.
        /// </summary>
        /// <param name="card">Information on the presented card</param>
        /// <param name="tspId">Terminal id used for the transaction</param>
        /// <returns>Information on how the verification went, and optional extra data for the transaction.</returns>
        CardValidationResult VerifyCard(CardInformation card, string tspId);

        /// <summary>
        /// This function is called to notify the POS component that a user input event
        /// has happened.  This means a key press or an on-screen button press.
        /// Note that each possible user input event must be enabled before a notification
        /// will be possible.
        /// </summary>
        /// <param name="item">Identifies the input event</param>
        /// <param name="keyCode">Where the user input is a Digit or Letter, this gives
        /// the ASCII key code value.  This is significant where it is a letter since
        /// the item parameter does not tell which letter was pressed</param>
        /// <param name="id">Where the user input is an event from a flexible
        /// display element this contains the ID that was specified when the element
        /// was created.</param>
        void HandleUserInput(UserInputs item, char keyCode, int id);

        /// <summary>
        /// Requests a piece of information from the POS component.
        /// </summary>
        /// <param name="required">Detail on the information that is required.</param>
        /// <param name="response">The response from the POS component</param>
        void GetSpecificData(SpecificRequest required, out SpecificResponse response);

        /// <summary>
        /// This function is called to provide receipt data to the POS component.  The POS
        /// component must check the values provided to determine when it should print a
        /// receipt and what should be on the receipt.
        /// </summary>
        /// <param name="item">Receipt data</param>
        void HandleReceiptData(ReceiptData item);

        /// <summary>
        /// This function is called to inform the POS component of the final outcome of
        /// the transaction.
        /// </summary>
        /// <param name="report">Information regarding the outcome of the transaction</param>
        void TransactionComplete(TransactionReport report);

        /// <summary>
        /// This function is called if the terminal has to restart because of a software
        /// upgrade or a change in settings in the management function menu.  The POS component
        /// should shut down cleanly when this is called.
        /// </summary>
        void ShutdownPOS();
    }

    /// <summary>
    /// Status outcomes possible from processing the Store and Forward queue
    /// </summary>
    public enum StoreAndForwardStatus
    {
        Ok,
        FailureOnHostAuth,
        QueueExtractionFailure,
        FailureToSendToHost,
    }

    /// <summary>
    /// Holds the terminal's power status.  This is used to determine if a warning is required
    /// to charge the terminal.
    /// </summary>
    public class PowerStatus
    {
        /// <summary>
        /// Percentage of battery charge available.  Note that this measurement is not
        /// perfect, and there is no good way to estimate remaining operating time based
        /// on this value.
        /// This value will be set to zero on terminals that have no battery in place.
        /// </summary>
        public int BatteryPercentage;

        /// <summary>
        /// True if the terminal is running on an AC power source rather than a battery.
        /// </summary>
        public bool OnAcPower;
    }

    /// <summary>
    /// Possible result when adding an Wifi SSID.
    /// </summary>
    public enum AddWifiResult
    {
        /// <summary>
        /// Successful operation.
        /// </summary>
        Success = 0,

        /// <summary>
        /// Connect to access point failed.
        /// </summary>
        ConnectFailed,

        /// <summary>
        /// Add the SSID failed.
        /// </summary>
        AddFailed
    }

    /// <summary>
    /// Defines the interface that the POS component can use to work with the
    /// payment application.
    /// </summary>
    public interface IPaymentApp
    {
        /// <summary>
        /// Supplies transaction amounts to the payment application in a swipe-ahead
        /// scenario.  Typically this will be called from a thread that is monitoring
        /// an external ECR device.
        /// </summary>
        /// <param name="amounts">Transaction amounts to use</param>
        /// <returns>True if successful, false if amounts have already been supplied
        /// or if no transaction is in progress.</returns>
        bool SetTransactionAmounts(TransactionAmounts amounts);

        /// <summary>
        /// Stores a value in the settings.
        /// </summary>
        /// <param name="name">The name of the name/value pair.</param>
        /// <param name="value">The value to be stored.</param>
        /// <returns>True on success, false if the attempt to store the data failed.</returns>
        bool StoreRegistryValue(string name, string value);

        /// <summary>
        /// Retrieves a value from the settings.
        /// </summary>
        /// <param name="name">The name of the name/value pair.</param>
        /// <param name="defaultValue">The value to return if the settings entry does not exist.</param>
        /// <returns>The value of the setting, or null if it does not exist.</returns>
        string GetRegistryValue(string name, string defaultValue = null);

        /// <summary>
        /// Instructs the payment application to use the supplied display data to update
        /// the terminal display in a pre-defined template layout.
        /// </summary>
        /// <param name="output">The display data</param>
        /// <returns>True on success, false if there was a problem with the data or if payment
        /// application is running a transaction and cannot update the display.</returns>
        bool Display(DisplayData output);

        /// <summary>
        /// Instructs the payment application to display the specified image on screen.  The
        /// image will occupy the full available space, which may not be the full screen dimensions
        /// since the status bar will not be hidden by this function.  The image will be aligned to
        /// the top-left corner of the screen.  If the image is smaller than the available display
        /// area then then a black background will appear in the empty space.  If the image is larger
        /// than the available display area then it will be cropped.
        /// Images must be no larger than 400Kb, and can only be displayed during pre-session or
        /// pre-transaction.
        /// </summary>
        /// <param name="output">Image data to display</param>
        /// <returns>True if the image was successfully displayed, false otherwise</returns>
        bool Display(ImageData output);

        /// <summary>
        /// Updates a single line of text on the display, when a pre-defined template layout is
        /// used.  This does not apply when a flexible display has been used.  The position and
        /// size of the text has already been defined in the Display method call.
        /// </summary>
        /// <param name="lineNumber">The line number of the text.  It is the callers
        /// responsibility to ensure that the line number is valid for the display
        /// layout being used, since the application will ignore invalid values.  The
        /// first line on the display is line 0.</param>
        /// <param name="text">The new text to display.</param>
        /// <returns>True on success, false if the text could not be updated or the
        /// line number is invalid</returns>
        bool UpdateLine(int lineNumber, string text);

        /// <summary>
        /// Displays the given FlexibleDisplay on screen.  The flexible display does not use
        /// pre-defined templates and layouts.  Instead, the display is automatically adapted
        /// to the contents that are given.
        /// </summary>
        /// <param name="output">A FlexibleDisplay instance that contains the display elements</param>
        /// <returns>True on success, false otherwise</returns>
        bool Display(FlexibleDisplay output);

        /// <summary>
        /// Updates a specific flexible display element with new text.
        /// </summary>
        /// <param name="elementId">ID of the display element to update.  If multiple
        /// elements have the same ID then the first element with a matching ID will be updated.</param>
        /// <param name="text">New text to show on the element, or null if the text should be unchanged.
        /// Use String.Empty to set an empty string.</param>
        /// <param name="foreground">Colour of the text, can be null to leave the colour unchanged.</param>
        /// <param name="background">Colour of the containing background, can be null to leave the colour unchanged.</param>
        /// <returns>True on success, false otherwise.</returns>
        bool UpdateFlexibleItem(int elementId, string text, Color? foreground, Color? background);

        /// <summary>
        /// Enables specific keypad-related user input events, so that when the
        /// specified key is pressed then the payment application will call the
        /// HandleUserInput method of the POS component interface (if there is 
        /// no transaction in progress).  Note that while on-screen buttons are
        /// included in the UserInputs enumeration, they will be ignored if included
        /// in this list.  On-screen buttons are enabled via the Display function.
        /// </summary>
        /// <param name="inputList">A list of keys to enable</param>
        /// <param name="enableBeep">True if the keys and on-screen buttons should
        /// beep when pressed</param>
        /// <returns>True on success, false if called during a transaction</returns>
        bool EnableKeypadInputs(List<UserInputs> inputList, bool enableBeep);

        /// <summary>
        /// This is identical to EnableKeypadInputs(inputList, enableBeep) except that
        /// the payment application will use the default beep behaviour specified in
        /// the SessionData instance supplied in RunPreSession.
        /// </summary>
        /// <param name="inputList">A list of keys to enable</param>
        /// <returns>True on success, false if called during a transaction</returns>
        bool EnableKeypadInputs(List<UserInputs> inputList);

        /// <summary>
        /// Sounds the terminals internal buzzer.  Note that this is run asynchronously
        /// in a separate thread, so the beep may still be sounding when this function
        /// returns.
        /// </summary>
        /// <param name="frequencyHz">The frequency in Hertz that the buzzer should sound</param>
        /// <param name="onTimeMs">The time in milliseconds that the beep should sound</param>
        /// <param name="offTimeMs">The time in milliseconds that the beep should not sound
        /// before repeating.  This value is only used where the repeat parameter is > 1.
        /// </param>
        /// <param name="repeats">The number of individual beeps to sound</param>
        /// <returns>True on success, false if terminal beeps are not permitted</returns>
        [Obsolete]
        bool Beep(int frequencyHz, int onTimeMs, int offTimeMs, int repeats);

        /// <summary>
        /// Sounds the terminals internal buzzer.  Note that this is run asynchronously
        /// in a separate thread, so the beep may still be sounding when this function
        /// returns.
        /// </summary>
        /// <param name="beep">The type of beep to sound.</param>
        /// <returns>True on success, false if terminal beeps are not permitted</returns>
        bool Beep(BeepType beep);

        /// <summary>
        /// Starts a new receipt.
        /// </summary>
        /// <returns>True on success, false otherwise</returns>
        bool RcptStart();

        /// <summary>
        /// Adds a text block to a receipt that has been started.  The text block is added at the receipt's current cursor
        /// position.
        /// </summary>
        /// <param name="text">Text to print.  Text will be wrapped if it is wider than the paper, and lines will be broken
        /// where the text contains newline characters.</param>
        /// <param name="style">The font style to print the text in</param>
        /// <param name="leftMarginPct">The left margin size as a percentage of the paper width.</param>
        /// <param name="rightMarginPct">The right margin size as a percentage of the paper width.</param>
        /// <param name="position">The alignment of the text block within the margins</param>
        /// <param name="newline">True if the cursor should be advanced following printing, false
        /// if the cursor should remain where it is.</param>
        /// <returns>True on success, false otherwise</returns>
        bool RcptAddTextBlock(string text, FontSelection style, int leftMarginPct, int rightMarginPct, StringAlignment position, bool newline);

        /// <summary>
        /// Adds a horizonal line to the active receipt.
        /// </summary>
        /// <param name="dashed">If true, a dashed / dotted line will be printed, otherwise a solid line will be printed</param>
        void RcptAddHorizontalLine(bool dashed);

        /// <summary>
        /// Adds a bitmap image to the receipt.
        /// Must be a correctly formatted Windows bitmap file. 
        /// The file must be a monochrome 16-bit RGB (5-bit each channel) bitmap and
        /// the maximum width must be 384 pixels. 
        /// </summary>
        /// <param name="filename">Bitmap image filename</param>
        /// <param name="position">Position of the bitmap on the paper</param>
        /// <param name="feedPaper">Set true to feed the paper following the print so that the
        /// printed area is clear of the cutting strip.  If left as false then the paper will
        /// not be fed, and this allows multiple images to be printed next to each other.</param>
        bool RcptAddImage(string filename, StringAlignment position, bool feedPaper);

        /// <summary>
        /// Cancels the ongoing receipt that was built up since RcptStart was called.
        /// </summary>
        void RcptCancel();

        /// <summary>
        /// Prints the receipt that has been built up since RcptStart was called.
        /// </summary>
        /// <param name="tearOff">Indicates the length of paper that should be fed as
        /// a tear-off section.  'None' means that the paper will not be advanced so a subsequent
        /// print will be immediately next to the current print vertically.</param>
        /// <returns>True if printing was successful, false otherwise</returns>
        bool RcptPrint(TearOffLength tearOff);
        
        /// <summary>
        /// Prints a bitmap to the terminal's printer.
        /// </summary>
        /// <param name="bmp">The Bitmap instance to print</param>
        /// <param name="feedPaper">Set true to feed the paper following the print so that the
        /// printed area is clear of the cutting strip.  If left as false then the paper will
        /// not be fed, and this allows multiple images to be printed next to each other.</param>
        /// <returns>True on success, false otherwise</returns>
        /// <remarks>NOTE: This method should be avoided due to the excessive memory demands of a .Net
        /// Compact Framework bitmap instance, which is typically 32bpp and therefore consumes
        /// a huge amount of memory.  The more recent receipt functions, including RcptAddImage,
        /// should be used instead.</remarks>
        bool PrintBitmap(Bitmap bmp, bool feedPaper);

        /// <summary>
        /// Gets the status of the attached printer.  If there is no printer attached then
        /// this is indicated in the <c>PrinterOnline</c> member of the resulting <c>PrinterState</c>
        /// instance.
        /// </summary>
        /// <param name="printerStatus">Current printer state, or null if the internal function
        /// call failed</param>
        /// <returns>True on success, false otherwise</returns>
        bool GetPrinterStatus(out PrinterState printerStatus);

        /// <summary>
        /// Extracts the terminal's log file to a temporary location where it can be
        /// retrieved by the POS component.  Not that this method is only valid while
        /// the POS component is in RunPreSession.
        /// </summary>
        /// <param name="fileType">Selects the log file to be extracted.</param>
        /// <param name="compressed">Indicates if the log file should be compressed using gzip.</param>
        /// <param name="logfilePath">The path to the temporary copy of the log file, if successful.
        /// May be null in the error case.</param>
        /// <returns>True on success, false otherwise</returns>
        bool ExtractLogFile(LogFileTypes fileType, bool compressed, out string logfilePath);

        /// <summary>
        /// This event is used to notify the POS component of events that can happen
        /// asynchronously while the POS component is running the pre-session or pre-transaction
        /// functions.  These events are related to scheduled activities such as timed
        /// parameter updates.
        /// </summary>
        event NotificationHandler EventNotification;

        /// <summary>
        /// This event is used to notify the POS component of screen updates by using a
        /// numeric ID to identify screens.  The POS component may find this useful to
        /// track the progress of a transaction.
        /// </summary>
        event ScreenStatusHandler DisplayNotification;

        /// <summary>
        /// This method provides a way for the POs component to attempt to perform asynchronous
        /// operations once the transaction has begun, e.g. cancelling the transaction due to
        /// some external event.
        /// </summary>
        /// <param name="operation">The operation to perform</param>
        /// <returns>True if the operation is possible at this time, false if it cannot be performed</returns>
        bool RequestAsynchronousOperation(AsynchronousOperations operation);

        /// <summary>
        /// Requests the payment application to restart the terminal.  If this function is successful then
        /// it will never return.  Reasons for this function to fail include being called at the wrong time,
        /// i.e. not during pre-session or pre-transaction, or setting the revertPaymentApplication flag
        /// to true when there are no other payment applications to fall back to.
        /// </summary>
        /// <param name="revertPaymentApplication">Set to true to revert the payment application to the
        /// previous version that is still in the terminal.</param>
        /// <returns>False if the terminal could not be restarted.  A successful operation will not
        /// return control to the caller.</returns>
        bool RestartTerminal(bool revertPaymentApplication);

        /// <summary>
        /// Asks the payment application to compare the given password against the active merchant password.
        /// If there is a user-defined merchant password in place then that is used, otherwise the password
        /// provided in PPL configuration files is used.
        /// </summary>
        /// <param name="password">Password to compare, in plain text</param>
        /// <returns>True if the password is correct, false if it is not</returns>
        bool VerifyMerchantPassword(string password);

        /// <summary>
        /// Retrieves a list of current Store and Forward transactions
        /// </summary>
        /// <returns>List of ReceiptData instances containing the Store and Forward queue</returns>
        List<ReceiptData> GetStoreAndForwardTransactions();

        /// <summary>
        /// Removes transactions from the Store and Forward queue.  Only valid in pre-session.
        /// </summary>
        /// <param name="firstOnly">True to remove only the first transaction in the queue, false
        /// to remove all transactions.</param>
        /// <returns></returns>
        bool RemoveStoreAndForwardTransaction(bool firstOnly);

        /// <summary>
        /// Get number of filed transactions 
        /// </summary>
        /// <returns>The number of transactions serialized to disc</returns>
        int GetNumberOfStoreAndForwardTransactions();

        /// <summary>
        /// Get calculated total amounts per card name from all filed transactions
        /// </summary>
        /// <returns>List of transaction summaries</returns>
        List<TransactionSummary> GetStoreAndForwardTransactionSummary();

        /// <summary>
        /// Force all filed transactions to be sent.
        /// </summary>
        /// <param name="queueLength">The length of the store and forward queue on completion</param>
        /// <returns>The status of the attempt to send the store and forward queue</returns>
        StoreAndForwardStatus SendStoreAndForwardTransactions(out int queueLength);

        /// <summary>
        /// Add a access point using a name (ssid) and a password.
        /// If attemptConnection is set and if the add operation was successful, 
        /// a connection to the added access point will be performed.
        /// </summary>
        /// <param name="ssid"> The name of the access point to connect to.</param>
        /// <param name="password">Password</param>
        /// <param name="attemptConnection">true = try to connect, false = don't try to connecct.</param>
        /// <returns>AddWifiResult.</returns>
        AddWifiResult AddWifiNetwork(string ssid, string password, bool attemptConnection);

        /// <summary>
        /// Disconnect from wifi.
        /// </summary>
        /// <param name="onlyCurrentAP">If true, only the current AP will be disconnected and removed. If false,
        /// disconnect and remove all preferred networks.</param>
        /// <returns>True if successful.</returns>
        bool DisconnectFromWifiNetwork(bool onlyCurrentAP);

        /// <summary>
        /// Set power level (enable or disable) on a network interface.
        /// NOTE: The
        /// </summary>
        /// <param name="interfaceType"> Type of interface</param>
        /// <param name="powerLevel"> Power level</param>
        /// <returns>True if successful</returns>
        bool SetNetworkInterfacePowerLevel(NetworkInterfaceAdapterType interfaceType, NetworkInterfacePower powerLevel);

        /// <summary>
        /// Set the network interface to use for outgoing connections
        /// </summary>
        /// <param name="interfaceType">Type of interface</param>
        /// <returns>True if successful</returns>
        bool SetNetworkDefaultGateway(NetworkInterfaceAdapterType interfaceType);

        /// <summary>
        /// Set 3G/GSM settings to use. Settings are accepted during init or pre-session.
        /// </summary>
        /// <param name="gsmSettings">APN and dial up</param>
        /// <returns>True if successful</returns>
        bool SetGsmSettings(GsmSettings gsmSettings);

        /// <summary>
        /// Get network interface adapter settings.
        /// NOTE: GSM interface can not be retrieved using this method. 
        /// </summary>
        /// <param name="networkInterfaceAdapterSettings">Object with current settings</param>
        /// <param name="interfaceType">Type of interface, e.g lan or wifi</param>
        /// <returns>True if successful</returns>
        bool GetNetworkInterfaceAdapterSettings(
            out NetworkInterfaceAdapterSettings networkInterfaceAdapterSettings,
            NetworkInterfaceAdapterType interfaceType);

        /// <summary>
        /// Set network interface adapter settings to use, i.e DHCP or manual ip-address.
        /// NOTE: Both wifi and LAN are not supported by all hardware models. The table
        /// below show what is supported for the different models.
        /// 
        /// Type    Adapter Name            T103    T103P   8006
        /// ----------------------------------------------------
        /// LAN     DM9001                   x
        /// LAN     AX887721                                  x
        /// WIFI    NETRTWLANU1              x
        /// WIFI    RT28701                           x
        /// GSM     SAIO WIRELESS MODEM      x        x
        /// 
        /// NOTE: GSM interface can not be changed using this method. 
        /// </summary>
        /// <param name="networkInterfaceAdapterSettings">Object with settings to use</param>
        /// <param name="interfaceType">Type of interface, e.g lan or wifi</param>
        /// <returns>True if successful</returns>
        bool SetNetworkInterfaceAdapterSettings(
            NetworkInterfaceAdapterSettings networkInterfaceAdapterSettings,
            NetworkInterfaceAdapterType interfaceType);

        /// <summary>
        /// Retrieves the current power status.  This is only valid during pre-session and
        /// pre-transaction.
        /// </summary>
        /// <returns>The terminal's power status, or null in the case of an error (e.g. if the
        /// terminal's power API reports a failure.)</returns>
        PowerStatus GetPowerStatus();

        /// <summary>
        /// Sets the merchant language in the payment application.  The merchant language is not
        /// used for customer-facing texts; instead, it is used for messages sent to the ECR.
        /// Since the POS component does not generally use these messages, the ability to change
        /// language has limited value.  But there are some messages and receipt elements that
        /// are affected by this, so it is recommended that if the POS component supports multiple
        /// languages then this function should be called whenever the POS component language changes.
        /// Note: This has no affect on the language of cardholder texts.  This function is only
        /// available in pre-session or pre-transaction.
        /// </summary>
        /// <param name="Iso639_2"></param>
        /// <returns>True if the language is available on the terminal and the selection was successful,
        /// false if the language was not available or the POS component was not in pre-session or
        /// pre-transaction.</returns>
        bool SetMerchantLanguage(string Iso639_2);

        /// <summary>
        /// Sets the operating logging level in the terminal.  Normal choices are Debug for a development /
        /// troubleshooting situation or Warning for normal operation.  The logging levels in order of
        /// priority are:  Status, Error, Warning, Debug, Spam.  Only messages at, or above, the priority
        /// of the operating level will be logged.
        /// For example, if the logging level is set to Warning then all Status, Error and Warning messages
        /// will be logged.  If set to Error then only Status and Error messages will be logged.
        /// </summary>
        /// <param name="level">Logging level to be used</param>
        void SetLoggingLevel(TerminalStatus.LoggingLevels level);

        /// <summary>
        /// Asks the payment application to compare the given password against the daily password.
        /// </summary>
        /// <param name="password">Password to compare, in plain text</param>
        /// <returns>True if the password is correct, false if it is not</returns>
        bool VerifyDailyPassword(string password);

        /// <summary> 
        /// Provides a method for the POS component to add log messages to the payment application's log files. 
        /// These are accessible via the ExtractLogFile function or manually via the payment application's web server. 
        /// POS components should take care not to log sensitive data by this method, and to restrict logging to 
        /// sensible quantities in order to avoid causing the log file to fill up too quickly.
        /// </summary>
        /// <param name="level">Log level</param>
        /// <param name="message">Log message</param>
        void Log(TerminalStatus.LoggingLevels level, string message);

        /// <summary>
        /// Read card numbers that are less than 12 digits.
        /// </summary>
        /// <param name="whichReader">Type of reader.</param>
        /// <param name="timeoutSeconds">The timeout in seconds if no card has been provided.</param>
        /// <param name="card">Card information or null.</param>
        /// <returns>True if successfull operation, false if unsuccessfull operation</returns>
        bool ReadCard(TypeOfCardReader whichReader, int timeoutSeconds, out CardInformation card);

        /// <summary>
        /// Shows a QR code URL on screen 
        /// </summary>
        /// <param name="prompt">Text to show above the QR code</param>
        /// <param name="url">QR code url to show</param>
        /// <returns>True on success, false otherwise</returns>
        bool ShowQRCode(string prompt, string url);
    }

    /// <summary>
    /// Card reader identifiers
    /// </summary>
    public enum TypeOfCardReader
    {
        MagneticStripeCard = 1,
        ChipCard = 2,
        ContactlessCard = 4
    }

    /// <summary>
    /// Different types of alert, i.e. beep patterns
    /// </summary>
    public enum BeepType
    {
        /// <summary>
        /// Beep once for 200 ms at 400 Hz.
        /// </summary>
        CardErrorOrDeclined,

        /// <summary>
        /// Beep three times for 80 ms at 800 Hz, with 100 ms pauses.
        /// </summary>
        ApprovedRemoveCard,

        /// <summary>
        /// Beep once for 100 ms at 800 Hz.
        /// </summary>
        TimeOutOrInterrupted,

        /// <summary>
        /// Beep once for 150 ms at 1100 Hz.
        /// </summary>
        KeyPress,

        /// <summary>
        /// Beep once for 200 ms at 400 Hz.
        /// </summary>
        InvalidKeyPress,

        /// <summary>
        /// Beep five times for 80 ms at 800 Hz, with 100 ms pauses.
        /// </summary>
        Attention,

        /// <summary>
        /// Beep once for 50 ms at 800 Hz.
        /// </summary>
        Default,
    }

    #endregion

    /// <summary>
    /// Various utility functions for working with the POSInterface objects.
    /// </summary>
    public static class POSInterfaceExtensions
    {
        /// <summary>
        /// Generate a string that represents a verification method and is suitable for receipt purposes.
        /// </summary>
        /// <param name="method">Verification method</param>
        /// <returns>Letter / symbol for code string</returns>
        public static string ToCodestring(this VerificationMethod method)
        {
            char result;
            if (method == VerificationMethod.ConsumerDevice)
            {
                result = (char)VerificationMethod.PINOffline;
            }
            else
            {
                result = (char)method;
            }
            return result.ToString();
        }
    }
}

// Re-enable warnings for unused variables
#pragma warning restore 169
